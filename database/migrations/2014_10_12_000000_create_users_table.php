<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('username')->unique();
            $table->string('email')->unique();

            $table->string('qualification')->nullable();
            $table->string('userType');
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->longText('description')->nullable();
            $table->string('verifyToken')->nullable();
            $table->boolean('verified')->default(0);
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google_plus')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('blocked')->default(0);
            $table->softDeletes();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
