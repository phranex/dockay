<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_adverts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('facility_id')->unsigned()->index();
            $table->string('title');
            $table->integer('specialty_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('establishment_id')->unsigned()->index();
            $table->string('gender')->nullable();
            $table->date('deadline')->nullable();
            $table->string('min')->nullable();
            $table->string('max')->nullable();
            $table->longText('description');
            $table->boolean('isFeatured')->default(0);
            $table->boolean('isActive')->default(0);
            $table->boolean('hasBeenRead')->default(0);
            $table->foreign('facility_id')
            ->references('id')->on('facilities')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_adverts');
    }
}
