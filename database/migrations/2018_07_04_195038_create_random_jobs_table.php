<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRandomJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('random_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('specialty_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('establishment_id')->unsigned()->index();
            $table->string('gender')->nullable();

            $table->longText('description');
            $table->boolean('isFeatured')->default(0);
            $table->boolean('isActive')->default(0);

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('random_jobs');
    }
}
