function deleteImage(id,holder) {
    $('#loaderImage').show();
    $.ajax({
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/ad/image/delete?id=' + id,
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                
                holder.parent('span.parent').siblings('.dropify').prop('disabled', false);
                console.log( holder.parent('span.parent').siblings('.dropify'));
                holder.remove();

                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: result.message, });
               
               
                //location.reload();
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}