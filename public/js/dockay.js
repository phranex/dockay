// base_url = 'http://localhost:8000';
// base_url = 'https://dockay.ng';
base_url = window.location.protocol + "//" + window.location.host;


function checkSalaryFields(minElement, maxElement, fieldError) {
    if (!isEmpty(minElement) || !isEmpty(maxElement)) {
        var messages = [];
        if (isEmpty(minElement)) {
            $(minElement).addClass(fieldError);
            messages.push('Please fill this as well');
            displayErrorMessages(minElement, messages);
        } else if (isEmpty(maxElement)) {
            $(maxElement).addClass(fieldError);
            messages.push('Please fill this as well');
            displayErrorMessages(maxElement, messages);
        } else {
            var min = parseInt($(minElement).val());
            var max = parseInt($(maxElement).val());
            if (!minimumIsLessThanMaximum(min, max)) {
                $(minElement).addClass(fieldError);
                messages.push('Minimum salary can not be greater than maximum salary');
                displayErrorMessages(minElement, messages);

            } else {
                return true;
            }
        }


        return false;
    }

    return true
}


function minimumIsLessThanMaximum(min, max) {
    return min < max;
}


function shortlist(data){
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/application/shortlist/selected',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({
                    title: 'Success!',
                    type: 'blue',
                    icon: 'fa fa-check',
                    content: result.message,
                });

                location.reload();
               
            } else {

                $.alert({
                    title: 'Updater!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content: result.message,
                });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({
                title: 'Update!',
                type: 'red',
                icon: 'fa fa-times',
                content: 'An error occurred. Please Try again.',
            });
            console.log(e);
        }
    });
}