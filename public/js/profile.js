

function updateProfile(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/update/profile',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({
                    title: 'Update!',
                    type: 'blue',
                    icon: 'fa fa-check',
                    content: 'Updated Successfully!',
                });
                $('.nav-pills a[href="#education"]').tab('show')
            } else {

                $.alert({
                    title: 'Update!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content: result.message,
                });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({
                title: 'Update!',
                type: 'red',
                icon: 'fa fa-times',
                content: 'An error occurred. Please Try again.',
            });
            console.log(e);
        }
    });
}

function updateFacilityProfile(data) {
    $('#loaderImage').show();
    
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url:  'https://dockay.ng/user/facility/update/',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                $('.nav-pills a[href="#gallery"]').tab('show')
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function updateCompanyProfile(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/facility/update',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                $('.nav-pills a[href="#gallery"]').tab('show')
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function addEducation(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/education/create',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                $('.nav-pills a[href="#work"]').tab('show')
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function editEducation(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/education/update',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                location.reload();
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function addWork(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/work/create',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                $('.nav-pills a[href="#social"]').tab('show')
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function editExperience(data) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/work/update',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                location.reload();
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}

function addSocial(data, url) {
    $('#loaderImage').show();
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/update/social',
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: 'Updated Successfully!', });
                window.location.href = url;
                //location.reload();
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}


function deleteMediaPicture(id,holder) {
    $('#loaderImage').show();
    $.ajax({
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/media/delete?id=' + id,
        success: function(result) {
            console.log(result);
            $('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab

                $.alert({ title: 'Update!', type: 'blue', icon: 'fa fa-check', content: result.message, });
                holder.remove();
                if($('.dropify').length <= 3){
                    //$('#more div').hide();
                    location.reload();
                }
                
                //location.reload();
            } else {
                $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: result.message, });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({ title: 'Update!', type: 'red', icon: 'fa fa-times', content: 'An error occurred. Please Try again.', });
            console.log(e);
        }
    });
}