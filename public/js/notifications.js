// function notif() {
//     $('#errorMess p').each(function(index, value) {
//         var mess = $(this).text();
//         var notify = $.notify(mess, 'error');

//     });

//     $('#successMess p').each(function(index, value) {
//         var mess = $(this).text();
//         console.log(mess);
//         $.notify(mess, 'success');

//     });

//     $('#errors li').each(function(index, value) {
//         var mess = $(this).text();
//         var notify = $.notify(mess, 'error');
//     });
// }

function notif() {
    if ($('#errors li').length) {
        var mess = '';
        if($('#errors li').length > 1){
            $('#errors li').each(function(index,obj) {
                var num = index + 1;
                mess += "<span class='text-danger'> "+num+ '. ' + $(this).html() + '<br/>';
            });
        }else{
            mess += "<span class='text-danger text-center block'> " +  $('#errors li').html() + '<br/>';
        }
        
        //$.notify(mess, 'error');
        $.alert({
            title: 'Status!',
            type: 'red',
            icon: 'fa fa-times',
            content: mess,
        });
    }
    if ($('#successMess p').length) {
        //$.notify($('#successMess p').text(), 'success');
        $.alert({
            title: 'Status!',
            type: 'blue',
            icon: 'fa fa-check',
            content:$('#successMess p').text(),
        });
    }
    if ($('#errorMess p').length) {
        //$.notify($('#error Mess').text(), 'error');
        $.alert({
            title: 'Status!',
            type: 'red',
            icon: 'fa fa-times',
            content:$('#errorMess p').text(),
        });
    }
}