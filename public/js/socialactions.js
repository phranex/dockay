
function addLike(id,holder, l){
    $.ajax({
        type: 'GET',
        // headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        // },
        
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/likes/create?id='+id,
        success: function(result) {
            console.log(result);
            //$('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                $(holder).html(result.likes);
                console.log(result.likes);
                $(l).toggleClass('fa-thumbs-o-up fa-thumbs-up');

               
            } else {

                $.alert({
                    title: 'Update!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content: result.message,
                });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({
                title: 'Update!',
                type: 'red',
                icon: 'fa fa-times',
                content: 'An error occurred. Please Try again.',
            });
            console.log(e);
        }
    });
}
function getComments(id,holder){
    $.ajax({
        type: 'GET',
        // headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        // },
        
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/comment/index?id='+id,
        success: function(result) {
            console.log(result);
            //$('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
                var vale = (!result.data['answer']['isAnonymous'])?"<a href=/user/"+result.data['answer']['user'].trim()+"/view ><i>"+result.data['answer']['user']+"</i></a>":"Anonymous";
                $('#viewAnswer').html(result.data['answer']['answer']).append("<small style='font-size:11px'>Answered by: "+vale+"</small>");
                if(result.data['comments'].length){
                    var html ='';
                    $('#cm').hide();
                    for(i = 0; i < result.data['comments'].length; i++){
                        
                        // var value = "<small><a href=/user/"+result.data['comments'][i].user.username.trim()+"/view >"+result.data['comments'][i].user.username+"</a></small>";
                        var value = "<small><a href='#'>"+result.data['comments'][i].user.username+"</a></small>";
                        var user = (result.data['comments'][i].answer.question.isAnonymous && result.data['comments'][i].user_id == result.data['comments'][i].answer.question.user_id)? "Anonymous": value;
                        var isSameUser = (result.data['comments'][i].user_id == result.user)?"<span class='pull-right'><i data="+result.data['comments'][i].id+" class='fa fa-edit text-primary editComment'></i> <i data="+result.data['comments'][i].id+" class='fa fa-trash  text-danger deleteComment'></i></span>":'';
                        html += "<div id=c"+result.data['comments'][i].id+" class='col-md-12 comment-item mt-20'>\
                        <div>\
                            <span class='text-bold'>"+user+"</span>\
                        "+isSameUser+"</div>\
                        <div style='margin-bottom:0px' id='answer{{ $answer->id }}' class='comment-ans'>"+result.data['comments'][i].comment+"\
                        </div>\
                        <div class='text-right'><span class='font-10'>"+moment(result.data['comments'][i].created_at).fromNow()+"</span></div>\
                        </div>";
                    }
                }else{
                    $('#cm').show();
                }
                $(holder).html(html);
               
               
               

               
            } else {

                $.alert({
                    title: 'Update!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content: result.message,
                });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({
                title: 'Update!',
                type: 'red',
                icon: 'fa fa-times',
                content: 'An error occurred. Please Try again.',
            });
            console.log(e);
        }
    });
}
function getAnswer(id,holder,user){
    $.ajax({
        type: 'GET',
        // headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        // },
        
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: base_url + '/user/answer/get?id='+id,
        success: function(result) {
            console.log(result);
            //$('#loaderImage').hide();
            if (result.status == 00) {
                //go to the next tab
               $(holder).html(result.data);
               if(result.isAnonymous)
               $(user).html('Anonymous');
               else
               $(user).html(result.user);
                console.log(result);
               

               
            } else {

                $.alert({
                    title: 'Update!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content: result.message,
                });
            }
        },
        error: function(e) {
            $('#loaderImage').hide();
            $.alert({
                title: 'Update!',
                type: 'red',
                icon: 'fa fa-times',
                content: 'An error occurred. Please Try again.',
            });
            console.log(e);
        }
    });
}


