<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/{rjobs?}', function () {

   $itemJob = null;
   
   if(!empty(request('rjobs'))){
       
        $job = request('rjobs');
        $itemJob = \App\RandomJobs::find($job);
        return view('welcome', compact('itemJob'));
    }
    if(auth()->check())
        return redirect(route('user.timeline', getFullNameWithSlug(auth()->user())))->with(['itemJob'=>$itemJob]);

    //get featured jobs
    $jobs = \App\JobAdvert::inRandomOrder()->take(4)->get();
    //get featured Items
    $products = \App\Product::inRandomOrder()->take(4)->get();
    //get featured questions
    $questions = \App\Question::inRandomOrder()->take(4)->get();

    return view('welcome', compact('itemJob','jobs','products','questions'));
})->name('welcome')->where('rjobs', '[0-9]+');




//job portal

Route::get('/search', 'SearchController@index')->name('search');

//job routes
Route::group(['prefix' => 'jobs'], function() {
//
    Route::get('/', 'JobsController@index')->name('jobsHomePage');
    Route::get('/vacancies', 'JobsController@listings')->name('job.listings');
    Route::get('/notify', 'JobsController@notify')->name('job.notify');
    Route::get('/vacancy/{job}', 'JobsController@show')->name('job.details');
});
//forum routes
Route::group(['prefix' => 'questions'], function() {
//
    Route::get('/{category?}', 'ForumController@index')->name('forumHomePage');
    
});



//sales routes
Route::group(['prefix' => 'market'], function() {
    //
        Route::get('/', 'MarketController@index')->name('marketHomePage');
        Route::get('/products', 'MarketController@listings')->name('product.listings');
        Route::get('/product/{product}', 'MarketController@show')->name('product.details');

    });

//user routes for jobs
Route::group(['prefix' => 'user/job'], function() {
    //
        Route::get('/{name?}/dashboard', 'JobUserController@dashboard')->name('job.user.dashboard');
        Route::get('/{name?}/profile', 'JobUserController@profile')->name('job.user.profile');

        Route::get('/{name?}/applications', 'JobUserController@applications')->name('job.user.applications');

});

//user route
Route::group(['prefix' => 'user'], function() {

    //questions
    Route::get('/question/create', 'QuestionsController@create')->name('question.create');
    Route::get('/question/delete/{slug}', 'QuestionsController@destroy')->name('question.delete');
    Route::post('/question/update/{slug}', 'QuestionsController@update')->name('question.update');
    Route::get('/question/edit/{slug}', 'QuestionsController@edit')->name('question.edit');
    Route::get('/questions/', 'QuestionsController@index')->name('question.index');
    Route::get('/question/{slug}', 'QuestionsController@show')->name('question.show');
    Route::post('/question/store', 'QuestionsController@store')->name('question.store');


    //answwers
    Route::get('/answer/create', 'AnswerController@create')->name('answer.create');
    Route::get('/answer/delete', 'AnswerController@destroy')->name('answer.delete');
    Route::post('answer/update', 'AnswerController@update')->name('answer.update');
    Route::get('/answer/edit/{id}', 'AnswerController@edit')->name('answer.edit');
    Route::get('/answer/', 'AnswerController@index')->name('answer.index');
    // Route::get('/answer/get', 'AnswerController@get')->name('answer.get');
    Route::get('/answer/get', 'AnswerController@show')->name('answer.show');
    Route::post('answer/store', 'AnswerController@store')->name('answer.store');

    //comments
    Route::get('/comment/index', 'CommentsController@index')->name('comment.index');
    Route::get('/comment/delete', 'CommentsController@destroy')->name('comment.delete');
    Route::post('comment/update', 'CommentsController@update')->name('comment.update');
    Route::get('/comment/edit/{id}', 'CommentsController@edit')->name('comment.edit');
    Route::get('/comment/', 'CommentsController@index')->name('comment.index');
    // Route::get('/answer/get', 'AnswerController@get')->name('answer.get');
    Route::get('/comment/get', 'CommentsController@show')->name('comment.show');
    Route::post('comment/store', 'CommentsController@store')->name('comment.store');

    //likes
    Route::get('/likes/create', 'LikesController@create')->name('like.create');
    Route::get('/likes/delete', 'LikesController@destroy')->name('like.delete');
   

    //
        Route::get('/{name?}/dashboard', 'UserController@dashboard')->name('user.dashboard');
        //edit profile of user
        Route::get('/{name?}/profile', 'UserController@profile')->name('user.profile');

        //show timeline of user
        Route::get('/{name}/timeline', 'UserController@timeline')->name('user.timeline');

        //view userprofile
        Route::get('/{name}/view', 'UserController@viewProfile')->name('user.profile.view');
        //update profile
        Route::post('/update/profile', 'UserController@editProfile')->name('user.edit.profile');
        Route::post('/update/social', 'UserController@editSocial')->name('user.edit.social');
        Route::post('/update/password', 'UserController@changePassword')->name('user.changePassword');


        //profile user image
        Route::post('/profile/image', 'ProfileImageController@store')->name('user.profileImage.store');

        //users education resource
        Route::post('/education/create', 'EducationController@store')->name('user.education.create');
        Route::post('/education/update', 'EducationController@editEducation')->name('user.education.update');
        Route::get('/education/delete', 'EducationController@delete')->name('user.education.delete');

        //users work resource
        Route::post('/work/create', 'WorkExperienceController@store')->name('user.work.create');
        Route::post('/work/update', 'WorkExperienceController@editExperience')->name('user.work.update');
        Route::get('/work/delete', 'WorkExperienceController@delete')->name('user.work.delete');
        Route::get('/logout', 'UserController@logout')->name('user.logout');

        //user creater random jobs
        Route::get('/random-job/create', 'RandomJobsController@create')->name('user.job.create');
        Route::post('/random-job/store', 'RandomJobsController@store')->name('user.create.randomJob');
        Route::get('/random-job/index', 'RandomJobsController@index')->name('user.job.index');
        Route::get('/random-job/delete', 'RandomJobsController@delete')->name('user.job.delete');


        //user create ad
        Route::get('/ad/create', 'AdController@create')->name('user.ad.create');
        Route::get('/ad/index', 'AdController@index')->name('user.ad.index');
        Route::post('/ad/update/{id}', 'AdController@update')->name('user.ad.update');
        Route::get('/ad/edit/{id}', 'AdController@edit')->name('user.ad.edit');
        Route::post('/ad/store', 'AdController@store')->name('user.ad.store');
        Route::get('/ad/delete', 'AdController@delete')->name('user.ad.delete');
        Route::get('/ad/image/delete', 'AdController@deleteImage')->name('user.ad.image.delete');

        //facility resource
        Route::get('/facility/{facilityName}/', 'FacilityController@editFacility')->name('user.facility.edit');
        Route::post('/facility/update', 'FacilityController@editFacilityProfile')->name('user.facility.update');
        Route::get('/facility/{facility}/profile', 'FacilityController@profile')->name('user.facility.profile');
        Route::get('/{id}/{facility}/profile', 'FacilityController@profileView')->name('user.facility.profile.view');

        //profile media objects
        Route::post('/media/store', 'MediaObjectController@store')->name('user.media.store');
        Route::get('/media/delete', 'MediaObjectController@delete')->name('user.media.delete');

        ///job adverts
        Route::get('/adverts/index', 'JobAdvertController@index')->name('user.adverts.index');
        Route::get('/adverts/{advert}', 'JobAdvertController@show')->name('user.adverts.show');
        Route::get('/adverts/{advert}/edit', 'JobAdvertController@edit')->name('user.adverts.edit');
        Route::post('/adverts/store', 'JobAdvertController@store')->name('user.adverts.store');
        Route::post('/adverts/update/{id}', 'JobAdvertController@update')->name('user.adverts.update');
        Route::get('/advert/create', 'JobAdvertController@create')->name('user.advert.create');
        Route::get('/advert/delete', 'JobAdvertController@delete')->name('user.advert.delete');
        Route::get('/advert/inactive/{id}', 'JobAdvertController@inactive')->name('advert.inactive');


        //user application
        Route::post('/application/store', 'ApplicationController@store')->name('user.applications.store');
        Route::get('/application/index', 'ApplicationController@index')->name('user.applications.index');
        Route::get('/application/shortlist', 'ApplicationController@shortlist')->name('shortlist');
        Route::post('/application/shortlist/selected', 'ApplicationController@shortlistSelected')->name('shortlistSelected');
        Route::post('/application/shortlist/email', 'ApplicationController@sendEmail')->name('user.application.email');

        Route::get('/activation', 'UserController@activate')->name('activation');


        //user subscriptions
        Route::get('/subscription/store/{job}', 'SubscriptionController@store')->name('user.subscriptions.store');

        //followers
        // Route::get('/follow/store/{user}/{type}', 'FollowerController@store')->name('user.followers.store');
        // Route::get('/unfollow/{id}/', 'FollowerController@delete')->name('user.followers.delete');
        // Route::get('/follow/', 'FollowerController@index')->name('user.followers.index');


});




//verification
Route::get('/verify-email/{email}/{token}', 'Auth\RegisterController@verifyEmail')->name('user.verifyEmail');
Route::get('/resend/activation', 'UserController@resendMail')->name('resendActivationMail');


Route::get('/login/{provider}', 'Auth\SocialController@redirectToProvider')->name('socialLogin');
Route::get('/auth/{provider}/callback', 'Auth\SocialController@handleProviderCallback');

Route::get('/download/cv/{job}', function(){
   
    $job = \App\JobAdvert::find(request('job'));
    if($job){
        if(auth()->user()->facility->id == $job->facility->id){
            $ids = $job->applications->pluck('cloud_id');
            
            $ids = $ids->toArray();
            // for($i = 0; $i < count($ids); $i++){
            //     return str_replace('cv/', '', $ids[$i]);
            // }

            activateCloudinary();
            $string =  \Cloudinary::download_archive_url($options = array(
                    'public_ids' => $ids,
                    'timestamp' =>  strtotime(\Carbon\Carbon::now()),
                    'resource_type' => 'raw',
                    'target_format' => 'zip',
                    'expires_at' => strtotime(\Carbon\Carbon::now()->addHours(2))));  
            return redirect(htmlentities($string));


        }
    }
})->name('downloadAllCV');



Route::group(['prefix' => 'follow'], function () {
    Route::get('/{type}', 'FollowerController@index')->name('followers.index');
    Route::get('/follow/{user}', 'FollowerController@follow')->name('followUser');
    Route::get('/unfollow/{user}', 'FollowerController@unfollow')->name('unFollowUser');


    
});














Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

VisitStats::routes();



Route::group(['prefix' => 'control-room'], function() {

    //
    Route::get('/marketing', 'AdminController@marketing');
    Route::get('/manager/inactive', 'AdminController@inactive')->name('getAllInactiveUsers');
    Route::get('/manager/facilities', 'AdminController@facilities')->name('getAllFacilities');
    Route::get('/manager/users', 'AdminController@getUsers')->name('getAllUsers');
    Route::get('/manager/incomplete', 'AdminController@getIncomplete')->name('getUserIncomplete');
    Route::get('/manager/inactive', 'AdminController@inactive')->name('getAllInactiveUsers');
    Route::get('/manager/emails', 'AdminController@emails')->name('getAllEmailsInDB');
    Route::get('/marketing', 'AdminController@marketing');
    Route::post('/marketing', 'AdminController@send')->name('admin.marketing.send');

    //dashboard
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

    // users
    Route::get('/users', 'AdminController@users')->name('admin.users');
    Route::get('/users/make-moderator/{id}', 'AdminController@makeModerator')->name('admin.user.make-moderator');

    //jobs
    Route::group(['prefix' => 'jobs'], function(){
        //posted jobs
        Route::get('/ad', 'AdminJobsController@adIndex')->name('admin.jobs.ad.index');
        

        //random jobs
        Route::get('/random', 'AdminJobsController@randomIndex')->name('admin.jobs.random.index');
    });


    //products
    Route::get('/products', 'AdminController@products')->name('admin.products');

    //questions
    Route::get('/questions', 'AdminController@questions')->name('admin.questions');
    Route::get('/question/category/', 'QuestionCategoryController@index')->name('admin.question.category');
    Route::get('/question/category/delete/{id?}', 'QuestionCategoryController@destroy')->name('admin.question.category.delete');

    //question category
    // Route::post('/question/category/create-many', 'QuestionCategoryController@store')->name('admin.question.category.storeMany');
    Route::post('/question/category/store', 'QuestionCategoryController@store')->name('admin.question.category.store');
    Route::post('/question/category/update', 'QuestionCategoryController@update')->name('admin.question.category.update');



    //login
    Route::get('/', 'AdminController@login')->name('admin.login');
    //login
    // Route::get('/', 'AdminController@register')->name('admin.register');
    

    // Route::get('/jobs/setup', 'AdminJobsController@setup')->name('jobs.setup');
    // Route::get('/jobs/index', 'AdminJobsController@index')->name('admin.jobs.index');
    // Route::get('/jobs/approve/', 'AdminJobsController@approve')->name('admin.jobs.approve');
    // Route::get('/jobs/reject/', 'AdminJobsController@reject')->name('admin.jobs.reject');
    // Route::get('/jobs/feature/', 'AdminJobsController@feature')->name('admin.jobs.feature');
    // Route::get('/jobs/delete/', 'AdminJobsController@destroy')->name('admin.jobs.destroy');


    //specialities
    Route::get('/specialties/', 'SpecialtyController@index')->name('admin.specialty.index');
    Route::get('/specialties/delete/{id?}', 'SpecialtyController@destroy')->name('admin.specialty.delete');
    Route::post('/specialties/store', 'SpecialtyController@store')->name('store.specialty');
    Route::post('/specialties/update', 'SpecialtyController@update')->name('update.specialty');

    // //job category
    Route::get('/job/categories', 'CategoryController@index')->name('category.index');
    Route::get('/categories/delete/{id?}', 'CategoryController@destroy')->name('delete.category');
    Route::post('/categories/store', 'CategoryController@store')->name('store.category');
    Route::post('/categories/update', 'CategoryController@update')->name('update.category');

    // //job category
    Route::get('/job/establishments/', 'EstablishmentController@index')->name('establishment.index');
    Route::get('/establishments/delete/{id?}', 'EstablishmentController@destroy')->name('delete.establishment');
    Route::post('/establishments/store', 'EstablishmentController@store')->name('store.establishment');
    Route::post('/establishments/update', 'EstablishmentController@update')->name('update.establishment');



});



