<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\sendApplicationNotificationToEmployer',
        'App\Console\Commands\sendSubscriptionMailToApplicant',
        'App\Console\Commands\sendActivationReminder',
        'App\Console\Commands\RemoveExpiredJobs',
        'App\Console\Commands\SendMailToNonUsers',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sendApplicationNotificationToEmployer:sendMail')
        ->dailyAt('16:00')->timezone('Africa/Lagos');

        $schedule->command('sendSubscriptionMailToApplicant')
        ->dailyAt('16:00')->timezone('Africa/Lagos');
        $schedule->command('sendActivationReminder')
        ->dailyAt('16:00')->timezone('Africa/Lagos');
        $schedule->command('RemoveExpiredJobs')
        ->dailyAt('16:00')->timezone('Africa/Lagos');
        $schedule->command('SendMailToNonUsers')
        ->dailyAt('16:00')->timezone('Africa/Lagos');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
