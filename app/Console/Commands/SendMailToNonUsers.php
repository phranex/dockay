<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserEmail;
use App\User;
use App\JobAdvert;
use Carbon\Carbon;

class SendMailToNonUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nonusers:sendJobMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send job notifications to marketing mails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $now = Carbon::now();
        $emails = UserEmail::all()->pluck('email');
        $registered_emails = User::all()->pluck('email')->toArray();
        $em = array();
        if(count($emails)){
            
            foreach($emails as $email){
                if(!in_array($email, $registered_emails)){
                    
                    $another_one = JobAdvert::whereDate('deadline','>', $now)
                    ->where('isActive', 1)                
                    ->inRandomOrder()->take(10)            
                    ->get();
                   
                
                        logger($another_one);
                        if(count($another_one)){
                            if(!in_array($email,$em)){
                               
                                sendJobMail($another_one,$email);
                               
                                array_push($em,$email);
                            }
                        }
                        
                    }
                    else{
                        logger('already registered');
                    }
                
                
            }
        }
    }
}
