<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class sendActivationReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendActivationReminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminds users to activate their profile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $users = User::where('verified', 0)->get();
        logger(count($users));

        if(count($users)){
            foreach($users as $user){
                logger($user);
                sendActivationReminder($user);
            }
        }
    }
}
