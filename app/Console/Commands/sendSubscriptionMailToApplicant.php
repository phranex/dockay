<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\JobAdvert;
use App\Subscription;
use App\User;

class sendSubscriptionMailToApplicant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendSubscriptionMailToApplicant:sendMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends email to a user who has subscribed to a particular job type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get jobs that have not been sent to users that have not passed their deadline
        $now = Carbon::now();
        $subscriptions = Subscription::all();
       
        if($subscriptions){
            foreach($subscriptions as $subscription){
                $jobs = JobAdvert::whereDate('deadline','>', $now)
                        ->where('isActive', 1)
                        ->where('specialty_id', $subscription->specialty_id)
                        ->where('category_id', $subscription->category_id)
                        ->inRandomOrder()->take(10)->get();
               
                if(count($jobs)){
                    logger($jobs);
                    $subject = "Available ". title_case($subscription->specialty->name).  " Jobs on Dockay";
                sendSubscriptionMail($jobs,$subscription->user, $subject);

                }
                
            }
        }

         //send job mails to matching their profile.
        //get all users
        $users = User::where('userType', 1)->get();
        
        if($users){
            $em = array();
            foreach($users as $user){
                $profession = $user->practitioner->specialty_id;
                
                if(isset($profession)){
                    $another_jobs = JobAdvert::whereDate('deadline','>', $now)
                    ->where('isActive', 1)
                    ->where('specialty_id', $profession)   
                    ->inRandomOrder()->take(10)            
                    ->get();
                        if(count($another_jobs)){
                            logger($another_jobs);
                            array_push($em,$user->email);
                           
                            sendSubscriptionMail($another_jobs,$user);
                        }
                       
                    
                }
               
                    
                
            }
            foreach($users as $user){
                
               
                    $another_one = JobAdvert::whereDate('deadline','>', $now)
                    ->where('isActive', 1)
                    
                    ->inRandomOrder()->take(10)            
                    ->get();
                    if(count($another_one)){
                        logger($another_one);
                        if(!in_array($user->email,$em))
                            sendSubscriptionMail($another_one,$user);
                    
              
                    }
                      
                    
                
            }


        }

        

    }

   
}
