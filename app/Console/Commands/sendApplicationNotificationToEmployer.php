<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\JobAdvert;

class sendApplicationNotificationToEmployer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendApplicationNotificationToEmployer:sendMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends mail to employer notifying him of recent applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get all jobadverts that hasnt been read and has new applications
        $unseenJobs = JobAdvert::where('hasBeenRead',0)->where('isActive', 1)->get();
        
        
        if(count($unseenJobs)){
            $emails = array();
            
            foreach($unseenJobs as $unseenJob){   
                
                if(count($unseenJob->applications)){
                    
                    $employer_email = $unseenJob->facility->user->email;
                   
                    if(!in_array($employer_email, $emails)){
                        logger('i got here');
                        sendApplicationNotificationToEmployers($employer_email);
                    }
                    array_push($emails, $employer_email);
                }
            }
            

        }

       
    }
}
