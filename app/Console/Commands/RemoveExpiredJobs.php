<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\JobAdvert;

class RemoveExpiredJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This removes old and expired jobs from the ssyetm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $now = Carbon::now();
        $jobs =JobAdvert::whereDate('deadline','<', $now)
                ->where('isActive', 1)
                
                ->update(['isActive' => 0]);
        
    }
}
