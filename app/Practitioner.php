<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Practitioner extends User
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create($user, $n=null)
    {
        $this->firstName = !empty(request('firstName'))?request('firstName'):$n['firstName'];
        $this->lastName = !empty(request('lastName'))?request('lastName'):$n['lastName'];
        $this->user_id = $user;
        $this->save();

    }
    public function educations()
    {
        return $this->hasMany('App\Education', 'user_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

    public function experiences()
    {
        return $this->hasMany('App\WorkExperience', 'user_id');
    }

    public function cv()
    {
        return $this->hasOne('App\CV', 'practitioner_id');
    }

    public function applications()
    {
        return $this->hasMany('App\Application', 'user_id');
    }
}
