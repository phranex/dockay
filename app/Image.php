<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Image extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create($id,$path, $cloud_id)
    {
        $this->product_id = $id;
        $this->path = $path;
        $this->cloud_id = $cloud_id;
        return $this->save();
    }
}
