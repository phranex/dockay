<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MediaObject extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    
    public function create($path, $type, $cloud)
    {
        $this->user_id = auth()->id();
        $this->path = $path;
        $this->type = $type;
        $this->cloud_id = $cloud;
        return $this->save();
    }
}
