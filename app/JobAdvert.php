<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Application;
// use Laravel\Scout\Searchable;

class JobAdvert extends Model
{
    //
    use SoftDeletes;
    // use Searchable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }
    public function establishment()
    {
        return $this->belongsTo('App\Establishment');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function facility()
    {
        return $this->belongsTo('App\Facility');
    }
    public function applications()
    {
        return $this->hasMany('App\Application', 'job_advert_id');
    }

    public function create()
    {
        $this->title = request('title');
        $this->facility_id = auth()->user()->facility->id;
        $this->specialty_id = request('specialty');
        $this->category_id = request('category');
        $this->establishment_id = request('establishment');
        $this->gender = request('gender');
        $this->deadline = request('deadline');
        $this->min = request('min');
        $this->max = request('max');
        $this->state = request('state');
        $this->country = request('country');
        $this->hasBeenRead = 1;
        $this->isActive = 1;
        $this->mode = (request('mode') == 'c')? request('ce'): request('mode');
        $this->description = request('description');
       $this->save();
       return $this;

    }

    public function isBeingRead()
    {
        
        $this->hasBeenRead = 1;
        $this->save();
       
    }

    public function inActivate()
    {
        # code...
        $this->isActive = 0;
        $this->save();
    }

    public function edit()
    {
        $this->title = request('title');

        $this->specialty_id = request('specialty');
        $this->category_id = request('category');
        $this->establishment_id = request('establishment');
        $this->gender = request('gender');
        $this->deadline = request('deadline');
        $this->min = request('min');
        $this->max = request('max');
        $this->country = request('country');
        $this->state = request('state');
        $this->description = request('description');
       $this->save();
       return $this;

    }

    public function skills()
    {
        return $this->hasMany('App\Skill', 'job_advert_id');
    }

    public function shortlist($applications = null)
    {
        if($applications == null){
            $applications = $this->applications;
                if(isset($applications)){
                    if(count($applications)){
                        foreach ($applications as $application) {
                            $application->shortlist($application);
                        }
                    }
                }
        }else{
            if(count($applications)){
                for($i = 0; $i < count($applications); $i++){
                    $application = Application::find($applications[$i]);
                    if($applications){
                        $application->shortlist('shortlist',$application);
                    }
                }
            }
        }


    }

    public function sendMail(){
        $shortlist = $this->applications->where('shortlist', 1);
        
    }


    public function searchableAs()
    {
        return 'title';
    }

}
