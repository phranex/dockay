<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cloudder;

class MediaObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "gallery"    => "required|array|min:1",
            // "ideo"    => "required|array|min:1",


        ]);
        if(request()->hasFile('gallery')){
            activateCloudinary();
            // if(env('APP_ENV') == 'local'){
                for($i = 0; $i < count($request->file('gallery')); $i++){
                    // $path = request()->file("gallery.$i")->store("public/media");
                    $img = \Cloudinary\Uploader::upload($request->file("gallery.$i"),array(                                            
                        'use_filename' => true,
                        'folder' => 'media/photo',                                            
                    ));
                    $path = $img['secure_url'];
                    $cloud_id = $img['public_id'];
                    $mediaObject = new \App\MediaObject;
                    $type = 'P';
                    $mediaObject->create($path,$type,$cloud_id);
                 }
            return back()->with('success', 'Image has been successfully saved');
        }
        // if(request()->hasFile('ideo')){
        //     return "k";
        //     // if(env('APP_ENV') == 'local'){
        //         for($i = 0; $i < count($request->file('ideo')); $i++){
        //             // $path = request()->file("gallery.$i")->store("public/media");
        //             $img = Cloudder::upload($request->file("ideo.$i"));
        //             return $path = $img->secureShow($img->getPublicId());
        //             $mediaObject = new \App\MediaObject;
        //             $type = 'V';
        //             $mediaObject->create($path,$type);
        //          }




        // }else{
        //     return "lp";
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete()
    {
        $id = request('id');
        $mediaObject = \App\MediaObject::find($id);
        activateCloudinary();
        if(isset($mediaObject->cloud_id)){
            \Cloudinary\Uploader::destroy($mediaObject->cloud_id);
        }
        if($mediaObject->delete())
            return array('status'=> 0, "message"=> 'Image has been deleted successfully');
        else
            return array('status'=> 99, "message"=> 'An unexpected error occurred!! Please try again.');

    }
}
