<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionCategory;
use App\Question;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        if(!empty(request('search'))){
            $questions = $this->search();
        }
        else if(!empty(request('category'))){
            $cat = str_replace('_', '/', request('category'));
            $id = QuestionCategory::where('name', $cat)->pluck('id')->first();
            $questions = Question::where('question_category_id', $id)->latest()->get();
        }else{
            
            $questions = Question::latest()->get();
        }
        $categories = QuestionCategory::orderBy('name')->get() ;
        
        return view('forum.index',compact('categories', 'questions'));
    }


    public function search()
    {
        # code...
        if(!empty(request('category'))){
            $cat = str_replace('_', '/', request('category'));
            $id = QuestionCategory::where('name', $cat)->pluck('id')->first();
            return $questions = Question::findQuery(request('search'), $id)->get();
        }else{
            return $questions = Question::findQuery(request('search'))->get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
