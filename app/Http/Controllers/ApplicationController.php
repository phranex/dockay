<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\Mail\UserShortListMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApplicationMail;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $applications = auth()->user()->practitioner->applications;
        return view('user.applications.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(checkProfileAndStatus()){
            return back()->with('error', "Only applicants who have successfully activated  and updated their profile can apply for jobs");
        }

        $rule = '';
       if(request()->hasFile('cv')){
            // $path = request()->file('cv')->store("public/profile");
            $rule = 'required|mimes:pdf,docx,doc';
       }
        $this->validate($request,[
            'job' => 'required',
            'cv' => $rule
        ]);
        //if user uploads a file
        if(request()->hasFile('cv')){
            //config cloudinary
            activateCloudinary();
            //get file ext
            $format = $request->file('cv')->getClientOriginalExtension();
            //upload to cloudinary
            //create an id.
            $file_id = auth()->user()->username.'-'.auth()->id().'-cv';
            $public_id = auth()->user()->practitioner->firstName.'_'.auth()->user()->practitioner->lastName.'_'.auth()->id();            $img = \Cloudinary\Uploader::upload($request->file('cv'),array('resource_type' => 'raw', 
                                                                    'format' => $format,
                                                                    // 'use_filename' => true,
                                                                    'folder' => 'cv',
                                                                    'public_id' => $public_id
                                                                    
                                                                ));
            //get secured path
            $path = $img['secure_url'];
            //get id
            $cloud_id = $img['public_id'];
            //check if user requested to use this cv as his default. this replaces any old one found
            //create a new application for the user
            $application = new Application;
            $applied = $application->create($path,$cloud_id);

            //check if user has uploaded a cv
            $cv = \App\CV::where('practitioner_id',auth()->user()->practitioner->id)->first();
            if(!empty($cv)){
                if(request('default') == 'Y'){
                    activateCloudinary();
                    \Cloudinary\Uploader::destroy($cv->cloud_id);

                    $cv->edit($path,$cloud_id);
                }
            }else{
                $cv = new \App\CV;
                $cv->create(auth()->user()->practitioner->id,$path,$cloud_id);
            }
           

        }else{
            $path = auth()->user()->practitioner->cv->path;
            $cloud_id = auth()->user()->practitioner->cv->cloud_id;
             //create a new application for the user
             $application = new Application;
             $applied = $application->create($path, $cloud_id); 
        }
       
        if($applied){

            return back()->with('success', 'Your application was sent successfully');
        }else{
            return back()->with('error', 'Your application could not be sent. Please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function shortlist()
    {
       
        //get application via id
        if(!empty(request('application'))){
            $application = Application::find(request('application'));
            //change shortlist flag
            if($application){
                $application->shortlist();
                //send mail
               
                
                return back()->with('success', 'Shortlisted');
            }else{
                return back()->with('error', 'An error occurred');
            }
        }else if(!empty(request('j'))){
            $job = \App\JobAdvert::find(request('j'));
            if($job){
                $job->shortlist();
                return back()->with('success', 'Shortlisted');
            }else{
                return back()->with('error', 'An error occurred');
            }

        }else{
            return back()->with('error', 'An error occurred');
        }



    }


    public function shortlistSelected()
    {
        //return request()->all();
        $application_ids = request()->all();
        if(count($application_ids)) {
            $num = count($application_ids);
            $c = 0;
            for($i = 0; $i < $num; $i++ ){
                $application = Application::find($application_ids[$i]);
                if($application){
                    $application->shortlist($application);
                    $c++;
                }else{
                    continue;
                }
            }
            if($c == $num)
                return array('status' => 0, 'message' => 'Shortlisted all Successfully');
            else
                return array('status' => 99, 'message' => 'One or more applicants was not successfully shortlisted');
        }
    }

    public function sendEmail(){
        $job = \App\JobAdvert::find(request('job_id'));
        if($job){
           $shortlisted = $job->applications->where('shortlist_flag', 1)->where('mail_sent', 0);
           if($shortlisted){
               $emails = array();
               foreach($shortlisted as $short){
                   //mark this application as sent
                   $short->mailSent();
                    array_push($emails,$short->practitioner->user->email);
               }
               
               sendMailToApplicants($job,$emails,request());

               //$job->hasBeenRead = 1;
               //$job->save();
               return back()->with('success', 'Mails sent successfully');
           }
           return back()->with('error', 'An error occurred');
            
        }
    }
}
