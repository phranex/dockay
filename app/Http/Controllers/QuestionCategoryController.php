<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionCategory;

class QuestionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = QuestionCategory::all();

        return view('admin.questions.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'category' => 'required',
        ]);
        $categories = explode(',', request('category'));
        if(count($categories) > 1){
            $categories = array_map('trim', $categories);
            foreach($categories as $cat){
                //check if name already exists
                $catAlreadyExists = QuestionCategory::where('name', $cat)->first();
                if(!$catAlreadyExists){
                    $category = new QuestionCategory;
                    $category->create($cat);
                }                
            }
            return back()->with('success', 'Created categories succesfully');
        }else{
           
            $this->validate($request,[
                'category' => 'required',
                'description' => 'required|min:10',
                "category_image"    => "required|min:1",
            ]);
           
            if(request()->hasFile('category_image')){
                
                try{
                   
                    $cat = request('category');
                    activateCloudinary();
                    //get file ext
                    $format = $request->file('category_image')->getClientOriginalExtension();
                    //upload to cloudinary
                    //create an id.
                   
                    $public_id = $cat.'_'.time();            
                    $img = \Cloudinary\Uploader::upload($request->file('category_image'),array('resource_type' => 'raw', 
                                                                            'format' => $format,
                                                                            // 'use_filename' => true,
                                                                            'folder' => 'question_category',
                                                                            'public_id' => $public_id
                                                                            
                                                                        ));
                    //get secured path
                    $path = $img['secure_url'];
                    //get id
                    $cloud_id = $img['public_id'];
                    $category = new QuestionCategory;
                    $category->create($cat, $path, $cloud_id);
                    return back()->with('success', 'added successfully');

                  
                }catch(\Exception $e){
                    \Log::info($e);
                    
                    $category->remove();
                    
                    return back()->with('error', 'Couldnt upload images. Please retry')->withInput();
                }
            }
                // if(env('APP_ENV') == 'local'){
                    
                    
        }
           

        return back()->with('error', 'Unexpected error occurred');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
       
        $rule = "";
        if(request('changeImage')){
            if(request()->hasFile('editImage'))
                $rule =  "required|min:1";
            else
                return back()->with('error', 'You clicked to change the image. Please upload one to continue');
        }
        $this->validate($request,[
            'editCategory' => 'required',
            'editDescription' => 'required|min:10',
            'id' => 'required',
            'editImage' => $rule
        ]);
        $category = QuestionCategory::find(request('id'));
        $cat = request('editCategory');

        if(request()->hasFile('editImage')){
                
            try{
               
               
                //activate cloudinary
                activateCloudinary();
                //get current category
               
                if($category){
                    //delete previous image from cloudinary
                    if(isset($category->cloud_id))
                        \Cloudinary\Uploader::destroy($category->cloud_id);
                        //get file ext
                    $format = $request->file('editImage')->getClientOriginalExtension();
                    //upload to cloudinary
                    //create an id.
                
                    $public_id = $cat.'_'.time();            
                    $img = \Cloudinary\Uploader::upload($request->file('editImage'),array('resource_type' => 'raw', 
                                                                            'format' => $format,
                                                                            // 'use_filename' => true,
                                                                            'folder' => 'question_category',
                                                                            'public_id' => $public_id
                                                                            
                                                                        ));
                    //get secured path
                    $path = $img['secure_url'];
                    //get id
                    $cloud_id = $img['public_id'];
                  
                    $category->edit($cat, $path, $cloud_id);
                    return back()->with('success', 'edited successfully');
                }else{
                    return back()->With('success', 'An unexpected error occurred');
                }
            }catch(\Exception $e){
                \Log::info($e);
                
                return back()->with('error', 'Couldnt upload images. Please retry')->withInput();
            }
        }else{
            $category->edit($cat);
            return back()->with('success', 'edited successfully');

        }
           
        
        return back()->with('error', 'An unexpected error occurred');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $category = QuestionCategory::find($id);
        if($category){
            if($category->delete()){
                 //activate cloudinary
                 activateCloudinary();
                 if(isset($category->cloud_id))
                    \Cloudinary\Uploader::destroy($category->cloud_id);
                return back()->with('success', 'Deleted Successfully');
            }
        }
        return back()->with('error', 'Couldnt delete. Try again');
    }
}
