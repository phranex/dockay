<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function profile()
    {
        $establishments = \App\Establishment::all();
        return view('user.facility.edit', compact('establishments'));
    }

    public function profileView()
    {
        $id = request('id');
        if(is_numeric($id)){
            $facility = \App\Facility::find($id);
            $follower = \App\Follower::where('follower_id',auth()->id())->where('facility_id',$facility->id)->first();
            if(isset($follower)){
                $user_is_following = count($follower);
                $id = $follower->id;
            }
            return view('user.facility.profile-view-by-user', compact('facility', 'user_is_following','id'));
        }
        else
            return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editFacility()
    {
        $establishments = \App\Establishment::all();
        return view('user.facility.company-profile', compact('establishments'));
    }
    public function editFacilityProfile(Request $request)
    {
        $this->validate($request,[

            'establishment' => 'required',
            'name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'description' => 'required',
            'username' => 'required',
        ]);
        $user = auth()->user();
        $user->edit();
        //$facility = \App\Facility::find(request('facility_id'));


        if($user->edit())
            return array('status'=> 0, "message"=> 'success');
        else
            return array('status'=> 99, "message"=> 'error');
    }
}
