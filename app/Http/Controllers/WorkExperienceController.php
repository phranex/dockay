<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WorkExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $workExperience = new \App\WorkExperience;
        if($workExperience->create())
        return array('status'=> 0, "message"=> 'success');
        else
            return array('status'=> 99, "message"=> 'error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function editExperience()
    {
        $exp = \App\WorkExperience::find(request('work_id'));
        if(auth()->user()->practitioner->id == $exp->user_id)
            return responseMessage($exp->edit());
        else
        return responseMessage(false);
    }

    public function delete()
    {
        $exp = \App\WorkExperience::find(request('id'));
        $exp->delete();
        return back()->with('success', 'Work Experience Deleted successfuly');
    }
}
