<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\QuestionCategory;
use App\Jobs\sendQuestionToUsers;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web')->except('show');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questions = auth()->user()->questions;
        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = QuestionCategory::all();
       return view('question.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $this->validate($request, [
            'title' => 'required|unique:questions',
            'category' => 'required'
        ]);
        //
        $question = new Question;
        if($question->create()){
            $this->dispatch(new sendQuestionToUsers($question));
            return redirect(route('user.timeline', getFullNameWithSlug(auth()->user())))->with('success', 'You asked a question!!');
        }
        else
            return back()->with('error', 'An unexpected error occured');
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $question = Question::findBySlug($slug);
        if($question){
          
            return view('question.show', compact('question'));
        }
        else
        return back()->with('error', 'An unexpected error occured');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        //
        $question = Question::findBySlug($slug);
        if($question)
        return view('question.edit', compact('question'));
        else
        return back()->with('error', 'An unexpected error occured');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        //
        $question = Question::findBySlug($slug);
        if($question){
            if($question->edit())
                return redirect(route('question.index'))->with('success','Question was updated!!');
            else
            return back()->with('error', 'An unexpected error occured');
        } else
        return back()->with('error', 'An unexpected error occured');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        //
        $question = Question::findBySlug($slug);
        if($question){
            $question->delete();
            
            return redirect(route('question.index'))->with('success','Question was deleted!!');

        }
        else
        return back()->with('error', 'An unexpected error occured');
    }
}
