<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Comment;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $comments = Comment::with(['user','answer.question'])->where('answer_id', request('id'))->latest()->get();
       
        $answer = Answer::find(request('id'));
        $user = (auth()->check())?auth()->id():false;
        $isAnonymous = ($answer->user_id == $answer->question->user_id && $answer->question->isAnonymous )?true:false;

        if($comments && $answer){
            return array(
                'status' => 0, 
                'data' => [
                    'comments' => $comments, 
                    'answer' => 
                        [ 'answer' => $answer->answer, 
                        'user' => $answer->user->username,
                        'isAnonymous' => $isAnonymous
                        ]
                ],
                "user" => $user,
                
                );
        }

        return displayErrorMessage();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'comment' => 'required',
            'id3' => 'required'
        ]);
        $answer = Answer::find(request('id3'));
        if($answer){
            $comment = new Comment;            
            $comment->create();
            return back()->with('success', 'Posted comment successfully');
        }
        return back()->with('error', 'An unexpected error occured');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $comment = Comment::find(request('id'));
        if($comment){
            return array('status' => 0, 'data' => $comment->comment, 'id' => $comment->id);
        }

        return displayErrorMessage();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'comment_edit' => 'required',
            'comment_id' => 'required'
        ]);
        $comment = Comment::find(request('comment_id'));
        if($comment && $comment->user_id == auth()->id()){
            $comment->edit();
            return back()->with('success', 'Edited succesfully');
        }
            return back()->with('error', 'An uncexpected error occurred');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        $id = request('id');
        $comment = Comment::find($id);
        if($comment && $comment->user_id == auth()->id()){
            $comment->delete();
            return back()->with('success', 'Comment was deleted successfully');
        }

        return displayMessage();
    }
}
