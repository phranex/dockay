<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Cloudder;

class AdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adverts = auth()->user()->products;
        return view('user.ad.index', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(checkProfileAndStatus()){
            return redirect(route('activation'));
        }
        return view('user.ad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return request();
        $this->validate($request, [
            'name' => 'required',
            'title' => 'required',
            'nature' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required|min:10',
            "product"    => "required|array|min:2",
            "product.*" => "required|min:1"
        ]);
       
            if(request()->hasFile('product')){
                $ad = new \App\Product;

                $r = $ad->create();
                if($r){
                    activateCloudinary();
                    
                        // $path = request()->file("gallery.$i")->store("public/media");
                        try{
                            for($i = 0; $i < count($request->file('product')); $i++){
                                $img = \Cloudinary\Uploader::upload($request->file("product.$i"));
                                $path = $img['secure_url'];
                                $cloud_id = $img['public_id'];
                                
                                $mediaObject = new Image;
        
                                $mediaObject->create($r->id,$path, $cloud_id);
                            }
                        }catch(\Exception $e){
                            \Log::info($e);
                            
                           // $r->remove();
                            
                            return back()->with('error', 'Couldnt upload images. Please retry')->withInput();
                        }catch(\Cloudinary\Error $e){
                            \Log::info($e);
                            return back()->with('error', 'Couldnt upload images. Please retry')->withInput();

                        }
                        
             

                }
                // if(env('APP_ENV') == 'local'){
                    
                    
                return redirect(route('user.ad.index'))->with('success', 'Posted successfully');
            }
        

        return back()->with('error', 'Unexpected error occured');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = \App\Product::find($id);

        return view('user.ad.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       
        
        $this->validate($request, [
            'name' => 'required',
            'title' => 'required',
            'nature' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required|min:10',
            "product"    => "required|array|min:1",
            "product.*" => "required|min:1"
        ]);
        $ad = \App\Product::find($id);

        $r = $ad->edit();
        if($r){
            activateCloudinary();
            
                // $path = request()->file("gallery.$i")->store("public/media");
                try{
                    for($i = 0; $i < count($request->file('product')); $i++){
                        $img = \Cloudinary\Uploader::upload($request->file("product.$i"));
                        $path = $img['secure_url'];
                        $cloud_id = $img['public_id'];
                        
                        $mediaObject = new Image;

                        $mediaObject->create($r->id,$path, $cloud_id);
                    }
                }catch(\Exception $e){
                    \Log::info($e);
                    
                   // $r->remove();
                    
                    return back()->with('error', 'Couldnt upload images. Please retry')->withInput();
                }catch(\Cloudinary\Error $e){
                    \Log::info($e);
                    return back()->with('error', 'Couldnt upload images. Please retry')->withInput();

                }
                
        return back()->with('success', 'Update Successfully');
                

        }

        return back()->with('error', 'Unexpected error occured');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete()
    {
        $exp = \App\Product::find(request('id'));
        $exp->delete();
        return back()->with('success', 'Ad Deleted successfuly');
    }



    public function deleteImage()
    {
        $id = request('id');
        $image = \App\Image::find($id);
        activateCloudinary();
        if(isset($image->cloud_id)){
            \Cloudinary\Uploader::destroy($image->cloud_id);
        }
        if($image->delete())
            return array('status'=> 0, "message"=> 'Image has been deleted successfully');
        else
            return array('status'=> 99, "message"=> 'An unexpected error occurred!! Please try again.');

    }
}
