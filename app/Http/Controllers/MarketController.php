<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarketController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $items = \App\Product::take(10)->latest()->get();
        return view('market.index', compact('items'));
    }


    public function listings()
    {
        $name = request('query');
        $brands = DB::table('products')                
                 ->select('brand', DB::raw('count(*) as total'))
                 ->where('name','LIKE', "%$name%")
                 ->groupBy('brand')
                 ->get();
        $nature = DB::table('products')                
                ->select('nature', DB::raw('count(*) as total'))
                ->where('name','LIKE', "%$name%")
                ->groupBy('nature')
                ->get();

        $page_size = !empty(request('page_size'))?request('page_size'):10;
        $count = 0;
        $items = \App\Product::where(function($query) use ($count){
            $brand = !empty(request('brand'))?request('brand'):null;
            $nature = !empty(request('nature'))?request('nature'):null;
            $q = !empty(request('query'))?request('query'):null;


            if(isset($q)){
                $query->where('name','LIKE', "%$q%");
                $count++;
            }

            if(isset($brand)){
                $query->where('brand', 'LIKE', $brand);
                $count++;
            }

            if(isset($nature)){  
                $query->where('nature', 'LIKE', $nature);
                $count++;
            }
        })->inRandomOrder()->paginate($page_size);
        
       
        return view('market.listing', compact('items','brands', 'nature'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = \App\Product::find($id);

        return view('market.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
