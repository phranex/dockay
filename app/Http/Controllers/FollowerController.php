<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class FollowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        if(!empty(request('type'))){
            if(request('type') == 'fg'){
                $leaders_id = auth()->user()->followings->pluck('followable_id');
                $follows = User::whereIn('id', $leaders_id)->get();

                return view('user.follows', compact('follows'));

                
            }
        }

        return back();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
    }

    public function follow($username)
    {
        # code...
       
            
        $leader = User::findByUsername($username);
        if($leader){
            $follower = auth()->user();
            $follower->follow($leader);
            $username = title_case($username);
            return back()->with('success', "You are now following $username");
        }else{
            return back()->with('error', 'There was an issue following that user');
        }
       
    }


    public function unfollow($username)
    {
        $leader = User::findByUsername($username);
        if($leader){
            $follower = auth()->user();
            $follower->unfollow($leader);
            $username = title_case($username);
            return back()->with('success', "You no longer folllow $username");
        }else{
            return back()->with('error', 'There was an issue following that user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
