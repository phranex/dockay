<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cloudder;
use App\ProfileImage;

class ProfileImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'profile' => 'required|mimes:jpeg,jpg,bmp,png,gif|max:3000',

        ]);
        if(request()->hasFile('profile')){
            activateCloudinary();
            //check if a profile image already exists on cloudinary
            $c = ProfileImage::where('user_id', auth()->id());
            if(isset($c->cloud_id)){
                \Cloudinary\Uploader::destroy($c->cloud_id);
            }
            $img = \Cloudinary\Uploader::upload($request->file('profile'),array(                                            
                                            'use_filename' => true,
                                            'folder' => 'profile',                                            
                                        ));
            $path = $img['secure_url'];
            $cloud_id = $img['public_id'];
            $profileImage = \App\ProfileImage::find(request('profile_image_id'));
            $status = $profileImage->edit($path,$cloud_id);
            if($status)
                return back()->with('success','Uploaded Sucessfully');
            else
            return back()->with('error','An error ocuured');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
