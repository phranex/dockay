<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\Jobs\sendQuestionToUsers;
use App\JobAdvert;

class JobAdvertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(auth()->user()->userType == 2){
            $adverts = auth()->user()->facility->jobAdverts;
        }

        return view('user.adverts.index', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(checkProfileAndStatus()){
            return redirect(route('activation'));
        }
        $categories = \App\Category::all();
        $specialties = \App\Specialty::all();
        $establishments = \App\Establishment::all();
        return view('user.adverts.create', compact('categories', 'specialties', 'establishments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        
        $this->validate($request,[
            'title' => 'required|string',
            'category' => 'required|',
            'specialty' => 'required|',
            'establishment' => 'required|',
            'gender' => 'required|',
            'description' => 'required|',
            'mode' => 'required'
        ]);
        $advert = new \App\JobAdvert;
        $ad = $advert->create();

        if($ad){
            
            if(!empty(request('skills'))){
                $skills = explode(',', request('skills'));
                $skills = array_map('trim', $skills);
                foreach($skills as $skill){
                    $s = new \App\Skill;
                    $s->create($skill,$ad->id);
                }
            }
            return redirect(route('user.adverts.index'));
        }else{
            return back()->with('error', 'an error occurred');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $advert = \App\JobAdvert::find($id);
        if($advert){
            $advert->isBeingRead();
            $page = request('size')?request('size'):25;            
            $applications = Application::where('job_advert_id', $advert->id)->paginate($page);
            $shortlisted = $applications->where('shortlist_flag' , 1)->count();
            $unsentMailNum = $applications->where('mail_sent' , 0)->count();
            $num_of_app = count($applications);
            $all_shortlisted = ($num_of_app == $shortlisted)?1:0;
            return view('user.adverts.show', compact('advert', 'applications','unsentMailNum', 'all_shortlisted', 'shortlisted'));
        }

        return back()->with('An error occurred');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $advert = \App\JobAdvert::find($id);
        if($advert->facility_id != auth()->user()->facility->id)
            return back();
        if($advert){
            $categories = \App\Category::all();
            $specialties = \App\Specialty::all();
            $establishments = \App\Establishment::all();
            $skills = implode(',', $advert->skills->pluck('description')->toArray());
            return view('user.adverts.edit', compact('advert','skills','categories','specialties','establishments'));
        }


        return back()->with('An error occurred');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //
        $this->validate($request,[
            'title' => 'required|string',
            'category' => 'required|',
            'specialty' => 'required|',
            'establishment' => 'required|',
            'gender' => 'required|',
            'description' => 'required|',
            'country' => 'required|',
            'state' => 'required|',
        ]);
        $advert = \App\JobAdvert::find($id);
        $advert->edit();
        if($advert){
            $skills = \App\Skill::where('job_advert_id', $advert->id);
            $skills->delete();
            if(!empty(request('skills'))){
                $skills = explode(',', request('skills'));
                $skills = array_map('trim', $skills);
                foreach($skills as $skill){
                    $s = new \App\Skill;
                    $s->create($skill,$advert->id);
                }
            }
            return redirect(route('user.adverts.index'));
        }else{
            return back()->with('error', 'an error occurred');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete()
    {
        $advert = \App\JobAdvert::find(request('id'));
        if($advert){
            $skills  = \App\Skill::where('job_advert_id', $advert->id);
            $skills->delete();
            $advert->delete();
            return back()->with('success'. 'deleted successfully');
        }else
            return back()->with('error', 'error occurred');
    }


    public function inactive($id)
    {
        # code...
        if(is_numeric($id)){
            $advert = JobAdvert::find($id);
            $advert->inActivate();
            return back()->with('success', 'Operation was successful');
        }

        return back()->With('error', 'An error occurred. Please try again');
    }
}
