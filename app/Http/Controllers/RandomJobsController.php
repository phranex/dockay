<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RandomJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(checkProfileAndStatus()){
            return redirect(route('activation'));
        }
        $randomJobs = \App\RandomJobs::where('user_id', auth()->id())->inRandomOrder()->get();
        return view('user.jobs.index', compact('randomJobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(checkProfileAndStatus()){
            return redirect(route('activation'));
        }
        $categories = \App\Category::all();
        $specialties = \App\Specialty::all();
        $establishments = \App\Establishment::all();
        return view('user.jobs.create', compact('categories', 'specialties', 'establishments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
        $this->validate($request,[
            'specialty' => 'required',
            'establishment' => 'required',
            'category' => 'required',
            'contact' => 'required',
            'gender' => 'required',
            'description' => 'required',
        ]);
        $randomJob = new \App\RandomJobs;
        $randomJob->create();
        return back()->with('success', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete()
    {
        $exp = \App\RandomJobs::find(request('id'));
        $exp->delete();
        return back()->with('success', 'Job Deleted successfuly');
    }
}
