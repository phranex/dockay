<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */

     public function login()
     {
        return view('admin.login');
     }
    public function dashboard()
    {
        return view('admin.dashboard');
    }


    public function users()
    {
        $users =  User::paginate(20);
        return view('admin.users', compact('users'));
    }

    public function jobs()
    {
        return view('admin.jobs');
    }

    public function products()
    {
        return view('admin.products');
    }

    public function questions()
    {
        return view('admin.questions');
    }

    public function marketing()
    {
        # code...
        return view('admin.marketing');
    }
    public function send(Request $request)
    {
        $this->validate($request,[
            'emails' => 'required',
            'message' => 'required',
            'subject' => 'required'
        ]);
        $emails = explode(',', request('emails'));
        $mess = request('message');
        $subject = request('subject');
        if(count($emails)){
            for($i = 0; $i < count($emails); $i++){
                if(filter_var($emails[$i], FILTER_VALIDATE_EMAIL)){
                    $data['mess'] = $mess;
                    logger($data);
                    sendMarketingMail($emails[$i], $data, $subject);
                }
            }

            return back()->with('success', 'sent');
        }

        return back()->with('error', 'NOt sent');
        
       
    }


    public function makeModerator($id)
    {
        # code...
        if(is_numeric($id)){
            $user = User::find($id);
            if($user){
                $user->makeModerator();
                return back()->with('success', 'Operation was successful');
            }
        }

        return back()->with('error', 'Án unknown error occurred');
    }

    public function inactive(){
         $users  = User::where('verified', 0)->pluck('email')->toArray();
        return implode(',', $users);
    }
    public function facilities(){
         $users  = User::where('userType', 2)->pluck('email')->toArray();
        return implode(',', $users);
    }
    public function getUsers(){
         $users  = User::where('userType', 1)->pluck('email')->toArray();
        return implode(',', $users);
    }
    public function getIncomplete(){
         $users  = User::where('phoneNumber', null)->pluck('email')->toArray();
        return implode(',', $users);
    }
    public function emails(){
         $users  = \App\UserEmail::all()->pluck('email')->toArray();
        return implode(',', $users);
    }
}
