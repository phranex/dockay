<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use App\Product;
use App\JobAdvert;
use \Illuminate\Support\Facades\Hash;

use Illuminate\Database\Eloquent\Collection;
use App\Practitioner;
use App\Question;


class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:web')->except(['viewProfile']);

       // $this->isVerified  = auth()->user()->verified;

    }
    public function dashboard()
    {
        return view('user.dashboard');
    }


    public function profile()
    {

        if(auth()->user()->userType == 1){
            $disciplines = auth()->user()->practitioner->educations;
            $experiences = auth()->user()->practitioner->experiences;
            $specialties = \App\Specialty::all();
            return view('user.profile', compact('disciplines','experiences','specialties'));
        }else if(auth()->user()->userType == 2){
            return view('user.facility.profile', compact('disciplines','experiences'));
        }

    }

    public function timeline()
    {

        $products = collect(Product::inRandomOrder()->get());
        //$items = $items->push($products);
        $randomJobs = (auth()->user()->userType == 1)?\App\RandomJobs::inRandomOrder()->get():auth()->user()->facility->jobAdverts;
        ///get random jobs
        $rj = collect($randomJobs);

       
       
        //get questions asked
        $questions = collect(Question::inRandomOrder()->get());

        //get jobs from followed facilities
        $followings = auth()->user()->followings->pluck('followable_id');
       $facilities = \App\Facility::whereIn('user_id', $followings)->pluck('id');
        $ad = collect(JobAdvert::whereIn('facility_id', $facilities)->get());
        $second_collection = $products->merge($ad)->sortByDesc('created_at');
        $third = $second_collection->merge($rj)->sortByDesc('created_at');
         //jobs matching your profile
         if(isset(auth()->user()->practitioner->specialty_id)){
            $specialty_jobs = (auth()->user()->userType == 1)?JobAdvert::where('specialty_id', auth()->user()->practitioner->specialty_id)->inRandomOrder()->get():auth()->user()->facility->jobAdverts;
            $sj = collect($specialty_jobs);
            $fourth = $third->merge($sj)->sortByDesc('created_at');
            $items = $fourth->merge($questions)->sortByDesc('created_at')->shuffle();
        }else{
            $items = $third->merge($questions)->sortByDesc('created_at')->shuffle();
        }
        
        


        //posts from leaders
        //jobs from followed companies
        //your recent posts
        //sponsored sales

        //$items = $randomJobs;
        //shuffle($items);

        // $items = $items->forPage(1,2);

        return view('user.timeline', compact('items'));
    }
    public function viewProfile($id)
    {
        $user = \App\User::findByUserName(request('name'));


        if($user){
            if($user->userType == 1){

                $doctors = Practitioner::where('specialty_id',$user->practitioner->specialty_id)
                                                // ->where('id', '!=', $user->practitioner->id)
                                                ->get();
                return view('user.profile-view',compact('user','doctors'));
            }else if($user->userType == 2){
                
                
                return view('user.facility.profile-view',compact('user'));
            }
        }
        // $user = \App\User::find($id);
        // $disciplines = $user->educations;
        // $experiences = $user->experiences;
        // foreach($experiences as $experience){
        //     if($experience->to == 'P'){
        //         $current = $experience;
        //         break;
        //     }
        // }

        // return view('user.profile-view', compact('disciplines','experiences','user', 'current'));

    }

    public function logout()
    {
        auth()->logout();
        return redirect(route('login'));
    }

    public function editProfile(Request $request)
    {
        $user = \App\User::find(auth()->id());
        $username = \App\User::where('username', request('username'))->where('id', '!=', auth()->id())->first();
        if($username){
            return array('status'=> 99, "message"=> 'Username already exists');
        }

        if( $user->edit())
            return array('status'=> 0, "message"=> 'success');
        else
            return array('status'=> 99, "message"=> 'error');
    }
    public function editSocial(Request $request)
    {
        $user = \App\User::find(auth()->id());

        if( $user->editSocial())
            return array('status'=> 0, "message"=> 'success');
        else
            return array('status'=> 99, "message"=> 'error');
    }

    public function resendMail()
    {
        if(!isVerified()){
           
            if(sendVerificationEmail(auth()->user()))
                return back()->with('success', 'Mail sent successsfully');
            else
                return back()->with('error', 'Mail could not be sent. Please try again.');
        }else{
            return redirect()->route('user.profile');
        }

    }

    public function changePassword(Request $request)
    {
        $rule = 'required|string';
        if(!empty(auth()->user()->provider_id)){
            $rule = '';
        }
        $this->validate($request,[
            'current' => $rule,
            'password' => 'required|confirmed',
        ]);
        if(!empty(auth()->user()->provider_id)){
            $user = \App\User::find(auth()->id());
            $user->password = Hash::make(request('password'));
            $user->save();
            session()->flash('success', 'You created a new password');

            auth()->logout();
            return back();
        }
        $old = request('current');
        $user = \App\User::find(auth()->id());

        if(Hash::check($old,$user->password)){
            $user->password = Hash::make(request('password'));
            $user->save();
            session()->flash('success', 'You changed your password');
            auth()->logout();
            return back();
        }else{
            return back()->with('error', 'Your password is incorrect');
        }
        //if(request('current') == )
        //auth()->user->changePassword();
    }


    public function activate()
    {
        if(!checkProfileAndStatus()){
            return redirect(route('user.timeline', getFullNameWithSlug(auth()->user())));
        }
        return view('user.activate');
    }
}
