<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;
use App\Answer;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(is_numeric(request('id'))){
            $x = new Like;
            //check if user has liked this answer before
            $has_liked = Like::where('user_id', auth()->id())->where('answer_id', request('id'))->first();
            if(!$has_liked){
                $like = $x->like();
                if($like){
                    $num_of_likes = count($like->answer->likes);
                    return array('status' => 0, 'message' => 'success', 'likes'=> $num_of_likes);
                }
            }else{
                $has_liked->delete();
                $answer = Answer::find(request('id'));
                $num_of_likes = count($answer->likes);
                return array('status' => 0, 'message' => 'success', 'likes'=> $num_of_likes);
            }
            
            
        }
        return array('status' => 99, 'message' => 'An expected error occurred');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
