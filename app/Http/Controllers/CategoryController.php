<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        return view('admin.jobs.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'categories' => 'required',
        ]);
        $categories = explode(',', request('categories'));
        $num = count($categories);
        $exists = 0;
        $categories = array_map('trim', $categories);
        foreach($categories as $cate){
            $catAlreadyExists = Category::where('name', $cate)->first();
            if(!$catAlreadyExists){
                $Category = new Category;
                $Category->create($cate);
            }else{
                $exists++;
            }
           
        }

        if($exists == $num){
            return back()->with('error', 'Those records already exists');   
        }else if($exists != 0){
            return back()->with('error', 'Some records were not added because they already exist.');   
        }else if($exists == 0)
            return back()->with('success', 'Created Specialties succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $this->validate($request,[
            'category' => 'required',
            'id' => 'required'
        ]);

        $category = Category::find(request('id'));
        if($category){
            if($category->edit(request('category')))
                return back()->with('success', 'Edited Successfully');
        }
        return back()->with('error', 'Couldnt edit. Try again');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $category = Category::find($id);
        if($category){
            if($category->delete())
                return back()->with('success', 'Deleted Successfully');
        }
        return back()->with('error', 'Couldnt delete. Try again');
    }
}
