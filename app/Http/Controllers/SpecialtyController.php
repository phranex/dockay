<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialty;

class SpecialtyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Specialty::all();
        return view('admin.specialties.index', compact('categories'));

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'specialties' => 'required',
        ]);
        $specialties = explode(',', request('specialties'));
        $num = count($specialties);
        $exists = 0;
        $specialties = array_map('trim', $specialties);
        foreach($specialties as $special){
            $catAlreadyExists = Specialty::where('name', $special)->first();
            if(!$catAlreadyExists){
                $specialty = new Specialty;
                $specialty->create($special);
            }else{
                $exists++;
            }
           
        }
        if($exists == $num){
            return back()->with('error', 'Those records already exists');   
        }else if($exists != 0){
            return back()->with('error', 'Some records were not added because they already exist.');   
        }else if($exists == 0)
            return back()->with('success', 'Created Specialties succesfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[
            'specialty' => 'required',
            'id' => 'required'
        ]);

        $specialty = Specialty::find(request('id'));
        if($specialty){
            if($specialty->edit(request('specialty')))
                return back()->with('success', 'Edited Successfully');
        }
        return back()->with('error', 'Couldnt edit. Try again');
    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $specialty = Specialty::find($id);
        if($specialty){
            if($specialty->delete())
                return back()->with('success', 'Deleted Successfully');
        }
        return back()->with('error', 'Couldnt delete. Try again');
    }
}
