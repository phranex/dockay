<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;

class JobsController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth', ['only' => 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = \App\Specialty::all();
        $jobs = \App\JobAdvert::where('isActive', 1)->take(10)->latest()->get();
        $facilities = \App\Facility::take(10)->latest()->get();
        return view('jobs.index', compact('categories','jobs', 'facilities'));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listings()
    {
        $categories = \App\Category::all();
        $specialties = \App\Specialty::all();
        $establishments = \App\Establishment::all();
        $page_size = !empty(request('page_size'))?request('page_size'):10;
        $count = 0;
        $items = \App\RandomJobs::where(function($query) use ($count){
            $specialty = !empty(request('specialty'))?request('specialty'):null;
                $category = !empty(request('category'))?request('category'):null;
                $establishment = !empty(request('establishment'))?request('establishment'):null;
                $q = !empty(request('query'))?request('query'):null;

                $gender = !empty(request('gender'))?request('gender'):null;

                if(isset($q)){
                    $s = \App\Specialty::where('name', 'LIKE', "%$q%")->first();
                    if(isset($s)){
                        $query->where('specialty_id', $s->id);
                        $count++;
                    }


                }


                if(isset($specialty)){
                    $query->where('specialty_id', $specialty);
                    $count++;
                }
                if(isset($category)){
                    $query->where('category_id', $category);
                    $count++;
                }
                if(isset($gender)){
                    $query->where('gender', $gender);
                    $count++;
                }
                if(isset($establishment)){
                    $query->where('establishment_id', $establishment);
                    $count++;
                }


        })->inRandomOrder()->paginate($page_size);
        $jobs = \App\JobAdvert::where(function($query) use ($count) {

                $specialty = !empty(request('specialty'))?request('specialty'):null;
                $category = !empty(request('category'))?request('category'):null;
                $establishment = !empty(request('establishment'))?request('establishment'):null;
                $q = !empty(request('query'))?request('query'):null;
                $title = !empty(request('title'))?request('title'):null;
                $location = !empty(request('location'))?request('location'):null;
                $gender = !empty(request('gender'))?request('gender'):null;

                if(isset($q)){
                    $s = \App\Specialty::where('name', 'LIKE', "%$q%")->first();
                    if(isset($s)){
                        $query->where('specialty_id', $s->id);
                        $count++;
                    }


                }

                if(isset($title)){

                    $query->where('title','LIKE', "%$%");
                    $count++;
                }
                if(isset($location)){

                    $query->where('state','LIKE', "%$location%")->orWhere('country','LIKE', "%$location%");
                    $count++;
                }
                if(isset($specialty)){
                    $query->where('specialty_id', $specialty);
                    $count++;
                }
                if(isset($category)){
                    $query->where('category_id', $category);
                    $count++;
                }
                if(isset($gender)){
                    $query->where('gender', $gender);
                    $count++;
                }
                if(isset($establishment)){
                    $query->where('establishment_id', $establishment);
                    $count++;
                }
                $query->where('isActive', 1);


            })->inRandomOrder()->paginate($page_size);

            // if(!empty(request('specialty')) || !empty(request('establishment')) || !empty(request('category')) || !empty(request('query'))){
            //     $specialty = !empty(request('specialty'))?"specialty=".request('specialty').'&':'';
            //     $category = !empty(request('category'))?"category=".request('category').'&':'';
            //     $establishment = !empty(request('establishment'))?"establishment=".request('establishment').'&':'';
            //     $q = !empty(request('query'))?"query=".request('query').'&':'';
            //     $string = $specialty.$category.$establishment.$q;
            //     $jobs->withPath("all?$string");
            // }
        return view('jobs.listings', compact('categories','items','jobs', 'specialties', 'establishments', 'count'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //return view('jobs.show',compact('job', 'relatedJobs', 'user_has_applied','num_of_applications', 'has_subscribed'));
        $job = \App\JobAdvert::find($id);
        if($job){
            //related jobs are jobs having specialty, get 5
            $relatedJobs = \App\JobAdvert::where('specialty_id',$job->specialty_id)->where('id', '!=', $job->id)->where('isActive', 1)->take(5)->get();
            //if if viewer has applied for this job
            if(auth()->check() ){
                if(auth()->user()->userType == 1){
                    
                    $user_has_applied = count(Application::where('user_id', auth()->user()->practitioner->id)->where('job_advert_id',$job->id)->get());
                    $num_of_applications = count($job->applications);

                    //if viewer has subscribed to this jon
                    $has_subscribed = count(\App\Subscription::where('user_id', auth()->id())
                                         ->where('specialty_id',$job->specialty_id)
                                         ->where('category_id',$job->category_id)
                                         ->get());
                }

            }

            return view('jobs.show',compact('job', 'relatedJobs', 'user_has_applied','num_of_applications', 'has_subscribed'));
        }else
            return redirect(route('job.listings'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notify(Request $request)
    {
        if(!empty(request('cancel'))){
            session(['emailSaved' => 1]);
            return "true";
        }
        $this->validate($request,[
            'email' => 'required|email',
            
           
        ]);
        $email = new \App\UserEmail;
        
        $email = $email->create(request('type'));
        if($email){
            session(['emailSaved' => request('email')]);
            return back()->with('success', request('email')." will receive job notifications when they are posted");
        }

        return back()->with('error', 'An unknoen error occurred');
    }
}
