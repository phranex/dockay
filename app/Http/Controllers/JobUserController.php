<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JobUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    public function applications()
    {
        //
        return view('user.jobs.applications');
    }

    public function create()
    {
        //
        $categories = \App\Category::all();
        $specialties = \App\Specialty::all();
        $establishments = \App\Establishment::all();
        return view('user.jobs.create', compact('categories', 'specialties', 'establishments'));
    }

    //user dashboard
    public function dashboard()
    {
        return view('user.jobs.dashboard');
    }

    public function profile()
    {
        $educations = auth()->user()->educations;
        $experiences = auth()->user()->experiences;
        $current = null;
        foreach($experiences as $experience){
            if($experience->to == 'P'){
                $current = $experience;
                break;
            }
        }
        return view('user.jobs.profile', compact('educations', 'experiences', 'current'));
    }

}
