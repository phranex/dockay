<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get job details
        $job = \App\JobAdvert::find(request('job'));
        $specialty = $job->specialty_id;
        $category = $job->category_id;
        //check if auth user has subscribed to this kind of job
        $has_subscribed = count(\App\Subscription::where('user_id', auth()->user()->practitioner->id)
                                        ->where('specialty_id',$specialty)
                                        ->where('category_id',$category)
                                        ->get());
        if(!$has_subscribed){
            $subscription = new \App\Subscription;
            $subscribed = $subscription->create($specialty,$category);
            if($subscribed){
                return back()->with('success', 'You will now receive notifications for this kind of jobs');
            }else{
                return back()->with('error', 'An error occurred. Please try again');
            }
        }else{
            return back()->with('error', 'You already subscribed to receive notifications for this kind of jobs');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
