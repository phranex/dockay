<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'answer' => 'required'
        ]);
        $answer = new Answer;
        if($answer->create())
            return back()->with('success', 'You answer was submitted!!');
        else
            return back()->with('error', 'An unexpected error occured');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $answer = Answer::find(request('id'));
        if($answer){
            $isAnonymous = ($answer->question->user_id == $answer->user_id && $answer->question->isAnonymous)?true:false;
            return array('status' => 0, 'isAnonymous' => $isAnonymous, 'data' => $answer->answer, 'user' => $answer->user->username);
        }

        return displayErrorMessage();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
       
        $this->validate($request, [
            'answer2' => 'required',
            'id2' => 'required'
        ]);
        $answer = Answer::find(request('id2'));
        if($answer){
            $answer->edit();
            return back()->with('success', 'Edited succesfully');
        }else{
            return back()->with('error', 'An uncexpected error occurred');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        $id = request('id');
        $answer = Answer::find($id);
        if($answer){
            $answer->delete();
            return back()->with('success', 'Answer was deleted successfully');
        }
    }
}
