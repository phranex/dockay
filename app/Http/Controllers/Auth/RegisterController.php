<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use App\Mail\WelcomeMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $health = 1;
    protected $facility = 2;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['verifyEmail']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $user = $this->checkUser(request('userType'));
        if($user == $this->health){
            return $this->validateHealthProfessional($data);
        }else{
            return $this->validateFacility($data);
        }

    }

    public function showRegistrationForm()
    {

        $establishments = \App\Establishment::all();
        $bg = 'set';
        return view('auth.register', compact('establishments','bg'));
    }

    //checks user type
    public function checkUser($user)
    {
        return strtolower($user) == 'health'?$this->health:$this->facility;
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = $this->checkUser(request('userType'));
        if($user == $this->health){
           return $this->createHealthProfessional($data, $this->health);
        }else{
           return  $this->createFacility($data);
        }

    }

    public function createHealthProfessional($data,$type)
    {
        $user = $this->createUser($data, $type);
        $practioner = new \App\Practitioner;
        $practioner->create($user->id);
        $this->sendWelcomeEmail($user);
        return $user;

    }

    public function createUser($data, $type)
    {
        $password = isset($data['password'])?$data['password']:$data['facility_password'];
        $username = isset($data['username'])?$data['username']:$data['facility_username'];
        $email= isset($data['email'])?$data['email']:$data['facility_email'];
        $user =  User::create([
            'username' => str_replace(' ', '_', strtolower($username)),
            'email' => $email,
            'verifyToken' => \Illuminate\Support\Str::random(40),
            'verified' => 0,
            'password' => Hash::make($password),
            'userType' => $type
        ]);
        // $u = array(
        //     'firstName' => $data['firstName'],
        //     'lastname' => $data['lastName'],
        //     'email' => $data['email'],
        //     'verifyToken' => $user['verifyToken']
        // );
        $profileImage = new \App\ProfileImage;
        $profileImage->create($user->id,null);
        return $user;
    }
    public function createFacility($data)
    {
       $user = $this->createUser($data, $this->facility);
       $facility = new \App\Facility;
       $facility = $facility->create($user->id);

        $facilityProfile = new \App\ProfileImage;
        $facilityProfile->createFacilityProfile($facility->id,null);
        $this->sendWelcomeEmail($user);
       return $user;
    }

     //valdates health profesional
     public function validateHealthProfessional($data)
     {
         return Validator::make($data, [
             'firstName' => 'required|string|max:255',
             'lastName' => 'required|string|max:255',
             'username' => 'required|string|max:255|unique:users',
             'email' => 'required|string|email|max:255|unique:users',
             'password' => 'required|string|min:6|confirmed',

         ]);
     }

     //validates facility
     public function validateFacility($data)
     {
         return Validator::make($data, [

            'facility_name' => 'required|string|max:255',
            'facility_username' => 'required|string|max:255|unique:users,username',
            'facility_type' => 'required|numeric',
            'facility_email' => 'required|string|email|max:255|unique:users,email',
            'facility_password' => 'required|string|min:6|confirmed',
         ]);
     }


     public function redirectTo(){
        if(auth()->check()){
                if(empty(request('p')))
                    return route('user.timeline',getFullNameWithSlug(auth()->user()));
                else
                    return base64_decode(request('p'));

        }
    }

    public function sendWelcomeEmail($user){
        $when = \Carbon\Carbon::now()->addSeconds(10);
        try{
            Mail::to($user)->bcc(env('ADMIN'))->queue(new WelcomeMail($user));
            \Log::info('mail sent succesfully');
            
        }
        catch(\Swift_TransportException $e){
            \Log::info('couldnt send mail');
            \Log::info($e);
        }
    }


    public function verifyEmail($email, $token){
        $email = base64_decode($email);
         $user = \App\User::where(['email' => $email, 'verifyToken' => $token])->first();
        if($user){
            $status = $user->update([
                'verified' => '1',
                'verifyToken'=> null
            ]);
        }else{
            $status = 0;
        }

        return view('user.verifyEmail', compact('status'));
    }

}
