<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use App\Practitioner;
use Auth;

class SocialController extends Controller
{
    //
    protected $redirectTo = '/user/dashboard';

    public function redirectToProvider($provider)
    {
        if(!empty(request('p'))){
            session(['ur' => request('p')]);
        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try{
            if(!empty(request('error_description'))){
                $des = request('error_description');
                return redirect(route('register', "r=$des"));
            } 
            if($provider == 'twitter')  
                $user = dd(Socialite::driver($provider)->user());
            else    
                $user = Socialite::driver($provider)->stateless()->user();    
            $authUser = $this->findOrCreateUser($user, $provider);
           
            $userType = $authUser->userType;
            Auth::login($authUser, true);
            if(session()->has('ur')){
                $url = session('ur');
                session()->forget('ur');
                return redirect(base64_decode($url))->with('success', 'Login was successful');
            }
            if($userType == 3)
                return redirect(route('forumHomePage'));
            return redirect(route('user.timeline',getFullNameWithSlug(auth()->user())));
        }
        catch(Exception $e){
            return redirect(route('login'))->with('error', "Unexpected error occurred while connecting to $provider");
        }
        

        // $user->token;
    }

     /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $user = $user->user;
        $user_type = 3;
        $user_id = ($user['id'])?$user['id']:$user->id;
        $authUser = User::where('provider_id', $user_id)->where('provider', $provider)->first();
        if($provider == 'facebook'){  
                
            $authEmail = User::where('email', $user['email'])->first();
            $email = $user['email']; 
            $name = explode(' ', $user['name']); 
            $firstName = $name[1];
            $lastName = $name[0];
            $id = $user['id'];
                   
        }else if($provider == 'twitter'){
            $authEmail = User::where('email', $user->email)->first(); 
            $email = $user->email;
            $name = explode(' ', $user->user['name']); 
            $firstName = $name[1];
            $lastName = $name[0];
            $id = $user->id;
            
            
        }else if($provider == 'google'){
           //dd($user);
            $authEmail = User::where('email', $user['emails'][0]['value'])->first();
            $email = $user['emails'][0]['value']; 
            $firstName =  $user['name']['givenName'];
            $lastName = $user['name']['familyName'];
            $id = $user['id'];
        }else{            
            $authEmail = User::where('email', $user['emailAddress'])->first();   
            $email = $user['emailAddress'];
            $firstName = $user['firstName'];
            $lastName = $user['lastName'];
            $user_type = 1;
            $id = $user['id'];

        }

        if ($authUser || $authEmail) {
            return (isset($authUser))?$authUser:$authEmail;
        }

        $new_user = $this->createUser($email,$provider,$firstName,$lastName,$user_type,$id);
        $user_profile = array('firstName' => $firstName, 'lastName' => $lastName);
        //create user as a practitioner
        if($user_type == 1){
            $practitioner = new Practitioner;
            $practitioner->create($new_user->id, $user_profile);
        }

        //create default profile image
        $profileImage = new \App\ProfileImage;
        $profileImage->create($new_user->id,null);

        //send mail to user
        sendVerificationEmail($new_user);

        return $new_user;
    }

    public function createUser($email,$provider,$firstName, $lastName,$user_type,$id){
        

       return $new_user = User::create([
            // 'firstName'     => $name[0],
            // 'lastName'     => $name[1],
            'username'    => str_replace(' ', '_',strtolower($firstName.$lastName.rand(1,100))),
            'email'    => $email,
            'provider' => $provider,
            'userType' => $user_type,
            // 'linkedin' => @$user['publicProfileUrl'],
            'provider_id' => $id,
            'verifyToken' => \Illuminate\Support\Str::random(40),
            'verified' => 0,
        ]);
    }

    
}
