<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Question extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create()
    {
        # code...
        $this->title = str_replace('?', '', request('title'));
        $slug_string = substr(request('title'),0,20);
        $this->slug = str_slug($slug_string);
        $this->question_category_id = request('category');
        $this->user_id = auth()->id();
        $this->isAnonymous = (!empty(request('anonymous')))?1:0;

        //send this question to all users..
        //sendQuestionToUsers($this->title);
        $this->save();
        return $this;
    }
    public function edit()
    {
        # code...
        $this->title = request('title');
        $this->slug = str_slug(request('title'));
        
        return $this->save();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function category()
    {
        return $this->belongsTo('App\QuestionCategory', 'question_category_id');
    }

    static function findBySlug($x)
    {
        return Question::where('slug', $x)->first();
    }


    public function answers()
    {
        return $this->hasMany('App\Answer', 'question_id')->latest();
    }

    
    static function findQuery($q, $catId = null)
    {
        # code...
        
        //    return $results = Question::with(['Answers'])->where(function($query) use ($q){
        //         if(isset($catId))
        //             $query->where('question_category_id', $catId);
        //         $query->where('title', 'LIKE', "%$q%");
        //         $query->whereHas('Answers', function($qry) use ($q){
        //             $qry->where('answer', 'LIKE', "%$q%");
        //         })->get();
                
        //     });

        return Question::where(function($query) use ($q){
            if(isset($catId))  
                $query->where('question_category_id', $catId);
            $query->where('title', 'LIKE', "%$q%");
        });
     
    }


}
