<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 

    public function create($category)
    {
        $this->name = $category;
        $this->save();
         return true;
    }
    public function edit($category)
    {
        $this->name = $category;
        $this->save();
         return true;
    }

    public function advert()
    {
        return $this->hasMany('App\JobAdvert', 'category_id');
    }
}
