<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class QuestionCategory extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function create($cat, $path = null,  $cloud_id =null )
    {
        # code...
        $this->description = request('description');
        $this->path = $path;
        $this->cloud_id = $cloud_id;
        $this->name = $cat;
        $this->save();
    }
    public function edit($cat,$path=null,$cloud_id=null)
    {
        # code...
        $this->description = request('editDescription');
        if(isset($path))
            $this->path = $path;
        if(isset($cloud_id))
            $this->cloud_id = $cloud_id;
        $this->name = $cat;
        $this->save();
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'question_category_id');
    }
}
