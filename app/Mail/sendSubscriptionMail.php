<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendSubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $jobs;
    public $subject;
    public function __construct($jobs, $user, $subject)
    {
        //
        $this->user = $user;
        $this->jobs = $jobs;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        logger($this->jobs);
        $brand = title_case(config('app.name'));
        $subject = isset($this->subject)?$this->subject:'Available jobs on '.$brand;
        return $this->view('emails.subscriptions')->subject($subject);
    }
}
