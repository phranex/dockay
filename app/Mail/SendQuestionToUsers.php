<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendQuestionToUsers extends Mailable
{
    use Queueable, SerializesModels;
    public $question;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title)
    {
        //
        $this->question = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        logger($this->question);
        
        $name = config('app.name');
        return $this->view('emails.question')
            ->subject(substr($this->question->title, 0, 50));
    }
}
