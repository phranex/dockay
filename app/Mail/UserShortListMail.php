<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserShortListMail extends Mailable
{
    use Queueable, SerializesModels;
    public $job;
    public $subject;
    public $body;
    public $replyMail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job,$subject, $body,$replyTo)
    {
        //
        $this->job = $job;
        $this->subject = $subject;
        $this->body = $body;
        $this->replyMail = $replyTo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // $name = $this->application->practitioner->firstName;
        return $this->view('emails.shortlist')
        ->subject(title_case($this->subject))
        ->from(['address' => 'hr@dockay.ng','name' => "Dockay HR"])
        ->replyTo($this->replyMail);
    }
}
