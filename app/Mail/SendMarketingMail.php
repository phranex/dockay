<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMarketingMail extends Mailable
{
    use Queueable, SerializesModels;
    public $mess = array();
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mess, $subject)
    {
        //
        $this->mess = $mess;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('emails.marketing')
            ->subject(title_case($this->subject))
           ->from('pristine@dockay.ng', 'Dockay')
            ->replyTo(env('DOCKAY_CUSTOMER_CARE'));;
    }
}
