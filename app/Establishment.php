<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Establishment extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create($x)
    {
        $this->name = $x;
        $this->save();
         return true;
    }
    public function edit($x)
    {
        $this->name = $x;
        $this->save();
         return true;
    }

    public function advert()
    {
        return $this->hasMany('App\JobAdvert', 'establishment_id');
    }
}
