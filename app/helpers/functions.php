<?php


use App\Facility;
use App\Category;
use Illuminate\Support\Facades\Mail;
// use Mail;
use App\Mail\SendApplicationNotificationToEmployers;
use App\Mail\sendSubscriptionMail;
use App\Mail\SendMarketingMail;
use App\Mail\VerificationReminder;

function activateCloudinary(){
    \Cloudinary::config(array(
        'cloud_name'  => env('CLOUDINARY_CLOUD_NAME'),
        "api_key" => env('CLOUDINARY_API_KEY'),
        "api_secret" => env('CLOUDINARY_API_SECRET')
      ));
}

function checkProfileAndStatus(){
    if(auth()->user()->verified == 0 || empty(auth()->user()->phoneNumber) || empty(auth()->user()->state) ){
        return true;
    }
    return false;
}

function displayShareMessage($type){
    if($type == 'question')
        return "Hello dockay, your professional input is needed to help clarify this question";
    else if($type == 'sales'){
        return "Hello dear, here is something you might like";
    }
}


function convertToInternationalNumber($num){
    
        if($num[0] ==  '0'){
            return $num = substr_replace($num,'234',0,1);           
        }
    
    return $num;
}



    function getFullNameWithSlug($user){
        if(auth()->user()->userType == 1)
            return strtolower($user->practitioner->firstName."-".$user->practitioner->lastName);
        else if(auth()->user()->userType == 2)
        return strtolower(str_slug($user->facility->name));
        else
            return auth()->user()->username;
    }
    function getFullName($user){
        if($user){
            if($user->userType == 1)
            echo title_case($user->practitioner->firstName." ".$user->practitioner->lastName);
            else if($user->userType == 2)
            echo title_case($user->facility->name);
        }
        

    }

    function displayDefaultPhoto($user){
        if(isset($user)){
            if(isset($user->profileImage->path)){
                return $user->profileImage->path;
            }
            if($user->userType == 1){
    
                if(strtolower($user->practitioner->gender) == 'male')
                    return url()->to('/images/pp/male.png');
                else if(strtolower($user->practitioner->gender) == 'female')
                    return url()->to('/images/pp/female.jpg');
                else
                    return url()->to('/images/pp/nodp.png');
            }else if($user->userType == 2){
                return url()->to('/images/pp/facility.png');
            }
        }
       
    }

    function getFacilityUser($id){
        $user = Facility::find($id);
        if($user)
        return $user->name;
    }

    function getCategory($id){
        $category = Category::find($id);
        if($category)
        return $category->name;
    }
    function getUserFullName($user){
        if($user->userType == 1)
            echo title_case($user->practitioner->firstName." ".$user->practitioner->lastName);
        else if($user->userType == 2)
            echo title_case($user->facility->name);

    }

    function getProfileImage($user){

         return $user->profileImage->path;
    }
    function getCV($obj){
        if(env('APP_ENV') == 'local')
        return asset('storage/'.$obj->cv);
        else
         return $obj->cv;
    }
    function getUserCV($obj){
        if(env('APP_ENV') == 'local')
        return asset('storage/'.$obj->cv->path);
        else
        return $obj->cv->path;
    }

    function getDateFormat($string,$format){
        echo \Carbon\Carbon::parse($string)->format($format);
    }
    function when($string){
        echo \Carbon\Carbon::parse($string)->diffForHumans();
    }

    function responseMessage($mode){
        if($mode)
            return array('status'=> 0, "message"=> 'success');
        else
            return array('status'=> 99, "message"=> 'error');
    }

    function isVerified()
{
    if(auth()->user()->verified){
        if( auth()->user()->blocked == 0){
            if(session()->has('blocked')){
                session()->forget('blocked');

            }
            return true;
        }
        else session(['blocked' => 'Your account has been blocked due to suspicious activities. Please contact support to resolved this.']);
    }

    return false;
}

    function getFacilityLocation($item){
        echo $item->facility->user->state.", ".$item->facility->user->country;
    }
    function getUserLocation($item){
        return $item->state.", ".$item->country;
    }

    function displayErrorMessage(){
        return array('status' => 99, 'message' => 'An unexpected error occurred');
    }
    function displayMessage(){
        return back()->with('error' ,'An unexpected error occurred');
    }

    
    function displayClassForCat($name){
        if(strtolower($name) == 'part time')
            echo "ptchek";
        else if(strtolower($name) == 'full time')
            echo "ftchek";
        else if(strtolower($name) == 'internships')
            echo "ischek";
        else if(strtolower($name) == 'locum')
            echo "tpchek";
    }


    function sendVerificationEmail($user){
        $when = \Carbon\Carbon::now()->addSeconds(10);
        try{
            Mail::to($user)->send(new \App\Mail\WelcomeMail($user));
            logger('sent');
             // check for failures
            if (Mail::failures()) {
                return true;
                // return response showing failed emails
            }

            return true;
        }catch(\Swift_TransportException $e){
            logger($e);
            return false;
        }
           

      

    }
    function sendQuestionToUsers($question){
       
        try{
            $users = \App\User::all()->pluck('email')->toArray();
            Mail::bcc($users)->send(new \App\Mail\SendQuestionToUsers($question));
            logger('sent');
             // check for failures
            if (Mail::failures()) {
                //return true;
                // return response showing failed emails
            }

            return true;
        }catch(\Swift_TransportException $e){
            logger($e);
            return false;
        }
           

      

    }

    function format_number($n){
        if ($n < 1000) {
            // Anything less than a thousand
            $n_format = number_format($n);
        } else if ($n < 1000000) {
            // Anything less than a million
            $n_format = number_format($n / 1000) . 'K';
        }

        echo $n_format;
    }


    function sendMailToApplicants($job,$emails,$request){
        $subject = $request->subject;
        $body = $request->body;
        //$when = \Carbon\Carbon::now()->addSeconds();
        $replyTo = request('replyTo');
        try{
            if(count($emails)){
                for($i = 0; $i < count($emails); $i++){
                    logger($emails[$i]);
                    Mail::to($emails[$i])->queue(new \App\Mail\UserShortListMail($job,$subject, $body, $replyTo));
                }
            }
            

        }catch(\Swift_TransportException $e){
            logger($e);
        }
    }
    function sendApplicationNotificationToEmployers($email){
        //$subject = $request->subject;
        //$body = $request->body;
        //$when = \Carbon\Carbon::now()->addSeconds(10);
        //$replyTo = request('replyTo');
        try{
            Mail::to($email)->send(new \App\Mail\sendApplicationNotificationToEmployers());
            
        }catch(\Swift_TransportException $e){
            logger($e);

        }
    }

    function sendSubscriptionMail($jobs, $user, $subject = null, $email = null){
        try{
            if(isset($email))
                Mail::to($email)->send(new sendSubscriptionMail($jobs,$user, $subject));
            Mail::to($user->email)->send(new sendSubscriptionMail($jobs,$user, $subject));
            
        }catch(\Swift_TransportException $e){
            logger($e);

        }
    }
    function sendJobMail($jobs, $email){
        try{
            if(isset($email))
                Mail::to($email)->send(new sendSubscriptionMail($jobs, $user = null, $subject = null));
        
            
        }catch(\Swift_TransportException $e){
            logger($e);

        }
    }
    function sendActivationReminder($user){
        try{
            Mail::to($user->email)->send(new VerificationReminder($user));
            
        }catch(\Swift_TransportException $e){
            logger($e);

        }
    }
    function sendMarketingMail($email, $mess, $subject){
        try{
            Mail::to($email)->queue(new SendMarketingMail($mess, $subject));
            
        }catch(\Swift_TransportException $e){
            logger($e);

        }
    }
