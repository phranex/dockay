<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RandomJobs extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create()
    {
        $this->specialty_id = request('specialty');
        $this->category_id = request('category');
        $this->contact = request('contact');       
        $this->allow_whatsapp = (!empty(request('whatsapp')))?1:0;
        $this->establishment_id = request('establishment');
        $this->gender = request('gender');
        $this->description = request('description');
        $this->user_id = auth()->id();
        $this->save();
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }
    public function establishment()
    {
        return $this->belongsTo('App\Establishment');
    }
}
