<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hypefactors\Laravel\Follow\Traits\CanFollow;
use Hypefactors\Laravel\Follow\Contracts\CanFollowContract;
use Hypefactors\Laravel\Follow\Traits\CanBeFollowed;
use Hypefactors\Laravel\Follow\Contracts\CanBeFollowedContract;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable implements CanFollowContract,CanBeFollowedContract
{
    use Notifiable;
    use CanFollow, CanBeFollowed;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'username',
        'email',
        'userType',
        'password',
        'password',
        'verifyToken',
        'verified',
        'provider',
        'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function edit()
    {
        if(auth()->user()->userType == 1)
            $this->updatePractitioner();
        elseif(auth()->user()->userType == 2)
            $this->updateFacility();
        $this->phoneNumber = request('phoneNumber');
        $this->description = request('description');
        $this->username = request('username');
        $this->country = request('country');


        $this->state = request('state');
        return $this->save();

    }

    public function makeModerator()
    {
        # code...
        $this->is_moderator = ($this->is_moderator == 0) ? 1 : 0;
        $this->save();
    }
    public function updatePractitioner()
    {
        $this->practitioner->firstName = request('firstName');
        $this->practitioner->lastName = request('lastName');
        $this->practitioner->specialty_id = request('qualification');
        $this->practitioner->gender = request('gender');
        $this->practitioner->save();
    }
    public function updateFacility()
    {
        $this->facility->name = request('name');
        $this->facility->establishment_id = request('establishment');

        $this->facility->save();
    }
    public function editSocial()
    {
        $this->facebook = request('facebook');
        $this->twitter = request('twitter');
        $this->google_plus = request('googleplus');
        $this->linkedin = request('linkedin');
        return $this->save();

    }


    public function profileImage()
    {
        return $this->hasOne('App\ProfileImage', 'user_id');
    }
    public function products()
    {
        return $this->hasMany('App\Product', 'user_id');
    }

    public function applications()
    {
        return $this->hasMany('App\Application', 'user_id');
    }
    public function answers()
    {
        return $this->hasMany('App\Answer', 'user_id')->latest();
    }

    public function followers()
    {
        return $this->hasMany('App\Follower', 'user_id');
    }
    public function mediaObjects()
    {
        return $this->hasMany('App\MediaObject', 'user_id');
    }

    public function leaders(){
        return $this->hasMany('App\Follower', 'follower_id');
    }

    public function randomJobs(){
        return $this->hasMany('App\RandomJobs', 'user_id')->latest();
    }
    public function questions(){
        return $this->hasMany('App\Question', 'user_id')->latest();
    }
    public function facility()
    {
        return $this->hasOne('App\Facility', 'user_id');
    }

    public function practitioner()
    {
        return $this->hasOne('App\Practitioner', 'user_id');
    }

    public function hasLikedThisAnswer($id)
    {
        return $hasLiked = Like::where('user_id', auth()->id())->where('answer_id', $id)->first();
    }

    static function findByUserName($name)
    {
        # code...
        return User::where('username', $name)->first();
    }
}
