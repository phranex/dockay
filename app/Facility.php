<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Illuminate\Database\Eloquent\SoftDeletes;


class Facility extends Model
{
    //
    use CanFollow, CanBeFollowed;
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create($user)
    {
        $this->name = request('facility_name');
        $this->establishment_id = request('facility_type');
        $this->user_id = $user;
        $this->save();
        return $this;
    }

    public function establishment()
    {
        return $this->belongsTo('App\Establishment', 'establishment_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function profileImage()
    {
        return $this->hasOne('App\ProfileImage', 'facility_id');
    }
    public function jobAdverts()
    {
        return $this->hasMany('App\JobAdvert', 'facility_id');
    }

    public function edit()
    {
        $this->name = request('name');
        $this->establishment_id = request('establishment_id');
        $this->description = request('description');
        $this->country = request('country');
        $this->state = request('state');
        $this->city = request('city');
        $this->email = request('email');
        return $this->save();

    }
}
