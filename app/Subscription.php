<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Subscription extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create($specialty, $category)
    {
        $this->specialty_id = $specialty;
        $this->category_id = $category;
        $this->user_id = auth()->id();
        return $this->save();
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function specialty()
    {
        return $this->belongsTo('App\Specialty', 'specialty_id');
    }
}
