<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CV extends Model
{
    //
    protected $table = 'c_vs';
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create($user, $path,$id)
    {
        $this->practitioner_id = $user;
        $this->path = $path;
        $this->cloud_id = $id;
        $this->save();
    }

    public function edit($path,$id)
    {
        $this->path = $path;
        $this->path = $id;
        $this->save();
    }


}
