<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProfileImage extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create($user,$path)
    {
        $this->user_id = $user;
        $this->cloud_id = '';
        $this->path = $path;
        return $this->save();
    }

    public function createFacilityProfile($facility, $path)
    {
        $this->facility_id = $facility;
        $this->path = $path;
        $this->cloud_id = '';
        return $this->save();
    }
    public function editFacilityProfile($path)
    {

        $this->path = $path;
        return $this->save();
    }

    public function edit($path,$cloud)
    {
        $this->path = $path;
        $this->cloud_id = $cloud;
        return $this->save();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function facility()
    {
        return $this->belongsTo('App\Facility', 'facility_id');
    }
}
