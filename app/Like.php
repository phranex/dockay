<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    //

    function like(){
        $this->answer_id = request('id');
        $this->user_id = auth()->id();
        $this->save();
        return $this;
    }
    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }
}
