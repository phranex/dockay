<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Specialty extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create($specialty)
    {
       $this->name = $specialty;
       $this->save();
        return true;
    }
    public function edit($specialty)
    {
       $this->name = $specialty;
       $this->save();
        return true;
    }

    public function adverts()
    {
        return $this->hasMany('App\JobAdvert', 'specialty_id');
    }
    public function items()
    {
        return $this->hasMany('App\Item', 'specialty_id');
    }
    public function equipments()
    {
        return $this->hasMany('App\equipment', 'specialty_id');
    }


}
