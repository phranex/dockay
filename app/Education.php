<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Education extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];  
    
    public function create()
    {
        $this->discipline = request('discipline');
        $this->institution = request('institution');
        $this->from = request('from');
        $this->user_id = auth()->user()->practitioner->id;
        $this->to = request('to');
        $this->description = request('description');
        return $this->save();
    }
    public function edit()
    {
        $this->discipline = request('discipline');
        $this->institution = request('institution');
        $this->from = request('from');
        $this->to = request('to');
        $this->description = request('description');
        return $this->save();
    }



}
