<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class WorkExperience extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public function create()
    {
        $this->user_id = auth()->user()->practitioner->id;
        $this->title   = request('title');
        $this->company = request('company');
        $this->from = request('from');
        $this->to = request('to');
        $this->description = request('description');
        return $this->save();
    }

    public function edit()
    {
        $this->title   = request('title');
        $this->company = request('company');
        $this->from = request('from');
        $this->to = request('to');
        $this->description = request('description');
        return $this->save();
    }
}
