<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Mail\UserShortListMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApplicationMail;


class Application extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function create($path, $c = null)
    {
        $this->user_id = auth()->user()->practitioner->id;
        $this->job_advert_id = request('job');
        $this->cv = $path;
        $this->cloud_id = $c;
        $this->save();
        $this->unreadJob();/// set this job as unread so employer can recieve mails
        $this->sendMail('application',$this);
        return $this;
    }

    public function unreadJob()
    {
        $this->jobAdvert->hasBeenRead = 0;
        $this->jobAdvert->save();
    }
    public function mailSent()
    {
        $this->mail_sent = 1;
        $this->save();
    }

    public function shortlist($application)
    {
        $this->shortlist_flag = 1;
        $this->save();
        
        //send a mail to all shortlisted applicants
        $this->sendMail('shortlist',$application);
    }

    
    public function sendMail($type,$application)
    {
        
        //$when = \Carbon\Carbon::now()->addSeconds(10);
        try{
            Mail::to($application->practitioner->user->email)->queue(new ApplicationMail($application));
            // if($type == 'shortlist'){
            //     Mail::to($application->practitioner->user->email)->queue($when,new UserShortListMail($application));
            // }else if($type == 'application'){
            //     Mail::to($application->practitioner->user->email)->send($when,new ApplicationMail($application));

            // }
            
        }
        catch(Swift_TransportException $e){
            logger($e);
            return false;
            //sesion(['error'=> 'Unexpected error occurred!. Try again later']);
        }
    }



    public function practitioner()
    {
        return $this->belongsTo('App\Practitioner', 'user_id');
    }

    public function jobAdvert()
    {
        return $this->belongsTo('App\JobAdvert');
    }
}
