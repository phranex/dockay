<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at']; 
    public function create()
    {
        $this->user_id = auth()->id();
        $this->answer_id = request('id3');
        $this->comment = request('comment');
        return $this->save();
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }
    public function question()
    {
        return $this->answer->question;
    }

    

    public function edit()
    {
        
       
    $this->comment = request('comment_edit');
        return $this->save();
    }


}
