<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Laravel\Scout\Searchable;


class Product extends Model
{
    //
    use SoftDeletes;
    // use Searchable;

    protected $dates = ['deleted_at'];

    public function create()
    {
        $this->name = request('name');
        $this->brand = request('brand');
        $this->title = request('title');
        $this->quantity = request('quantity');
        $this->user_id = auth()->id();
        $this->description = request('description');
        $this->negotiable = !empty(request('negotiable'))?request('negotiable'):'N';
        $this->nature = request('nature');
        $this->price = request('price');
        $this->save();
        return $this;


    }
    public function edit()
    {
        $this->name = request('name');
        $this->brand = request('brand');
        $this->title = request('title');
        $this->quantity = request('quantity');
        $this->user_id = auth()->id();
        $this->description = request('description');
        $this->negotiable = !empty(request('negotiable'))?request('negotiable'):'N';
        $this->nature = request('nature');
        $this->price = request('price');
        //$this->deleteImages();
        $this->save();
        return $this;


    }
    public function images()
    {
        return $this->hasMany('App\Image', 'product_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function remove(){
        $this->deleteImages();
        $this->delete();
    }

    public function deleteImages()
    {
        Image::where("product_id", $this->id)->delete();
    }
}
