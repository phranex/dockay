<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Answer extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function create()
    {
        $this->question_id = request('id');
        $this->user_id = auth()->id();
        $this->answer = request('answer');
        return $this->save();
    }
    public function edit()
    {
        
       
        $this->answer = request('answer2');
        return $this->save();
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likes()
    {
        return $this->hasMany('App\Like', 'answer_id');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment', 'answer_id');
    }

    
}



