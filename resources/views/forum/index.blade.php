@extends('layouts.master')

@push('title')
    Dockay | Health Information
@endpush
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }
    body{
        background: #eaeaea
    }

    .dropdown{
        display: inline-block;
    }
    
    .app-search{
        padding-top:unset;
    }
    .owl-item{
        {{--  width:auto !important;  --}}
        height:50px !important;
    }

    .fa-star{
        color: gold;
    }

    

    .cat-item{
       
        margin:5px;
        {{--  background: #64bdf9;  --}}
        border-radius:20px;
        display:block;
        font-size:11px;
        text-align:center;
    }

    .others a{
        font-size:11px !important;
    }

    .cat-item-active{
        background: #64bdf9;
        color:white !important;
            padding: 0px 20px;
    }

    .cat-xl-item{
        padding-left:5px;

    }
    .cat-xl-item:hover, .cat-active{
        padding-left:25px;
        text-decoration:undeline;
        color: #64bdf9;
        transition:.2s;
        cursor:pointer;
        font-weight:bold;
       
    }

    .cat-active a{
         color: grey !important;

    }

    </style>

@endpush

@push('title')
   
@endpush
@section('content')
<div class="container-fluid">
        <div class="row">
               
            <div class="col-md-12   dockay-card  m-auto">
                <div class="col-12 col-xl-10">
                            <h6 class='text-center d-block d-xl-none'>Categories</h6>
                            @if(isset($categories))
                                @if(count($categories))
                                    <div class='categories owl-carousel m-auto owl-theme d-block d-xl-none' style='width:100%'>
                                        @foreach($categories as $category)
                                             <a href='{{ route('forumHomePage', str_replace('/', '_',$category->name)) }}'>
                                            <div class='content'>
                                                <img class='img-fluid' style='height:100%' src='@if(isset($category->path)) {{ $category->path }} @else data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///8jHyAAAAAgHB0JAAAdGBkbFhcXERMcFxgRCgwWEBL7+/sFAAANAwYgGx0PBwn19fXt7e29vLzGxcWioaErJyiYl5eysbHn5+esq6vd3d2Ih4c9Ojvw8PCSkZHU09NFQkMvKyxqaGmbmpplY2R/fX7X19c5Njd5eHhVU1NCP0BoZmfLyspOTExdW1yEg4P1jokDAAAXRElEQVR4nO1dh7aiyBZtDkmSIKgEc7qi1/D/fzeVEJBUIDfNcq+33nS3qYo6OdW/f2+88cYbb7zxxhtvvPHGG2/8SgTuNN6fbtv7+LIoe90JgigKAue719UTguUEwNaVgaLqFsB8GaWvLS7j+CTAA5vbzP8wfm6ttRh548NtPhEUUThet7OQnVawB00UMpBtuHrkA368AbD0QeZlUdEkMK/T1Y9upQQLN1bxYhVRHIoIsooWCso2DJagCgUMYH3ZHcHUxeJr5HUb4NP9RTTr3gDUksUOVQm08j3IYFfsLgF6YtvLT++MYLGUQKlfLN4RPlN8yhX7QscuD59fU2Hi//T2/jmzxsNA0GA98y+rlTdGnJd//xALH7Am8/X6ODHRH20ly5Yg/PAex1kqZOdEYaVkq8IySD8SjDfJK0MN4BiHl+AhPJ3I3e0VkFKiGMI6KPvl70E0h2SxIlrsFcnPaLRwnEWwcnfxHJ2HjF6CfV5khEP6PCw4zrxSaRKFe0gF1ADG37KbEuxAfpySMLsUtJhxma7Rae7y/3gD9AEFqcVx3dkYXgxm8vWw73vpXDBOwBagw7ZSfS1W+VNabHRMeadVqWmT/wF/njxCe/0DRkAw0emvK3AYcX9qISAOU22X8+1ewgb6udsqX8AHe7wi3FoIAmOCNiidWhyIz4SOdmu/xpewYs9WtXiPg+CKzh3iVr+0uEqUF6etPvYqVkDlIZxaWVYzxLlmuw0ixJTfIWp+a2+I2AnCstXHVmipegdqi8kpytfZ/nS6xeOi0O4BI3e5XU8UZTA57+9jT2AbbGlubGRhOOny8zfCizLyvwYKNoFurVijGcH0CKCpskicBvwTbINeu+8J0RHCR5cVuFbOBkTiW9/1d5DBtsLwlNoa/qIoaIf2C4gOGhR+fCjpfdmrszL3jqiJtcuvBjF8QKqltcv3cYJyd0SEa7vfL0dwlMr3h3/ChOu4xY+cZUFrJ5kQDsQxE5lpo0hZi16B19lxBYNkO1rqNOjo96iHg4znEy8zBpgLm021/GcmtoCN3uNeJv/dhysvjAeJOyq2lOVFJGoduXfzmfsxchwn+PDGB2RO73WwFPogJyHXl40tQWmpKSJsesvwufp3RguBbUIw3i2xV1taD88IEpsQZiWGWRTegJIQCDzUclIEs51wIL8vnZGin5poM1kX6mPNhA9sW33lEzYy+5Iq2nL8GwmjiXBqJj8kkKGdaMC/D3f0BwNt1Z7lXwwTlfXCKR6oEoJaIhwtTYuwSBM7IjYU22l7/PvUGHVBGKrPL0cCFfLdeXHFrMEmCjSmWJ+I9Q8C8Q60ZMMI/b5F1edBF+yi3W2sdb4VVuFMaJTHMFvsofGskRck6Pc2v79XBfFI/7iWBShzsa9si530oi8QSanxUbmPmQJqbRwkSltpQ6Jc2DdOqlj4TAh10ME79ifUOxJVTusP6xVRr3sz2mEJpdW+X76yP0+GApS62Q51AVq7jpdNogf5nQfMtc/iLodQErS6159xHaTK5VpBpfSk8TpbxRudz0eEUG0h/HbIvIOaQ0TyUG0h2A3I8Fe5pCHwyRZbyTAXWIAJGRPzNo8GM0uNVPtA2mKe/MVoJH4kScVNuiZEjRVv3BNWrJcBOdyTA5Rh3c7929mCXuMbOfhQkGcR+PHcRu7mdVorAbFy+Uz+Qg60QlQviMSQ17zLPJmJTTtv6/0hThycal5H2kfyx3OQdHGIvQUL6tjSN7PK5a6hFVUQ1NhscYjGnCUhpEl7LToCUasLTS/Rd1tWEsXGMGvCvFgwpcrFQZQlTyosQyJPOTlxrjMC7WQIhWYt30aQ+ngm+h9al76u/rK86MUCRZmU0zUVNlzi9ERPUDt+TdiOGEk63MIPxI4LH0f3pUpCxVSa5eoD2oZslpOiiA/R5jiVO+VB+Gx+aye4OJY4SwnzpNfEQC9I0uRofgtYOpSKsil2kznMeo8pz1bGYysg83K4ybDeZiioVe4doumnwCOJC2tlGY8RJdMmynMs8Ys3iFWioGfE7QWqrWaDKZcMxtixL40/XXGsxd4V/j2PLZEy2leRKMESLzvDeogzrark59XW9ad/io4mCZsUuDHELurg+vzPeVBvUD62W3JbENZL6QxJQbNKQIz2pyLZ7agr+hxVptYp1P/2Wuai5RdhYGWfketX2LSLvhFXdCg+/zNRifWLp2LGejU41wisFOVN+vf2vusYp6+eSXuPnVmz1kohvDqssm5LEEyvotYho4fVs/ZSfGymPXz/B7C9VO9+fkA7bzDYgz0Qh12I+i61z1nlYJRIYB+LIL3OOYtVPp3JMGa5jE6JpLPSNbLCcFMKWS/CZEqNVeyQIzT5Ytf//n3itw8Ayi2MJiALXVAaJHstplpBxxBNUOfZhCT9UuehZ3GyiOJ1Rx0zeNh6eyWn4hfjIYTLUge7CCJn6tzXLLbocSh6y+RoDjhOqHT/OHpCz2ul2lw9V6keZtfxMRVO5Krzlyo/FzUuOwcQ0z0LlQu1qRW9QvT5Er+cwU9Dmb+YYUbOQEHg86NkhwYrDamQ7hHxrzjDtZjCzJZZwAJIzKaz+VRCpWjXVLqLVomU/qD7rwhIPiF6UUowbNVqk7sRJZIGUf6NEWpR3LAsqNhgtzIcdEGpCzfxL7LSM2xERX5gTLdYMOUftT98jxQ5kS1Ck5WIoFa218MFudQocpkLn6fTkdRqg15NaLYNkLwSh50/PZ2Xy4yQbPGpoGXC0rycrHXXO5RTlKB9upQLLP6dlfQ3loDj1d84ct9HqwciqD7Y+RlMsWeOa8eZ5s1+g9nHUuZyu1QbL4gHkUkAsRgtv3nhdXz07umQrZ8KSJXpVxTgE9smE6M70sgFf1YP2SJd2DAARQeQb7PQ9Tx3d8I1KtZrhTAVoE7SgxEpjarJoRiRF05n99k09KoeL1LUXdQ9DZGIioZbR0x7QOLqHdbPAdLJkZDHgv6uio3okXs4ol+XbE3XbAlAjEuV3lzsZmytwc61Dclw+6LS+6ucCUkdNCZGjfAKT/V/ol5ay4Xe1L64EGO1u+EHqOmqqtsA654LYFNkd0glqxo7B5BoeR4mI8mULH1Azbjhsw5BVC4WCne4EXjh8rDdHqYtizZb4Zih0hk5wkFM+5Zk9GQ3n7Nx6Ifj+wlIplt8LqjCevqLQ8avgpRxMTpjdb+k9M6C626V5YzVgWzcOub8pA9IC0B+J4j+Y3TmPgqLRdiU1ME6M5wHUYZZsbpqW7j17SC+PFvjZyLcpE0F20dHDZfHZzb/vTv8CGfxFqGNVCKxb5aEguQAazQ4TkoOJin1fuMOnaVCRa+qtjCEDUh9eVZ4KNY7ezi1rKec11Rtsbh2ifOXYZdpD2gRsyJRUcaGY4tusEF/42aV9JSb/Nat1fiFXBjNqcCwaFk5f1SI6AoWw9mqfF4TSfglYUbst8rV7/U7N4zkccF2q2hKMTZjvfIm01J4GSLF9ldDKoPCEMVM9ZVVyDxngLu2utRBPoNUcmpyh+QNKfaQGZWpWQu1DphhrSS3z+xSx1vu5xN5cv6cekzUBjvV5qB6DpBQBGw72K1UzrAsIvEyxOc8eSnuVpoNQ7Rtj31kxWoKbYFCltBku1xuJ0QyiG0boUoQcpQbV4HY1hJdKzFKOSMlpGKelsHgigBbSppMmL5RNY1QhGhOXmfCMbzwoO7Y1mQFCyS1P9w0fOIfey/mCzjF8fXh/SQNNNpjtwpYTdUeHKAb7BoJoiqQRhPpGXLGBVekQWWgqjQuJ1uwXnrRwllE7vKKnSI8ZmDfTjLshDJOW760QVawIBESp96+xPnBSEiKTkXVhHPeio3cMHTbCpgViNqxIOfiFzdI/SUWSSKNWfx9VuMjG48T+704d1hzyTDLfdflaJfVyrQBI1OyLaI62nzbaHW5RP3NiyGVuhrcfHr6xsf4jHlhYL6mboitRwvgSWBR6p6lfBkxIXxca6ptNkjpkNJaa/MihRBpSnM+xC7tnv7pAd4kGVkwZGNp1G6VD1lc0vIvQrGi9vpCX4C/BinpBxV1gG0zhd6hfhyDIaWWKfBZ3l+LINxuqAg7xj6H2LsgEbyufQd1gYkPR5wL9UtrLTmxCAJemY5FsPZc/h6sVqkIJB4i1RduRrD+VhRFN65E1zepIjXcrUpoYD2m+44y3EdKglt1InGhv1jo4gjK+v400eyKY0dJOvcSS8mEMVliFV2Zun0aL+35EEcC9JYWnEnI30YC6JYTFidMhpJ+GE/3ADqpsZCJ+SLSOUSk+InKl6BlOSLfqizB6mvg0ZKZigqcs4YcUaSibmkkWIib6E/7OWCKtPDDPeipB0VD/P1mEKbQ30MzbhqbpCXnEoCuZSUqRoPNlKgYZ4xTP1JMuq/QXinFbporh9tjrbyS2njG4rJbkwRDbhKWMZVAsi0A8Z4e7gK3kyLqJKYMq9WxCZX2PJrwxZK1EkSELJ/G9qzC3dh98k2OMvbpp9rjDKkDpfWdx5tavWdvVqLCQ/ukcdgjk2yo7U0SpGKh6v1VvFayVvGdG3Q8zdWvS00YzjMtB4s2bn4LfCovlKxVAIckmoMkeEe0P5sWIzmZRNTT90UrZAh1Vd2++QXJjbHEEzc7sWxT0lQhVUia+DHvdht2qAjBadTefRZDGnLQ/timSiQh6Io+k6QREh+wBPNxa1GEk/2918ogR6HZX49ZB0EilE7lfSYjkGVFUWQ2n1KCJdceFyvX9z38vE6DXsoX8witZhuaViYQvU9BDNOS8nX3uL7tb2sR2BBRS270Io3xGUcVTQBtu7rrbefQcAD5TY0RCUp9mfY/tyGyb+AhohJuu26qmvJMSX5QNo5N9C5MSXK2yeANngP5Doeb74QCFkj1g3xolbGs63oyHreXCs0cCk2zJSBnmCulIOq/sVV/isvAzbotYnK34BrP7nuBFpT373byzJwgYaicpnL5/KcIV6TUTQ/xQINE5Ea0W7f3M9xx1AySCEZesLDRJ02i3Tkq9aX83izzos9lf7QF7uZqcvRiXXgWAbRVXxSa/HxHzSaCm3AWEw+0PxCruukkqILPK6ozSY4NxCZrYdXGJ/oKffipNldjrfK1pRRBUnPSJN3x5A1eI93u36bxefpYSHy0YBL7yVy3qsIoBuIT8YWPg5cKGEuB1YCy5ngTknEFichq2YUh2CXzuFMgjdCojyh8qXffAnvvjWSxJhxXctI7SMM6sI53vne5lI0QBN4OzFujf+hEnlf2G1UIeWh0yYZ6lIgUNzOfVNZtPLq/bFAfLjThIdNRg9Qb7c5sguaeN8w3EZsrfx7sVvqbp8fkqwQl+n2p8elxJJPk6rKhYAvJDB5RAb7Op6g4IaOAkG2wSuB6m+f7KYpEwTkUMKgNmE7plC1RUWmWnStlz9HnNkt82mpz0buBmbtjpLDFEV+nEg3plcO5EtPYhPPn9jYBW+cLZS6Lrdt5BGuz+mRSLPytRqvpWTPb89PgqtN/muyRw0hWscw++/RbgmX1bQM5IGOsbuHGDNISpqY+/9EFV9OzWd3WJi9Yjs2K3FkjL8OuoKiRhhhQF9vnZPEOKz81WmZvEBnyRWqTnsshrP3MoeH5k/W1Gy7OHegV4sOYoCfdqZEEdwSWezaOe8vf08M7XDO59QCzzHWGFKTnTsng3lpf0luT6rCqPvYbWkq3SgScQbJhG0a5pzPypld4CEiRxaC4pzYLjydDb44xWVNPpfCLprSyAqrELdZXUqdmLlavraJ1HG/xbDqdLg/7M5IZWjr6TZNnaqU2LIWxLahISgVlawzcAy2+FPRKC94wO8/CCNOB80NF1zRb03RFzi5Ph/u/GVZDnHYlhTcp3WPRhXI2SSpWg7hSki2tzunmzbDsWT8gWhAHrBiqZebS37CV57d4yxKq490Fxgoq3GskrSQW5wLxYWYV1pDZngbCDj84VuHdNlW4upMb71RFIXeo0e0qMNhi89wNp7gWJml5U2p5DLeAd8tyTVmUF1cl53cnayZskvAJCap1qp0J3N1he9vHS//DEZhiHaqWSVrtckaQNq8hkQt0nA9/YH28/mp6IyJPsixLIt7BdZamSIN+6p9Gau3lajJY00pOmwqfHTRhNKckyq41QV6XG45349D3PvK/RFop6xolOLE4QxnXP+SaaPd6151zZ3pZlhv4i8a2ewnwjfUnJ0TUTVgrUuaCNWHaIUYz21yfKSyYJeaKbDV9IysN6qckwT+xZleVOMukZtgYy6lqGdown+asO+OjSU2cYKjkKhNHfmqO6WLTBkdlwyJegLEKl/F2Gx+mfnqxmH/MHK5sg7bfeeTW1JF3R1KhXspQUa/C5h66F8/fxRswH9/Gcd0SmxXxhb23dJmnxwxpTL0K6crC4g/rk9rklSE8akyJuMzcKoi23axF6QPSvnBmZ4Lont5194y65NVer/gQ0r375oMxWFXbt9xZavjrqvss7coRw3co/QC2V/Y8lfZ0eMn33ZAYTDfsdpYnVN3SGJduUNbAKrsXpQhq9Midx/l0QTQ+mSCxO36HqbFllsTVo2PG7BQtZCGqOrYQ5zPOvhIWRfzWK9kwjI/wQO9plm/nxxYLTePRNinXJ/93DQ/bz228DFfcPJUMTuo9m8cJMj1/Z2dPibRMOfgabm95TGLR6oRssf3kGxYm7XJZXY+45Bnt0faW3mQtXVlvZNusI7PLB18wj6kNHBaFqLozmMymIOnblvMaEh7OjQz4EZAGa/X+uDQ1D0vH8of1urSQF6M4scsbzdYvB61Mmv1zz09XrGcrrggn8t+0s4oTC0rRf3yDrDIJZ5CjGbI4mRoRZeSWzMeJ1Nxlh5LUwon8WH3ky+zjL+gYWWRaIP4F/p2qEfV68IPnN1lNIxmC6RkyV84PX7uTrDeQUeFWQ6HDtjhMrgA8nznLzLb2ZbOK2oFcXVA7UOPfIw1fx4nj/F2TGtx/4J7nUnxkWqyrwby8amP7llWsign3n1YSGYhpi3U16CFWFiYZ58TpFxXcJ+P/lvMjIPqisUeBzh6ruJzBOOoJbQq3ZYtpIN+DKDtzpBIG1ZalCSrjSFlQgeXPq78ykC6IxpB0cklKiQZg9wBavdwe+xUITS6jjF10U4wHJBcdfsmsxV5gAN+VTOyyIhlmWTninNkGvyHY1BlLapQ1ukdrLZEns8eBexpLz/YWEP0KOEADYo1i4ppcsKzBfOkFo8g/9XCj6neAjqKWG4t12awF8mbiKyfTcH6JBVoDOg12IDcGCH0oi5/+bhIliJJi3VmTMeLEhT3K3xcOfQFJ9aBdk2ZkiLZgZXxlETY9zEj7BkyTQYYcacbFjt3pLio2773fvwC79H5TEJdNx0JalsXN566P+eHfBS9lMFGDSX1cm9hwg989TLQIZ5tJM6JNSofqTUYtBq79JnzcsjOZRR02VUxm/IHRJOUIDlKmIFQYmlrFHjclLTB/BAayxDI3rg7LbjT8x+LgjZca/laMxtdMrr7kRsN/uWE6fxO43iLJiotlXkeUTrX6s3D8dcKSZVtU/y4jZrC6Jq0gxeAuCRLz5zJ+LUJq6ZTcReV/0VyLb8eHRYdZiM9OB0t4/HUyRQhMcorFmy3JTASOYce/H8ltMM+n5Zd1hf5N0JKKwkxcarj1P77gJ3Avj8URs6b/KUE/ApLlLqQ22l4A+5vhlqc2yHy5v2y5ZXAsHRJPajRE+0dW1Dd8s9QK/R/YpgmMUvV+4Z8d//tBLk15bjU+8mV0/gbIFTH5/j/jqv9/ZOmj7yWTv/6YkMRvD50ivwTDdBY3RhSz/pWex3P+IIgBM1RwAs65LOdJ8cwfyMnwgtYninC+bjINor8/q9YCc1YSnWkQbRwz9rewKpTt2+YPDxzvG+PcFodWc67xz8EHjd0grkqw2f3BiH4jFrMJqXKfbP3fWfvUB0Yfqx7v0XjjjTfeeOONN95444033ngF/wHl00sjYBtuRgAAAABJRU5ErkJggg== @endif'>
                                                <span class='cat-item @if(strtolower(request('category')) == str_replace('/', '_',strtolower($category->name))) cat-item-active @endif'>{{ $category->name }}</span>
                                            </div>
                                            </a>
                                        @endforeach
                                        
                                    </div>
                                @endif

                            @endif



                            <div class='d-xl-none text-center others'>
                                 <h6 class='text-center '>Others</h6>
                                <a class='col' href='#'>Trending</a>
                                <a class='col' href='#'>New</a>
                                 <a href='{{ route('question.create') }}' class='btn'>Ask a question</a>
                            </div>
                            <div class='row'>
                                
                                <div class="col-xl-3 d-none d-xl-block car fixe">
                                        <div class="card m-b-30">
                                                <div class="card-body">
                                                    <p id='refine' class="mb-10 font-14 text-dark text-bold">Questions about the: <small class='pull-right d-block d-sm-none cursor-pointer'><i class='fa fa-bars'></i></small></p>
                                                    @if(isset($categories))
                                                        @if(count($categories))
                                                        
                                                                @foreach($categories as $category)
                                                                            <p class='cat-xl-item @if(strtolower(request('category')) == str_replace('/', '_',strtolower($category->name))) cat-active @endif'>
                                                                           
                                                                            <a href='{{ route('forumHomePage', str_replace('/', '_',$category->name)) }}'>
                                                                            <img src='@if(isset($category->path)) {{ $category->path }} @else data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///8jHyAAAAAgHB0JAAAdGBkbFhcXERMcFxgRCgwWEBL7+/sFAAANAwYgGx0PBwn19fXt7e29vLzGxcWioaErJyiYl5eysbHn5+esq6vd3d2Ih4c9Ojvw8PCSkZHU09NFQkMvKyxqaGmbmpplY2R/fX7X19c5Njd5eHhVU1NCP0BoZmfLyspOTExdW1yEg4P1jokDAAAXRElEQVR4nO1dh7aiyBZtDkmSIKgEc7qi1/D/fzeVEJBUIDfNcq+33nS3qYo6OdW/f2+88cYbb7zxxhtvvPHGG2/8SgTuNN6fbtv7+LIoe90JgigKAue719UTguUEwNaVgaLqFsB8GaWvLS7j+CTAA5vbzP8wfm6ttRh548NtPhEUUThet7OQnVawB00UMpBtuHrkA368AbD0QeZlUdEkMK/T1Y9upQQLN1bxYhVRHIoIsooWCso2DJagCgUMYH3ZHcHUxeJr5HUb4NP9RTTr3gDUksUOVQm08j3IYFfsLgF6YtvLT++MYLGUQKlfLN4RPlN8yhX7QscuD59fU2Hi//T2/jmzxsNA0GA98y+rlTdGnJd//xALH7Am8/X6ODHRH20ly5Yg/PAex1kqZOdEYaVkq8IySD8SjDfJK0MN4BiHl+AhPJ3I3e0VkFKiGMI6KPvl70E0h2SxIlrsFcnPaLRwnEWwcnfxHJ2HjF6CfV5khEP6PCw4zrxSaRKFe0gF1ADG37KbEuxAfpySMLsUtJhxma7Rae7y/3gD9AEFqcVx3dkYXgxm8vWw73vpXDBOwBagw7ZSfS1W+VNabHRMeadVqWmT/wF/njxCe/0DRkAw0emvK3AYcX9qISAOU22X8+1ewgb6udsqX8AHe7wi3FoIAmOCNiidWhyIz4SOdmu/xpewYs9WtXiPg+CKzh3iVr+0uEqUF6etPvYqVkDlIZxaWVYzxLlmuw0ixJTfIWp+a2+I2AnCstXHVmipegdqi8kpytfZ/nS6xeOi0O4BI3e5XU8UZTA57+9jT2AbbGlubGRhOOny8zfCizLyvwYKNoFurVijGcH0CKCpskicBvwTbINeu+8J0RHCR5cVuFbOBkTiW9/1d5DBtsLwlNoa/qIoaIf2C4gOGhR+fCjpfdmrszL3jqiJtcuvBjF8QKqltcv3cYJyd0SEa7vfL0dwlMr3h3/ChOu4xY+cZUFrJ5kQDsQxE5lpo0hZi16B19lxBYNkO1rqNOjo96iHg4znEy8zBpgLm021/GcmtoCN3uNeJv/dhysvjAeJOyq2lOVFJGoduXfzmfsxchwn+PDGB2RO73WwFPogJyHXl40tQWmpKSJsesvwufp3RguBbUIw3i2xV1taD88IEpsQZiWGWRTegJIQCDzUclIEs51wIL8vnZGin5poM1kX6mPNhA9sW33lEzYy+5Iq2nL8GwmjiXBqJj8kkKGdaMC/D3f0BwNt1Z7lXwwTlfXCKR6oEoJaIhwtTYuwSBM7IjYU22l7/PvUGHVBGKrPL0cCFfLdeXHFrMEmCjSmWJ+I9Q8C8Q60ZMMI/b5F1edBF+yi3W2sdb4VVuFMaJTHMFvsofGskRck6Pc2v79XBfFI/7iWBShzsa9si530oi8QSanxUbmPmQJqbRwkSltpQ6Jc2DdOqlj4TAh10ME79ifUOxJVTusP6xVRr3sz2mEJpdW+X76yP0+GApS62Q51AVq7jpdNogf5nQfMtc/iLodQErS6159xHaTK5VpBpfSk8TpbxRudz0eEUG0h/HbIvIOaQ0TyUG0h2A3I8Fe5pCHwyRZbyTAXWIAJGRPzNo8GM0uNVPtA2mKe/MVoJH4kScVNuiZEjRVv3BNWrJcBOdyTA5Rh3c7929mCXuMbOfhQkGcR+PHcRu7mdVorAbFy+Uz+Qg60QlQviMSQ17zLPJmJTTtv6/0hThycal5H2kfyx3OQdHGIvQUL6tjSN7PK5a6hFVUQ1NhscYjGnCUhpEl7LToCUasLTS/Rd1tWEsXGMGvCvFgwpcrFQZQlTyosQyJPOTlxrjMC7WQIhWYt30aQ+ngm+h9al76u/rK86MUCRZmU0zUVNlzi9ERPUDt+TdiOGEk63MIPxI4LH0f3pUpCxVSa5eoD2oZslpOiiA/R5jiVO+VB+Gx+aye4OJY4SwnzpNfEQC9I0uRofgtYOpSKsil2kznMeo8pz1bGYysg83K4ybDeZiioVe4doumnwCOJC2tlGY8RJdMmynMs8Ys3iFWioGfE7QWqrWaDKZcMxtixL40/XXGsxd4V/j2PLZEy2leRKMESLzvDeogzrark59XW9ad/io4mCZsUuDHELurg+vzPeVBvUD62W3JbENZL6QxJQbNKQIz2pyLZ7agr+hxVptYp1P/2Wuai5RdhYGWfketX2LSLvhFXdCg+/zNRifWLp2LGejU41wisFOVN+vf2vusYp6+eSXuPnVmz1kohvDqssm5LEEyvotYho4fVs/ZSfGymPXz/B7C9VO9+fkA7bzDYgz0Qh12I+i61z1nlYJRIYB+LIL3OOYtVPp3JMGa5jE6JpLPSNbLCcFMKWS/CZEqNVeyQIzT5Ytf//n3itw8Ayi2MJiALXVAaJHstplpBxxBNUOfZhCT9UuehZ3GyiOJ1Rx0zeNh6eyWn4hfjIYTLUge7CCJn6tzXLLbocSh6y+RoDjhOqHT/OHpCz2ul2lw9V6keZtfxMRVO5Krzlyo/FzUuOwcQ0z0LlQu1qRW9QvT5Er+cwU9Dmb+YYUbOQEHg86NkhwYrDamQ7hHxrzjDtZjCzJZZwAJIzKaz+VRCpWjXVLqLVomU/qD7rwhIPiF6UUowbNVqk7sRJZIGUf6NEWpR3LAsqNhgtzIcdEGpCzfxL7LSM2xERX5gTLdYMOUftT98jxQ5kS1Ck5WIoFa218MFudQocpkLn6fTkdRqg15NaLYNkLwSh50/PZ2Xy4yQbPGpoGXC0rycrHXXO5RTlKB9upQLLP6dlfQ3loDj1d84ct9HqwciqD7Y+RlMsWeOa8eZ5s1+g9nHUuZyu1QbL4gHkUkAsRgtv3nhdXz07umQrZ8KSJXpVxTgE9smE6M70sgFf1YP2SJd2DAARQeQb7PQ9Tx3d8I1KtZrhTAVoE7SgxEpjarJoRiRF05n99k09KoeL1LUXdQ9DZGIioZbR0x7QOLqHdbPAdLJkZDHgv6uio3okXs4ol+XbE3XbAlAjEuV3lzsZmytwc61Dclw+6LS+6ucCUkdNCZGjfAKT/V/ol5ay4Xe1L64EGO1u+EHqOmqqtsA654LYFNkd0glqxo7B5BoeR4mI8mULH1Azbjhsw5BVC4WCne4EXjh8rDdHqYtizZb4Zih0hk5wkFM+5Zk9GQ3n7Nx6Ifj+wlIplt8LqjCevqLQ8avgpRxMTpjdb+k9M6C626V5YzVgWzcOub8pA9IC0B+J4j+Y3TmPgqLRdiU1ME6M5wHUYZZsbpqW7j17SC+PFvjZyLcpE0F20dHDZfHZzb/vTv8CGfxFqGNVCKxb5aEguQAazQ4TkoOJin1fuMOnaVCRa+qtjCEDUh9eVZ4KNY7ezi1rKec11Rtsbh2ifOXYZdpD2gRsyJRUcaGY4tusEF/42aV9JSb/Nat1fiFXBjNqcCwaFk5f1SI6AoWw9mqfF4TSfglYUbst8rV7/U7N4zkccF2q2hKMTZjvfIm01J4GSLF9ldDKoPCEMVM9ZVVyDxngLu2utRBPoNUcmpyh+QNKfaQGZWpWQu1DphhrSS3z+xSx1vu5xN5cv6cekzUBjvV5qB6DpBQBGw72K1UzrAsIvEyxOc8eSnuVpoNQ7Rtj31kxWoKbYFCltBku1xuJ0QyiG0boUoQcpQbV4HY1hJdKzFKOSMlpGKelsHgigBbSppMmL5RNY1QhGhOXmfCMbzwoO7Y1mQFCyS1P9w0fOIfey/mCzjF8fXh/SQNNNpjtwpYTdUeHKAb7BoJoiqQRhPpGXLGBVekQWWgqjQuJ1uwXnrRwllE7vKKnSI8ZmDfTjLshDJOW760QVawIBESp96+xPnBSEiKTkXVhHPeio3cMHTbCpgViNqxIOfiFzdI/SUWSSKNWfx9VuMjG48T+704d1hzyTDLfdflaJfVyrQBI1OyLaI62nzbaHW5RP3NiyGVuhrcfHr6xsf4jHlhYL6mboitRwvgSWBR6p6lfBkxIXxca6ptNkjpkNJaa/MihRBpSnM+xC7tnv7pAd4kGVkwZGNp1G6VD1lc0vIvQrGi9vpCX4C/BinpBxV1gG0zhd6hfhyDIaWWKfBZ3l+LINxuqAg7xj6H2LsgEbyufQd1gYkPR5wL9UtrLTmxCAJemY5FsPZc/h6sVqkIJB4i1RduRrD+VhRFN65E1zepIjXcrUpoYD2m+44y3EdKglt1InGhv1jo4gjK+v400eyKY0dJOvcSS8mEMVliFV2Zun0aL+35EEcC9JYWnEnI30YC6JYTFidMhpJ+GE/3ADqpsZCJ+SLSOUSk+InKl6BlOSLfqizB6mvg0ZKZigqcs4YcUaSibmkkWIib6E/7OWCKtPDDPeipB0VD/P1mEKbQ30MzbhqbpCXnEoCuZSUqRoPNlKgYZ4xTP1JMuq/QXinFbporh9tjrbyS2njG4rJbkwRDbhKWMZVAsi0A8Z4e7gK3kyLqJKYMq9WxCZX2PJrwxZK1EkSELJ/G9qzC3dh98k2OMvbpp9rjDKkDpfWdx5tavWdvVqLCQ/ukcdgjk2yo7U0SpGKh6v1VvFayVvGdG3Q8zdWvS00YzjMtB4s2bn4LfCovlKxVAIckmoMkeEe0P5sWIzmZRNTT90UrZAh1Vd2++QXJjbHEEzc7sWxT0lQhVUia+DHvdht2qAjBadTefRZDGnLQ/timSiQh6Io+k6QREh+wBPNxa1GEk/2918ogR6HZX49ZB0EilE7lfSYjkGVFUWQ2n1KCJdceFyvX9z38vE6DXsoX8witZhuaViYQvU9BDNOS8nX3uL7tb2sR2BBRS270Io3xGUcVTQBtu7rrbefQcAD5TY0RCUp9mfY/tyGyb+AhohJuu26qmvJMSX5QNo5N9C5MSXK2yeANngP5Doeb74QCFkj1g3xolbGs63oyHreXCs0cCk2zJSBnmCulIOq/sVV/isvAzbotYnK34BrP7nuBFpT373byzJwgYaicpnL5/KcIV6TUTQ/xQINE5Ea0W7f3M9xx1AySCEZesLDRJ02i3Tkq9aX83izzos9lf7QF7uZqcvRiXXgWAbRVXxSa/HxHzSaCm3AWEw+0PxCruukkqILPK6ozSY4NxCZrYdXGJ/oKffipNldjrfK1pRRBUnPSJN3x5A1eI93u36bxefpYSHy0YBL7yVy3qsIoBuIT8YWPg5cKGEuB1YCy5ngTknEFichq2YUh2CXzuFMgjdCojyh8qXffAnvvjWSxJhxXctI7SMM6sI53vne5lI0QBN4OzFujf+hEnlf2G1UIeWh0yYZ6lIgUNzOfVNZtPLq/bFAfLjThIdNRg9Qb7c5sguaeN8w3EZsrfx7sVvqbp8fkqwQl+n2p8elxJJPk6rKhYAvJDB5RAb7Op6g4IaOAkG2wSuB6m+f7KYpEwTkUMKgNmE7plC1RUWmWnStlz9HnNkt82mpz0buBmbtjpLDFEV+nEg3plcO5EtPYhPPn9jYBW+cLZS6Lrdt5BGuz+mRSLPytRqvpWTPb89PgqtN/muyRw0hWscw++/RbgmX1bQM5IGOsbuHGDNISpqY+/9EFV9OzWd3WJi9Yjs2K3FkjL8OuoKiRhhhQF9vnZPEOKz81WmZvEBnyRWqTnsshrP3MoeH5k/W1Gy7OHegV4sOYoCfdqZEEdwSWezaOe8vf08M7XDO59QCzzHWGFKTnTsng3lpf0luT6rCqPvYbWkq3SgScQbJhG0a5pzPypld4CEiRxaC4pzYLjydDb44xWVNPpfCLprSyAqrELdZXUqdmLlavraJ1HG/xbDqdLg/7M5IZWjr6TZNnaqU2LIWxLahISgVlawzcAy2+FPRKC94wO8/CCNOB80NF1zRb03RFzi5Ph/u/GVZDnHYlhTcp3WPRhXI2SSpWg7hSki2tzunmzbDsWT8gWhAHrBiqZebS37CV57d4yxKq490Fxgoq3GskrSQW5wLxYWYV1pDZngbCDj84VuHdNlW4upMb71RFIXeo0e0qMNhi89wNp7gWJml5U2p5DLeAd8tyTVmUF1cl53cnayZskvAJCap1qp0J3N1he9vHS//DEZhiHaqWSVrtckaQNq8hkQt0nA9/YH28/mp6IyJPsixLIt7BdZamSIN+6p9Gau3lajJY00pOmwqfHTRhNKckyq41QV6XG45349D3PvK/RFop6xolOLE4QxnXP+SaaPd6151zZ3pZlhv4i8a2ewnwjfUnJ0TUTVgrUuaCNWHaIUYz21yfKSyYJeaKbDV9IysN6qckwT+xZleVOMukZtgYy6lqGdown+asO+OjSU2cYKjkKhNHfmqO6WLTBkdlwyJegLEKl/F2Gx+mfnqxmH/MHK5sg7bfeeTW1JF3R1KhXspQUa/C5h66F8/fxRswH9/Gcd0SmxXxhb23dJmnxwxpTL0K6crC4g/rk9rklSE8akyJuMzcKoi23axF6QPSvnBmZ4Lont5194y65NVer/gQ0r375oMxWFXbt9xZavjrqvss7coRw3co/QC2V/Y8lfZ0eMn33ZAYTDfsdpYnVN3SGJduUNbAKrsXpQhq9Midx/l0QTQ+mSCxO36HqbFllsTVo2PG7BQtZCGqOrYQ5zPOvhIWRfzWK9kwjI/wQO9plm/nxxYLTePRNinXJ/93DQ/bz228DFfcPJUMTuo9m8cJMj1/Z2dPibRMOfgabm95TGLR6oRssf3kGxYm7XJZXY+45Bnt0faW3mQtXVlvZNusI7PLB18wj6kNHBaFqLozmMymIOnblvMaEh7OjQz4EZAGa/X+uDQ1D0vH8of1urSQF6M4scsbzdYvB61Mmv1zz09XrGcrrggn8t+0s4oTC0rRf3yDrDIJZ5CjGbI4mRoRZeSWzMeJ1Nxlh5LUwon8WH3ky+zjL+gYWWRaIP4F/p2qEfV68IPnN1lNIxmC6RkyV84PX7uTrDeQUeFWQ6HDtjhMrgA8nznLzLb2ZbOK2oFcXVA7UOPfIw1fx4nj/F2TGtx/4J7nUnxkWqyrwby8amP7llWsign3n1YSGYhpi3U16CFWFiYZ58TpFxXcJ+P/lvMjIPqisUeBzh6ruJzBOOoJbQq3ZYtpIN+DKDtzpBIG1ZalCSrjSFlQgeXPq78ykC6IxpB0cklKiQZg9wBavdwe+xUITS6jjF10U4wHJBcdfsmsxV5gAN+VTOyyIhlmWTninNkGvyHY1BlLapQ1ukdrLZEns8eBexpLz/YWEP0KOEADYo1i4ppcsKzBfOkFo8g/9XCj6neAjqKWG4t12awF8mbiKyfTcH6JBVoDOg12IDcGCH0oi5/+bhIliJJi3VmTMeLEhT3K3xcOfQFJ9aBdk2ZkiLZgZXxlETY9zEj7BkyTQYYcacbFjt3pLio2773fvwC79H5TEJdNx0JalsXN566P+eHfBS9lMFGDSX1cm9hwg989TLQIZ5tJM6JNSofqTUYtBq79JnzcsjOZRR02VUxm/IHRJOUIDlKmIFQYmlrFHjclLTB/BAayxDI3rg7LbjT8x+LgjZca/laMxtdMrr7kRsN/uWE6fxO43iLJiotlXkeUTrX6s3D8dcKSZVtU/y4jZrC6Jq0gxeAuCRLz5zJ+LUJq6ZTcReV/0VyLb8eHRYdZiM9OB0t4/HUyRQhMcorFmy3JTASOYce/H8ltMM+n5Zd1hf5N0JKKwkxcarj1P77gJ3Avj8URs6b/KUE/ApLlLqQ22l4A+5vhlqc2yHy5v2y5ZXAsHRJPajRE+0dW1Dd8s9QK/R/YpgmMUvV+4Z8d//tBLk15bjU+8mV0/gbIFTH5/j/jqv9/ZOmj7yWTv/6YkMRvD50ivwTDdBY3RhSz/pWex3P+IIgBM1RwAs65LOdJ8cwfyMnwgtYninC+bjINor8/q9YCc1YSnWkQbRwz9rewKpTt2+YPDxzvG+PcFodWc67xz8EHjd0grkqw2f3BiH4jFrMJqXKfbP3fWfvUB0Yfqx7v0XjjjTfeeOONN95444033ngF/wHl00sjYBtuRgAAAABJRU5ErkJggg== @endif' style='width:20px' class='img-fluid'>
                                                                             {{ $category->name }} <i class='fa fa-caret-right'></i>
                                                                             
                                                                             </a>
                                                                             </p>
                                                                    
                                                                @endforeach
                                                            
                                                        @endif

                                                    @endif
                                                   
                                                </div>
                                        </div>
                                        
                                </div>
                                <div class='col-xl-8'>
                                    <div class="">
                                        <div class="">
                                            {{--  <h4 class="mt-0 header-title"></h4>  --}}
                                            <div class="list-inline-item app-search app-search-2 flex" >
                                                <form id='s-q' action='{{route('forumHomePage')}}/{{ request('category') }}' method='get' role="search" class="m-auto w-100">
                                                    @csrf
                                                    <div class="form-group pt-1">
                                                        <input name='search' type="text" class="form-control s-icn w-100" value='{{ request('query') }}' placeholder="Search for a question">
                                                        <a><button class='q-btn' type='submit'><i class="fa fa-search s-ico"></i></button></a>
                                                    </div>
                                                </form>

                                            </div>


                                        {{--  @if(@$count == 0)
                                                No results for the search <strong>{{request('query')}}</strong>. Showing all jobs instead.
                                        @endif  --}}
                                        </div>
                                    </div>
                                    <h5 class='text-center w-100 d-xl-none'>Questions</h5>
                                    <hr class='d-xl-none' />
                                    @if(isset($questions))
                                        @if(count($questions))
                                            @foreach($questions as $question)
                                                <div class=''>
                                                    <div style="" class="card dockay-card col-12 m-b-30 ">
                                                        <h3 class='post-content'><a href='{{ route('question.show', $question->slug) }}'>{{ title_case($question->title) }} ?</a>
                                                        
                                                         <small>Category: <i>{{title_case(@$question->category->name)}}</i></small>
                                                        </h3> 
                                                       
                                                        <small class='pull-right'>- posted {{ when($question->created_at) }}</small>
                                                        <div class='flex mt-10'>
                                                            <p class='col pull-right'>{{ count($question->answers) }} <i class='fa fa-comments'></i> Answers</p>
                                                            {{--  <p class='col'>3 <i class='fa fa-star'></i>Stars</p>  --}}
                                                        </div>                       
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class='card jumbotron'>
                                                <h3 class='text-center'>
                                                    <i class="em em-cry"></i>
                                                </h3>
                                                @if(empty('search'))
                                                <h5 class='text-center'>OOPs!!! NO <strong>{{ str_replace('_', ' or ',request('category')) }}</strong> QUESTION</h5>
                                                @else
                                                    <h5 class='text-center'>oops!!! We couldn't find  questions under <strong>{{ str_replace('_', ' or ',request('category')) }}</strong> @if(!empty(request('search'))) relating to <strong>{{ request('search') }} @endif </strong> </h5>
                                                @endif
                                            </div>
                                        @endif

                                    @endif
                                   
                                

                                </div>
                                <div class='col-xl-1 d-none d-xl-block'>
                                 <a href='{{ route('question.create') }}' class='btn'>Ask a question</a>
                                   
                                </div>
                            </div>
                </div>

            </div>

               
             
        </div>

        

</div>





@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
    
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>



<script>
     $('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        300:{
            items:2
        },
        500:{
            items:3
        },
        600:{
            items:5
        },
        1000:{
            items:5
        }
    }
})

</script>


@endpush
