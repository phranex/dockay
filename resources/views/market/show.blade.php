@extends('layouts.master')

@push('styles')
<link  href="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/css/magnify.css" rel="stylesheet">

    <style>
        .other-product-img{
            padding:30px;
        }

        .magnify{
    width: 100% !important;
}
        @media (max-width:720px){
    .prof{
        display: block;
    }
    .profile-pix{
        display: flex;
    }

    .pi{
        text-align: center
    }

    
}
.magnify{
        height:100%;
        margin:auto;
    }

.flexer{
    display: flex !important
}
    </style>

@endpush

@push('title')
   Dockay | {{  $product->title}}

@endpush

@section('content')
<div class="container-fluid">
    <div class='row'>
        <div class="col-md-6 dockay-card dockay-bd-rad  col-12">
            <div class="col-12" style='height:350px' >
                {{--  <a class='w-100 h-100 flexer' style='' href="{{@$product->images[0]->path}}">  --}}
                    <img id='mainPicture' src="{{@$product->images[0]->path}}" alt="user" class="zoom h-100 img-fluid dockay-bd-rad img-thumbnail">

                {{--  </a>  --}}
            </div>
            <div class="col-12  other-product-img owl-carousel m-auto owl-theme">
                @if(isset($product->images))
                    @if(count($product->images))
                        @foreach ($product->images as $images)
                            <div class="content">
                                <img src="{{@$images->path}}" alt="user" class="dockay-bd-rad img-thumbnail img-fluid">
                            </div>
                        @endforeach
                    @endif
                @endif


            </div>
        </div>
        <div class="col-md-6 col-12 dockay-card dockay-bd-rad">
                <div class=" card dockay-card dockay-bd-rad  col-12">

                        <div class="profile-info ">
                            <div>
                                <h3 class="text-bold">{{$product->title}}</h3>
                                <h6 class="text-bold">N{{number_format($product->price)}}</h6>
                                <p>Nature: {{title_case($product->nature)}}</p>
                                <p>Brand: {{title_case($product->brand)}}</p>
                                <p>Negotiable: @if(strtolower($product->negotiable) == 'n') No @else Yes @endif</p>

                                <p>Quantity: {{title_case($product->quantity)}}</p>
                                <p class="text-muted ">
                                    Description
                                    <p>
                                            {{$product->description}}
                                    </p>
                                </p>

                            </div>


                        </div>
                    </div>

            <div class="card  mt-10 dockay-card d-inline-box prof col-12">
                <div class="profile-pix  col-md-2 ">
                    <img class="img-fluid rounded-circle d-none d-md-block " src="{{ displayDefaultPhoto($product->user)}}"/>
                    <img style='width:70px;height:70px' class=" img-fluid m-auto rounded-circle  d-block d-md-none  " src="{{ displayDefaultPhoto($product->user)}}"/>
                </div>
                <div class="profile-info pi  col-md-9" >
                    <div>
                        <strong>{{getFullName($product->user)}} @auth
                                                                    @if( auth()->id() == $product->user_id )
                                                                        <span style="font-size:10px">(you posted this)</span>
                                                                    @endif
                                                                @endauth
                    </strong><br/>
                        <small><a href="{{route('user.profile.view', $product->user->username)}}">View Profile</a></small>
                        <small class="block text-muted text-bold">Posted: {{when($product->created_at)}}</small>

                        <span id='halfNumber'>
                            <i class="fa fa-phone-square"></i><strong> {{substr($product->user->phoneNumber,0,4)}}<span style="font-size:9px">XX</span> </strong>
                            <strong><span style="font-size:9px">XX</span> </strong>
                            <button id='show'  class="btn btn-sm ">Show number</button>
                        </span>
                        <span  style="display:none" id='fullNumber'>
                                <i class="fa fa-phone-square"></i><strong> {{$product->user->phoneNumber}}</strong>


                        </span>



                    </div>


                </div>
            </div>

        </div>
    </div>
</div>



@endsection

@push('scripts')
{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/js/jquery.magnify.min.js"></script>  --}}
<script src="{{ asset('js/blowup.js') }}"></script>
<script>

    $(document).ready(function(){
        $('#show').click(function(){
            $('#halfNumber').hide();
            $('#fullNumber').show();
        });
       // $('.zoom').magnify();
       $('.zoom').blowup();
        $('.content').click(function(){
            $('.content').removeClass('image-active');
            $(this).addClass('image-active');
            var src = $(this).find('img').attr('src');
            $('#mainPicture').attr('src',src);
            $('.magnify-lens').css('background-image','url('+src+')');

        });
        $(".owl-carousel").owlCarousel({
            //loop:true,
            margin:10,
            responsiveClass:true,
            //autoplay:true,
            //autoplayTimeout:3000,
            //autoplayHoverPause:true,
            nav:true,
            responsive:{
                0:{
                    items:4,

                },
                600:{
                    items:4,

                },
                1000:{
                    items:4,
                    nav:true,

                }
            }
        });
    });
</script>
@endpush
