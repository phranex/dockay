@extends('layouts.master')

@push('styles')
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">
<style>
    .refine-box, .refine-content{
        padding: 10px !important;

    }

    .refine-box h5 a{
        font-size: 14px !important;
    }

    .contain{
        font-size: 12px !important;
    }
    .num_of
    {
        font-size:12px !important;
        color:#7070709c;
        
    }

    @media (max-width: 620px){
        .pt-1 input, .pt-1 a i {
            padding: 10px !important;
        }
    }

   

    .checkmark{
        height: 15px;
        width: 15px;
        top:21%;
    }

    ::placeholder{
        font-size:13px;
    }

    .contain .checkmark:after{
        left: 5px;
    top: 2px;
    }
</style>
@endpush

@push('title')
    Dockay | {{ request('query') }} Products

@endpush

@section('content')
  <div class="container-fluid " >
      <div class="row">
          <div class="col-xl-2 car fixe">
                <div class="card m-b-30">
                        <div class="card-body">
                            <p id='refine' class="mb-0 font-14">Refine Your results <small class='pull-right d-block d-sm-none cursor-pointer'><i class='fa fa-bars'></i></small></p>
                        </div>
                </div>
                <div id="accordion" role="tablist" class='d-none d-sm-block' aria-multiselectable="true">
                        {{--  specialty  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingOne">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="text-dark">
                                        Brand
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse @if(!empty(request('brand')))show @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body refine-content">
                                    @if(isset($brands))
                                        @if(count($brands))
                                            @foreach ($brands as $brand)
                                                <label class="contain"> {{title_case($brand->brand)}} <span class='num_of'  >({{ $brand->total }})</span>
                                                    <input  @if(request('brand') == $brand->brand) checked @endif  name='brand' value='{{$brand->brand}}' type="radio" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif


                                </div>
                            </div>
                        </div>
                        {{--  category  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingTwo">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Nature
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse @if(!empty(request('nature')))show @endif" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-body refine-content">
                                        @if(isset($nature))
                                        @if(count($nature))
                                            @foreach ($nature as $nat)
                                            <label class="contain">{{title_case($nat->nature)}} <span class='num_of'  >({{ $nat->total }})</span>
                                                    <input @if(request('nature') == $nat->nature) checked @endif name="nature"  value='{{$nat->nature}}' type="radio" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>
                        {{--  establishment  --}}
                        {{--  <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                       Price
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse @if(!empty(request('establishment')))show @endif" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    @if(isset($establishments))
                                        @if(count($establishments))
                                            @foreach ($establishments as $establishment)
                                                <label class="contain"> {{title_case($establishment->name)}}
                                                    <input @if(request('establishment') == $establishment->id) checked @endif name='establishment' value='{{$establishment->id}}' type="radio" name="type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>  --}}

                        {{--  gender  --}}
                        {{--  <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#gender" aria-expanded="false" aria-controls="collapseThree">
                                        Gender
                                    </a>
                                </h5>
                            </div>
                            <div id="gender" class="collapse @if(!empty(request('gender')))show @endif" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    <label class="contain">Male
                                        <input @if(request('gender') == 'male') checked @endif name='gender' value='male' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="contain">Female
                                        <input  @if(request('gender') == 'female') checked @endif name='gender' value='female' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="contain">Any
                                        <input  @if(request('gender') == 'any') checked @endif name='gender' value='any' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>  --}}

                        {{--  location  --}}
                        {{--  <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#location" aria-expanded="false" aria-controls="collapseThree">
                                      Location
                                    </a>
                                </h5>
                            </div>
                            <div id="location" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                        <form method="get" action="{{url()->full()}}" class="mb-0">
                                                <div class="form-row row align-items-center">

                                                    <div class="col-12">
                                                        <label class="sr-only" for="inlineFormInputGroup">Location</label>
                                                        <span class="bmd-form-group"><div class="input-group">
                                                            <div class="input-group-prepend">
                                                            </div>
                                                            <input name="location" class="form-control" id="inlineFormInputGroup" placeholder="location">
                                                        </div></span>
                                                    </div>

                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-link pull-right">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                </div>
                            </div>
                        </div>  --}}
                        {{--  facility  --}}
                        {{--  <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Coy Name
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    <label class="contain">Facility/Institution
                                        <input value='#F' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>  --}}
                </div>
          </div>
          <div class="mt-2 col-xl-7 m-aut">
                <div class="c">
                        <div class="card m-b-30">
                            <div class="card-body">
                                {{--  <h4 class="mt-0 header-title"></h4>  --}}
                                <div class="list-inline-item app-search app-search-2 flex" >
                                    <form action='{{route('product.listings')}}' method='get' role="search" class="m-auto w-100">
                                        @csrf
                                        <div class="form-group pt-1">
                                            <input name='query' type="text" class="form-control s-icn w-100" value='{{ request('query') }}' placeholder="Search by name e.g Stethoscope">
                                             <a href="#"><i class="fa fa-search s-ico"></i></a>
                                        </div>
                                    </form>

                                </div>
                                    <small class='pull-right text-right'>Show <input type='number' class='form-control inline' value='{{request('page_size')}}' style='width:30% !important' id='page_size' placeholder='20'/> Results</small>


                               {{--  @if(@$count == 0)
                                    No results for the search <strong>{{request('query')}}</strong>. Showing all jobs instead.
                               @endif  --}}
                            </div>
                        </div>
                    </div>
                @if(isset($items))
                    @if(count($items))
                        @foreach ($items as $item)
                        <div class="card m-b-30 ">
                                <div class="card-header">
                                <div style="height:55px">
                                        @if(isset($item->images))
                                                @if(count($item->images))
                                                    @foreach ($item->images as $images)
                                                        <img style='height:50px;width:50px' src='{{$images->path}}' class="img-fluid rounded-circle" />
                                                        @break
                                                    @endforeach

                                                @endif
                                            @endif
                                            <a href="{{route('product.details', $item->id)}}"><span class='poster-details'>{{$item->name}}</span>
                                             <small>{{ $item->brand }}</small>
                                            </a>
                                        <span class='pt job-type'>
                                                {{title_case($item->nature)}}
                                        </span>

                                    </div>

                                </div>
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                    <h5 class='post-title'><a href="{{route('product.details', $item->id)}}">{{$item->title}}</a></h5>
                                    <p class="post-content">{{ substr($item->description, 0, 150) }} ...</p>
                                    <footer class="blockquote-footer">posted {{when($item->created_at)}}</footer>
                                    </blockquote>
                                    <div class="card-body flex  text-center">
                                    <p class="col more-details"><i class="text-success fa fa-money"></i> #{{  format_number($item->price)}}</p>
                                    <p class="col more-details"><i class=" text-danger fa fa-clock-o"></i>Qty: {{$item->quantity}}</p>

                                    </div>
                                </div>
                        </div>
                        @endforeach
                    @else
                        <div class="card  bg-transparent m-t-30 dockay-card">
                            <div class="alert alert-info text-center">

                                    Sorry, No item matching your request was found.
                            </div>
                        </div>

                    @endif
                @endif


                {{ $items->links()  }}

          </div>
      </div>
  </div>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('js/validation.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('#refine small').click(function(){
                $('#accordion').toggleClass('d-block d-sm-none, d-none d-sm-block').fadeIn();
            });

            $('[name=brand]').change(function(){
                var sp = $(this).val();
                search('brand',sp);
            });
            $('[name=nature]').change(function(){
                var sp = $(this).val();
                search('nature',sp);
            });

            $('#page_size').blur(function(){
                var sp = $(this).val();
                search('page_size',sp);
            });

            $('#page_size').keydown(function(e){
                if(e.which == 13){
                    var sp = $(this).val();
                    search('page_size',sp);
                }
            });
           

        });

        function search(x,value){
            var param = getUrlParameter(x);
            var url = @if(env('APP_ENV') != 'local') "{!! str_replace('http', 'https',url()->full()) !!}" @else "{!! url()->full() !!}"  @endif;
            if(x == 'specialty'){
                var url = changeUrl(url,'query','{{request('query')}}');
            }
            if(param  != undefined){


                var url = changeUrl(url,'query','{{request('query')}}');

                var currentUrl = changeUrl(url,x,value);
                console.log(currentUrl +' '+value);
                window.location.href = currentUrl;
            }
            else{
                var currentUrl = url;
                if(currentUrl.indexOf('?') !== -1)
                    window.location.href = currentUrl+'&'+x+'='+value;
                else
                    window.location.href = currentUrl+'?'+x+'='+value;
            }
        }

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }

        function changeUrl(uri,key,value){
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }

        }

    </script>
@endpush
