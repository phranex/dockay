@extends('layouts.email')

@push('mail.greetings')
Hey dear,

@endpush

@push('mail.content')
 The {{ config('app.name') }} community needs your help with this question. <br/>


<p><strong>{{  $question['title'] }} </strong></p>


@endpush

@push('mail.url')

  {{ route('question.show',  $question['slug'])}}
@endpush

@push('mail.action.word')

Click to View
@endpush