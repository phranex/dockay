@extends('layouts.email-subscription')

@push('mail.greetings')
Hello @if(isset($user['username'])) {{ $user['username'] }} @else dear @endif, we found the following the jobs for you.

<br/>
<br/>
<p>To receive job notifications that matches your profession. Please @if(!isset($user['username'])) <a href="{{ route('register') }}">Register</a> and @endif update your profile.</p>

@endpush

@foreach ($jobs as $job)
    @push('mail.content')

        <div style='border-bottom:1px solid grey;padding:10px;margin:5px auto'>
            <h2><a href="{{ route('job.details', $job['id']) }}"><strong>{{  $job['title'] }} </strong></a></h2>
            <i style='font-size:10px'>Posted: <strong>{{  getDateFormat($job['created_at'], 'd-M-Y') }} </strong></i><br/>
            <a style='text-decoration:underline' href="{{ route('job.details', $job['id']) }}">Apply Now</a>
        </div>

    @endpush

  

    
@endforeach




