@extends('layouts.email')

@push('mail.greetings')
Hey {{ $user->username }},

@endpush

@push('mail.content')
 We're really excited for you to join our community! You're just one click away from activating your account.



@endpush

@push('mail.url')

  {{ route('user.verifyEmail',  [base64_encode($user['email']), $user['verifyToken']])}}
@endpush

@push('mail.action.word')

Activate
@endpush