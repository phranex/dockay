@extends('layouts.email')

@push('mail.greetings')
Hey {{ $user->username }},

@endpush

@push('mail.content')
    <p>We really need activation of your profile on {{ config('app.name') }}.</p>

    <p>We are highly focused on security and able to resolve any issues in short terms when you activate your account. By activating your mail,
    you let us securely protect all members of the community. </p>

   
    
    <p>Activation of profile also gives you full access to all features on dockay.</p>

     <p>Please click on the button below to activate.</p>
     <small>Or click or copy this link to your browser <a href='{{ route('user.verifyEmail',  [base64_encode($user['email']), $user['verifyToken']]) }}'>{{ route('user.verifyEmail',  [base64_encode($user['email']), $user['verifyToken']]) }}</a></small>
@endpush

@push('mail.url')

  {{ route('user.verifyEmail',  [base64_encode($user['email']), $user['verifyToken']]) }}
@endpush

@push('mail.action.word')

Activate
@endpush