@extends('layouts.email')
{{--  greetings  --}}
@push('mail.greetings')
Hello <strong>{{ $application->practitioner->firstName }}</strong>, <br/>
@endpush

{{--  content  --}}
@push('mail.content')
You have successfully applied for the following job.<br/>



<p><strong>{{ $application->jobAdvert->title }}</strong></p>.

@push('mail.url')

  {{ route('user.applications.index')}}
@endpush

@push('mail.action.word')

Click to View
@endpush


Good Luck.

@endpush

