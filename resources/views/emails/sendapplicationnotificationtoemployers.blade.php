
@extends('layouts.email')


@push('mail.greetings')

Hello,
@endpush

@push('mail.content')


You have new job applications for the jobs you have posted on <strong>{{ config('app.name') }}</strong>

@endpush

@push('mail.url')

{{ route('user.adverts.index') }}

@endpush

@push('mail.action.word')

View Applications

@endpush

