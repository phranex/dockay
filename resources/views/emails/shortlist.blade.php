
@extends('layouts.email')

@push('mail.greetings')
Hello Applicant, <br/>
@endpush


@push('mail.content')

You have been shortlisted for an interview for the  following job you applied for.

<p>Title: <strong>{{ title_case($job->title) }}</strong></p>.
<p>Firm: <strong>{{ title_Case($job->facility->name) }}</strong></p>
<p>Find below useful information</p>

<p>
    <strong>
        {{$body}}
    </strong>
</p>

Good Luck.

@endpush