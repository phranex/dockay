@extends('layouts.master')

@push('styles')
    <style>
            .bg{
                background:url("assets2/images/carousel/job.png");
                /* Full height */
            height: -webkit-fill-available;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-color:#64bdf99c;
            background-blend-mode:overlay;
            }
           
     .pt-1 input, .pt-1 a i{
        padding: 10px !important; 
    }
    

            @media (max-width: 620px){
.app-search-2 {
    display:  block;
    margin-right: 0
}

.app-search-2 form{
    width: 100% !important;

}

.pt-1 input{
    color: black;
    font-weight: 900;
}
.pt-1 input, .pt-1 a i{
    padding: 20px !important;
}
.app-search a i{
    top: 0;
}
            }



.bgimg-1, .bgimg-2, .bgimg-3 {
    position: relative;
    margin: auto;
    width: 100%;
    opacity: 0.65;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

  }


  .caption {

    text-align: center;
    color: #000;
  }

  .caption span.border {
    background-color: #111;
    color: #fff;
    padding: 18px;
    font-size: 25px;
    letter-spacing: 10px;
  }



@media (min-width:980px){
    .carousel-item{
        height: 100vh;
    }
    

    .carousel-caption{
        bottom: 250px;
        position: absolute;
        z-index: 11;
    }
    .caption div h3{
        font-size:50px;
    }

    .caption div small{
        font-size:20px;text-shadow:1px 1px 1px black;
    }

    .carousel-caption h3{
        font-size: 70px;
        font-weight: 900;

    }


    .overlay{
        height: 100%;
        width: 100%;
        background: #64bdf957;
        position: absolute;
        top: 0;
        z-index: 10;
    }

    .service{
        padding: 2vh 17vh;
    }

    .content{
        padding: 2vh;
    }
    .carousel-item img{
        filter: blur(6px);
    }
}
    </style>

@endpush

@push('title')
    Dockay | Jobs
@endpush

@push('bg-content')
<div class="bg-content flex">
            <div>
                <h3>Nigeria's Largest Medical Job Directory</h3>
                <small class='text-center font-20 block'>Full-time, Part-time, Locum or Volunteering. Find your next job in the blink of an eye!</small>

                <div class="list-inline-item app-search app-search-2 flex" >
                                         <form action='{{route('job.listings')}}' method='get' role="search" class="m-auto w-50">
                                            @csrf
                                            <div class="form-group pt-1">
                                                <input name='query' type="text" class="form-control text-center  s-icon w-100" placeholder="Search by discipline">
                                                <a href="#"><button class='q-btn' type='submit'><i class="fa fa-search s-icon"></i></button></a>                                               </div>
                                        </form>
                                    </div>
                </div>
        
        </div>

@endpush

@section('content')
<div class='row'>
    <h4 class='text-center w-100'><i class='fa text-danger fa-fire '></i> Hot Jobs</h4>
  
    <div class="container mt-20">
            @if(isset($jobs))
                @if(count($jobs))
                    @foreach ($jobs as $item)
                    <div class="col-xl-10 m-auto">
                            <div class="card m-b-30 ">
                                    <div class="card-header">
                                    <div style="height:55px">
                                            <img src="{{  displayDefaultPhoto($item->facility->user) }}" alt="user" class="rounded-circle avatar-img img-fluid">
                                            <a href="{{route('user.profile.view', $item->facility->user->username)}}"><span class='poster-details'>{{$item->facility->name}}</span></a>
                                        <span class='pt job-type animated infinite swing'>
                                        {{$item->category->name}}
                                        </span>

                                        </div>

                                    </div>
                                    <div class="card-body">
                                        <blockquote class="blockquote mb-0">
                                        <h5 class='post-title'><a href="{{route('job.details', $item->id)}}">{{$item->title}}</a></h5>
                                        <p class="post-content">{{ substr($item->description,0, 75)}} ....</p>
                                        <footer class="blockquote-footer">posted {{when($item->created_at)}}</footer>
                                        </blockquote>
                                        <div class="card-body flex  text-center">
                                        <p class="col more-details"><i class="text-success fa fa-money"></i> @if(isset($item->min)) #{{  format_number($item->min,2,'.',',')}} - #{{ format_number($item->max,2,'.','')}} @else N/A @endif</p>
                                        <p class="col more-details"><i class=" text-danger fa fa-clock-o"></i> {{ when($item->deadline)}}</p>
                                        <p class="col more-details"><i class="fa fa-map-marker"></i> {{getFacilityLocation($item)}}</p>
                                        </div>
                                    </div>
                            </div>
                    </div>
                    @endforeach
                
                 <div class="col-xl-10 m-auto text-center">
                    <a href="{{ route('job.listings') }}" class="btn btn-sm">See More</a>
                </div>

                @endif
            @endif

       
       


    </div>
</div>


@endsection

@push('scripts')

@endpush
