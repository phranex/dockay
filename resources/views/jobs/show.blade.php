@extends('layouts.master')

@push('styles')
{{--  <link href="{{asset('assets2/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet">  --}}

<style>
        .job-details {
            float: left;
            width: 100%;
            padding-top: 20px;
        }
        .job-details h3 {
            float: left;
            width: 100%;
            font-family: Open Sans;
            font-size: 15px;
            color: #202020;
            margin-bottom: 15px;
            margin-top: 10px;
        }
        .recent-jobs > h3 {
            float: left;
            width: 100%;
            font-family: Open Sans;
            font-size: 15px;
            color: #202020;
        }
        .job-listings-sec {
            float: left;
            width: 100%;
        }
        .job-listings-sec {
            float: left;
            width: 100%;
        }
        .job-listings-sec.no-border .job-listing {
            border: 2px solid #ffffff;
        }
        .job-list-modern .job-listing.wtabs {
            margin: 0;
            margin-top: 0px;
            -webkit-border-radius: 0 0;
            -moz-border-radius: 0 0;
            -ms-border-radius: 0 0;
            -o-border-radius: 0 0;
            border-radius: 0 0;
            border-left-color: #ffffff;
            border-right-color: #ffffff;
            border-top-color: #edeff7;
            border-bottom-color: #edeff7;
            margin-top: -1px;
            padding: 30px 0px;
        }

        .job-listing.wtabs {
            border: 1px solid #ebefef;
            margin-top: 30px;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            -ms-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
            display: inherit;
            text-align: left;
            position: relative;
        }
        .job-listing.wtabs .job-title-sec {
            float: left;
            width: 70%;
        }

        .c-logo {
            float: left;
            width: 130px;
            text-align: center;
        }
        .c-logo img {
            float: none;
            display: inline-block;
            max-width: 100%;
        }
        .job-title-sec h3 {
            display: table;
            font-size: 15px;
            font-family: Open Sans;
            color: #202020;
            margin: 0;
            margin-bottom: 0px;
            margin-bottom: 7px;
            margin-top: 3px;
        }

        .job-listing.wtabs .job-title-sec > span {
            color: #1e83f0;
            display: table;
            float: none;
        }
        .job-listing.wtabs .job-lctn {
            display: inline;
            padding-top: 20px;
            width: 100%;
            font-size: 13px;
        }

        .job-listing.wtabs .job-lctn i {
            float: none;
            font-size: 15px;
        }

        .job-lctn i {
            font-size: 24px;
            float: left;
            margin-right: 7px;
        }

        .recent-jobs {
            float: left;
            width: 100%;
            padding-top: 20px;
        }
        .job-details p, .job-details li {
            float: left;
            width: 100%;
            font-size: 13px;
            color: #888888;
            line-height: 24px;
            margin: 0;
            margin-bottom: 19px;
        }
        .job-details > ul {
            float: left;
            width: 100%;
            margin-bottom: 20px;
        }
        .job-details > ul li {
            float: left;
            width: 100%;
            margin: 0;
            margin-bottom: 0px;
            position: relative;
            padding-left: 23px;
            line-height: 21px;
            margin-bottom: 10px;
            font-size: 13px;
            color: #888888;
        }
        ul {
            list-style: outside none none;
            margin: 0 0 30px;
            padding: 0;
        }
        .job-details > ul li::before{
            position: absolute;
            left: 0;
            top: 13px;
            width: 10px;
            height: 1px;
            background: #888888;
            content: "";
        }
        .similar-box ul li:hover{
            padding-left: 5px;
            transition: .5s;
        }
        .similar-box ul li::before{

            content: "-";
            width: 10px;
            height: 1px;
            margin: 10px;
        }

        .job-single-head {
            float: left;
            width: 100%;
            padding-bottom: 30px;
            border-bottom: 1px solid #e8ecec;
            display: table;
        }
        .job-thumb {
            display: table-cell;
            vertical-align: top;
            width: 107px;
        }
        .job-thumb img {
            float: left;
            width: 100%;
            border: 2px solid #e8ecec;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            -ms-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
        }
        .share-bar {
            float: left;
            width: 100%;
            padding-top: 20px;
            padding-bottom: 20px;
            border-top: 1px solid #e8ecec;
            border-bottom: 1px solid #e8ecec;
        }

        .share-bar span {
            float: left;
            font-size: 15px;
            color: #202020;
            line-height: 40px;
            margin-right: 14px;
        }
        .share-bar a.share-fb {
            color: #3b5998;
            border-color: #3b5998;
        }

        .share-bar a {
            float: none;
            display: inline-block;
            width: 47px;
            height: 35px;
            border: 2px solid;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            -ms-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
            line-height: 30px;
            font-size: 18px;
            margin: 0 5px;
            margin-top: 0px;
            text-align: center;
            margin-top: 0px;
            margin-top: 6px;
        }
        .share-bar a.share-twitter {
            color: #1da1f2;
            border-color: #1da1f2;
        }
        .share-bar a.share-twitter:hover {
            background: #1da1f2;
            border-color: #1da1f2;
            color: #ffffff !important;
        }
        .share-bar a.share-fb:hover {
            background: #3b5998;
            border-color: #3b5998;
            color: #ffffff !important;
        }

        .job-title-sec span {
            float: left;
            font-family: Open Sans;
            font-size: 13px;
            margin-top: 1px;
        }
        .job-title-sec {
            display: table-cell;
            vertical-align: middle;
            width: 60%;
        }

        .job-listing {
            float: left;
            width: 100%;
            display: table;
            border-bottom: 1px solid #e8ecec;
            padding: 20px 0;
            background: #ffffff;
            border-left: 2px solid #ffffff;
            padding-right: 30px;
        }
        .job-lctn {
            display: table-cell;
            vertical-align: middle;
            font-family: open Sans;
            font-size: 13px;
            color: #888888;
            line-height: 23px;
            width: 25%;
        }

        .job-head-info {
            display: table-cell;
            vertical-align: middle;
            padding-left: 25px;
        }

        .job-head-info h4 {
            float: left;
            width: 100%;
            font-family: Open Sans;
            font-size: 15px;
            color: #202020;
            margin: 0;
            margin-bottom: 0px;
            margin-bottom: 10px;
        }

        .job-head-info span {
            float: left;
            width: 100%;
            font-size: 13px;
            color: #888888;
            line-height: 10px;
        }
        .job-head-info p {
            float: left;
            margin: 0;
            margin-top: 0px;
            margin-right: 0px;
            font-size: 13px;
            margin-right: 40px;
            color: #888;
            margin-top: 11px;
        }

        .job-head-info p i {
            float: left;
            font-size: 21px;
            line-height: 27px;
            margin-right: 9px;
        }

        .similar-box{
            margin: auto;
            clear: both;
        }

        .job-overview {
            float: left;
            width: 100%;
            margin-top: 30px;
            margin-bottom: 30px;
        }
        .job-overview > h3 {
            float: left;
            width: 100%;
            font-family: Open Sans;
            font-size: 15px;
        }
        .job-overview ul {
            float: left;
            width: 100%;
            border: 2px solid #e8ecec;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            -ms-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
            margin: 0;
            padding-left: 15px !important;
        }

        .job-overview ul > li {
            float: left;
            width: 100%;
            margin: 0;
            position: relative;
            padding-left: 67px;
            margin: 15px 0;
        }

        .job-overview ul > li i {
            position: absolute;
            left: 23px;
            top: 5px;
            font-size: 30px;
            color: #8b91dd;
        }

        .job-overview ul > li h3 {
            float: left;
            width: 100%;
            font-size: 13px;
            font-family: Open Sans;
            margin: 0;
        }

        .job-overview ul > li span {
            float: left;
            width: 100%;
            font-size: 13px;
            color: #888888;
            margin-top: 7px;
        }
</style>

@endpush

@push('title')
   Dockay | {{ $job->title }} 
@endpush

@section('content')
    <div>
     {{--  @include('inc.messages')  --}}


    </div>
    
    <div class="container  card">
        <div class="row card-body ">
            <div class="col-lg-9 mt-20">
                <div class="row">
                        <div class="job-single-head ">
                                <div class="job-thumb"> <img src="{{ displayDefaultPhoto($job->facility->user) }}" alt=""> </div>
                                <div class="job-head-info">
                                    <h4><a href="{{route('user.profile.view', $job->facility->user->username)}}">{{$job->facility->name}}</a>
                                        {{-- @auth
                                        @if($job->facility_id == auth()->user()->facility->id)
                                        <small style="font-size:15px" class="">
                                        <a href="{{route('user.adverts.edit', $job->id)}}"><i class="fa fa-edit"></i></a>
                                        </small>
                                        @endif
                                        @endauth --}}
                                    </h4>
                                    <span>{{$job->facility->user->state}}, {{$job->facility->user->country}}</span>
                                    {{--  <p><i class="fa fa-unlink"></i> www.jobhunt.com</p>  --}}
                                    <p><i class="fa fa-phone"></i>{{$job->facility->user->phoneNumber}}</p>
                                    {{-- <p><i class="fa fa-envelope-o"></i> ali.tufan@jobhunt.com</p> --}}
                                </div>
                            </div>
                </div>

                <div class="row">
                        <div class="job-details">
                                <h3>Job Title  </h3>
                                <p>{{$job->title}}</p>
                                <h3>Job Description</h3>
                                <p>{{$job->description}}</p>
                                <h3>Required Knowledge, Skills, and Abilities</h3>
                                <ul>
                                    @if(isset($job->skills))
                                        @if(count($job->skills))
                                            @foreach ($job->skills as $skill)
                                                <li>{{title_case($skill->description)}}</li>
                                            @endforeach
                                        @endif
                                    @endif
                                </ul>

                            </div>
                </div>


                    <div class="share-bar ">
                        <span>Share</span><a target='_blank' href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&src=sdkpreparse"" title="" class="share-fb"><i class="fa fa-facebook"></i></a>
                        <a class="twitter-share-button" data-size="large"
  data-text="custom share text"
  
  data-hashtags="example,demo" data-hashtags='jealthJobs, dockay.ng, medicalJobs' target='_blank' href="https://twitter.com/intent/tweet?text= Hi there, heres a new job ad on dockay.ng. {{ title_case($job->title) }} . Copy this link to apply {{ url()->current() }}. Get more medical/health jobs at {{ url('/') }}. Retweet to let inform more of your friends.&hastags=medicalJobs" title="" data-mobile-iframe="true" class="share-twitter"><i class="fa fa-twitter"></i></a>
                    </div>

                



            </div>
            <div class="col-lg-3 mt-20">
                    @auth
                        @if($job->facility->user->id == auth()->id())
                            <a href="{{route('user.adverts.edit', $job->id)}}" class="btn btn-block btn-lg" ><i class="fa fa-edit"></i> Edit</a>
                        @else
                            @if(auth()->user()->userType == 1)
                                @if(!$user_has_applied)
                                   @if($job->mode == 'dockay')
                                        <button data-toggle="modal" data-target="#exampleModalCenter"  class="btn btn-block btn-lg">Apply</button>
                                    @elseif($job->mode == 'p')
                                        <i>Send an application to <a href='mailto:{{ $job->facility->user->email }}?subject={{ $job->title }}'>{{ $job->facility->user->email }}</a></i>
                                    @else
                                        @if(filter_var($job->mode, FILTER_VALIDATE_EMAIL) )
                                        <i>Send an application to <a href='mailto:{{ $job->mode }}?subject={{ $job->title }} from {{ config('app.name') }}.ng'>{{ $job->mode }}</a> Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>
                                        @elseif(filter_var($job->mode, FILTER_VALIDATE_URL))
                                        <i>Visit <a target='_blank' href='{{ $job->mode }}'>{{ $job->mode }}</a> to apply. Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>
                                        @else
                                        <i>Call <a href='tel:{{ $job->mode }}'>{{ $job->mode }}</a> to apply. Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>

                                        @endif
                                    @endif 
                                @else
                                <button style="color:#009344 !important" class="btn btn-link text-success btn-lg">You Applied for this job</button>
                                @endif

                                @if(!$has_subscribed)
                                    <a href="{{route('user.subscriptions.store', $job->id)}}" class="btn btn-outline pull-right">Subscribe</a>
                                @else
                                <a  class="btn btn-link pull-right">Subscribed</a>
                                @endif
                            @endif
                        @endif
                    @else
                        @if($job->mode == 'dockay')
                                        <button data-toggle="modal" data-target="#exampleModalCenter"  class="btn btn-block btn-lg">Apply</button>
                                    @elseif($job->mode == 'p')
                                        <i>Send an application to <a href='mailto:{{ $job->facility->user->email }}?subject={{ $job->title }}'>{{ $job->facility->user->email }}</a></i>
                                    @else
                                        @if(filter_var($job->mode, FILTER_VALIDATE_EMAIL) )
                                        <i>Send an application to <a href='mailto:{{ $job->mode }}?subject={{ $job->title }} from {{ config('app.name') }}.ng'>{{ $job->mode }}</a> Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>
                                        @elseif(filter_var($job->mode, FILTER_VALIDATE_URL))
                                        <i>Visit <a target='_blank' href='{{ $job->mode }}'>{{ $job->mode }}</a> to apply. Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>
                                        @else
                                        <i>Call <a  href='tel:{{ $job->mode }}'>{{ $job->mode }}</a> to apply. Tell employer you found them on dockay.ng. Do not send money to any employer. Please report any employer requesting for money to us.</i>

                                        @endif
                        @endif                    
                @endauth


                <div class="job-overview">
                        <h3>Job Overview @if(!$job->isActive) <strong class='text-danger'>Expired <i class='em em-cry'></i></strong>@endif</h3>
                        <ul>
                            <li><i class="fa fa-money"></i><h3>Offerd Salary</h3><span>@if(isset($job->min)) #{{number_format($job->min,2,'.',',')}} - #{{number_format($job->max,2,'.',',')}} @else N/A @endif</span></li>
                            <li><i class="fa fa-mars-double"></i><h3>Gender</h3><span>{{$job->gender}}</span></li>
                            {{-- <li><i class="fa fa-thumb-tack"></i><h3>Career Level</h3><span>Executive</span></li> --}}
                            <li><i class="fa fa-puzzle-piece"></i><h3>Industry</h3><span>Health ({{$job->specialty->name}})</span></li>
                            {{-- <li><i class="fa fa-shield"></i><h3>Experience</h3><span>2 Years</span></li> --}}
                            <li><i class="fa fa-clock-o "></i><h3>Deadline</h3><span>{{when($job->deadline)}}</span></li>
                        </ul>
                </div>
                <div  class="row card similar-box">
                    <p class="text-center mt-20">Similar Jobs</p>
                    <ul class="card-body">
                        @if(isset($relatedJobs))
                            @if(count($relatedJobs))
                                @foreach ($relatedJobs as $job)
                                    <li><a href="{{route('job.details', $job->id)}}">{{$job->title}}</a></li>
                                @endforeach
                            @else
                                <li> No jobs</li>

                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>






 <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$job->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="resumeadd-form">
                <form enctype="multipart/form-data" id='apply-form' action='{{route('user.applications.store')}}' method='post'>
                    @csrf
                    <div class="row text-center">
                            <div class="col-lg-12">
                                <p> Suitable files are .pdf,.docx &amp; .doc</p>
                            </div>
                            <div style="border:none !important" class="text-center w-100 mb-5 error">

                            </div>
                            <div class="col-12">
                                    <input type='hidden' name='job' value='{{$job->id}}' />
                                @auth
                                <label data='upd' class="btn btn-sm btn-outline cvUpload" for="upd">Upload CV</label>

                                    @if(isset(auth()->user()->practitioner->cv->path))
                                        <label class="btn btn-outline cvUpload" for='myCv'>Use my {{config('app.name')}} cv</label>
                                        <input type="checkbox" id='brandCv' class="hide-input"  />
                                    @endif
                                @else
                                <a class='btn btn-xs btn-success pull-right mt-10' href="{{route('login')}}?p={{base64_encode(url()->current())}}">Login to Apply</a>
                                @endauth
                            </div>
                            <div id='uploadBox' style="display:none" class="col-lg-12">

                                <div class="col-xl-6 m-auto">
                                        <div class="card m-b-30">
                                            <div class="card-body">
                                                    <input type="file" name='cv'  id="input-file-now" class="dropify" />
                                                    <input type="hidden" name='id'  value='' class=" transaction-id" />
                                                    <label for="def">Set as Default</label><input id="def" type="checkbox" name='default' value="N" />
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-12">
                                    @auth
                                    <button type='submit' id='upd_btn' class='btn btn-xs btn-success pull-right mt-10'>Apply</button>


                                    @endauth
                            </div>

                        </div>
                    </div>
                </form>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>

@endsection

@push('scripts')
{{--  <script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
    <script src='{{asset('assets2/pages/upload-init.js')}}'></script>  --}}

    <script type="text/javascript">
$(document).ready(function() {

    $('.cvUpload').click(function(){
        $('.cvUpload').removeClass('btn-active');
        $('div.error').text('');
        $(this).addClass('btn-active');
        if($(this).attr('data') == 'upd'){
            $('.submit').hide();
            $('#uploadBox').show();
            $('#brandCv').prop('checked', false);

        }
        else{
            $('.submit').show();
            $('#input-file-now').val('');
            $('.dropify-clear').trigger('click');
            $('#brandCv').prop('checked', true);
            $('#uploadBox').hide();
        }

    });
  $('#input-file-now').change(function(){
      console.log($(this).val());
    if($(this).val().length > 0){
        var ext = $(this).val().split(".").pop().toLowerCase();
        console.log(ext);
        if(ext =="pdf" || ext == "docx" || ext == "doc"){
             $('#fileStat').text('1 File Selected. Click to change');
            $('#fileName').text("File: "+$(this).val().replace(/C:\\fakepath\\/i, ''));
            $('div.error').text('');
            $('#defaultCV').show();
                        $('#upd_btn').removeAttr('disabled');

            $('#brandCv').prop('checked', false);

        }else{
            $('div.error').text('Only .pdf,.docx and .doc files are allowed');
            $('#upd_btn').attr('disabled', 'disabled');
            $('.dropify-clear').trigger('click');

        }

    }else{
        $('#fileStat').text('Upload CV');
    }
  });



  $('#apply-form').submit(function(){
      event.preventDefault();

      if($('#brandCv').is(':checked')){

          $(this).unbind('submit').submit();
      }else if($('#input-file-now').val().length > 0){
          if($('[name=default]').is(':checked')){
              $('[name=default]').val('Y');
          }
           $(this).unbind('submit').submit();
      }else{

          $('div.error').text('Please select an option before applying.');
      }
  });
});
</script>
@endpush
