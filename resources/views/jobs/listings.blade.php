@extends('layouts.master')

@push('styles')
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">
<style>
    .refine-box, .refine-content{
        padding: 10px !important;

    }

    .refine-box h5 a{
        font-size: 14px !important;
    }

    .contain{
        font-size: 12px !important;
    }

    .checkmark{
        height: 15px;
        width: 15px;
        top:21%;
    }

    @media (max-width: 620px){
        .pt-1 input, .pt-1 a i {
            padding: 10px !important;
        }
    }

     ::placeholder{
        font-size:13px;
    }
    

    .contain .checkmark:after{
        left: 5px;
    top: 2px;
    }
</style>
@endpush

@push('title')
    Dockay | {{ request('query') }} Vacancies

@endpush

@section('content')
  <div class="container-fluid " >
      <div class="row">
          <div class="col-xl-2 car fixe">
                <div class="card m-b-30">
                        <div class="card-body">
                            <p id='refine' class="mb-0 font-14">Refine Your results <small class='pull-right d-block d-sm-none cursor-pointer'><i class='fa fa-bars'></i></small></p>

                        </div>
                </div>
                <div id="accordion" class='d-none d-sm-block' role="tablist" aria-multiselectable="true">
                         {{--  location  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#location" aria-expanded="false" aria-controls="collapseThree">
                                      Location
                                    </a>
                                </h5>
                            </div>
                            <div id="location" class="collapse @if(!empty(request('location')))show @endif" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                        <form  method="get" action="{{url()->full()}}" class="mb-0">
                                                <div class="form-row row align-items-center">

                                                    <div class="col-12">
                                                        <label class="sr-only" for="inlineFormInputGroup">Location</label>
                                                        <span class="bmd-form-group"><div class="input-group">
                                                            <div class="input-group-prepend">
                                                            </div>
                                                            <input name="location" id='loc' class="form-control" value='{{ request('location') }}' placeholder="location">
                                                        </div></span>
                                                    </div>

                                                    <div class="col-12">
                                                        <button type="button" id='locationBtn' class=" mt-15 font-10 btn btn-outline pull-right">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                </div>
                            </div>
                        </div>
                        {{--  specialty  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingOne">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="text-dark">
                                        Specialty
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse @if(!empty(request('specialty')))show @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body refine-content">
                                    @if(isset($specialties))
                                        @if(count($specialties))
                                            @foreach ($specialties as $specialty)
                                                <label class="contain"> {{title_case($specialty->name)}}
                                                    <input  @if(request('specialty') == $specialty->id) checked @endif  name='specialty' value='{{$specialty->id}}' type="radio" name="type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif


                                </div>
                            </div>
                        </div>
                        {{--  category  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingTwo">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Job Type
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse @if(!empty(request('category')))show @endif" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-body refine-content">
                                        @if(isset($categories))
                                        @if(count($categories))
                                            @foreach ($categories as $category)
                                            <label class="contain">{{title_case($category->name)}}
                                                    <input @if(request('category') == $category->id) checked @endif name="category"  value='{{$category->id}}' type="radio" name="type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>
                        {{--  establishment  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Establishment
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse @if(!empty(request('establishment')))show @endif" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    @if(isset($establishments))
                                        @if(count($establishments))
                                            @foreach ($establishments as $establishment)
                                                <label class="contain"> {{title_case($establishment->name)}}
                                                    <input @if(request('establishment') == $establishment->id) checked @endif name='establishment' value='{{$establishment->id}}' type="radio" name="type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>

                        {{--  gender  --}}
                        <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#gender" aria-expanded="false" aria-controls="collapseThree">
                                        Gender
                                    </a>
                                </h5>
                            </div>
                            <div id="gender" class="collapse @if(!empty(request('gender')))show @endif" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    <label class="contain">Male
                                        <input @if(request('gender') == 'male') checked @endif name='gender' value='male' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="contain">Female
                                        <input  @if(request('gender') == 'female') checked @endif name='gender' value='female' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="contain">Any
                                        <input  @if(request('gender') == 'any') checked @endif name='gender' value='any' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                       
                        {{--  facility  --}}
                        {{--  <div class="card">
                            <div class="card-header refine-box" role="tab" id="headingThree">
                                <h5 class="mb-0 mt-0 font-16">
                                    <a class="collapsed text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Coy Name
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-body refine-content">
                                    <label class="contain">Facility/Institution
                                        <input value='#F' type="radio" name="type">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>  --}}
                    </div>
          </div>
          <div class="mt-2 col-xl-7 m-aut">
                <div class="c">
                        <div class="card m-b-30">
                            <div class="card-body">
                                 <div class="list-inline-item app-search app-search-2 flex" >
                                    <form action='{{route('job.listings')}}' method='get' role="search" class="m-auto w-100">
                                        @csrf
                                        <div class="form-group pt-1">
                                            <input name='query' type="text" class="form-control no-padding s-ico w-100" value='{{ request('query') }}' placeholder="Search by discipline e.g Optometry">
                                             <a href="#"><i class="fa fa-search no-padding s-ico"></i></a>
                                        </div>
                                    </form>

                                </div>
                                    <small class='pull-right text-right'>Show <input type='number' class='form-control inline' value='{{request('page_size')}}' style='width:30% !important' id='page_size' placeholder='20'/> Results</small>

                            </div>
                        </div>
                    </div>
                @if(isset($jobs))
                    @if(count($jobs))
                        @foreach ($jobs as $item)
                        <div class="card m-b-30 ">
                                <div class="card-header">
                                <div style="height:55px">
                                        <img style='width:60px' src="{{ displayDefaultPhoto($item->facility->user)}}" alt="user" class="rounded-circle avatar-img img-fluid">
                                        <a href="{{route('user.profile.view', $item->facility->user->username)}}"><span class='poster-details'>{{title_case($item->facility->name)}}</span></a>
                                        <span style='margin-left:5px' class='pt job-type'>
                                        {{$item->category->name}}
                                        </span>
                                        
                                        <span class='pt job-type'>
                                        {{$item->specialty->name}}
                                        </span>

                                    </div>

                                </div>
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                    <h5 class='post-title'><a href="{{route('job.details', $item->id)}}">{{$item->title}}</a></h5>
                                    <p class="post-content">{{ substr($item->description, 0, 150) }} ...</p>
                                    <footer class="blockquote-footer">posted {{when($item->created_at)}}</footer>
                                    </blockquote>
                                    <div class="card-body flex  text-center">
                                    <p class="col more-details"><i class="text-success fa fa-money"></i> @if(isset($item->min)) #{{ format_number($item->min) }} - #{{ format_number($item->max)}} @else N/A @endif</p>
                                    <p class="col more-details"><i class=" text-danger fa fa-clock-o"></i> {{ when($item->deadline)}}</p>
                                    <p class="col more-details"><i class="fa fa-map-marker"></i> {{ $item->state}} , {{ $item->country}}</p>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    @else
                        <div class="card  bg-transparent m-t-30 dockay-card">
                            <div class="alert alert-info text-center">

                                    Sorry, No jobs matching your request was found.
                            </div>
                        </div>

                    @endif
                @endif


                 {{ $jobs->links()  }}

          </div>

          <div class="col-xl-3">
            <div class="card dockay-card animated infinite shake">
                <div >
                    <h5 class=''>Need to hire ? <small><a href='{{ route('register') }}'>Get Started</small></a></h5>
                </div>
            </div>
            <div class="card dockay-card mt-10">
                
                <p class="text-right">Random Job Posts</p>
                <small class="text-muted">Job information as posted by other dockay users</small>
                <hr/>

                @if(isset($items))
                    @if(count($items))
                        @foreach ($items as $item)
                            <div style="border:1px solid grey" class=" m-b-30 ">
                                    <div class="card-header">
                                    <div >
                                            {{--  <img style="width:55px" src="@if(isset($item->user->profileImage->path)){{ $item->user->profileImage->path }} @else {{ displayDefaultPhoto($item->user) }} @endif" alt="user" class="rounded-circle avatar-img img-fluid">  --}}
                                       posted by <a href="{{route('user.profile.view', $item->user->username)}}"><span class='poster-details'>{{$item->user->username}}</span></a>
                                        <span class='pt job-type'>
                                            {{$item->category->name}}

                                        </span>
                                        <span style="background:transparent;color:#64bdf9;border-radius:none;font-size:12px" class=' job-type'>

                                            <i class="fa fa-{{$item->gender}}"></i>
                                        </span>


                                        </div>

                                    </div>
                                    <div class="card-body">
                                        <blockquote class="blockquote mb-0">
                                        <h5 class='post-title'>{{$item->description}}</h5>
                                        <footer class="blockquote-footer">Posted <small>{{when($item->created_at)}}</small></footer>
                                        </blockquote>
                                        <div class="card-body flex  text-center">
                                        {{--  <p class="col more-details">{{$item->gender}}</p>  --}}
                                        <p class="col more-details">{{$item->specialty->name}}</p>
                                        <p class="col more-details">{{$item->establishment->name}}</p>
                                         @if($item->allow_whatsapp || $item->contact)
                                                    <p class="col more-details"><a onclick="window.open('{{ env('WHATSAPP_URL') }}phone={{convertToInternationalNumber($item->contact)}}&text=I am interested in the following job posted on {{ env('APP_URL') }}. {{  title_case($item->description)}}')" href='javascript:void()'><i class='fa fa-whatsapp'></i></a></p>
                                                    @endif
                                        <p class="col more-details">
                                            @if(auth()->id() == $item->user_id)
                                                    <i data='{{$item->id}}' class="fa fa-trash cursor-pointer delete"></i>
                                                @else
                                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-share">Share</i></a>
                                            @endif
                                        </p>
                                        </div>
                                    </div>
                            </div>
                        @endforeach
                    @endif

                @endif

            </div>
          </div>
      </div>
  </div>

  <!-- Modal for items -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Share </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="flex text-center">
                    <a href='#' class='col social-reg '><i class="fa fa-facebook w-100"></i></a>
                    <a href='#' class='col social-reg '><i class="fa fa-twitter w-100"></i></a>
                    <a href='#' class='col social-reg '><i class="fa fa-google-plus w-100"></i></a>
                    <a href='#' class='col social-reg '><i class="fa fa-whatsapp w-100"></i></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-danger ml-2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('js/validation.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('#refine small').click(function(){
                $('#accordion').toggleClass('d-block d-sm-none, d-none d-sm-block').fadeIn();
            });

            $('[name=specialty]').change(function(){
                var sp = $(this).val();
                search('specialty',sp);
            });
            $('#locationBtn').click(function(){
                var sp = $('#loc').val();
                search('location',sp);
            });
            $('#page_size').blur(function(){
                var sp = $(this).val();
                search('page_size',sp);
            });
             $('#page_size').keydown(function(e){
                if(e.which == 13){
                    var sp = $(this).val();
                    search('page_size',sp);
                }
            });
           
            $('[name=category]').change(function(){
                var sp = $(this).val();
                search('category',sp);
            });
            $('[name=gender]').change(function(){
                var gender = $(this).val();
                search('gender',gender);
            });
            $('[name=establishment]').change(function(){
                var sp = $(this).val();
                search('establishment',sp);
            });

        });

        function search(x,value){
            var param = getUrlParameter(x);
            {{--  var url = @if(env('APP_ENV') != 'local') "{!! str_replace('http', 'https',url()->full()) !!}" @else "{!! url()->full() !!}"  @endif;  --}}
            var url = "{!! url()->full() !!}";
            if(x == 'specialty'){
                var url = changeUrl(url,'query','');
            }
            if(param  != undefined){


                var url = changeUrl(url,'query','{{request('query')}}');

                var currentUrl = changeUrl(url,x,value);
                console.log(currentUrl +' '+value);
                window.location.href = currentUrl;
            }
            else{
                var currentUrl = url;
                if(currentUrl.indexOf('?') !== -1)
                    window.location.href = currentUrl+'&'+x+'='+value;
                else
                    window.location.href = currentUrl+'?'+x+'='+value;
            }
        }

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }

        function changeUrl(uri,key,value){
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }

        }

    </script>
@endpush
