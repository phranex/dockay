@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link rel="stylesheet" href="{{asset('css2/profile.css')}}">
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">

    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }
    body{
        background: #eaeaea
    }

    form{
        padding: 0 30px;
    }

    .bmd-form-grou{
        padding-top: unset !important;
    }

    </style>

@endpush

@push('title')
    {{ auth()->user()->username }} | Edit Profile

@endpush
@section('content')
<div class="container-fluid">
        @include('inc.activation')
        <div class="row">
                {{-- @include('inc.user.profile') --}}

                <div class="col-md-12 car bg-transparent m-auto dockay-card col-xl-8">
                        <div class="card m-b-30">
                                <div class="card-body">

                                        <h4 class="mt-0 header-title">Profile Settings <small style="float:right"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small></h4>

                                        <!-- Nav tabs -->
                                        <ul class="nav nav-pills nav-justified" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true"><i class="fa fa-user"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#education" role="tab" aria-selected="false"><i class="fa fa-graduation-cap"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#work" role="tab" aria-selected="false"><i class="fa fa-briefcase"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#social" role="tab" aria-selected="false"><i class="fa fa-globe"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false"><i class="fa fa-cog"></i></a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane pt-3 active show" id="profile" role="tabpanel">
                                                <p class="font-14 mb-0">
                                                        <form style="padding: 0 30px" class="form-inline mb-0 row">
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputName3" class="bmd-label-floating">First Name *</label>
                                                                    <input id="fn" required type="text" name='firstName' value="{{auth()->user()->practitioner->firstName}}" class="form-control w-100" id="exampleInputName3">
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputName3" class="bmd-label-floating">Last Name *</label>
                                                                    <input id="ln" required type="text" name='lastName' value="{{auth()->user()->practitioner->lastName}}" class="form-control w-100" id="exampleInputName3">
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputMobile3" class="bmd-label-floating">Mobile No</label>
                                                                    <input id="pn" type="number" name="phoneNumber" value="{{auth()->user()->phoneNumber}}" class="form-control w-100" id="exampleInputMobile3">
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">Gender
                                                                    </label>
                                                                    <select id="gender" name='gender' class="form-control w-100" name="gender">
                                                                        <option value="">Select</option>
                                                                        <option @if(auth()->user()->practitioner->gender == 'male') selected @endif value="male">Male</option>
                                                                        <option @if(auth()->user()->practitioner->gender == 'female') selected @endif value="female">Female</option>

                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputEmail3" class="bmd-label-floating">Username</label>
                                                                    <input id="un" type="text" name='username' value="{{auth()->user()->username}}" class="form-control w-100" id="exampleInputEmail3">
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">Specialty</label>
                                                                   <select id="q" class="form-control w-100" name="qualification">
                                                                        <option  value=''>Select</option>
                                                                        @if(isset($specialties))
                                                                            @if(count($specialties))
                                                                                @foreach ($specialties as $specialty)
                                                                                    <option @if(auth()->user()->practitioner->specialty_id == $specialty->id) selected @endif value="{{$specialty->id}}">{{$specialty->name}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        @endif

                                                                   </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">Country</label>
                                                                   <select id="country" class="form-control w-100" name="country">
                                                                       <option>Select</option>

                                                                   </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">State</label>
                                                                   <select id='state' class="form-control w-100" name="state">
                                                                       <option>State</option>

                                                                   </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-12 bmd-form-group">
                                                                    <label class="bmd-label-floating">Email</label>
                                                                    <input type='email' class='form-control w-100' disabled value='{{ auth()->user()->email }}' readonly />
                                                                </div>
                                                                <div class="form-group col-12 bmd-form-group">
                                                                    <label for="exampleTextarea" class="bmd-label-floating">Describe yourself</label>
                                                                    <textarea id='desc' name="description" required class="form-control w-100" id="exampleTextarea3" rows="3">{{auth()->user()->description}}</textarea>
                                                                </div>
                                                                <div class="form-grou bmd-form-group pt-3 col-12"> <!-- needed to match padding for floating labels -->
                                                                    {{-- <button type="button" data-target='#education' class="btn btn-raised btn-primary next mb-0">Next</button> --}}
                                                                    <button type="button" class=" upd-user-profile btn btn-raised btn-danger ml-1 mb-0">Update</button>
                                                                </div>
                                                        </form>
                                                </p>
                                            </div>
                                            <div class="tab-pane pt-3" id="education" role="tabpanel">
                                                <p class="font-14 mb-0">
                                                        <div class="">
                                                            <h5>Add/Edit Education</h5>
                                                                @if(isset($disciplines))
                                                                    @if(count($disciplines))
                                                                        <a href="javascript:void()" title="" class='addEducation'>
                                                                            <i class="fa fa-plus"></i>
                                                                        </a>
                                                                     @endif
                                                                @endif
                                                        </div>



                                                        <div @if(isset($disciplines)) @if(count($disciplines)) style='display:none' @endif @endif class="resumeadd-form edu-box">
                                                            <div class="row">
                                                                    <form  class="form-inline mb-0 row">
                                                                            <div class="form-group col-12 col-lg-12 bmd-form-group">
                                                                                <label for="exampleInputName3" class="bmd-label-floating">Discipline *</label>
                                                                                <input id="discipline" required type="text" name='firstName' value="" class="form-control w-100" >
                                                                            </div>
                                                                            <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                                <label for="exampleInputName3" class="bmd-label-floating">From *</label>
                                                                                <input id="from" required type="date" name='lastName'  class="form-control w-100" >
                                                                            </div>
                                                                            <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                                <label for="exampleInputMobile3" class="bmd-label-floating">To *</label>
                                                                                <input id="to" type="date" name="phoneNumber"  class="form-control w-100">
                                                                            </div>
                                                                            <div class="form-group col-12 col-lg-12 bmd-form-group">
                                                                                <label for="exampleInputMobile3" class="bmd-label-floating">Institution</label>
                                                                                <input id="institution" type="text" name="phoneNumber"  class="form-control w-100" >
                                                                            </div>



                                                                            <div class="form-group col-12 bmd-form-group">
                                                                                <label for="exampleTextarea" class="bmd-label-floating">Brief Description</label>
                                                                                <textarea id='edu-desc' name="description" required class="form-control w-100" rows="3"></textarea>
                                                                            </div>
                                                                            <div class="form-grou bmd-form-group pt-3 col-12"> <!-- needed to match padding for floating labels -->
                                                                                {{-- <button type="button" data-target='#work' class="btn btn-raised btn-primary next mb-0">Next</button> --}}
                                                                                <button type="button" class=" add-education btn btn-raised btn-danger ml-1 mb-0">Update</button>
                                                                            </div>
                                                                    </form>


                                                            </div>
                                                        </div>

                                                        @if(isset($disciplines))
                                                            @if(count($disciplines))
                                                                <div class="edu-history-sec">
                                                                @foreach ($disciplines as $discipline)

                                                                    <div class="edu-history">
                                                                        <i class="fa fa-graduation-cap"></i>
                                                                        <div class="edu-hisinfo">
                                                                            <h3>{{$discipline->discipline}}</h3>
                                                                            <i>{{\Carbon\Carbon::parse($discipline->from)->format('M/Y')}} - {{\Carbon\Carbon::parse($discipline->to)->format('M/Y')}}</i>
                                                                            <span>{{$discipline->institution}} <i>{{$discipline->discipline}}</i></span>
                                                                            <p>{{$discipline->description}}</p>
                                                                        </div>
                                                                        <ul class="action_job">
                                                                            <li><span>Edit</span><a href="javascript:void()" class='education-edit' data='{{$discipline}}' title=""><i class="fa fa-pencil"></i></a></li>
                                                                            <li><span>Delete</span><a href="javascript:void()" class='education-delete'  data='{{$discipline->id}}' title=""><i class="fa fa-trash-o"></i></a></li>
                                                                        </ul>
                                                                    </div>


                                                                @endforeach
                                                                </div>
                                                            @endif
                                                        @endif

                                                </p>
                                            </div>
                                            <div class="tab-pane pt-3" id="work" role="tabpanel">
                                                <p class="font-14 mb-0">
                                                    <div class="">
                                                            <h5>Add/Edit Work Experience</h5>
                                                                @if(isset($experiences))
                                                                    @if(count($experiences))
                                                                        <a href="javascript:void()" title="" class='addWork'>
                                                                            <i class="fa fa-plus"></i>
                                                                        </a>
                                                                     @endif
                                                                @endif
                                                        </div>

                                                        {{--  @if(isset($experiences)) @if(count($experiences))
                                                        <div class="border-title">Add/Edit Work Experience
                                                        <a href="javascript:void()" class='addWork' title=""><i class="fa fa-plus"></i></a></div>
                                                        @endif @endif  --}}

                                                        <div @if(isset($experiences)) @if(count($experiences)) style='display:none' @endif @endif class="resumeadd-form work-box">
                                                                <form class='form-inline mb-0 row'>
                                                            <div class="row">
                                                                   

                                                                    <div class="col-lg-12 form-group bmd-form-grou">

                                                                        <div class="col-12">
                                                                         <span class="pf-title">Title</span>
                                                                            <input id='title' class='work-data form-control w-100 col-12' placeholder="Job Title" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-5 form-group bmd-form-grou">
                                                                        <span class="pf-title">From Date</span>
                                                                        <div class="">
                                                                            <input class='work-data form-control' id='work-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-5 work-end form-group bmd-form-grou">
                                                                        <span class="pf-title">To Date</span>
                                                                        <div class="">
                                                                            <input  class='work-data form-control work-ended' id='work-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-2 form-group bmd-form-grou">
                                                                        <span class="pf-titl">I work here</span>
                                                                        <label class="contain h-100 inline">
                                                                            <input id='work-present' value='P' type="checkbox" class='work-ended' name="cb" >
                                                                            {{--  <label class='work-ended' for="work-present">I work Here</label>  --}}
                                                                            <span style='bottom: 0;left: 0;top:unset;right:unset' class="checkmark"></span>
                                                                        </label>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 form-group bmd-form-grou">

                                                                        <div class="w-100">
                                                                        <span class="pf-title">Name of Company</span>
                                                                            <input type="text" id='company' class='work-data form-control w-100 col-12' placeholder='Name of Company'>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 form-group bmd-form-grou">
                                                                        <span class="pf-title">Brief Description</span>
                                                                        <div class="col-12">
                                                                            <textarea class='work-data form-control col-12 w-100' id='work-desc'></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                            {{-- <button class='upd-education' type="button">Update</button> --}}
                                                                            <button class='add-work  btn btn-raised btn-danger ml-1 mb-0' type="button">Add</button>
                                                                    </div>

                                                            </div>
                                                        </form>
                                                        </div>
                                                        @if(isset($experiences))
                                                            @if(count($experiences))
                                                            <div class="edu-history-sec">
                                                                @foreach ($experiences as $experience)

                                                                    <div class="edu-history style2">
                                                                        <i></i>
                                                                        <div class="edu-hisinfo">
                                                                            <h3>{{$experience->title}} <span>at {{$experience->company}}</span></h3>
                                                                            <i>{{\Carbon\Carbon::parse($experience->from)->format('M/Y')}} - @if(strtolower($experience->to) == 'p') Present  @else {{\Carbon\Carbon::parse($experience->to)->format('M/Y')}} @endif</i>
                                                                            <p>{{$experience->description}}</p>
                                                                        </div>
                                                                        <ul class="action_job">
                                                                            <li><span>Edit</span><a href="javascript:void()" data='{{$experience}}' class='edit-work-exp' title=""><i class="fa fa-pencil"></i></a></li>
                                                                            <li><span>Delete</span><a href="javascript:void()" data='{{$experience->id}}' class='delete-work-exp' title=""><i class="fa fa-trash-o"></i></a></li>
                                                                        </ul>
                                                                    </div>


                                                                @endforeach

                                                            </div>
                                                            @endif
                                                        @endif

                                                </p>
                                            </div>
                                            <div class="tab-pane pt-3" id="social" role="tabpanel">
                                                <p class="font-14 mb-0">
                                                        <form>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Facebook</span>
                                                                        <div class="">
                                                                            <input type="url" class="form-control" id='facebook' value='{{auth()->user()->facebook}}' placeholder="e.g http://facebook.com/dockay">
                                                                            <i class="fa fa-facebook"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Twitter</span>
                                                                        <div class="">
                                                                            <input type="url" class="form-control" id='twitter' value='{{auth()->user()->twitter}}' placeholder="e.g http://twitter.com/dockay">
                                                                            <i class="fa fa-twitter"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Google</span>
                                                                        <div class="">
                                                                            <input type="url" class="form-control" id='google-plus' value='{{auth()->user()->googleplus}}' placeholder="e.g http://google-plus.com/dockay">
                                                                            <i class="fa fa-google"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Linkedin</span>
                                                                        <div class="">
                                                                            <input type="url" class="form-control" id='linkedin' value='{{auth()->user()->linkedin}}' placeholder="e.g http://Linkedin.com/dockay">
                                                                            <i class="fa fa-linkedin"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 text-right">
                                                                        {{-- <button class='upd-education' type="button">Update</button> --}}
                                                                        <button class='add-social btn btn-raised btn-danger ml-1 mb-0' type="button">Submit</button>
                                                                </div>
                                                                </div>

                                                            </form>

                                                </p>
                                            </div>
                                            <div class='tab-pane pt-3' id='settings' role='tabpanel'>
                                                <div class='container'>
                                                    <div class='row'>
                                                        <div class='col-12'>
                                                            <h4>Change Password</h4>
                                                            <hr/>
                                                            <form action='{{ route('user.changePassword') }}' class="form-inline mb-0 row" method='post'>
                                                               <p class="font-14 mb-0">

                                                                        @csrf

                                                                        @if(!empty(auth()->user()->password))
                                                                            <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                                <label for="exampleInputName3" class="bmd-label-floating">Current Password *</label>
                                                                                <input id="password" required type="password" name='current' value="" class="form-control w-100" id="exampleInputName3">
                                                                            </div>
                                                                        @else
                                                                            <h6 class='col-12 text-center text-bold'>Please create a password</h6>
                                                                        @endif
                                                                            <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                                <label for="exampleInputName3" class="bmd-label-floating">New Password *</label>
                                                                                <input id="password" required type="password" name='password' value="" class="form-control w-100" id="exampleInputName3">
                                                                            </div>
                                                                            <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                                <label for="exampleInputName3" class="bmd-label-floating">Confirm Password *</label>
                                                                                <input id="password" required type="password" name='password_confirmation' value="" class="form-control w-100" id="exampleInputName3">
                                                                            </div>


                                                                            <div class="form-grou bmd-form-group pt-3 col-12"> <!-- needed to match padding for floating labels -->
                                                                                {{-- <button type="button" data-target='#education' class="btn btn-raised btn-primary next mb-0">Next</button> --}}
                                                                                <button type="submit" class="  btn btn-raised btn-danger ml-1 mb-0">Update</button>
                                                                            </div>
                                                                </p>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                </div>
        </div>

</div>


<!-- education Modal -->
<div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Education</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" ">
        <form action='#' id='edit-education-form'>
            <div class="row">

                <div class="col-lg-12">
                    <span class="pf-title">Discipline</span>
                    <div class="">
                        <input required id='edit-discipline' class='edit-education-data form-control' placeholder="Course of Study" type="text">
                    </div>
                </div>
                <div class="col-lg-6">
                    <span class="pf-title">From Date</span>
                    <div class="">
                        <input required class='edit-education-data form-control' id='edit-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-6">
                    <span class="pf-title">To Date</span>
                    <div class="">
                        <input required class='edit-education-data form-control' id='edit-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Institute</span>
                    <div class="">
                        <input type="text" required id='edit-institution' class='edit-education-data form-control' placeholder='Name of Institution'>
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Brief Description</span>
                    <div class="">
                        <textarea required class='edit-education-data form-control' id='edit-edu-desc'></textarea>
                    </div>
                    <input type='hidden' id='edit-edu-id' />
                </div>
                <div class="col-lg-12">
                        {{-- <button class='upd-education' type="button">Update</button> --}}
                        <button class='edit-education btn btn-raised btn-danger ml-1 mb-0 mt-20' type="submit">Update</button>
                </div>

            </div>
         </form>
        </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>


<!-- work Modal -->
<div class="modal fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Work Experience</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" ">
        <form action='#' id='edit-work-form'>
             <div class="row">
                <div class="col-lg-12">
                    <span class="pf-title">Job Title</span>
                    <div class="">
                        <input id='edit-title' class='edit-work-data form-control' placeholder="Job Title" type="text">
                    </div>
                </div>
                <div class="col-lg-5">
                    <span class="pf-title">From Date</span>
                    <div class="">
                        <input class='edit-work-data form-control' id='edit-work-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-5 edit-work-end">
                    <span class="pf-title">To Date</span>
                    <div class="">
                        <input  class='edit-work-data form-control edit-work-ended' id='edit-work-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-2">
                    <p style='    float: right;
    margin-top: 70px;' class="remember-label">
                        <input id='edit-work-present' value='P' type="checkbox" class='edit-work-ended' name="cb" ><label class='work-ended' for="edit-work-present">I Work Here</label>
                    </p>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Company</span>
                    <div class="">
                        <input type="text" id='edit-company' class='work-data form-control' placeholder='Name of Company'>
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Brief Description</span>
                    <div class="">
                        <textarea class='edit-work-data form-control form-control' id='edit-work-desc'></textarea>
                    </div>
                </div>
                <input type='hidden' id='work_id' />
                <div class="col-lg-12">
                        {{-- <button class='upd-education' type="button">Update</button> --}}
                        <button class=' btn btn-raised btn-danger ml-1 mb-0' type="submit">Update</button>
                </div>
            </div>
         </form>
        </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>





@endsection






@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
    //$('.upd-social,.upd-profile,.upd-contact').hide();

        $(document).ready(function(){
            populateCountries("country", "state", '{{auth()->user()->country}}', '{{auth()->user()->state}}');
            $('.profile,.social,.contact').attr('disabled','disabled');
            $('.upd-social,.upd-profile,.upd-contact').hide();

            $('.edit-profile').click(function(){
                $(this).hide();
                $('.profile').removeAttr('disabled');
                $('.upload-info').removeClass('invisible');
                $('.upd-profile').show()
            });
            $('.edit-social').click(function(){
                $(this).hide();
                $('.social').removeAttr('disabled');
                $('.upd-social').show()
            });
            $('.edit-work').click(function(){
                $(this).hide();
                $('.work').removeAttr('disabled');
                $('.upd-work').show();
            });
           // $('.edit-education').click(function(){
             //   $(this).hide();
               // $('.education').removeAttr('disabled');
                //$('.upd-education').show();
            //});

            $('#edit-education-form').submit(function(){
                event.preventDefault();

                var formData = [];
                formData['#edit-discipline'] = 'required|alpha';
                formData['#edit-from'] = 'required';
                formData['#edit-to'] = 'required';
                formData['#edit-institution'] = 'required';
                formData['#edit-edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#edit-discipline').val(),
                        "from": $('#edit-from').val(),
                        "to": $('#edit-to').val(),
                        "institution": $('#edit-institution').val(),
                        "description": $('#edit-edu-desc').val(),
                        "education_id": $('#edit-edu-id').val()

                    }
                    console.log(data);
                    if(!displayAlert($('#edit-from').val(),$('#edit-to').val())){
                        editEducation(data);
                    }


                }


            });
            $('#edit-work-form').submit(function(){
                event.preventDefault();
                var formData = [];
                formData['#edit-title'] = 'required|alpha';
                formData['#edit-work-from'] = 'required';

                formData['#edit-company'] = 'required';
                formData['#edit-work-desc'] = 'required|max=500';
                if($('#edit-work-present').is(':checked')){
                   var  end = $('#edit-work-present').val();

                }else if(!isEmpty('#edit-work-to')){

                    var end = $('#edit-work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#edit-title').val(),
                            "from": $('#edit-work-from').val(),
                            "to": end,
                            "company": $('#edit-company').val(),
                            "description": $('#edit-work-desc').val(),
                            "work_id": $('#work_id').val()
                        }
                        console.log(data);
                        if(!displayAlert($('#edit-work-from').val(),end)){
                            editExperience(data);
                        }

                    }
                }else{
                    $('.edit-work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }
            });


            $('.upd-user-profile').click(function(){
                var formData = [];
                formData['#fn'] = 'required|alpha';
                formData['#ln'] = 'required|alpha';
                formData['#pn'] = 'number';
                formData['#un'] = 'required';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#gender'] = 'required';
                formData['#q'] = 'required';
                formData['#desc'] = 'max=500';

                if(validate(formData, 'field-error')){
                    var data = {
                        "firstName": $('#fn').val(),
                        "lastName": $('#ln').val(),
                        "username": $('#un').val(),
                        "phoneNumber": $('#pn').val(),
                        "gender": $('#gender').val(),
                        "qualification": $('#q').val(),
                        "country": $('#country').val(),
                        "state": $('#state').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateProfile(data);
                }
            });
            $('.add-education').click(function(){
                var formData = [];
                formData['#discipline'] = 'required|alpha';
                formData['#from'] = 'required';
                formData['#to'] = 'required';
                formData['#institution'] = 'required';
                formData['#edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }

                    if(!displayAlert($('#from').val(),$('#to').val())){
                        addEducation(data);
                    }

                    console.log(data);

                }
            });


            $('.upd-education').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    if(!displayAlert($('#from').val(),$('#to').val())){
                        updateEducation(data);
                    }

                }
            });
            $('#work-present').click(function(){

                if($(this).is(':checked')){
                   $('.work-end').hide();
                }else{
                    $('.work-end').show();
                }
                $('.work-ended').removeClass('field-error');
            });
            $('#edit-work-present').click(function(){

                if($(this).is(':checked')){
                   $('.edit-work-end').hide();
                }else{
                    $('.edit-work-end').show();
                }
                $('.edit-work-ended').removeClass('field-error');
            });
            $('.add-work').click(function(){
                var formData = [];
                formData['#title'] = 'required|alpha';
                formData['#work-from'] = 'required';

                formData['#company'] = 'required';
                formData['#work-desc'] = 'required|max=500';
                if($('#work-present').is(':checked')){
                   var  end = $('#work-present').val();

                }else if(!isEmpty('#work-to')){
                    console.log($('#work-to').val());
                    var end = $('#work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#title').val(),
                            "from": $('#work-from').val(),
                            "to": end,
                            "company": $('#company').val(),
                            "description": $('#work-desc').val(),

                        }
                        if(!displayAlert($('#work-from').val(),end)){
                            addWork(data);
                        }

                        console.log(data);

                    }
                }else{
                    $('.work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }

            });
            $('.upd-work').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });

             $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data, '{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}');
                }

            });



            $('.addEducation').click(function(){
                $('.edu-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });
            $('.addWork').click(function(){
                $('.work-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });

        });
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function() {
  $('.next').click(function(){
      var target = $(this).attr('data-target');
        $('a[href=#education]').tab('show');
  });
})
</script>



<script>
    $(document).ready(function(){
        $('#image-upload').change(function(){
            if($(this).val() != ''){
                $('#upload-btn').show();
            }else{
                $('#upload-btn').hide();
            }
        });




        $('.education-edit').click(function(){
             $('.edit-education-data').val();
            var data = JSON.parse($(this).attr('data'));
            $('#edit-discipline').val(data.discipline);
            $('#edit-to').val(data.to);
            $('#edit-from').val(data.from);
            $('#edit-institution').val(data.institution);
            $('#edit-edu-desc').val(data.description);
            $('#edit-edu-id').val(data.id);
             $('#educationModal').modal();

        });
        $('.edu-edit').click(function(){

             $('#educationModal').modal();

        });
        $('.edit-work-exp').click(function(){
            $('.edit-work-data').val('');
            var data = JSON.parse($(this).attr('data'));
            $('#edit-title').val(data.title);

            $('#edit-work-from').val(data.from);
            $('#edit-company').val(data.company);
            $('#edit-work-desc').val(data.description);
            $('#work_id').val(data.id);
            if(data.to == 'P'){
                //check the present box
                $('#edit-work-present').prop('checked', true);
                //hide the to input
                $('.edit-work-end').hide();
            }else{
                $('#edit-work-to').val(data.to);
                $('.edit-work-end').show();
                $('#edit-work-present').removeAttr('checked');
            }

             $('#workModal').modal();

        });

        $('.education-delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Education!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.education.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        $('.delete-work-exp').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Work Experience!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.work.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });



    function displayAlert(from, to){
        if(to == 'P'){
            return false;
        }
        var from = new Date(from);
            var to = new Date(to);
            if(from.getTime() > to.getTime()){
                $.alert({
                    title: 'Error!',
                    type: 'red',
                    icon: 'fa fa-times',
                    content:'Please check the dates entered',
                });
                return true;
            }
        return false;
    }

</script>


@endpush
