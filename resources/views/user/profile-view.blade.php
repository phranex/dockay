@extends('layouts.master')
@push('styles')
    <style>
            .checked {
                color: orange;
            }

            .socials {
    padding: 6px;
      font-size: 14px;
      width: 25px;
    text-align: center;
    text-decoration: none;
    margin: 5px 2px;
    color: white !important;
  }

  .fa:hover {
      opacity: 0.7;
  }

  .fa-facebook {
    background: #3B5998;
    color: white;
  }

  .fa-twitter {
    background: #55ACEE;
    color: white;
  }

  .fa-google {
    background: #dd4b39;
    color: white;
  }

  .fa-linkedin {
    background: #007bb5;
    color: white;
  }

  .caption div h3{
    color:#64bdf9;
    font-weight:bolder;
    font-size:50px;
        text-shadow: 3px 2px 4px black;
  line-height: 1;
}

@media (max-width:720px){
    .prof{
        display: block;
    }

    .profile-info{
        text-align: center
    }
}


    </style>

@endpush

@push('title')
    {{ $user->username }} | Profile

@endpush
@section('content')

<div class="container-fluid">

       <div class="row">
            <div class="col-md-12  col-xl-9">
                <div class="card dockay-card d-inline-box prof col-lg-11">
                    <div class="profile-pix col-md-3 ">
                        <img class="img-fluid rounded-circle h-100 d-none d-md-block" src="@if(isset($user->profileImage->path)){{ $user->profileImage->path }} @else {{ displayDefaultPhoto($user) }} @endif"/>
                        <img class="img-fluid m-auto rounded-circle h-100  d-block d-md-none " src="@if(isset($user->profileImage->path)){{ $user->profileImage->path }} @else {{ displayDefaultPhoto($user) }} @endif"/>
                    </div>
                    <div class="profile-info col-md-9">
                        <h5>
                            <strong> {{getFullName($user)}}</strong><small> @if(isset($user->practitioner->specialty->name)) ({{ @$user->practitioner->specialty->name }}) @endif</small>
                            @auth
                            @if(strtolower(request('name')) == strtolower(auth()->user()->username))
                                <a href="{{route('user.profile', getFullNameWithSlug(auth()->user()))}}"><i class="fa fa-edit"></i></a>
                            @endif
                            @endauth

                            <small class="pull-right"><a style="font-size:12px" href="{{url()->previous()}}"><i class="fa fa-backward "> Go Back</i></a></small>
                        </h5>

                        {{--  <p>MD, Eye Masters</p>  --}}
                        <div>

                           @if(isset($user->facebook)) <a href="{{ $user->facebook }}" target='_blank' class="fa fa-facebook socials"></a>@endif
                            @if(isset($user->twitter))<a href="{{ $user->twitter }}" target='_blank' class="fa fa-twitter socials"></a>@endif
                            @if(isset($user->google_plus))<a href="{{ $user->google_plus }}" target='_blank' class="fa fa-google socials"></a>@endif
                            @if(isset($user->linkedin))<a href="{{ $user->linkedin }}" target='_blank' class="fa fa-linkedin socials"></a>@endif
                        </div>
                        <p><i class="fa fa-map"></i> {{$user->state}},{{$user->country}}</p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        {{--  <p> - has {{ $user->followers->count() }} followers.</p>  --}}
                        @auth
                            @if(auth()->user()->followings->count())
                                @if($user->id == auth()->id())
                                
                                    <p> - You are following <a href='{{ route('followers.index', 'fg') }}'  >{{ auth()->user()->followings->count() }}</a> users.</p>
                                    
                                @else
                                    <p> - is  following {{ $user->followings->count() }} users.</p>
                                @endif
                            @endif
                        @else
                            
                            <p> - is following {{ $user->followings->count() }} users.</p>

                        @endauth

                        <div class="follow-buttons">
                            @auth
                                @if(auth()->user()->isFollowing($user))
                                     <a href='{{ route('unFollowUser', $user->username) }}' class="btn btn-link">Unfollow</a>
                                @else
                                    @if(auth()->id() != $user->id)
                                     <a href='{{ route('followUser', $user->username) }}' class="btn btn-primary">Follow</a>
                                     @endif
                                @endif
                           
                            @endauth
                            {{--  <a class="btn btn-primary">Insights</a>
                            <a class="btn btn-primary">Contact</a>  --}}
                        </div>
                    </div>
                </div>
                <div class="card m-t-20 dockay-card col-lg-11">
                    <h6 class="profile-heading">Summary</h6>
                    <p class="profile-desc">
                        {{$user->description}}
                    </p>

                    <div class="box">
                            <h6 class="profile-heading">Education</h6>
                            <ul class="other-info-list">
                                @if(isset($user->practitioner->educations))
                                    @if(count($user->practitioner->educations))
                                        @foreach ($user->practitioner->educations as $education)
                                            <li>
                                                <span>Studied <span class='text-muted'>{{$education->discipline}}</span></span> <i class="text-muted">at</i> {{$education->institution}} <br/>
                                                <span style='font-size:12px'>{{$education->description}}</span><br/>
                                                <span style='font-size:12px' class='text-muted'>From {{getDateFormat($education->from, 'M-Y')}} to @if($education->to != 'P'){{getDateFormat($education->to, 'M-Y')}} @else Present @endif</span>
                                            </li>
                                        @endforeach

                                    @endif
                                @endif

                            </ul>
                    </div>
                    <div class="box">
                            <h6 class="profile-heading">Experience</h6>
                            <ul class="other-info-list">
                                    @if(isset($user->practitioner->experiences))
                                        @if(count($user->practitioner->experiences))
                                            @foreach ($user->practitioner->experiences as $experience)
                                                <li>
                                                    <span>@if($experience->to != 'P') worked as @else works as @endif {{$experience->title}}</span> <i class="text-muted">at</i> {{$experience->company}} <br/>
                                                    <span  style='font-size:12px' class='text-muted'>{{$experience->description}}</span><br/>
                                                    <span  style='font-size:12px' class='text-muted'>From {{getDateFormat($experience->to, 'M-Y')}} to @if($experience->to != 'P'){{getDateFormat($experience->to, 'M-Y')}} @else Present @endif</span>
                                                </li>
                                            @endforeach

                                        @endif
                                    @endif

                                </ul>
                    </div>


                </div>
                <div class="card m-t-20 dockay-card col-lg-11">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-justified" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link active show" data-toggle="tab" href="#job" role="tab" aria-selected="true">Jobs</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" data-toggle="tab" href="#article" role="tab" aria-selected="false">Answers</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" data-toggle="tab" href="#post" role="tab" aria-selected="false">Posts</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane pt-3 active show" id="job" role="tabpanel">
                                    <div class="card m-b-30 ">
                                        @if(isset($user->randomJobs))
                                            @if(count($user->randomJobs))
                                                @foreach ($user->randomJobs as $job)
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title'>{{$job->description}}</h5>
                                                        {{-- <p class="post-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p> --}}
                                                        <footer class="blockquote-footer">Posted {{when($job->created_at)}}</footer>
                                                        </blockquote>

                                                    </div>
                                                @endforeach
                                            @else
                                                <div class='text-center dockay-card'>{{  $user->username }} is yet to post a job</div>
                                            

                                            @endif
                                        @endif
                                    </div>
                            </div>
                            <div class="tab-pane pt-3" id="article" role="tabpanel">
                                    <div class="card m-b-30 ">

                                        @if(isset($user->answers))
                                            @if(count($user->answers))
                                                @foreach ($user->answers as $answer)
                                                    @if(!$answer->question->isAnonymous)
                                                        <div class="card-body">
                                                            <blockquote class="blockquote mb-0">
                                                            <h5 class='post-title text-bold text-capitalize'><a href='{{ route('question.show', $answer->question->slug) }}#a{{ $answer->id }}'>{{$answer->question->title}}?</a></h5>
                                                            <p class="post-content"> {!! $answer->answer !!}</p>
                                                            <footer class="blockquote-footer">Posted {{when($answer->created_at)}}</footer>
                                                            </blockquote>

                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class='text-center dockay-card'>{{  $user->username }} is yet to answer a question</div>

                                            @endif
                                        @endif
                                    </div>
                            </div>
                            <div class="tab-pane pt-3" id="post" role="tabpanel">
                                    <div class="card m-b-30 ">

                                             @if(isset($user->products))
                                            @if(count($user->products))
                                                @foreach ($user->products as $product)
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title text-bold text-capitalize'><a href='{{ route('product.details', $product->id) }}'>{{$product->title}}?</a></h5>
                                                        {{--  <p class="post-content"> {!! $product->answer !!}</p>  --}}
                                                        <footer class="blockquote-footer">Posted {{when($product->created_at)}}</footer>
                                                        </blockquote>

                                                    </div>
                                                @endforeach
                                            @else
                                                <div class='text-center dockay-card'>{{  $user->username }} is yet to sell an item</div>

                                            @endif
                                        @endif
                                    </div>
                            </div>
                            <div class="tab-pane pt-3" id="" role="tabpanel">

                            </div>
                        </div>


                </div>
            </div>
            
            {{--  <div class="col-md-12 col-xl-3">
                <div class="card  dockay-card ">
                    <div class="card-header">
                        <span>Other Doctors</span>
                    </div>  --}}
                    
                    @if(false)
                        @if(count($doctors))
                            @foreach($doctors as $doctor)
                                @if($doctor->user->username == $user->username)
                                    @continue
                                @endif
                                <div class=" d-inline-box row m-t-10">
                                    <div class="profile-pix h-100 col-4 col-md-5 ">
                                        <img class="img-fluid  rounded-circle  " src=" @if(isset($doctor->user->profileImage->path)) {{ @$doctor->user->profileImage->path }} @else {{ displayDefaultPhoto(@$doctor->user) }} @endif"/>

                                    </div>
                                    <div class="profile-info  col-8 col-md-8">
                                        <h6><strong><a href='{{ route('user.profile.view', $doctor->user->username) }}'>{{ title_case($doctor->firstName) }} {{ title_case($doctor->lastName) }}</strong></a></h6>
                                        {{--  <span>MD, Eye Masters</span><br/>  --}}

                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>

                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @endif


                    {{--  <div class="option text-right w-100">
                        <a href="#" class="btn btn-link"> See More</a>
                    </div>


                </div>


            </div>  --}}
            
       </div>


</div>


@auth
         <div class="modal fade" id="followers" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">People You are Following</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="flex text-center">
                            <div>
                                <div class=" card d-inline-box row m-t-10">
                                    <div class="profile-pix h-100 col-4 col-md-5 ">
                                        <img style='width:150px' class="img-fluid  rounded-circle  " src=" @if(isset($user->profileImage->path)) {{ @$user->profileImage->path }} @else {{ displayDefaultPhoto(@$user) }} @endif"/>

                                    </div>
                                    <div class="profile-info  col-8 col-md-8">
                                        <h6><strong><a href='{{ route('user.profile.view', $user->username) }}'>Etoka Francis</strong></a></h6>
                                        {{--  <span>MD, Eye Masters</span><br/>  --}}
                                         <a href='{{ route('followUser', $user->username) }}' class="btn btn-outline">Unfollow</a>
                                       

                                    </div>
                                </div>


                              

                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
       @endauth
@endsection

@push('scripts')
    <Script>
        $(document).ready(function(){
            $('.getFollows').click(function(){
                getFollowings('.follow');
            });
        });
    </Script>

@endpush
