@extends('layouts.job-user')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">

@endpush
@section('content')
    <section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background:  url({{asset('images/resource/mslider1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome Tera Planer</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-3 column border-right">
				 		<div class="widget">
				 			<div class="tree_widget-sec">
				 				@include('inc.user.nav')
				 			</div>
				 		</div>
				 	</aside>
				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="profile-title">
					 			<h3>Post a New Job</h3>

					 		</div>
					 		<div class="profile-form-edit">
					 			<form id='job-form' action='{{route('user.adverts.store')}}' method='post'>
                                    @csrf
					 				<div class="row">
					 					<div class="col-lg-12">
					 						<span class="pf-title">Job Title *</span>
					 						<div class="pf-field">
					 							<input  class='post-job' name='title' id='title' type="text" placeholder="e.g Medical Officer" />
					 						</div>
					 					</div>


					 					<div class="col-lg-4">
					 						<span class="pf-title">Job Type *</span>
					 						<div class="pf-field">
					 							<select  name='category' id='category' class=" post-job chose">
													<option value=''>Select</option>
													@if(isset($categories))
                                                        @if(count($categories))
                                                            @foreach ($categories as $category)
                                                                <option value='{{$category->id}}'>{{$category->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endif
												</select>
					 						</div>
					 					</div>
					 					<div class="col-lg-4">
					 						<span class="pf-title">Specialty *</span>
					 						<div class="pf-field">
					 							<select name='specialty' id='specialty' class="post-job chose">
													<option value=''>Select</option>
													@if(isset($specialties))
                                                        @if(count($specialties))
                                                            @foreach ($specialties as $specialty)
                                                                <option value='{{$specialty->id}}'>{{$specialty->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endif
												</select>
					 						</div>
					 					</div>
					 					<div class="col-lg-4">
					 						<span class="pf-title">Establishment *</span>
					 						<div class="pf-field">
					 							<select name='establishment' id='establishment' class="post-job chose">
													<option value=''>Select</option>
													@if(isset($establishments))
                                                        @if(count($establishments))
                                                            @foreach ($establishments as $establishment)
                                                                <option value='{{$establishment->id}}'>{{$establishment->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endif
												</select>
					 						</div>
					 					</div>

					 					<div class="col-lg-4">
					 						<span class="pf-title">Gender *</span>
					 						<div class="pf-field">
					 							<select name='gender' id='gender' class="post-job chose">
													<option value=''>Select</option>
													<option value='any'>Any</option>
													<option value='male'>Male</option>
													<option value='female'>Female</option>

												</select>
					 						</div>
					 					</div>

					 					<div class="col-lg-8">
					 						<span class="pf-title">Application Deadline *</span>
					 						<div class="pf-field">
					 							<input  name='deadline' id='deadline' type="date"   class="form-control post-job datepicker" />
					 						</div>
					 					</div>
					 					<div class="col-lg-6">
					 						<span class="pf-title">Minimum Salary</span>
					 						<div class="pf-field">
					 							<input  name='min' id='min' type="number" min='1000' placeholder='1000'  class="form-control post-job " />
					 						</div>
					 					</div>
					 					<div class="col-lg-6">
					 						<span class="pf-title">Maximum Salary</span>
					 						<div class="pf-field">
					 							<input  name='max' id='max' type="number"  min='1000' placeholder='1000'  class="form-control post-job " />
					 						</div>
					 					</div>

                                         <div class="col-lg-12">
					 						<span class="pf-title">Description *</span>
					 						<div class="pf-field">
					 							<textarea class='post-job' name='description' id='description'></textarea>
					 						</div>
					 					</div>

                                        <div class="col-lg-12">
					 						<span class="pf-title">Relevant skills  (add as many skills as required for the job with each separated by a  comma)</span>
					 						<div class="pf-field" id="tags">
					 							<input type="text" placeholder='e.g Ability to handle the ophthalmoscope' class="labelinpu">
                                                 <input type="hidden" name='skills' value="" id="skills">
					 						</div>
					 					</div>


                                        <div class='col-lg-12 text-right' style='margin-bottom:10px'>
                                            <button class='post' type='submit'>Post</button>
                                        </div>





					 				</div>
					 			</form>
					 		</div>

					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>


<script>

    $(document).ready(function(){

        $('#job-form').submit(function(){
            event.preventDefault();
            var formData = [];
            formData['#title'] = 'required|alpha';
            formData['#category'] = 'required';
            formData['#specialty'] = 'required';
            formData['#establishment'] = 'required';
            formData['#gender'] = 'required';
            formData['#deadline'] = 'required';
            formData['#min'] = 'number';
            formData['#max'] = 'number';
            formData['#description'] = 'required|min=100';
            if(validate(formData, 'field-error')){
                if(checkSalaryFields('#min', '#max', 'field-error')){
                      var data = {
                        "title": $('#title').val(),
                        "category": $('#category').val(),
                        "specialty": $('#specialty').val(),
                        "establishment": $('#establishment').val(),
                        "gender": $('#gender').val(),
                        "deadline": $('#deadline').val(),
                        "min": $('#min').val(),
                        "max": $('#max').val(),
                        "skills": $('#skills').val(),
                        "description": $('#description').val()

                    }
                    console.log(data);
                    $('#job-form').unbind().submit();
                }
            }else{
                $.notify('Please fix the errors and resubmit', 'error');
            }

        });

        $('#tags').tagInput({
            labelClass:"badge badge-success"

        });
    });

</script>

@endpush
