@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<!-- Dropzone css -->
<link href="{{asset('assets2/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }

    </style>

@endpush
@push('title')
    {{ auth()->user()->username }} | Edit Profile

@endpush
@section('content')

<div class="container-fluid">
        @include('inc.activation')
        <div class="row">
                {{--  @include('inc.messages')  --}}

                <div class="col-md-12  bg-transparent m-auto dockay-card col-xl-8">
                        <div class="card m-b-30">
                                <div class="card-body">

                                        <h4 class="mt-0 header-title">Profile Settings <small style="float:right"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small></h4>

                                        <!-- Nav tabs -->
                                        <ul class="nav nav-pills nav-justified" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true"><i class="fa fa-user"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link " data-toggle="tab" href="#gallery" role="tab" aria-selected="true"><i class="fa fa-photo"></i></a>
                                            </li>

                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#social" role="tab" aria-selected="false"><i class="fa fa-globe"></i></a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false"><i class="fa fa-cog"></i></a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane pt-3 active show" id="profile" role="tabpanel">
                                                <p class="font-14 mb-0">
                                                        <form style="padding: 0 30px" class="form-inline mb-0 row">
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputName3" class="bmd-label-floating">Facility Name *</label>
                                                                    <input id="name" required type="text" name='name' value="{{auth()->user()->facility->name}}" class="form-control w-100" id="exampleInputName3">
                                                                </div>

                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputMobile3" class="bmd-label-floating">Mobile No</label>
                                                                    <input id="pn" type="number" name="phoneNumber" value="{{auth()->user()->phoneNumber}}" class="form-control w-100" id="exampleInputMobile3">
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">Establishment
                                                                    </label>
                                                                    <select id="est" name='gender' class="form-control w-100" name="gender">
                                                                        <option value="">Select</option>
                                                                        @if(isset($establishments))
                                                                                        @if(count($establishments))
                                                                                            @foreach ($establishments as $establishment)
                                                                                                <option @if($establishment->id == auth()->user()->facility->establishment_id) selected @endif value='{{$establishment->id}}'>{{$establishment->name}}</option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endif
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label for="exampleInputEmail3" class="bmd-label-floating">Username</label>
                                                                    <input id="un" type="text" name='username' value="{{auth()->user()->username}}" class="form-control w-100" id="exampleInputEmail3">
                                                                </div>

                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">Country</label>
                                                                   <select id="country" class="form-control w-100" name="country">
                                                                       <option>Select</option>

                                                                   </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-6 bmd-form-group">
                                                                    <label class="bmd-label-floating">State</label>
                                                                   <select id='state' class="form-control w-100" name="state">
                                                                       <option>State</option>

                                                                   </select>
                                                                </div>
                                                                <div class="form-group col-12 col-lg-12 bmd-form-group">
                                                                    <label class="bmd-label-floating">Email</label>
                                                                    <input type='email' class='form-control w-100' disabled value='{{ auth()->user()->email }}' readonly />
                                                                </div>
                                                                <div class="form-group col-12 bmd-form-group">
                                                                    <label for="exampleTextarea" class="bmd-label-floating">Describe your facility</label>
                                                                    <textarea id='desc' name="description" required class="form-control w-100" id="exampleTextarea3" rows="3">{{auth()->user()->description}}</textarea>
                                                                </div>
                                                                <div class="form-grou bmd-form-grou pt-3 text-right col-12"> <!-- needed to match padding for floating labels -->
                                                                    {{-- <button type="button" data-target='#education' class="btn btn-raised btn-primary next mb-0">Next</button> --}}
                                                                    <button type="button" class=" upd-user-profile btn btn-raised btn-danger ml-1 mb-0">Update</button>
                                                                </div>
                                                        </form>
                                                </p>
                                            </div>
                                            <div class="tab-pane pt-3 " id="gallery" role="tabpanel">
                                                @if(isset(auth()->user()->mediaObjects))
                                                    @if(count(auth()->user()->mediaObjects))
                                                        <form enctype="multipart/form-data" method="post" action="{{route('user.media.store')}}">
                                                            @csrf
                                                            <div class="row">
                                                                @foreach (auth()->user()->mediaObjects as $media)

                                                                        <div  class="col-md-6 media col-lg-4">

                                                                                    <div class="card m-b-30">

                                                                                        <div class="card-body">
                                                                                                <input type="text" image-id={{$media->id}}  name='oldGallery' data-default-file={{$media->path}}  id="input-file-now" class="dropify" />
                                                                                                <input type="hidden" name='id'  value='' class=" transaction-id" />

                                                                                        </div>
                                                                                    </div>

                                                                        </div>





                                                                @endforeach
                                                            </div>
                                                            <div class='row' id='more'>
                                                                    <div style="display:none"  class="col-12">More Photo</div>
                                                            </div>
                                                            {{--  <div class='row' id='video'>
                                                                <div class="col-12">Video</div>
                                                            </div>  --}}
                                                            <div class="col-12 text-right">
                                                                <button type="button" id='addMore' class="btn btn-link">Add More Photo</button>
                                                                {{--  <button type="button" id='addVideo' class="btn btn-link">Add Video</button>  --}}
                                                                <button type="submit"  class="btn btn-outline">Upload</button>
                                                            </div>
                                                        </form>
                                                    @else
                                                        <form enctype="multipart/form-data" method="post" action="{{route('user.media.store')}}">
                                                            @csrf
                                                            <div class="row">
                                                                    <div class="col-md-6 col-lg-4">

                                                                            <div class="card m-b-30">

                                                                                <div class="card-body">
                                                                                        <input type="file" name='gallery[]'  id="input-file-now" class="dropify" />
                                                                                        <input type="hidden" name='id'  value='' class=" transaction-id" />

                                                                                </div>
                                                                            </div>

                                                                    </div>
                                                                    <div class="col-md-6 col-lg-4">

                                                                            <div class="card m-b-30">

                                                                                <div class="card-body">
                                                                                        <input type="file" name='gallery[]'  id="input-file-now" class="dropify" />
                                                                                        <input type="hidden" name='id'  value='' class=" transaction-id" />

                                                                                </div>
                                                                            </div>

                                                                    </div>
                                                                    <div class="col-md-6 col-lg-4">

                                                                            <div class="card m-b-30">

                                                                                <div class="card-body">
                                                                                        <input type="file" name='gallery[]'  id="input-file-now" class="dropify" />
                                                                                        <input type="hidden" name='id'  value='' class=" transaction-id" />

                                                                                </div>
                                                                            </div>

                                                                    </div>

                                                                    
                                                                    <div class='col-12 row' id='more'>
                                                                            <div style="display:none"  class="col-12">More Photo</div>
                                                                    </div>
                                                                    {{--  <div class='row' id='video'>
                                                                        <div class="col-12">Video</div>
                                                                    </div>  --}}
                                                                        <div class="col-12 text-right">
                                                                            <button type="button" id='addMore' class="btn btn-link">Add More Photo</button>
                                                                            {{--  <button type="button" id='addVideo' class="btn btn-link">Add Video</button>  --}}
                                                                            <button type="submit"  class="btn btn-outline">Upload</button>
                                                                        </div>
                                                            </div>

                                                        </form>
                                                    @endif
                                                @endif
                                            </div>



                                            <div class="tab-pane pt-3" id="social" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                            <form>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Facebook</span>
                                                                        <div class="pf-field">
                                                                            <input type="url" class="form-control" id='facebook' value='{{auth()->user()->facebook}}' placeholder="e.g http://facebook.com/dockay">
                                                                            <i class="fa fa-facebook"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Twitter</span>
                                                                        <div class="pf-field">
                                                                            <input type="url" class="form-control" id='twitter' value='{{auth()->user()->twitter}}' placeholder="e.g http://twitter.com/dockay">
                                                                            <i class="fa fa-twitter"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Google</span>
                                                                        <div class="pf-field">
                                                                            <input type="url" class="form-control" id='google-plus' value='{{auth()->user()->google_plus}}' placeholder="e.g http://google-plus.com/dockay">
                                                                            <i class="fa fa-google"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span class="pf-title">Linkedin</span>
                                                                        <div class="pf-field">
                                                                            <input type="url" class="form-control" id='linkedin' value='{{auth()->user()->linkedin}}' placeholder="e.g http://Linkedin.com/dockay">
                                                                            <i class="fa fa-linkedin"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 text-right">
                                                                        {{-- <button class='upd-education' type="button">Update</button> --}}
                                                                        <button class='add-social btn btn-raised btn-danger ml-1 mb-0' type="button">Submit</button>
                                                                </div>
                                                                </div>

                                                            </form>

                                                    </p>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                </div>
        </div>

</div>






@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
    <script src='{{asset('js/jquery.initialize.min.js')}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <!-- Dropzone js -->
    <script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
    <script src='{{asset('assets2/pages/upload-init.js')}}'></script>

    <script>
        $(document).ready(function(){
            uploadC = 1;
            populateCountries("country", "state", '{{auth()->user()->country}}', '{{auth()->user()->state}}');

            $('#addMore').click(function(){

                var uploadBox = "<div id=box"+uploadC+" class='col-md-6 col-lg-4'><div class='card m-b-30'><div class='card-body'>\
                                <input type='file' name='gallery[]'  id='input-file-now' class='dropify' />\
                                <input type='hidden' name='id'  value='' class='transaction-id'/>\
                                <button target=#box"+uploadC+" class='deleteBox pull-right btn btn-link text-danger'>Delete</button></div></div></div>\
                               ";
                uploadC++;
                $('#more').append(uploadBox);
                if($('#more div').css('display') == 'none')
                    $('#more div').show();
                $.initialize(".dropify", function() {
                    $(this).dropify();
                });
            });
            $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data, '{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}');
                }

            });
            $('#addVideo').click(function(){

                var uploadBox = "<div id=box"+uploadC+" class='col-md-6 col-lg-4'><div class='card m-b-30'><div class='card-body'>\
                                <input type='file' name='ideo[]'  id='input-file-now' class='dropify' />\
                                <input type='hidden' name='id'  value='' class='transaction-id'/>\
                                <button target=#box"+uploadC+" class='deleteBox pull-right btn btn-link text-danger'>Delete</button></div></div></div>\
                               ";
                uploadC++;
                $('#video').append(uploadBox);
                $.initialize(".dropify", function() {
                    $(this).dropify();
                });
            });

            $(document).on('click', '.deleteBox', function(){
                var target = $(this).attr('target');
                $(target).remove();
                if($('.dropify').length <= 3){
                    //$('#more div').hide();
                }
            });

            clear = $('.dropify').dropify();
            clear.on('dropify.beforeClear', function(event){
                event.preventDefault();
                var image_id = $(this).attr('image-id');
                var answer = confirm('Are you sure?');
                console.log(image_id);
                if(answer){
                    deleteMediaPicture(image_id, $(this).closest('.media'));
                    $(this).attr('name', 'gallery[]');
                    return true;
                }else
                    return false

                {{--  $.confirm({
                    title: 'Delete Photo!',
                    content: 'Are you sure?',
                    theme: 'supervan',
                    closeIcon: true,
                    buttons: {
                        Yes: function () {
                            deleteMediaPicture(image_id, $(this).closest('.media'));
                            $(this).attr('name', 'gallery[]');
                            return true;

                        },
                        

                    }
                });  --}}

            });
            $(document).on('click', '.dropify-clear', function(){


            });

            $('.upd-user-profile').click(function(){
                var formData = [];
                formData['#name'] = 'required|alpha';

                formData['#pn'] = 'number';
                formData['#un'] = 'required';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#est'] = 'required';

                formData['#desc'] = 'max=500';

                if(validate(formData, 'field-error')){
                    var data = {
                        "name": $('#name').val(),
                        "username": $('#un').val(),
                        "phoneNumber": $('#pn').val(),
                        "establishment": $('#est').val(),
                        "facility_id": '{{auth()->user()->facility->id}}',
                        "country": $('#country').val(),
                        "state": $('#state').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateCompanyProfile(data);
                }
            });
        });
    </script>

@endpush
