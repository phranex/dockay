@extends('layouts.job-user')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }

    </style>

@endpush
@section('content')



	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{auth()->user()->facility->name}} </h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-3 column border-right">
				 		<div class="widget">
				 			<div class="tree_widget-sec">
				 				@include('inc.user.nav')
				 			</div>
				 		</div>

				 	</aside>
				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
                            <div class='col-12 profile-tabs'>

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-home" aria-selected="true">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#education" role="tab" aria-controls="pills-profile" aria-selected="false">Education</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#work" role="tab" aria-controls="pills-contact" aria-selected="false">Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#social" role="tab" aria-controls="pills-contact" aria-selected="false">Social</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="profile-title">
                                                                <h3>My Profile</h3>
                                                                @include('inc.messages')
                                                                <div class="upload-img-bar" >
                                                                    <span class="round" style='background-image:url({{getProfileImage(auth()->user())}})' id="image-preview"><img class='profile-pix' src="" alt="" /></span>
                                                                    <div class="upload-info invisible">
                                                                      <label for="image-upload" id="image-label">Choose File</label>
                                                                      <form enctype="multipart/form-data" action='{{route('user.profileImage.store')}}' method='post'>
                                                                        @csrf
                                                                        <button type='submit' id='upload-btn' style='display:none' class='float-none'>Upload File</button>
                                                                        <input type='hidden' name='profile_image_id' value='{{auth()->user()->profileImage->id}}' />
                                                                       <input type="file" name="profile" id="image-upload" />

                                                                      </form>

                                                                        <span>Max file size is 1MB, Minimum dimension: 270x210 And Suitable files are .jpg & .png</span>
                                                                    </div>
                                                                    <button class='edit-profile btn btn-xs ' type="button"><i class='fa fa-pencil'></i> Edit</button>
                                                                </div>

                                                                {{-- <div class='profile-pix'  id="image-preview">
                                                                <label for="image-upload" id="image-label">Choose File</label>
                                                                <input type="file" name="image" id="image-upload" />
                                                                </div> --}}

                                                            </div>
                                                            <div class="profile-form-edit">
                                                                <form>
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <span class="pf-title">First Name</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='fn' type="text" value='{{auth()->user()->firstName}}' placeholder="First Name" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <span class="pf-title">Last Name</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='ln' type="text" value='{{auth()->user()->lastName}}' placeholder="Last Name" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Phone Number</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='phonenum' type="number" min='1' value='{{auth()->user()->phoneNumber}}' placeholder="Phone Number" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Country</span>
                                                                            <div  class="pf-field">

                                                                                <select id='country'  class="chose profile form-contro  contact">
                                                                                    <option>Web Development</option>
                                                                                    <option>Web Designing</option>
                                                                                    <option>Art & Culture</option>
                                                                                    <option>Reading & Writing</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">City</span>
                                                                            <div class="pf-field">
                                                                                <select id='state' data-placeholder="Please Select Specialism" class="profile hosen   contact">
                                                                                    <option value=''>State *</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <span class="pf-title">Gender</span>
                                                                            <div class="pf-field">
                                                                                <select id='gender' data-placeholder="Please Select Specialism" class="profile hosen   contact">
                                                                                    <option  value=''>Select</option>
                                                                                    <option @if(auth()->user()->gender == 'male') selected @endif value='male'>Male</option>
                                                                                    <option @if(auth()->user()->gender == 'female') selected @endif value='female'>Female</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <span class="pf-title">Qualiication</span>
                                                                            <div class="pf-field">
                                                                                <select id='qualification' data-placeholder="Please Select Specialism" class="profile hosen   contact">
                                                                                    <option  value=''>Select</option>
                                                                                    <option @if(auth()->user()->qualification == 'student') selected @endif value='student'>Student</option>
                                                                                    <option @if(auth()->user()->qualification == 'ond') selected @endif value='ond'>OND</option>
                                                                                    <option @if(auth()->user()->qualification == 'hnd') selected @endif value='hnd'>HND</option>
                                                                                    <option @if(auth()->user()->qualification == 'bsc') selected @endif value='bsc'>BSC</option>
                                                                                    <option @if(auth()->user()->qualification == 'pharmd') selected @endif value='pharmd'>PharmD</option>
                                                                                    <option @if(auth()->user()->qualification == 'bpharm') selected @endif value='bpharm'>BPharm</option>
                                                                                    <option @if(auth()->user()->qualification == 'boptom') selected @endif value='boptom'>BOptom</option>
                                                                                    <option @if(auth()->user()->qualification == 'od') selected @endif value='od'>OD</option>
                                                                                    <option @if(auth()->user()->qualification == 'mbbs') selected @endif value='mbbs'>MBBS</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <span class="pf-title">Describe your self</span>
                                                                            <div class="pf-field">
                                                                                <textarea id='desc' class='profile'>{{auth()->user()->description}}</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">

                                                                            <button class='upd-profile' type="button">Update</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>

                                </div>
                                <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <div class="border-title"><h3>Education</h3>
                                        @if(isset($disciplines)) @if(count($disciplines))
                                        <a href="javascript:void()" title="" class='addEducation'><i class="la la-plus"></i></a></div>
                                        @endif @endif


                                        <div @if(isset($disciplines)) @if(count($disciplines)) style='display:none' @endif @endif class="resumeadd-form edu-box">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Discipline</span>
                                                    <div class="pf-field">
                                                        <input id='discipline' class='education-data' placeholder="Course of Study" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <span class="pf-title">From Date</span>
                                                    <div class="pf-field">
                                                        <input class='education-data' id='from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <span class="pf-title">To Date</span>
                                                    <div class="pf-field">
                                                        <input  class='education-data' id='to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Institute</span>
                                                    <div class="pf-field">
                                                        <input type="text" id='institution' class='education-data' placeholder='Name of Institution'>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Brief Description</span>
                                                    <div class="pf-field">
                                                        <textarea class='education-data' id='edu-desc'></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                        {{-- <button class='upd-education' type="button">Update</button> --}}
                                                        <button class='add-education' type="button">Add</button>
                                                </div>
                                            </div>
                                        </div>

                                        @if(isset($disciplines))
                                            @if(count($disciplines))
                                                <div class="edu-history-sec">
                                                @foreach ($disciplines as $discipline)

                                                    <div class="edu-history">
                                                        <i class="la la-graduation-cap"></i>
                                                        <div class="edu-hisinfo">
                                                            <h3>{{$discipline->discipline}}</h3>
                                                            <i>{{\Carbon\Carbon::parse($discipline->from)->format('M/Y')}} - {{\Carbon\Carbon::parse($discipline->to)->format('M/Y')}}</i>
                                                            <span>{{$discipline->institution}} <i>{{$discipline->discipline}}</i></span>
                                                            <p>{{$discipline->description}}</p>
                                                        </div>
                                                        <ul class="action_job">
                                                            <li><span>Edit</span><a href="javascript:void()" class='education-edit' data='{{$discipline}}' title=""><i class="la la-pencil"></i></a></li>
                                                            <li><span>Delete</span><a href="javascript:void()" class='education-delete'  data='{{$discipline->id}}' title=""><i class="la la-trash-o"></i></a></li>
                                                        </ul>
                                                    </div>


                                                @endforeach
                                                </div>
                                            @endif
                                        @endif
                                </div>
                                <div class="tab-pane fade" id="work" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="border-title"><h3>Work Experience</h3>
                                        @if(isset($experiences)) @if(count($experiences))
                                        <a href="javascript:void()" class='addWork' title=""><i class="la la-plus"></i></a></div>
                                        @endif @endif

                                        <div @if(isset($experiences)) @if(count($experiences)) style='display:none' @endif @endif class="resumeadd-form work-box">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Job Title</span>
                                                    <div class="pf-field">
                                                        <input id='title' class='work-data' placeholder="Job Title" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <span class="pf-title">From Date</span>
                                                    <div class="pf-field">
                                                        <input class='work-data' id='work-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 work-end">
                                                    <span class="pf-title">To Date</span>
                                                    <div class="pf-field">
                                                        <input  class='work-data work-ended' id='work-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <p class="remember-label">
                                                        <input id='work-present' value='P' type="checkbox" class='work-ended' name="cb" ><label class='work-ended' for="work-present">I work Here</label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Company</span>
                                                    <div class="pf-field">
                                                        <input type="text" id='company' class='work-data' placeholder='Name of Company'>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <span class="pf-title">Brief Description</span>
                                                    <div class="pf-field">
                                                        <textarea class='work-data' id='work-desc'></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                        {{-- <button class='upd-education' type="button">Update</button> --}}
                                                        <button class='add-work' type="button">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                        @if(isset($experiences))
                                            @if(count($experiences))
                                            <div class="edu-history-sec">
                                                @foreach ($experiences as $experience)

                                                    <div class="edu-history style2">
                                                        <i></i>
                                                        <div class="edu-hisinfo">
                                                            <h3>{{$experience->title}} <span>{{$experience->company}}</span></h3>
                                                            <i>{{\Carbon\Carbon::parse($experience->from)->format('M/Y')}} - @if(strtolower($experience->to) == 'p') Present @else {{\Carbon\Carbon::parse($experience->to)->format('M/Y')}} @endif</i>
                                                            <p>{{$experience->description}}</p>
                                                        </div>
                                                        <ul class="action_job">
                                                            <li><span>Edit</span><a href="javascript:void()" data='{{$experience}}' class='edit-work-exp' title=""><i class="la la-pencil"></i></a></li>
                                                            <li><span>Delete</span><a href="javascript:void()" data='{{$experience->id}}' class='delete-work-exp' title=""><i class="la la-trash-o"></i></a></li>
                                                        </ul>
                                                    </div>


                                                @endforeach
                                            </div>
                                            @endif
                                        @endif
                                </div>
                                <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="pills-contact-tab">
                                       <div class="social-edit">
                                            <h3>Social Edit</h3>
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <span class="pf-title">Facebook</span>
                                                        <div class="pf-field">
                                                            <input type="url" id='facebook' value='{{auth()->user()->facebook}}' placeholder="e.g http://facebook.com/dockay">
                                                            <i class="fa fa-facebook"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <span class="pf-title">Twitter</span>
                                                        <div class="pf-field">
                                                            <input type="url" id='twitter' value='{{auth()->user()->twitter}}' placeholder="e.g http://twitter.com/dockay">
                                                            <i class="fa fa-twitter"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <span class="pf-title">Google</span>
                                                        <div class="pf-field">
                                                            <input type="url" id='google-plus' value='{{auth()->user()->googleplus}}' placeholder="e.g http://google-plus.com/dockay">
                                                            <i class="la la-google"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <span class="pf-title">Linkedin</span>
                                                        <div class="pf-field">
                                                            <input type="url" id='linkedin' value='{{auth()->user()->linkedin}}' placeholder="e.g http://Linkedin.com/dockay">
                                                            <i class="fa fa-linkedin"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        {{-- <button class='upd-education' type="button">Update</button> --}}
                                                        <button class='add-social' type="button">Submit</button>
                                                </div>
                                                </div>

                                            </form>
                                        </div>
                                </div>
                            </div>



					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>



{{-- modals --}}

<!-- education Modal -->
<div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Education</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" ">
        <form action='#' id='edit-education-form'>
            <div class="row">

                <div class="col-lg-12">
                    <span class="pf-title">Discipline</span>
                    <div class="pf-field">
                        <input required id='edit-discipline' class='edit-education-data' placeholder="Course of Study" type="text">
                    </div>
                </div>
                <div class="col-lg-6">
                    <span class="pf-title">From Date</span>
                    <div class="pf-field">
                        <input required class='edit-education-data' id='edit-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-6">
                    <span class="pf-title">To Date</span>
                    <div class="pf-field">
                        <input required class='edit-education-data' id='edit-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Institute</span>
                    <div class="pf-field">
                        <input type="text" required id='edit-institution' class='edit-education-data' placeholder='Name of Institution'>
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Brief Description</span>
                    <div class="pf-field">
                        <textarea required class='edit-education-data' id='edit-edu-desc'></textarea>
                    </div>
                    <input type='hidden' id='edit-edu-id' />
                </div>
                <div class="col-lg-12">
                        {{-- <button class='upd-education' type="button">Update</button> --}}
                        <button class='edit-education' type="submit">Update</button>
                </div>

            </div>
         </form>
        </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>
<!-- work Modal -->
<div class="modal fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Work Experience</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" ">
        <form action='#' id='edit-work-form'>
             <div class="row">
                <div class="col-lg-12">
                    <span class="pf-title">Job Title</span>
                    <div class="pf-field">
                        <input id='edit-title' class='edit-work-data' placeholder="Job Title" type="text">
                    </div>
                </div>
                <div class="col-lg-5">
                    <span class="pf-title">From Date</span>
                    <div class="pf-field">
                        <input class='edit-work-data' id='edit-work-from' placeholder="06.11.2007" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-5 edit-work-end">
                    <span class="pf-title">To Date</span>
                    <div class="pf-field">
                        <input  class='edit-work-data edit-work-ended' id='edit-work-to' placeholder="06.11.2012" type="date" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-lg-2">
                    <p style='    float: right;
    margin-top: 70px;' class="remember-label">
                        <input id='edit-work-present' value='P' type="checkbox" class='edit-work-ended' name="cb" ><label class='work-ended' for="edit-work-present">I Work Here</label>
                    </p>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Company</span>
                    <div class="pf-field">
                        <input type="text" id='edit-company' class='work-data' placeholder='Name of Company'>
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="pf-title">Brief Description</span>
                    <div class="pf-field">
                        <textarea class='edit-work-data' id='edit-work-desc'></textarea>
                    </div>
                </div>
                <input type='hidden' id='work_id' />
                <div class="col-lg-12">
                        {{-- <button class='upd-education' type="button">Update</button> --}}
                        <button class='' type="submit">Update</button>
                </div>
            </div>
         </form>
        </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>




@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
    $('.upd-social,.upd-profile,.upd-contact').hide();

        $(document).ready(function(){
            populateCountries("country", "state");
            $('.profile,.social,.contact').attr('disabled','disabled');
            $('.upd-social,.upd-profile,.upd-contact').hide();

            $('.edit-profile').click(function(){
                $(this).hide();
                $('.profile').removeAttr('disabled');
                $('.upload-info').removeClass('invisible');
                $('.upd-profile').show()
            });
            $('.edit-social').click(function(){
                $(this).hide();
                $('.social').removeAttr('disabled');
                $('.upd-social').show()
            });
            $('.edit-work').click(function(){
                $(this).hide();
                $('.work').removeAttr('disabled');
                $('.upd-work').show();
            });
           // $('.edit-education').click(function(){
             //   $(this).hide();
               // $('.education').removeAttr('disabled');
                //$('.upd-education').show();
            //});

            $('#edit-education-form').submit(function(){
                event.preventDefault();

                var formData = [];
                formData['#edit-discipline'] = 'required|alpha';
                formData['#edit-from'] = 'required';
                formData['#edit-to'] = 'required';
                formData['#edit-institution'] = 'required';
                formData['#edit-edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#edit-discipline').val(),
                        "from": $('#edit-from').val(),
                        "to": $('#edit-to').val(),
                        "institution": $('#edit-institution').val(),
                        "description": $('#edit-edu-desc').val(),
                        "education_id": $('#edit-edu-id').val()

                    }
                    console.log(data);
                    editEducation(data);
                }


            });
            $('#edit-work-form').submit(function(){
                event.preventDefault();
                var formData = [];
                formData['#edit-title'] = 'required|alpha';
                formData['#edit-work-from'] = 'required';

                formData['#edit-company'] = 'required';
                formData['#edit-work-desc'] = 'required|max=500';
                if($('#edit-work-present').is(':checked')){
                   var  end = $('#edit-work-present').val();

                }else if(!isEmpty('#edit-work-to')){

                    var end = $('#edit-work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#edit-title').val(),
                            "from": $('#edit-work-from').val(),
                            "to": end,
                            "company": $('#edit-company').val(),
                            "description": $('#edit-work-desc').val(),
                            "work_id": $('#work_id').val()
                        }
                        console.log(data);
                        editExperience(data);
                    }
                }else{
                    $('.edit-work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }
            });


            $('.upd-profile').click(function(){
                var formData = [];
                formData['#fn'] = 'required|alpha';
                formData['#ln'] = 'required|alpha';
                formData['#phonenum'] = 'number';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#gender'] = 'required';
                formData['#qualification'] = 'required';
                formData['#desc'] = 'max=500';

                if(validate(formData, 'field-error')){
                    var data = {
                        "firstName": $('#fn').val(),
                        "lastName": $('#ln').val(),
                        "phoneNumber": $('#phonenum').val(),
                        "gender": $('#gender').val(),
                        "qualification": $('#qualification').val(),
                        "country": $('#country').val(),
                        "state": $('#state').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateProfile(data);
                }
            });
            $('.add-education').click(function(){
                var formData = [];
                formData['#discipline'] = 'required|alpha';
                formData['#from'] = 'required';
                formData['#to'] = 'required';
                formData['#institution'] = 'required';
                formData['#edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    addEducation(data);
                }
            });
            $('.upd-education').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });
            $('#work-present').click(function(){

                if($(this).is(':checked')){
                   $('.work-end').hide();
                }else{
                    $('.work-end').show();
                }
                $('.work-ended').removeClass('field-error');
            });
            $('#edit-work-present').click(function(){

                if($(this).is(':checked')){
                   $('.edit-work-end').hide();
                }else{
                    $('.edit-work-end').show();
                }
                $('.edit-work-ended').removeClass('field-error');
            });
            $('.add-work').click(function(){
                var formData = [];
                formData['#title'] = 'required|alpha';
                formData['#work-from'] = 'required';

                formData['#company'] = 'required';
                formData['#work-desc'] = 'required|max=500';
                if($('#work-present').is(':checked')){
                   var  end = $('#work-present').val();

                }else if(!isEmpty('#work-to')){
                    console.log($('#work-to').val());
                    var end = $('#work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#title').val(),
                            "from": $('#work-from').val(),
                            "to": end,
                            "company": $('#company').val(),
                            "description": $('#work-desc').val(),

                        }
                        console.log(data);
                        addWork(data);
                    }
                }else{
                    $('.work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }

            });
            $('.upd-work').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });

             $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data);
                }

            });



            $('.addEducation').click(function(){
                $('.edu-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });
            $('.addWork').click(function(){
                $('.work-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });

        });
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>



<script>
    $(document).ready(function(){
        $('#image-upload').change(function(){
            if($(this).val() != ''){
                $('#upload-btn').show();
            }else{
                $('#upload-btn').hide();
            }
        });


        $('.education-edit').click(function(){
             $('.edit-education-data').val();
            var data = JSON.parse($(this).attr('data'));
            $('#edit-discipline').val(data.discipline);
            $('#edit-to').val(data.to);
            $('#edit-from').val(data.from);
            $('#edit-institution').val(data.institution);
            $('#edit-edu-desc').val(data.description);
            $('#edit-edu-id').val(data.id);
             $('#educationModal').modal();

        });
        $('.edit-work-exp').click(function(){
            $('.edit-work-data').val('');
            var data = JSON.parse($(this).attr('data'));
            $('#edit-title').val(data.title);

            $('#edit-work-from').val(data.from);
            $('#edit-company').val(data.company);
            $('#edit-work-desc').val(data.description);
            $('#work_id').val(data.id);
            if(data.to == 'P'){
                //check the present box
                $('#edit-work-present').prop('checked', true);
                //hide the to input
                $('.edit-work-end').hide();
            }else{
                $('#edit-work-to').val(data.to);
                $('.edit-work-end').show();
                $('#edit-work-present').removeAttr('checked');
            }

             $('#workModal').modal();

        });

        $('.education-delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Education!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.education.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        $('.delete-work-exp').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Work Experience!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.work.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });

</script>


@endpush
