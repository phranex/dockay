@extends('layouts.job-user')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }

    </style>

@endpush
@section('content')

<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{$facility->name}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
				 	<div class="col-lg-12 column">
				 		<div class="job-single-sec style3">
				 			<div class="job-head-wide">
				 				<div class="row">
				 					<div class="col-lg-10">
				 						<div class="job-single-head3 emplye">
							 				<div class="job-thumb"> <img src="{{getProfileImage($facility)}}" alt="" /></div>
							 				<div class="job-single-info3">
							 					<h3>{{$facility->name}}</h3>
							 					<span><i class="la la-map-marker"></i>{{$facility->state}}, {{$facility->country}}</span>
							 					<ul class="tags-jobs">
								 					{{-- <li><i class="la la-file-text"></i> Applications 1</li> --}}
								 					<li><i class="la la-calendar-o"></i> Joined: {{getDateFormat($facility->created_at, 'M-Y')}} <small style='font-size:10px;color:#b5acac'>{{when($facility->created_at)}}</small></li>
								 					{{-- <li><i class="la la-eye"></i> Views 5683</li> --}}
								 				</ul>
							 				</div>
							 			</div><!-- Job Head -->
				 					</div>
				 					<div class="col-lg-2">
				 						{{-- <div class="share-bar">
							 				<a href="#" title="" class="share-google"><i class="la la-google"></i></a><a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a><a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
							 			</div> --}}

								 		<div class="emply-btns">
								 			{{-- <a class="seemap" href="#" title=""><i class="la la-map-marker"></i> See On Map</a> --}}
                                            @if(isset($user_is_following))
                                                 <a class="followus" href="{{route('user.followers.delete',$id)}}" title=""><i class="la la-paper-plane"></i> Unfollow</a>
                                                @else

                                                <a class="followus" href="{{route('user.followers.store',["user"=>$facility->id,"type" => 2])}}" title=""><i class="la la-paper-plane"></i> Follow </a>

                                           @endif
                                         </div>
				 					</div>
				 				</div>
				 			</div>
				 			<div class="job-wide-devider">
							 	<div class="row">
							 		<div class="col-lg-8 column">
							 			<div class="job-details">
							 				<h3>About {{$facility->name}}</h3>
							 				<p>{{$facility->description}}</p>

							 			</div>
								 		<div class="recent-jobs">
							 				<h3>Jobs from {{$facility->name}}</h3>
							 				<div class="job-list-modern">
											 	<div class="job-listings-sec no-border">
                                                    @if(isset($facility->jobAdverts))
                                                        @if(count($facility->jobAdverts))
                                                            @foreach ($facility->jobAdverts as $job)
                                                                @if($loop->iteration == 5) @break @endif
                                                                <div class="job-listing wtabs noimg">
                                                                    <div class="job-title-sec">
                                                                        <h3><a href="{{route('job.details',[$job->id, str_slug($job->title)])}}" title="">{{$job->title}}</a></h3>
                                                                        <span>{{$facility->name}}</span>
                                                                        {{-- <div class="job-lctn"><i class="la la-map-marker"></i>{{$facility->state}}, {{$facility->country}}</div> --}}
                                                                    </div>
                                                                    <div class="job-style-bx">
                                                                        <span class="job-is ft">{{title_case($job->category->name)}}</span>
                                                                        <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                                        <i>{{when($job->created_at)}}</i>
                                                                    </div>
                                                                </div>
                                                            @endforeach

                                                        @endif
                                                    @endif


												</div>
											 </div>
							 			</div>
							 		</div>
							 		<div class="col-lg-4 column">
							 			<div class="job-overview">
								 			<h3>Company Information</h3>
								 			<ul>
								 				{{-- <li><i class="la la-eye"></i><h3>Viewed </h3><span>164</span></li> --}}
								 				<li><i class="la la-file-text"></i><h3>Posted Jobs</h3><span>{{count($facility->jobAdverts)}}</span></li>
								 				<li><i class="la la-map"></i><h3>Locations</h3><span>{{$facility->state}}, {{$facility->country}}</span></li>
								 				<li><i class="la la-bars"></i><h3>Establishment</h3><span>{{title_case($facility->establishment->name)}}</span></li>
								 				<li><i class="la la-clock-o"></i><h3>Since</h3><span>{{getDateFormat($facility->created_at, 'Y')}}</span></li>
								 				{{-- <li><i class="la la-users"></i><h3>Team Size</h3><span>15</span></li> --}}
								 				<li><i class="la la-user"></i><h3>Followers</h3><span>15</span></li>
								 			</ul>
								 		</div><!-- Job Overview -->
								 		{{-- <div class="quick-form-job">
								 			<h3>Contact Business Network</h3>
								 			<form>
								 				<input type="text" placeholder="Enter your Name *" />
								 				<input type="text" placeholder="Email Address*" />
								 				<input type="text" placeholder="Phone Number" />
								 				<textarea placeholder="Message should have more than 50 characters"></textarea>
								 				<button class="submit">Send Email</button>
								 				<span>You accepts our <a href="#" title="">Terms and Conditions</a></span>
								 			</form>
								 		</div> --}}
							 		</div>
							 	</div>
							 </div>
					 	</div>
				 	</div>
				</div>
			</div>
		</div>
	</section>






@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>



@endpush
