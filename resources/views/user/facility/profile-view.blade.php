@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.css">
    <style>
    .socials {
    padding: 6px;
      font-size: 14px;
      width: 25px;
    text-align: center;
    text-decoration: none;
    margin: 5px 2px;
    color: white !important;
  }
  
  .fa:hover {
      opacity: 0.7;
  }
  
  .fa-facebook {
    background: #3B5998;
    color: white;
  }
  
  .fa-twitter {
    background: #55ACEE;
    color: white;
  }
  
  .fa-google {
    background: #dd4b39;
    color: white;
  }
  
  .fa-linkedin {
    background: #007bb5;
    color: white;
  }

  .caption div h3{
    color:#64bdf9;
    font-weight:bolder;
    font-size:50px;
        text-shadow: 3px 2px 4px black;
  line-height: 1;
}

    #image-preview{
            background-size: cover;
        background-position: center center;
    }
@media (max-width:720px){
    .prof{
        display: block;
    }

    .profile-info{
        text-align: center
    }
}

.profile-x{
    border:1px solid gray;
    width:150px;
    height:150px;
}
    </style>

@endpush

@push('title')
    {{ $user->username }} | Profile

@endpush
@section('content')


<div class="container-fluid">

       <div class="row">
            <div class="col-md-12  col-xl-9">
                <div class="card dockay-card d-inline-box prof col-lg-11">
                    <div class="profile-pix col-md-3 ">
                        <img class="img-fluid profile-x rounded-circle d-none d-md-block" src="@if(isset($user->profileImage->path)){{ $user->profileImage->path }} @else {{ displayDefaultPhoto($user) }} @endif"/>
                        <img class="img-fluid profile-x  m-auto rounded-circle  d-block d-md-none " src="@if(isset($user->profileImage->path)){{ $user->profileImage->path }} @else {{ displayDefaultPhoto($user) }} @endif"/>
                    </div>
                    <div class="profile-info col-md-9">
                        <h5><strong>{{$user->facility->name}}</strong>@auth @if( strtolower($user->username) == strtolower(auth()->user()->username)) <a href="{{route('user.facility.profile', getFullNameWithSlug(auth()->user()))}}"><i class="fa fa-edit"></i></a> @endif @endauth</h5>

                        <p><i class="fa fa-map"></i> {{$user->state}},{{$user->country}},
                            <i class="fa fa-hospital-o"></i> {{$user->facility->establishment->name}},
                            {{--  <i class="fa fa-users"> {{ $user->followers->count()}}</i>  --}}
                        </p>
                        <div>
                            @if(isset($user->facebook)) <a href="{{ $user->facebook }}" target='_blank' class="fa fa-facebook socials"></a>@endif
                            @if(isset($user->twitter))<a href="{{ $user->twitter }}" target='_blank' class="fa fa-twitter socials"></a>@endif
                            @if(isset($user->google_plus))<a href="{{ $user->google_plus }}" target='_blank' class="fa fa-google socials"></a>@endif
                            @if(isset($user->linkedin))<a href="{{ $user->linkedin }}" target='_blank' class="fa fa-linkedin socials"></a>@endif
                        </div>


                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <div class="follow-buttons">
                            @auth
                            @if( strtolower($user->username) != strtolower(auth()->user()->username))
                              @auth
                            @if($user->id == auth()->id())
                                <p> - You are following <a href='{{ route('followers.index', 'fg') }}'  >{{ auth()->user()->followings->count() }}</a> users.</p>
                            @else
                                <p> - is  following {{ $user->followings->count() }} users.</p>
                            @endif
                        @else
                            <p> - is following {{ $user->followings->count() }} users.</p>

                        @endauth

                        <div class="follow-buttons">
                            @auth
                                @if(auth()->user()->isFollowing($user))
                                     <a href='{{ route('unFollowUser', $user->username) }}' class="btn btn-link">Unfollow</a>
                                @else
                                    @if(auth()->id() != $user->id)
                                     <a href='{{ route('followUser', $user->username) }}' class="btn btn-primary">Follow</a>
                                     @endif
                                @endif
                           
                            @endauth
                            {{--  <a class="btn btn-primary">Insights</a>
                            <a class="btn btn-primary">Contact</a>  --}}
                        </div>
                           
                            @endif
                            @endauth
                       
                        </div>
                    </div>
                </div>
                <div class="card m-t-20 dockay-card col-lg-11">
                    <h6 class="profile-heading">Summary</h6>
                    <p class="profile-desc">
                        {{$user->description}}
                    </p>

                    <div class="box">
                            <h6 class="profile-heading">Photo Description</h6>
                            <div class="row">
                                <div class="col-12">
                                        <div class="owl-carousel m-auto owl-theme" style="width:100%">
                                            @if(isset($user->mediaObjects))
                                                @if(count($user->mediaObjects))
                                                    @foreach ($user->mediaObjects as $media)
                                                        <div class="content flex" >
                                                            <a href="{{$media->path}}" data-lightbox="image-1">
                                                                <img  class="img-fluid m-auto img-thumbnail h-100" src='{{$media->path}}' />
                                                            </a>
                                                        </div>
                                                    @endforeach

                                                @endif
                                            @endif
                                        </div>
                                </div>
                            </div>

                    </div>
                    {{--  <div class="box">
                            <h6 class="profile-heading">Video Gallery</h6>

                    </div>  --}}


                </div>
                <div class="card m-t-20 dockay-card col-lg-11">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-justified" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link active show" data-toggle="tab" href="#job" role="tab" aria-selected="true">Jobs</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" data-toggle="tab" href="#article" role="tab" aria-selected="false">Answers</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" data-toggle="tab" href="#post" role="tab" aria-selected="false">Ad</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane pt-3 active show" id="job" role="tabpanel">
                                    <div class="card m-b-30 ">
                                        @if(isset($user->facility->jobAdverts))
                                            @if(count($user->facility->jobAdverts))
                                                @foreach ($user->facility->jobAdverts as $job)
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title'><a href="{{route('job.details', $job->id)}}">{{$job->title}}</a></h5>
                                                        <p class="post-content">{{$job->description}}</p>
                                                        <footer class="blockquote-footer">Posted {{when($job->created_at)}}</footer>
                                                        </blockquote>

                                                    </div>
                                                @endforeach

                                            @endif
                                        @endif
                                    </div>
                            </div>
                             <div class="tab-pane pt-3" id="article" role="tabpanel">
                                    <div class="card m-b-30 ">

                                        @if(isset($user->answers))
                                            @if(count($user->answers))
                                                @foreach ($user->answers as $answer)
                                                     @if(!$answer->question->isAnonymous)
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title text-bold text-capitalize'><a href='{{ route('question.show', $answer->question->slug) }}#a{{ $answer->id }}'>{{$answer->question->title}}?</a></h5>
                                                        <p class="post-content"> {!! $answer->answer !!}</p>
                                                        <footer class="blockquote-footer">Posted {{when($answer->created_at)}}</footer>
                                                        </blockquote>

                                                    </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class='text-center dockay-card'>{{  $user->username }} is yet to answer a question</div>

                                            @endif
                                        @endif
                                    </div>
                            </div>
                            <div class="tab-pane pt-3" id="post" role="tabpanel">
                                    <div class="card m-b-30 ">

                                            @if(isset($user->products))
                                            @if(count($user->products))
                                                @foreach ($user->products as $product)
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title text-bold text-capitalize'><a href='{{ route('product.details', $product->id) }}'>{{$product->title}}?</a></h5>
                                                        {{--  <p class="post-content"> {!! $product->answer !!}</p>  --}}
                                                        <footer class="blockquote-footer">Posted {{when($product->created_at)}}</footer>
                                                        </blockquote>

                                                    </div>
                                                @endforeach
                                            @else
                                                <div class='text-center dockay-card'>{{  $user->username }} is yet to sell an item</div>

                                            @endif
                                        @endif
                                    </div>
                            </div>
                            <div class="tab-pane pt-3" id="" role="tabpanel">

                            </div>
                        </div>


                </div>
            </div>
            @auth
            @if( strtolower($user->username) != strtolower(auth()->user()->username))
            {{--  <div class="col-md-12 col-xl-3">
                <div class="card  dockay-card ">
                    <div class="card-header">
                        <span>Other Facilities</span>
                    </div>
                    <div class=" d-inline-box row m-t-10">
                        <div class="profile-pix h-100 col-4 col-md-5 ">
                            <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                        </div>
                        <div class="profile-info  col-8 col-md-8">
                            <h6><strong>Dr. Etoka Francis</strong></h6>
                            <span>MD, Eye Masters</span><br/>

                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>

                        </div>
                    </div>
                    <div class=" d-inline-box row m-t-10">
                        <div class="profile-pix h-100 col-4 col-md-5 ">
                            <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                        </div>
                        <div class="profile-info  col-8 col-md-8">
                            <h6><strong>Dr. Etoka Francis</strong></h6>
                            <span>MD, Eye Masters</span><br/>

                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>

                        </div>
                    </div>
                    <div class=" d-inline-box row m-t-10">
                        <div class="profile-pix h-100 col-4 col-md-5 ">
                            <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                        </div>
                        <div class="profile-info  col-8 col-md-8">
                            <h6><strong>Dr. Etoka Francis</strong></h6>
                            <span>MD, Eye Masters</span><br/>

                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>

                        </div>
                    </div>
                    <div class=" d-inline-box row m-t-10">
                        <div class="profile-pix h-100 col-4 col-md-5 ">
                            <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                        </div>
                        <div class="profile-info  col-8 col-md-8">
                            <h6><strong>Dr. Etoka Francis</strong></h6>
                            <span>MD, Eye Masters</span><br/>

                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>

                        </div>
                    </div>

                    <div class="option text-right w-100">
                        <a href="#" class="btn btn-link"> See More</a>
                    </div>


                </div>


            </div>  --}}
            @endif
            @else
            {{--  <div class="col-md-12 col-xl-3">
                    <div class="card  dockay-card ">
                        <div class="card-header">
                            <span>Other Facilities</span>
                        </div>
                        <div class=" d-inline-box row m-t-10">
                            <div class="profile-pix h-100 col-4 col-md-5 ">
                                <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                            </div>
                            <div class="profile-info  col-8 col-md-8">
                                <h6><strong>Dr. Etoka Francis</strong></h6>
                                <span>MD, Eye Masters</span><br/>

                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>

                            </div>
                        </div>
                        <div class=" d-inline-box row m-t-10">
                            <div class="profile-pix h-100 col-4 col-md-5 ">
                                <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                            </div>
                            <div class="profile-info  col-8 col-md-8">
                                <h6><strong>Dr. Etoka Francis</strong></h6>
                                <span>MD, Eye Masters</span><br/>

                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>

                            </div>
                        </div>
                        <div class=" d-inline-box row m-t-10">
                            <div class="profile-pix h-100 col-4 col-md-5 ">
                                <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                            </div>
                            <div class="profile-info  col-8 col-md-8">
                                <h6><strong>Dr. Etoka Francis</strong></h6>
                                <span>MD, Eye Masters</span><br/>

                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>

                            </div>
                        </div>
                        <div class=" d-inline-box row m-t-10">
                            <div class="profile-pix h-100 col-4 col-md-5 ">
                                <img class="img-fluid rounded-circle  " src="{{asset('assets2/images/users/avatar-3.jpg')}}"/>

                            </div>
                            <div class="profile-info  col-8 col-md-8">
                                <h6><strong>Dr. Etoka Francis</strong></h6>
                                <span>MD, Eye Masters</span><br/>

                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>

                            </div>
                        </div>

                        <div class="option text-right w-100">
                            <a href="#" class="btn btn-link"> See More</a>
                        </div>


                    </div>


                </div>  --}}
            @endauth
       </div>


</div>







@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js'></script>

    <script>

            $(document).ready(function(){
                $(".owl-carousel").owlCarousel({
                    loop:false,
                    margin:10,
                    responsiveClass:true,
                    {{--  autoplay:true,  --}}
                    autoplayTimeout:3000,
                    //autoplayHoverPause:true,
                    responsive:{
                        0:{
                            items:1,
                            nav:true
                        },
                        600:{
                            items:3,
                            nav:true
                        },
                        1000:{
                            items:5,
                            nav:true,

                        }
                    }
                });
            });
        </script>

@endpush
