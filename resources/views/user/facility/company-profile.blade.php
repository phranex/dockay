@extends('layouts.job-user')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }

    </style>

@endpush

@section('content')



	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{auth()->user()->facility->name}} </h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-3 column border-right">
				 		<div class="widget">
				 			<div class="tree_widget-sec">
				 				@include('inc.user.nav')
				 			</div>
				 		</div>

				 	</aside>
				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
                            <div class='col-12 profile-tabs'>

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-home" aria-selected="true">Profile</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="profile-title">
                                                                <h3>My Facility</h3>
                                                                @include('inc.messages')
                                                                <div class="upload-img-bar" >
                                                                    <span class="round" style='background-image:url({{getProfileImage(auth()->user()->facility)}})' id="image-preview"><img class='profile-pix' src="" alt="" /></span>
                                                                    <div class="upload-info invisible">
                                                                      <label for="image-upload" id="image-label">Choose File</label>
                                                                      <form enctype="multipart/form-data" action='{{route('user.profileImage.store')}}' method='post'>
                                                                        @csrf
                                                                        <button type='submit' id='upload-btn' style='display:none' class='float-none'>Upload File</button>
                                                                        <input type='hidden' name='profile_image_id' value='{{auth()->user()->facility->profileImage->id}}' />
                                                                       <input type="file" name="profile" id="image-upload" />

                                                                      </form>

                                                                        <span>Max file size is 1MB, Minimum dimension: 270x210 And Suitable files are .jpg & .png</span>
                                                                    </div>
                                                                    <button class='edit-profile btn btn-xs ' type="button"><i class='fa fa-pencil'></i> Edit</button>
                                                                </div>

                                                                {{-- <div class='profile-pix'  id="image-preview">
                                                                <label for="image-upload" id="image-label">Choose File</label>
                                                                <input type="file" name="image" id="image-upload" />
                                                                </div> --}}

                                                            </div>
                                                            <div class="profile-form-edit">
                                                                <form>
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <span class="pf-title">Facility Name</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='fn' type="text" value='{{auth()->user()->facility->name}}' placeholder="Facility Name" />
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Phone Number</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='phonenum' type="number" min='1' value='{{auth()->user()->facility->phoneNumber}}' placeholder="Phone Number" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Official E-Mail</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='email' type="email"  value='{{auth()->user()->facility->email}}' placeholder="Official E-mail" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Facility Type</span>
                                                                            <div class="pf-field">
                                                                                <select class='profile' id='establishment_id' class="chose">
                                                                                    <option>Select</option>
                                                                                    @if(isset($establishments))
                                                                                        @if(count($establishments))
                                                                                            @foreach ($establishments as $establishment)
                                                                                                <option @if($establishment->id == auth()->user()->facility->establishment_id) selected @endif value='{{$establishment->id}}'>{{$establishment->name}}</option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">Country</span>
                                                                            <div  class="pf-field">

                                                                                <select id='country'  class="chose profile form-contro  contact">
                                                                                    <option>Country *</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">State</span>
                                                                            <div class="pf-field">
                                                                                <select id='state' data-placeholder="Please Select Specialism" class="profile hosen   contact">
                                                                                    <option value=''>State *</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <span class="pf-title">City</span>
                                                                            <div class="pf-field">
                                                                                <input class='profile' id='city' type="email"  value='{{auth()->user()->facility->city}}' placeholder="City" />

                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <span class="pf-title">Describe your facility</span>
                                                                            <div class="pf-field">
                                                                                <textarea id='desc' class='profile'>{{auth()->user()->facility->description}}</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">

                                                                            <button class='upd-profile' type="button">Update</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>

                                </div>

                            </div>



					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>







@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
    $('.upd-social,.upd-profile,.upd-contact').hide();

        $(document).ready(function(){
            populateCountries("country", "state");
            $('.profile,.social,.contact').attr('disabled','disabled');
            $('.upd-social,.upd-profile,.upd-contact').hide();

            $('.edit-profile').click(function(){
                $(this).hide();
                $('.profile').removeAttr('disabled');
                $('.upload-info').removeClass('invisible');
                $('.upd-profile').show()
            });
            $('.edit-social').click(function(){
                $(this).hide();
                $('.social').removeAttr('disabled');
                $('.upd-social').show()
            });
            $('.edit-work').click(function(){
                $(this).hide();
                $('.work').removeAttr('disabled');
                $('.upd-work').show();
            });
           // $('.edit-education').click(function(){
             //   $(this).hide();
               // $('.education').removeAttr('disabled');
                //$('.upd-education').show();
            //});

            $('#edit-education-form').submit(function(){
                event.preventDefault();

                var formData = [];
                formData['#edit-discipline'] = 'required|alpha';
                formData['#edit-from'] = 'required';
                formData['#edit-to'] = 'required';
                formData['#edit-institution'] = 'required';
                formData['#edit-edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#edit-discipline').val(),
                        "from": $('#edit-from').val(),
                        "to": $('#edit-to').val(),
                        "institution": $('#edit-institution').val(),
                        "description": $('#edit-edu-desc').val(),
                        "education_id": $('#edit-edu-id').val()

                    }
                    console.log(data);
                    editEducation(data);
                }


            });
            $('#edit-work-form').submit(function(){
                event.preventDefault();
                var formData = [];
                formData['#edit-title'] = 'required|alpha';
                formData['#edit-work-from'] = 'required';

                formData['#edit-company'] = 'required';
                formData['#edit-work-desc'] = 'required|max=500';
                if($('#edit-work-present').is(':checked')){
                   var  end = $('#edit-work-present').val();

                }else if(!isEmpty('#edit-work-to')){

                    var end = $('#edit-work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#edit-title').val(),
                            "from": $('#edit-work-from').val(),
                            "to": end,
                            "company": $('#edit-company').val(),
                            "description": $('#edit-work-desc').val(),
                            "work_id": $('#work_id').val()
                        }
                        console.log(data);
                        editExperience(data);
                    }
                }else{
                    $('.edit-work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }
            });


            $('.upd-profile').click(function(){
                var formData = [];
                formData['#fn'] = 'required|alpha';
                formData['#phonenum'] = 'number';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#city'] = 'required';
                formData['#email'] = 'required';
                formData['#desc'] = 'max=500';
                formData['#establishment_id'] = 'required';

                if(validate(formData, 'field-error')){
                    var data = {
                        "name": $('#fn').val(),
                        "email": $('#email').val(),
                        "phoneNumber": $('#phonenum').val(),
                        "state": $('#state').val(),
                        "country": $('#country').val(),
                        "city": $('#city').val(),
                        "facility_id": '{{auth()->user()->facility->id}}',
                        "establishment_id": $('#establishment_id').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateCompanyProfile(data);
                }
            });
            $('.add-education').click(function(){
                var formData = [];
                formData['#discipline'] = 'required|alpha';
                formData['#from'] = 'required';
                formData['#to'] = 'required';
                formData['#institution'] = 'required';
                formData['#edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    addEducation(data);
                }
            });
            $('.upd-education').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });
            $('#work-present').click(function(){

                if($(this).is(':checked')){
                   $('.work-end').hide();
                }else{
                    $('.work-end').show();
                }
                $('.work-ended').removeClass('field-error');
            });
            $('#edit-work-present').click(function(){

                if($(this).is(':checked')){
                   $('.edit-work-end').hide();
                }else{
                    $('.edit-work-end').show();
                }
                $('.edit-work-ended').removeClass('field-error');
            });
            $('.add-work').click(function(){
                var formData = [];
                formData['#title'] = 'required|alpha';
                formData['#work-from'] = 'required';

                formData['#company'] = 'required';
                formData['#work-desc'] = 'required|max=500';
                if($('#work-present').is(':checked')){
                   var  end = $('#work-present').val();

                }else if(!isEmpty('#work-to')){
                    console.log($('#work-to').val());
                    var end = $('#work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#title').val(),
                            "from": $('#work-from').val(),
                            "to": end,
                            "company": $('#company').val(),
                            "description": $('#work-desc').val(),

                        }
                        console.log(data);
                        addWork(data);
                    }
                }else{
                    $('.work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }

            });
            $('.upd-work').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });

             $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data);
                }

            });



            $('.addEducation').click(function(){
                $('.edu-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });
            $('.addWork').click(function(){
                $('.work-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });

        });
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>



<script>
    $(document).ready(function(){
        $('#image-upload').change(function(){
            if($(this).val() != ''){
                $('#upload-btn').show();
            }else{
                $('#upload-btn').hide();
            }
        });


        $('.education-edit').click(function(){
             $('.edit-education-data').val();
            var data = JSON.parse($(this).attr('data'));
            $('#edit-discipline').val(data.discipline);
            $('#edit-to').val(data.to);
            $('#edit-from').val(data.from);
            $('#edit-institution').val(data.institution);
            $('#edit-edu-desc').val(data.description);
            $('#edit-edu-id').val(data.id);
             $('#educationModal').modal();

        });
        $('.edit-work-exp').click(function(){
            $('.edit-work-data').val('');
            var data = JSON.parse($(this).attr('data'));
            $('#edit-title').val(data.title);

            $('#edit-work-from').val(data.from);
            $('#edit-company').val(data.company);
            $('#edit-work-desc').val(data.description);
            $('#work_id').val(data.id);
            if(data.to == 'P'){
                //check the present box
                $('#edit-work-present').prop('checked', true);
                //hide the to input
                $('.edit-work-end').hide();
            }else{
                $('#edit-work-to').val(data.to);
                $('.edit-work-end').show();
                $('#edit-work-present').removeAttr('checked');
            }

             $('#workModal').modal();

        });

        $('.education-delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Education!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.education.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        $('.delete-work-exp').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Work Experience!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.work.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });

</script>


@endpush
