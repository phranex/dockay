@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .action_job{
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    .action_job li{
        display: inline;
        padding: 5px;
    }
</style>

@endpush
@push('title')
    {{ auth()->user()->username }} | Ads

@endpush
@section('content')

<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

                <div class="col-md-12 card  m-auto dockay-card col-xl-10">
                    <div class="table-responsive">
                            <small style="float:right;font-size:13px"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small>
                            <small style="float:right;font-size:13px;margin: auto 10px;"><a href='{{route('user.ad.create')}}'>Create an Ad</a></small>
                        <table class="table  table-hover w-100 mb-0">
                                <thead>
                                    <tr>
                                        <td scope='col'>S/N</td>
                                        <td scope='col'>Name</td>
                                        <td scope='col'>Title</td>
                                        <td scope='col'>Price</td>
                                        <td scope='col'>Posted</td>
                                        <td scope='col'>Status</td>
                                        <td scope='col'>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if(isset($adverts))
                                       @if(count($adverts))
                                           @foreach ($adverts as $advert)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                   <td>
                                                       <div class="table-list-title">
                                                           @if(isset($advert->images))
                                                            @if(count($advert->images))
                                                                @foreach ($advert->images as $images)
                                                                    <img style='height:50px' src='{{$images->path}}' class="img-fluid" />
                                                                    @break
                                                                @endforeach

                                                            @endif
                                                           @endif
                                                           <a href="{{route('product.details',$advert->id)}}" title="">{{$advert->name}}</a>

                                                       </div>
                                                   </td>
                                                   <td>
                                                       <span class="applied-field">
                                                           {{$advert->title}}

                                                        </span>
                                                   </td>
                                                   <td>
                                                       <span class="applied-field">
                                                           N{{number_format($advert->price)}}

                                                        </span>
                                                   </td>
                                                   <td>
                                                       <span>{{when($advert->created_at)}}</span><br />

                                                   </td>
                                                   <td>
                                                       @if($advert->isActive)
                                                           <span class="status active">Active</span>
                                                       @else
                                                           <span class="status ">Inactive</span>
                                                       @endif
                                                   </td>
                                                   <td>
                                                       <ul class="action_job">
                                                           <li><a href="{{route('product.details',$advert->id)}}" title=""><i class="text-success fa fa-eye"></i></a></li>
                                                           <li><a href="{{route('user.ad.edit', $advert->id)}}" title=""><i class="text-primary fa fa-pencil"></i></a></li>
                                                           <li><a data='{{$advert->id}}' class='delete' href="javascript:void" title=""><i class=" text-danger fa fa-trash-o"></i></a></li>
                                                       </ul>
                                                   </td>
                                               </tr>
                                           @endforeach
                                       @else
                                           <tr>
                                               <td colspan='7'>
                                                   <div class='alert alert-default text-center'> You haven't created an ad yet!!</div>
                                               </td>
                                           </tr>
                                       @endif
                                   @endif


                                </tbody>
                        </table>
                    </div>


                </div>

        </div>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
    $(document).ready(function(){
        $('.delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete  Ad!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.ad.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });


</script>



@endpush
