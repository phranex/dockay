@extends('layouts.master')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">
<link href="{{asset('assets2/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet">


@endpush
@push('title')
    {{ auth()->user()->username }} | Create Ad

@endpush
@section('content')
<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

                <div class="col-md-12 card m-auto dockay-card col-xl-8">
                        <div class="car">
                                <h4 class="mt-0 header-title">Sell something<small style="float:right"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small></h4>
                                {{--  @include('inc.messages')  --}}
                                <form enctype="multipart/form-data" id='ad-form' action="{{route('user.ad.store')}}" method="post" class="form-inline mb-0 row">
                                    @csrf
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Name of Product</label>
                                        <input class="form-control w-100" value="{{old('name')}}" name='name' id='name' required />
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Brand (Manufacturer name)</label>
                                        <input class="form-control w-100" value="{{old('brand')}}" name='brand' id='brand' />
                                    </div>
                                    <div class="form-group col-12 col-lg-12 bmd-form-group">
                                            <label for="exampleTextarea" class="bmd-label-floating">Ad Title</label>
                                            <input class="form-control w-100" value="{{old('title')}}" name='title' id='title' required />
                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Nature of Item</label>
                                       <select required id='nature' name="nature" class="form-control w-100">
                                           <option value=''>select</option>
                                           <option @if (old('nature') == 'new')
                                               selected
                                           @endif value='new'>New</option>
                                           <option @if (old('nature') == 'fairly used')
                                           selected
                                       @endif  value='fairly used'>Fairly Used</option>


                                       </select>
                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Quantity</label>
                                        <input required class="form-control w-100" name='quantity' value="{{old('quantity')}}" type="number" min='1' step="1" id='quantity' required />
                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Unit Price</label>
                                        <input required class="form-control w-100" name='price' value="{{old('price')}}" type="number" min='1' step="1" id='price' required />
                                    </div>
                                    <div class="form-group col-12 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Description</label>
                                        <textarea name="description" class="form-control w-100"  id="description" rows="3">{{old('description')}}</textarea>
                                    </div>
                                    <div class=" col-12 col-md-3 bmd-form-group">
                                            <label style="display:block" class="contain"> <i style="font-size:12px">Price Negotiable?</i>
                                                    <input  name='negotiable' value='Y' @if (old('negotiable'))
                                                        checked
                                                    @endif type="checkbox" name="type">
                                                    <span class="checkmark"></span>
                                                </label>
                                    </div>


                                    <div class="col-12 mt-15">
                                        <p>Add Product Images</p>
                                        <div class="row">
                                                <div class="col-md-6 col-lg-3">

                                                        <div class="card m-b-30">

                                                            <div class="card-body">
                                                                    <input type="file" name='product[]'  id="input-file-now" class="dropify" />


                                                            </div>
                                                        </div>

                                                </div>
                                                <div class="col-md-6 col-lg-3">

                                                        <div class="card m-b-30">

                                                            <div class="card-body">
                                                                    <input type="file" name='product[]'  id="input-file-now" class="dropify" />


                                                            </div>
                                                        </div>

                                                </div>
                                                <div class="col-md-6 col-lg-3">

                                                        <div class="card m-b-30">

                                                            <div class="card-body">
                                                                    <input type="file" name='product[]'  id="input-file-now" class="dropify" />


                                                            </div>
                                                        </div>

                                                </div>
                                                <div class="col-md-6 col-lg-3">

                                                        <div class="card m-b-30">

                                                            <div class="card-body">
                                                                    <input type="file" name='product[]'  id="input-file-now" class="dropify <input type="hidden" name='id'  value='' class=" transaction-id" />

                                                            </div>
                                                        </div>

                                                </div>
                                        </div>
                                    </div>

                                    <div class=" bmd-form-group pt-3 text-right col-12"> <!-- needed to match padding for floating labels -->
                                        <button type="submit" class="btn btn-raised btn-primary mb-0">Post</button>
                                        {{--  <button type="submit" class="btn btn-raised btn-danger ml-1 mb-0">Cancel</button>  --}}
                                    </div>
                                </form>
                        </div>
                </div>

        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>
<script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
<script src='{{asset('assets2/pages/upload-init.js')}}'></script>


<script>

    $(document).ready(function(){

        $('#ad-form').submit(function(){
            event.preventDefault();
            var formData = [];
            formData['#title'] = 'required|alpha';
            formData['#name'] = 'required';
            formData['#nature'] = 'required';
            formData['#establishment'] = 'required';
            formData['#quantity'] = 'equired|number';
            formData['#price'] = 'required|number';

            formData['#description'] = 'required|min=20';
            if(validate(formData, 'field-error')){
                if($('[name=negotiable]')is(':checked')){
                    $('[name=negotiable]').val('Y');
                }
                $('#job-form').unbind().submit();
            }else{
                $.notify('Please fix the errors and resubmit', 'error');
            }

        });

        $('#tags').tagInput({
            labelClass:"badge badge-success"

        });
    });

</script>

@endpush
