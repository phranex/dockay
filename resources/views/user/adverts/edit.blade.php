@extends('layouts.master')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />


@endpush
@push('title')
    {{ auth()->user()->username }} | Edit Job

@endpush
@section('content')
<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

                <div class="col-md-12 card m-auto dockay-card col-xl-8">
                        <div class="car">
                                <h4 class="mt-0 header-title">Edit this job<small style="float:right"><a  href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'> Go to timeline</a></small>
                                    <small style="float:right"><a href='{{url()->previous()}}' style="margin:0 5px;"> Go Back</a></small>
                                </h4>
                                @include('inc.messages')
                                <form id='job-form' action='{{route('user.adverts.update',$advert->id)}}' method="post" class="form-inline mb-0 row">
                                    @csrf
                                    <div class="form-group col-12 bmd-form-group">

                                        <input  class='post-job form-control w-100' name='title' value='{{$advert->title}}' id='title' type="text" placeholder="e.g Medical Officer" />
                                        <i class="text-muted" >Title*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">

                                       <select  name='specialty' id='specialty' class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($specialties))
                                                @if(count($specialties))
                                                    @foreach ($specialties as $specialty)
                                                    <option @if($advert->specialty_id == $specialty->id) selected @endif value='{{$specialty->id}}'>{{$specialty->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                       <i class="text-muted" >Discipline*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">

                                       <select name='establishment' id='establishment' class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($establishments))
                                                @if(count($establishments))
                                                    @foreach ($establishments as $establishment)
                                                    <option @if($advert->establishment_id == $establishment->id) selected @endif value='{{$establishment->id}}'>{{$establishment->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                       <i class="text-muted" >Establishment*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">

                                       <select name='category' id='category' class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($categories))
                                                @if(count($categories))
                                                    @foreach ($categories as $category)
                                                    <option @if($advert->category_id == $category->id) selected @endif value='{{$category->id}}'>{{$category->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                       <i class="text-muted" >Job Type*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">

                                       <select name='gender' id='gender'  class="form-control w-100">
                                            <option value=""> Select</option>
                                            <option @if($advert->gender == 'any') selected @endif value='any'>Any</option>
													<option @if($advert->gender == 'male') selected @endif value='male'>Male</option>
													<option @if($advert->gender == 'female') selected @endif value='female'>Female</option>

                                       </select>
                                       <i class="text-muted" >Gender*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">
                                            <input  name='deadline' value='{{$advert->max}}' readonly id='deadline' type="text"   class="form- w-75 post-job datepicker" />
                                            <i class="text-muted" >Deadline*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">

                                            <input  name='min' value='{{$advert->min}}' id='min' type="number" min='1000' placeholder='1000'  class="form-control w-100 post-job " />
                                            <i class="text-muted" >Minimum Salary</i>

                                    </div>
                                    <div class="form-group col-12 col-lg-4 bmd-form-group">

                                            <input value='{{$advert->max}}' name='max' id='max' type="number"  min='1000' placeholder='1000'  class="form-control post-job w-100 " />
                                            <i class="text-muted" >Maximum Salary</i>
                                    </div>
                                    <div class="form-group col-12 bmd-form-group">
                                        <textarea name='description' id='description' class="form-control w-100" id="exampleTextarea3" rows="3">{{$advert->description}}</textarea>
                                        <i class="text-muted" >Description*</i>
                                    </div>
                                    <div class="form-group col-12 bmd-form-group" id="tags">
                                        {{-- <label for="exampleTextarea" class="bmd-label-floating">Relevant skills  (add as many skills as required for the job with each separated by a  comma)</label> --}}
                                        <input type="text"  placeholder="" class="labelinput form-control w-100">
                                        <input type="hidden" name='skills' value="@if(isset($skills)) {{$skills}} @endif" id="skills">
                                        <i class="text-muted" >Relevant skills  (add as many skills as required for the job with each separated by a  comma)</i>

                                    </div>
                                       
                                    
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                           <select id='country' name='country' class='form-control w-100'>
                                                <option value=''>Select Country</option>
                                               
                                           </select>
                                            <i class="text-muted" >Country*</i>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                           <select id='state' name='state' class='form-control w-100'>
                                                <option value=''>Select State</option>
                                               
                                           </select>
                                            <i class="text-muted" >State*</i>
                                    </div>
                                    <div class="form-group bmd-form-group pt-3 text-right col-12"> <!-- needed to match padding for floating labels -->
                                        <button type="submit" class="btn btn-raised btn-primary mb-0">Send</button>
                                        {{--  <button type="submit" class="btn btn-raised btn-danger ml-1 mb-0">Cancel</button>  --}}
                                    </div>
                                </form>
                </div>

        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>
<script src='{{asset('js/countries.js')}}'></script>

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>


<script>

    $(document).ready(function(){
        populateCountries("country", "state", '{{ $advert->country}}', '{{ $advert->state}}');

        $('#job-form').submit(function(){
            event.preventDefault();
            var formData = [];
            formData['#title'] = 'required';
            formData['#category'] = 'required';
            formData['#specialty'] = 'required';
            formData['#establishment'] = 'required';
            formData['#gender'] = 'required';
            formData['#deadline'] = 'required';
            formData['#min'] = 'number';
            formData['#country'] = 'required';
            formData['#state'] = 'required';
            formData['#max'] = 'number';
            formData['#description'] = 'required|min=30';
            if(validate(formData, 'field-error')){
                if(checkSalaryFields('#min', '#max', 'field-error')){
                      var data = {
                        "title": $('#title').val(),
                        "category": $('#category').val(),
                        "specialty": $('#specialty').val(),
                        "establishment": $('#establishment').val(),
                        "gender": $('#gender').val(),
                        "deadline": $('#deadline').val(),
                        "min": $('#min').val(),
                        "max": $('#max').val(),
                        "skills": $('#skills').val(),
                        "description": $('#description').val()

                    }
                    console.log(data);
                    $('#job-form').unbind().submit();
                }
            }else{
                $.notify('Please fix the errors and resubmit', 'error');
            }

        });

        $('#tags').tagInput({
            labelClass:"badge badge-success"

        });


         $('.datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            value: '{{getDateFormat($advert->deadline, 'Y-m-d')}}',
             format: 'yyyy-mm-dd',
             minDate: '{{\Carbon\Carbon::now()}}'
        });
    });

</script>

@endpush
