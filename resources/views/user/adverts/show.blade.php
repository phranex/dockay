@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">

<style>
.contain .checkmark:after {
    left: 5px;
    /* background: red; */
    top: 3px;
    width: 5px;
    height: 7px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}



.checkmark {
    position: absolute;
    top: 10px;
    right: 0;
    height: 15px;
    width: 15px;
    background-color: #eee;
}
        @media (max-width:720px){
            .prof{
                display: block;
            }
            .profile-pix{
                display: block;
                margin: auto;
            }

            .profile-info{
                text-align: center
            }
        }
</style>

@endpush
@section('content')

@push('title')
    {{ auth()->user()->username }} | {{ $advert->title }}

@endpush
<div class="container-fluid">

       <div class="row">
            <div class="col-md-12 ">

                <div class="car col-12 col-lg-8 dockay-card m-auto ">
                        <h3 style='margin-bottom:100px'>Applications 
                            <small class="pull-right" style="font-size:12px">
                                <a href='{{route('downloadAllCV', $advert->id)}}' class=" btn btn-sm ">Download All CV</a>
                                {{--  <a href='{{route('shortlist')}}?j={{$advert->id}}' class=" btn btn-sm">Select All</a>  --}}
                                @if(!$all_shortlisted)
                                    <button class=" btn btn-sm select-all">Select All</button>
                                     <button  class="disabled short-select btn btn-sm">Shortlist Selected</button>
                                @endif
                                
                               @if($unsentMailNum)
                                @if($shortlisted)
                                    <button data-toggle="modal" data-target="#sendEmail"  class='btn btn-link'>Mail Shortlisted</button>
                               
                                @endif
                                @endif
                            </small>
                            <small class='pull-right'>
                                <label>Show</label>
                                <input style='width:80%'  class='pull-right size form-control'placeholder='25'  type='number'  />
                                

                                </select>
                            </small>
                        </h3>
                        @if(isset($applications))
                            @if(count($applications))
                                @foreach ($applications as $application)
                                    <div class=" card col-12 dockay-card d-inline-box prof row m-t-10">
                                        <div class="profile-pix h-100 col-4 col-md-5 " style='width:150px'>
                                            <img class="img-fluid rounded-circle  " src="@if(isset($application->practitioner->user->profileImage->path)){{ $application->practitioner->user->profileImage->path }} @else {{ displayDefaultPhoto($application->practitioner->user) }} @endif"/>
                                            <img class="img-fluid rounded-circle  " src=""/>

                                        </div>
                                        <div class="profile-info  col-12 col-md-8">
                                            <h6><strong><a href="#" title="">{{getUserFullName($application->practitioner->user)}}</a></strong>
                                            @if($application->shortlist_flag == 1)
                                                <small class="text-right"><span class='pt job-type'>
                                                   Shortlisted
                                                 </span></small>
                                            @else 
                                                <label class="contain w-100"> <span style='font-size:10px;margin-right:32px' class='num_of'>Click to Shortlist</span>
                                                     <input type='checkbox' data-id={{ $application->id }}  class='' name='shortlist' />
                                                    <span style='right:unset' class="checkmark pull-left"></span>
                                                </label>
                                                
                                              
                                            @endif
                                            </h6>
                                            @if(strlen(getUserLocation($application->practitioner->user)) > 1)
                                                <span>{{ getUserLocation($application->practitioner->user)}}</span>
                                            @endif
                                            <div class="w-100">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            </div>
                                            <div class="action-center">

                                                    <div class="">
                                                        {{--  <li class="open-letter"><a href="#" title="">Cover Letter</a></li>  --}}
                                                        <span class="bg-primary dockay-bd-rad"><i class="fa fa-file noti-icon"></i><a class="btn-link" href="{{ $application->cv}}" title=""> CV</a></span>
                                                        @if($application->shortlist_flag == 0)
                                                        {{--  <span class="bg-success dockay-bd-rad"><i class='fa fa-check'></i><a href='{{route('shortlist')}}?application={{$application->id}}' class="btn-link" href="#" title=""> ShortList</a></span>  --}}

                                                        @endif
                                                        {{--  <li class="open-contact"><a href="#" title="">Send a Message</a></li>  --}}
                                                        <span class="bg-info dockay-bd-rad"><i class="fa fa-eye"></i><a class="btn-link" href="{{route('user.profile.view', $application->practitioner->user->username)}}" title="">View</a></span>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                @endforeach

                                {{ $applications->links() }}
                            @endif
                        @endif
                </div>
            </div>
       </div>
</div>


 <div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Send Email to shortlisted applicants </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="flex ">
                            <div>
                                <form action='{{route('user.application.email')}}' method='post'>
                                @csrf
                                    <div class="row">
                                            <div class="col-lg-12">
                                                <p> Please Mail your successful applicants the next step to take.</p>
                                            </div>
                                            <div style="border:none !important" class="text-center w-100 error">

                                            </div>
                                            
                                            <div  class="col-lg-12 ">
                                                <div col='col-xl-12 '>
                                                    <label>Subject</label>
                                                    <input value='{{$advert->title}}' name='subject' required type='text' placeholder='Enter a Subject of the mail'  class='form-control'/>
                                                </div>
                                                <div col='col-xl-12 '>
                                                    <label>Enter reply email</label>
                                                    <input value='{{ auth()->user()->email }}' name='replyTo' required type='text' placeholder='Enter email'  class='form-control'/>
                                                </div>
                                                <div style='margin-top:10px' col='col-xl-12 mt-10'>
                                                    <label>Body</label>
                                                    <textarea style='height:200px' required name='body' placeholder='Please give your applicants necessary and detailed information on how to be available for the next phase which might be interview at your location.'  class='form-control'></textarea>
                                                </div>
                                                <input type='hidden' name='job_id' value='{{ $advert->id }}' />

                                               
                                            </div>
                                            <div class="col-12 text-right mt-10">
                                                   <button  type='submit' class='btn'>Send</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
    $(document).ready(function(){
        $('[name=shortlist]').change(function(){
            var num_of_checked = $('[name=shortlist]:checked').length;
            if(num_of_checked > 0){
                $('.short-select').removeClass('disabled');
            }else{
                $('.short-select').addClass('disabled');
            }
        });

        $('.size').blur(function(){
            
            window.location.href = '{{ url()->current() }}'+'?size='+$(this).val();
        });

        $('.select-all').click(function(){
          
            
            if($(this).text().toLowerCase() == 'uncheck'){
                $('[name=shortlist]').prop('checked', false);
                $(this).text('Select All');
                 $(this).removeClass('btn-outline');
            }else{
                $('[name=shortlist]').prop('checked', true);
                $(this).addClass('btn-outline');
                $(this).text('Uncheck');
            }
        });

        $('.short-select').click(function(){
            var num_of_checked = $('[name=shortlist]:checked').length;
            if(num_of_checked){
                var values = [];
                $('[name=shortlist]:checked').each(function(){
                    values.push($(this).attr('data-id'));
                });
                shortlist(values);

                
            }else{
                $.alert({
                    title: "Error shortlisting",
                    content: "Couldn't shortlist. Please select at least one applicant",
                     type: 'red',
                    icon: 'fa fa-times',
                });
            }
        });
       



        $('.delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Work Experience!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.advert.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
   
    });

</script>

@endpush
