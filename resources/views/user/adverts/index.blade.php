@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<style>
    .action_job{
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    .action_job li{
        display: inline;
        padding: 5px;
    }
</style>

@endpush
@push('title')
    {{ auth()->user()->username }} | Jobs

@endpush
@section('content')
<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

                <div class="col-md-12 card  m-auto dockay-card col-xl-10">
                    <div class="table-responsive">
                            <small style="float:right;font-size:13px"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small>
                            <small style="float:right;font-size:13px;margin: auto 10px;"><a href='{{route('user.advert.create')}}'>Create a Job ad</a></small>

                        <table class="table  table-hover w-100 mb-0">
                                <thead>
                                    <tr>
                                        <td scope='col'>S/N</td>
                                        <td scope='col'>Title</td>
                                        <td scope='col'>Applications</td>
                                        <td scope='col'>Deadline</td>
                                        <td scope='col'>Status</td>
                                        <td scope='col'>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if(isset($adverts))
                                       @if(count($adverts))
                                           @foreach ($adverts as $advert)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                   <td>
                                                       <div class="table-list-title">
                                                           <a href="#" title="">{{$advert->title}}</a>

                                                       </div>
                                                   </td>
                                                   <td>
                                                       <span class="applied-field">
                                                        @if($advert->mode == 'dockay')
                                                           @if(count($advert->applications))
                                                           <a  href="{{route('user.adverts.show', $advert->id)}}">{{number_format(count($advert->applications))}} Applied
                                                            @else
                                                            <i style="font-size:10px">No application yet</i>
                                                            @endif
                                                        @else
                                                             <i style="font-size:10px">Please check 
                                                                <strong>
                                                                @if($advert->mode == 'p')
                                                                    {{ auth()->user()->email }} 
                                                                @else
                                                                    {{ $advert->mode }}
                                                                @endif
                                                                </strong>
                                                                to view applications
                                                             </i>
                                                        @endif
                                                        </span>
                                                   </td>
                                                   <td>
                                                       <span>{{when($advert->deadline)}}</span><br />

                                                   </td>
                                                   <td>
                                                       @if($advert->isActive)
                                                           <span class="status active">Active</span>
                                                           <a style='font-size:11px' href='{{ route('advert.inactive', $advert->id) }}' class='btn btn-link'>Make Inactive</a>
                                                       @else
                                                           <span class="status ">Inactive</span>
                                                          

                                                       @endif
                                                   </td>
                                                   <td>
                                                       <ul class="action_job">
                                                           <li><a href="{{route('job.details', $advert->id)}}" title=""><i class="text-success fa fa-eye"></i></a></li>
                                                           <li><a href="{{route('user.adverts.edit', $advert->id)}}" title=""><i class="text-primary fa fa-pencil"></i></a></li>
                                                           <li><a data='{{$advert->id}}' class='delete' href="javascript:void" title=""><i class=" text-danger fa fa-trash-o"></i></a></li>
                                                       </ul>
                                                   </td>
                                               </tr>
                                           @endforeach
                                       @else
                                           <tr>
                                               <td colspan='6'>
                                                   <div class='alert alert-default text-center'> You haven't posted a job yet!!</div>
                                               </td>
                                           </tr>
                                       @endif
                                   @endif


                                </tbody>
                        </table>
                    </div>


                </div>

        </div>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
    $(document).ready(function(){
        $('.delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Job Ad!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.advert.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });


</script>



@endpush
