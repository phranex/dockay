@extends('layouts.master')

@push('styles')
    {{--  {{ auth()->user()->username }} | Verify  --}}
    <style>
     h5,h3,h4,h2,h1,.medium-text, *{
        color:black;
    }

    </style>

@endpush

@push('title')

    Verification
@endpush



@push('bg-content')
    <div class='bg-content'>
        <div class="flex">
                <div class='auth-bo' >
                    <div class="row full-screen">

                            {{-- professioal --}}
                            <div id='HP' class="col-md-12 ffset-lg-6 col-lg-4 m-auto " >
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Verification</h5>
                                        </div>
                                        <div class="card-body">
                                                {{--  @include('inc.messages')  --}}
                                            <div class="">
                                                    @if($status)
                                                    <div class='text-center'>
                                                            Your account has been successfully verified <a class='btn btn-less mt-10 btn-corner right-15' href='{{route('login')}}'>Login</a>
                                                    </div>
                                                    @else
                                                        <div>
                                                            Could not be verified. You may have already verified your account or there was a problem somewhere.<a class='btn btn-link btn-corner right-15' href='{{route('login')}}'>Login</a>
                                                        </div>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>

                            </div>

                    </div>
                </div>
        </div>
    </div>


@endpush


