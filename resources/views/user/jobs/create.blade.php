@extends('layouts.master')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">

<style>
      .checkmark{
        height: 15px;
        width: 15px;
        top:21%;
    }

    .contain{
        font-size: 12px !important;
        padding: 1px 20px;
    }
    .contain .checkmark:after{
        left: 4px;
    top: 1px;
    }

    @media (max-width: 620px){
     
    

    .contain .checkmark:after{
        left: 5px;
    top: 2px;
    }}
</style>

@endpush

@push('title')
    {{ auth()->user()->username }} | Create Job

@endpush
@section('content')
{{--  <div class="mb-90"></div>  --}}
<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

                <div class="col-md-12 card m-auto dockay-card col-xl-6">
                        <div class="car">
                                <h4 class="mt-0 header-title">Create a random job<small style="float:right"><a href='{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}'>Go to timeline</a></small></h4>
                                {{--  @include('inc.messages')  --}}
                                <form action="{{route('user.create.randomJob')}}" method="post" class="form-inline mb-0 row">
                                    @csrf
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Discipline</label>
                                       <select required name="specialty" class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($specialties))
                                                @if(count($specialties))
                                                    @foreach ($specialties as $specialty)
                                                        <option value="{{$specialty->id}}">{{$specialty->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Establishment</label>
                                       <select required name="establishment" class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($establishments))
                                                @if(count($establishments))
                                                    @foreach ($establishments as $establishment)
                                                        <option value="{{$establishment->id}}">{{$establishment->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Category</label>
                                       <select required name="category" class="form-control w-100">
                                            <option value=""> Select</option>
                                            @if(isset($categories))
                                                @if(count($categories))
                                                    @foreach ($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                       </select>
                                    </div>
                                    <div class="form-group col-12 col-lg-6 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Gender</label>
                                       <select required name="gender" class="form-control w-100">
                                            <option value=""> Select</option>
                                            <option value="male"> Male</option>
                                            <option value="female"> Female</option>
                                            <option value="any"> Any</option>

                                       </select>
                                    </div>
                                    <div class="form-group col-12 bmd-form-group">
                                        <label for="exampleTextarea" class="bmd-label-floating">Description</label>
                                        <textarea required name="description" class="form-control w-100" id="exampleTextarea3" rows="3"></textarea>
                                    </div>
                                    <div class="form-group col-12 bmd-form-group">
                                        
                                        <label for="exampleTextarea" class="bmd-label-floating contact">Contact</label>
                                        <input type='number' name='contact' class='form-control w-100 contact' placeholder='Number to be contacted for this job?' />
                                       
                                        <label class="contain mt-20">Or  Use my Profile Number - {{ auth()->user()->phoneNumber }}
                                                    <input name='useNum' value='Y' type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                    </div>
                                    
                                    <div style='display:none;' class="form-group chat-on-whatsapp col-12 bmd-form-grou">
                                        
                                      
                                        <label class="contain">Allow Interested applicants to chat me on whatsapp - {{ auth()->user()->phoneNumber }}
                                                    <input name='whatsapp' value='Y' type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                    </div>


                                    
                                    <div class="form-grou bmd-form-group pt-3 text-right col-12"> <!-- needed to match padding for floating labels -->
                                        <button type="submit" class="btn btn-raised btn-primary mb-0">Post</button>
                                        {{--  <button type="submit" class="btn btn-raised btn-danger ml-1 mb-0">Cancel</button>  --}}
                                    </div>
                                </form>
                        </div>
                </div>

        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>


<script>

    $(document).ready(function(){
        $('[name=useNum]').change(function(){
            if($(this).is(':checked')){
                $('.contact').fadeOut();
                $('[name=contact]').val('{{ auth()->user()->phoneNumber }}');
                $('.chat-on-whatsapp').fadeIn();
            }else{
                 $('.contact').fadeIn();
                $('[name=contact]').val('');
                $('.chat-on-whatsapp').fadeOut();
            }
        });
        $('#job-form').submit(function(){
            event.preventDefault();
            var formData = [];
            formData['#title'] = 'required|alpha';
            formData['#category'] = 'required';
            formData['#specialty'] = 'required';
            formData['#establishment'] = 'required';
            formData['#gender'] = 'required';
            formData['#deadline'] = 'required';
            formData['#min'] = 'number';
            formData['#max'] = 'number';
            formData['#description'] = 'required|min=100';
            if(validate(formData, 'field-error')){
                if(checkSalaryFields('#min', '#max', 'field-error')){
                      var data = {
                        "title": $('#title').val(),
                        "category": $('#category').val(),
                        "specialty": $('#specialty').val(),
                        "establishment": $('#establishment').val(),
                        "gender": $('#gender').val(),
                        "deadline": $('#deadline').val(),
                        "min": $('#min').val(),
                        "max": $('#max').val(),
                        "skills": $('#skills').val(),
                        "description": $('#description').val()

                    }
                    console.log(data);
                    $('#job-form').unbind().submit();
                }
            }else{
                $.notify('Please fix the errors and resubmit', 'error');
            }

        });

        $('#tags').tagInput({
            labelClass:"badge badge-success"

        });
    });

</script>

@endpush
