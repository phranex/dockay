@extends('layouts.job-user')
@push('styles')


@endpush
@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{asset('images/resource/mslider1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<div class="container">
								<div class="row">
									<div class="col-lg-6">
										{{-- <div class="skills-btn">
											<a href="#" title="">Photoshop</a>
											<a href="#" title="">Designers</a>
											<a href="#" title="">Illustrator</a>
										</div> --}}
									</div>
									<div class="col-lg-6">
										<div class="action-inner">
											<a href="#" title=""><i class="la la-paper-plane"></i>Save Resume</a>
											<a href="#" title=""><i class="la la-envelope-o"></i>Contact David</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="overlape">
		<div class="block remove-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="cand-single-user">
							<div class="share-bar circle">
                                @if(isset(auth()->user->facebook))
                                     <a href="{{auth()->user->facebook}}" target='_blank' title="" class="share-fb"><i class="fa fa-facebook"></i></a>
                                @endif
                                @if(isset(auth()->user->google_plus))
				 				    <a href="{{auth()->user->google_plus}}"  title="" target='_blank'  class="share-google"><i class="la la-google"></i></a>
                                @endif
                                @if(isset(auth()->user->twitter))
                                    <a href="{{auth()->user->twitter}}" title="" target='_blank'  class="share-twitter"><i class="fa fa-twitter"></i></a>
                                @endif
                                @if(isset(auth()->user->linkedin))
                                     <a href="{{auth()->user->linkedin}}" target='_blank' title="" class="share-li"><i class="la la-linkedin"></i></a>
                                @endif

				 			</div>
				 			<div class="can-detail-s">
				 				<div class="cst"><img class='img-fluid' style='height:100%' src="{{getProfileImage(auth()->user())}}" alt="" /></div>
				 				<h3>{{getFullName(auth()->user())}}</h3>
                                 @if(isset($current))
				 				<span><i>{{$current->title}}</i> at {{$current->company}}</span>
                                 @endif
                                 {{-- @if(auth()->user()->settings->show_mail == 1) --}}
				 				<p>{{auth()->user()->email}}</p>
                                 {{-- @endif --}}
				 				<p>Member Since, {{getDateFormat(auth()->user()->created_at, 'M Y')}}</p>
                                 {{-- @if(auth()->user()->settings->show_location == 1) --}}
				 				<p><i class="la la-map-marker"></i>{{auth()->user()->state}} / {{auth()->user()->country}}</p>
                                 {{-- @endif --}}
				 			</div>
				 			<div class="download-cv">
				 				<a href="#" title="">Download CV <i class="la la-download"></i></a>
				 			</div>
				 		</div>
				 		<ul class="cand-extralink">
				 			<li><a href="#about" title="">About</a></li>
				 			<li><a href="#education" title="">Education</a></li>
				 			<li><a href="#experience" title="">Work Experience</a></li>
				 			{{-- <li><a href="#portfolio" title="">Portfolio</a></li>
				 			<li><a href="#skills" title="">Professional Skills</a></li>
				 			<li><a href="#awards" title="">Awards</a></li> --}}
				 		</ul>
				 		<div class="cand-details-sec">
				 			<div class="row">
				 				<div class="col-lg-8 column">
				 					<div class="cand-details" id="about">
				 						<h2>About {{getFullName(auth()->user())}}</h2>
				 						<p>{{auth()->user()->description}}</p>
				 						<div class="edu-history-sec" id="education">
				 							<h2>Education</h2>
                                             @if(isset($educations))
                                                @if(count($educations))
                                                    @foreach ($educations as $education)
                                                        <div class="edu-history">
                                                            <i class="la la-graduation-cap"></i>
                                                            <div class="edu-hisinfo">
                                                                <h3>{{$education->discipline}}</h3>
                                                                <i>{{getDateFormat($education->from, 'Y')}} - {{getDateFormat($education->to, 'Y')}}</i>
                                                                <span>{{$education->institution}} <i>{{$education->discipline}}</i></span>
                                                                <p>{{$education->description}}</p>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                @endif
                                             @endif

				 						</div>
				 						<div class="edu-history-sec" id="experience">
				 							<h2>Work & Experience</h2>
                                             @if(isset($experiences))
                                                @if(count($experiences))
                                                    @foreach ($experiences as $experience)
                                                        <div class="edu-history style2">
                                                            <i></i>
                                                            <div class="edu-hisinfo">
                                                                <h3>{{$experience->title}} <span>{{$experience->company}}</span></h3>
                                                                <i>{{getDateFormat($experience->from, 'Y')}} - @if($experience->to == 'P') Present @else {{getDateFormat($experience->to, 'Y')}} @endif</i>
                                                                <p>{{$experience->description}}</p>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                @endif
                                             @endif

				 						</div>
				 						{{-- <div class="mini-portfolio" id="portfolio">
				 							<h2>Portfolio</h2>
				 							<div class="mp-row">
				 								<div class="mp-col">
				 									<div class="mportolio"><img src="{{asset('images/resource/p1.jpg')}}" alt="" /><a href="#" title=""><i class="la la-search"></i></a></div>
				 								</div>
				 								<div class="mp-col">
				 									<div class="mportolio"><img src="{{asset('images/resource/p2.jpg')}}" alt="" /><a href="#" title=""><i class="la la-search"></i></a></div>
				 								</div>
				 								<div class="mp-col">
				 									<div class="mportolio"><img src="{{asset('images/resource/p3.jpg')}}" alt="" /><a href="#" title=""><i class="la la-search"></i></a></div>
				 								</div>
				 								<div class="mp-col">
				 									<div class="mportolio"><img src="{{asset('images/resource/p4.jpg')}}" alt="" /><a href="#" title=""><i class="la la-search"></i></a></div>
				 								</div>
				 							</div>
				 						</div> --}}
				 						{{-- <div class="progress-sec" id="skills">
				 							<h2>Professional Skills</h2>
				 							<div class="progress-sec">
				 								<span>Adobe Photoshop</span>
				 								<div class="progressbar"> <div class="progress" style="width: 80%;"><span>80%</span></div> </div>
				 							</div>
				 							<div class="progress-sec">
				 								<span>Production In Html</span>
				 								<div class="progressbar"> <div class="progress" style="width: 60%;"><span>60%</span></div> </div>
				 							</div>
				 							<div class="progress-sec">
				 								<span>Graphic Design</span>
				 								<div class="progressbar"> <div class="progress" style="width: 75%;"><span>75%</span></div> </div>
				 							</div>
				 						</div> --}}
				 						{{-- <div class="edu-history-sec" id="awards">
				 							<h2>Awards</h2>
				 							<div class="edu-history style2">
				 								<i></i>
				 								<div class="edu-hisinfo">
				 									<h3>Perfect Attendance Programs</h3>
				 									<i>2008 - 2012</i>
				 									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin a ipsum tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
				 								</div>
				 							</div>
				 							<div class="edu-history style2">
				 								<i></i>
				 								<div class="edu-hisinfo">
				 									<h3>Top Performer Recognition</h3>
				 									<i>2008 - 2012</i>
				 									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin a ipsum tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
				 								</div>
				 							</div>
				 							<div class="edu-history style2">
				 								<i></i>
				 								<div class="edu-hisinfo">
				 									<h3>King LLC</h3>
				 									<i>2008 - 2012</i>
				 									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin a ipsum tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
				 								</div>
				 							</div>
				 						</div> --}}
				 						{{-- <div class="companyies-fol-sec">
				 							<h2>Companies Followed By</h2>
				 							<div class="cmp-follow">
				 								<div class="row">
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em1.jpg')}}" alt="" /><span>King LLC</span></a>
				 									</div>
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em2.jpg')}}" alt="" /><span>Telimed</span></a>
				 									</div>
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em3.jpg')}}" alt="" /><span>Ucesas</span></a>
				 									</div>
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em4.jpg')}}" alt="" /><span>TeraPlaner</span></a>
				 									</div>
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em5.jpg')}}" alt="" /><span>Cubico</span></a>
				 									</div>
				 									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				 										<a href="#" title=""><img src="{{asset('images/resource/em6.jpg')}}" alt="" /><span>Airbnb</span></a>
				 									</div>
				 								</div>
				 							</div>
				 						</div> --}}
				 					</div>
				 				</div>
				 				<div class="col-lg-4 column">
						 			<div class="job-overview">
							 			<h3>Overview</h3>
							 			<ul>
							 				{{-- <li><i class="la la-money"></i><h3>Offerd Salary</h3><span>£15,000 - £20,000</span></li> --}}
							 				<li><i class="la la-mars-double"></i><h3>Gender</h3><span>{{title_case(auth()->user()->gender)}}</span></li>
							 				{{-- <li><i class="la la-thumb-tack"></i><h3>Career Level</h3><span>Executive</span></li> --}}
							 				{{-- <li><i class="la la-puzzle-piece"></i><h3>Industry</h3><span>Management</span></li> --}}
							 				{{-- <li><i class="la la-shield"></i><h3>Experience</h3><span>2 Years</span></li> --}}
							 				<li><i class="la la-line-chart "></i><h3>Qualification</h3><span>{{strtoupper(auth()->user()->qualification)}}</span></li>
							 			</ul>
							 		</div><!-- Job Overview -->
							 		{{-- <div class="quick-form-job">
							 			<h3>Contact</h3>
							 			<form>
							 				<input type="text" placeholder="Enter your Name *" />
							 				<input type="text" placeholder="Email Address*" />
							 				<input type="text" placeholder="Phone Number" />
							 				<textarea placeholder="Message should have more than 50 characters"></textarea>
							 				<button class="submit">Send Email</button>
							 				<span>You accepts our <a href="#" title="">Terms and Conditions</a></span>
							 			</form>
							 		</div> --}}
						 		</div>
				 			</div>
				 		</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')


@endpush
