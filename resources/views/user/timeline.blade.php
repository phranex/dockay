@extends('layouts.master')

@push('title')
    {{ auth()->user()->username }} | Feed

@endpush
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link href="{{asset('assets2/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet">

    <style>
   .owl-stage{
       margin:auto;
   }

    #image-preview{
            background-size: cover;
        background-position: center center;
    }
    body{
        background: #eaeaea
    }

    .ti{
        text-align: right;
        font-weight:400;
        padding: 0 20px;
        font-size:12px;
        margin:0;
    }

    .th{
        top:10px !important;
        width:50px;
        min-width:unset;
    }

    .th .dropdown-item{
            min-width: unset;
            max-width: unset;
    }

    .font-10{
        font-size:10px;
    }

    </style>

@endpush
@section('content')

<div class="container-fluid">

        <div class="row">

                @include('inc.user.profile')
                <div class="col-12 d-block d-md-none d-lg-none owl-carousel m-auto owl-theme d-xl-none" style="width:100%">
                        <div class="content"><a  href='{{route('user.job.create')}}' class="btn  btn-outline-primary top-menu-tl"><i class="fa fa-plane"></i></a></div>

                        <div class="content"><a  href="{{route('user.ad.create')}}" class="btn btn-outline-light  top-menu-tl"><i class="fa fa-shopping-cart "></i> </a></div>
                        {{--  <div class="content"><a  href="#" class="btn  btn-outline-dark top-menu-tl "><i class="fa fa-share "></i> </a></div>  --}}
                        @if(auth()->user()->userType == 1)
                        <div class="content"><a  style="" class="btn   btn-outline-dark top-menu-tl" href="{{route('user.applications.index')}}"><i class="fa fa-briefcase"></i></a></div>
                        @else
                        <div class="content"><a style="width:100%" class="btn btn-outline-dark top-menu-tl" href="{{route('user.adverts.index')}}"><i class="fa fa-briefcase"></i></a></div>
                        @endif
                </div>
                <div class="col-md-12 car bg-transparent dockay-card col-xl-6">
                        @include('inc.activation')
                        @if(isset($items))
                            @if(count($items))
                                @foreach ($items as $item)
                                    @if(count($items))
                                        @if(isset($item->hasBeenRead))
                                            <div class="card m-b-30 ">
                                                    <p class='ti'>Job</p>
                                                    <div class="card-header">
                                                    <div style="height:55px">
                                                            <img src="{{  displayDefaultPhoto($item->facility->user)}}" alt="user" class="rounded-circle avatar avatar-img img-fluid">
                                                            <a href="{{route('user.profile.view', $item->facility->user->username)}}"><span class='poster-details'>{{ getFacilityUser($item->facility_id)}}</span></a>
                                                        <span class='pt job-type'>
                                                        {{$item->category->name}}
                                                        </span>

                                                        </div>

                                                    </div>
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title'><a href="{{route('job.details', $item->id)}}">{{$item->title}}</a></h5>
                                                        <p class="post-content">{{$item->description}}</p>
                                                        <footer class="blockquote-footer">posted {{when($item->created_at)}}</footer>
                                                        </blockquote>
                                                        <div class="card-body flex  text-center">
                                                        <p class="col more-details"><i class="text-success fa fa-money"></i> @if(isset($item->min)) #{{number_format($item->min,2,'.',',')}} - #{{number_format($item->max,2,'.','')}} @else N/A @endif</p>
                                                        <p class="col more-details"><i class=" text-danger fa fa-clock-o"></i> {{$item->deadline}}</p>
                                                        <p class="col more-details"><i class="fa fa-map-marker"></i> {{getFacilityLocation($item)}}</p>
                                                        </div>
                                                    </div>
                                            </div>
                                        @elseif(isset($item->post))

                                        @elseif(isset($item->slug))
                                             <div class="card m-b-30 ">
                                                <p class='ti'>Question</p>
                                                <div class="card-header">
                                                <div style="height:55px">
                                                        <img src="@if (!$item->isAnonymous) {{displayDefaultPhoto($item->user)}} @else {{ url()->to('/images/pp/nodp.png') }} @endif" alt="user" class="rounded-circle avatar avatar-img img-fluid">
                                                    @if(!$item->isAnonymous)
                                                    <a href="{{route('user.profile.view', $item->user->username)}}"><span class='poster-details'>{{$item->user->username}}</span></a>
                                                    @else
                                                        <a href="#"><span class='poster-details'>Anonymous</span></a>
                                                    @endif
                                                    <span class='pt job-type'>
                                                        Question
                                                    </span>

                                                    </div>

                                                </div>
                                                <div class="card-body">
                                                    <blockquote class="blockquote mb-0">
                                                    <h5 class='post-content text-bold'><a href='{{ route('question.show', $item->slug) }}'>{{ title_case($item->title)}} ?</a></h5>
                                                    <footer class="blockquote-footer">Asked <small>{{when($item->created_at)}}</small> <i class='pull-right'>{{ count($item->answers) }} answers</i></footer>
                                                    </blockquote>
                                                    <div class="card-body flex  text-center">
                                                    <p class="col more-details"><a href='{{ route('question.show', $item->slug) }}'><i class='fa fa-comments'></i> Answer</a></p>
                                                    <p class="col more-details "><i type='question' data-url="{{  route('question.show', $item->slug) }}" class='fa fa-share-alt-square share-bn'></i> Share</p>
                                                    {{--  <p class="col more-details"><i  class='fa fa-arrow-up'></i> Upvote</p>  --}}
                                                   
                                                    </div>
                                                </div>
                                        </div>

                                        @elseif(isset($item->brand))
                                            <div class="card m-b-30 ">
                                                    <p class='ti'>Sales</p>
                                                    <div class="card-header">
                                                    <div style="height:55px">
                                                            @if(isset($item->images))
                                                                    @if(count($item->images))
                                                                        @foreach ($item->images as $images)
                                                                            <img style='height:50px' src='{{$images->path}}' class="img-fluid avatar rounded-circle" />
                                                                            @break
                                                                        @endforeach

                                                                    @endif
                                                                @endif
                                                                <a href="{{route('product.details', $item->id)}}"><span class='poster-details'>{{$item->name}}</span></a>
                                                            <span class='pt job-type'>
                                                                    {{title_case($item->nature)}}
                                                            </span>

                                                        </div>

                                                    </div>
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <h5 class='post-title'><a href="{{route('product.details', $item->id)}}">{{$item->title}}</a></h5>
                                                        <p class="post-content">{{  substr($item->description,0, 100)}} ...</p>
                                                        <footer class="blockquote-footer">posted {{when($item->created_at)}}</footer>
                                                        </blockquote>
                                                        <div class="card-body flex  text-center">
                                                        <p class="col more-details"><i class="text-success fa fa-money"></i> #{{number_format($item->price,2,'.',',')}}</p>
                                                        <p class="col more-details"><i class=" text-danger fa fa-clock-o"></i> {{$item->quantity}}</p>
                                                        <p class="col more-details "><i type='sales' data-url="{{route('product.details', $item->id)}}" class='fa fa-share-alt-square share-bn'></i> Share</p>


                                                        </div>
                                                    </div>
                                            </div>

                                        @else
                                        <div class="card m-b-30 ">
                                                <div class='ti'>Random Jobs
                                                    <span class="dropdown show">
                                                        
                                                         @if(auth()->id() == $item->user_id)
                                                         <a class="btn btn-link dropdown-toggle" href="https://example.com" id="dropdownMenuLinkR{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                           
                                                        </a>
                                                        <div class="dropdown-menu th" aria-labelledby="dropdownMenuLinkR{{ $item->id }}">
                                                            <a class="dropdown-item" href="#"><i class='fa fa-edit'></i></a>
                                                            <a class="dropdown-item" href="#"><i  data='{{$item->id}}'  class='fa fa-trash delete'></i></a>
                                                        </div>
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="card-header">
                                                <div style="height:55px">
                                                        <img src="{{displayDefaultPhoto($item->user)}}" alt="user" class="rounded-circle avatar avatar-img img-fluid">
                                                    <a href="{{route('user.profile.view', $item->user->username)}}"><span class='poster-details'>{{$item->user->username}}</span></a>
                                                    <span class='pt job-type'>
                                                        {{$item->category->name}}
                                                    </span>

                                                    </div>

                                                </div>
                                                
                                                <div class="card-body ">
                                                    <blockquote class="blockquote mb-0">
                                                    <h5 class='post-content'>{{$item->description}}</h5>
                                                    <footer class="blockquote-footer">Posted <small>{{when($item->created_at)}}</small></footer>
                                                    </blockquote>

                                                    
                                                    
                                                    <div class='card-body text-center'>
                                                        Tags: 
                                                        <span class='more-details-mobile'><i class="fa fa-{{$item->gender}}"></i> {{$item->gender}}</span>
                                                        <span class='more-details-mobile'>{{$item->specialty->name}}</span>
                                                        <span class='more-details-mobile'>{{$item->establishment->name}}</span>
                                                        <span class='more-details-mobile'>
                                                                <a class='share-b' target='_blank' href="{{ env('WHATSAPP_URL') }}text=Hi guys, I found a job I think you should checkout on {{ url('/') }}/{{$item->id}} . {{  title_case($item->description)}}" data-url='' ><i class="fa fa-share"> Share</i></a>

                                                        </span>
                                                         @if($item->allow_whatsapp || $item->contact)
                                                        <span class='more-details-mobile btn-outline'>contact
                                                            <a onclick="window.open('{{ env('WHATSAPP_URL') }}phone={{convertToInternationalNumber($item->contact)}}&text=I am interested in the following job posted on {{ env('APP_URL') }}. {{  title_case($item->description)}}')" href='javascript:void()'><i class='fa fa-whatsapp'></i></a>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                        </div>

                                        @endif
                                    @endif
                                @endforeach
                                @if(auth()->user()->userType == 1)
                                    <div class='card dockay-card'>
                                     <p class='text-info'>In the mean time, if you are privy to any information regarding jobs, 
                                     locum, cover ups etc. Click on the <strong>Post Job</strong> button and post the job.  You can also sell your used/new medical equipment or device.
                                     There are people ready to buy. You can also respond to questions asked in the forum. The more questions you answer, the highly ranked you become 
                                     . When patients come looking for a professional, be sure to top the list. <i class="em em-slightly_smiling_face"></i>.
                                    <strong>{{ title_case(config('app.name')) }}</strong> is an information sharing community.
                                    </p>
                                </div>
                                @else
                                <div class='card dockay-card'>
                                     <p class='text-info'>In the mean time, you can interact with the questions posted in the form. Remember: the more answers you give, the higher your rating
                                     on Dockay. When patients come to search for a facility or hospital close by, you will top the search result. <i class="em em-slightly_smiling_face"></i>.

                                    <strong>{{ title_case(config('app.name')) }}</strong> is an information sharing community.
                                    </p>
                                </div>
                                @endif
                            @else
                                @if(auth()->user()->userType == 1)
                                <div class='alert alert-info text-center'>
                                    <h3>It appears your timeline is sick and hungry <i class='em em-cry'></i>. Here's the cure. Update your Profile, choose your specialty and jobs matching your profile
                                    will automatically show on your timeline when posted.</h3>

                                   
                                </div>
                                <div class='card dockay-card'>
                                     <p class='text-info'>In the mean time, if you are privy to any information regarding jobs, 
                                     locum, cover ups etc. Click on the <strong>Post Job</strong> button and post the job.  You can also sell your used/new medical equipment or device.
                                     There are people ready to buy.
                                    <strong>{{ title_case(config('app.name')) }}</strong> is an information sharing community.
                                    </p>
                                </div>
                                @else
                                    <div class='alert alert-info text-center'>
                                    <h3>It appears your timeline is sick and hungry for content. <i class='em em-cry'></i>. Here's the cure. Update your Profile, Post vacancies, sell used/new on Dockay.

                                 </h3>

                                   
                                </div>
                                <div class='card dockay-card'>
                                     <p class='text-info'>In the mean time, you can interact with the questions posted in the form. Remember: the more answers you give, the higher your rating
                                     on Dockay. When patients come to search for a facility or hospital close by, you will top the search result. <i class="em em-slightly_smiling_face"></i>.

                                    <strong>{{ title_case(config('app.name')) }}</strong> is an information sharing community.
                                    </p>
                                </div>
                                @endif

                            @endif

                        @endif

                </div>
                {{--  @include('inc.user.quick-links')  --}}
        </div>

</div>


<!-- Modal for items -->



@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
    <script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
    <script src='{{asset('assets2/pages/upload-init.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
   
    
    $('.upd-social,.upd-profile,.upd-contact').hide();

        $(document).ready(function(){
            populateCountries("country", "state", '{{auth()->user()->country}}', '{{auth()->user()->state}}');
            $('.profile,.social,.contact').attr('disabled','disabled');
            $('.upd-social,.upd-profile,.upd-contact').hide();
   
            

            $('.edit-profile').click(function(){
                $(this).hide();
                $('.profile').removeAttr('disabled');
                $('.upload-info').removeClass('invisible');
                $('.upd-profile').show()
            });
            $('.edit-social').click(function(){
                $(this).hide();
                $('.social').removeAttr('disabled');
                $('.upd-social').show()
            });
            $('.edit-work').click(function(){
                $(this).hide();
                $('.work').removeAttr('disabled');
                $('.upd-work').show();
            });
           // $('.edit-education').click(function(){
             //   $(this).hide();
               // $('.education').removeAttr('disabled');
                //$('.upd-education').show();
            //});

            $('#edit-education-form').submit(function(){
                event.preventDefault();

                var formData = [];
                formData['#edit-discipline'] = 'required|alpha';
                formData['#edit-from'] = 'required';
                formData['#edit-to'] = 'required';
                formData['#edit-institution'] = 'required';
                formData['#edit-edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#edit-discipline').val(),
                        "from": $('#edit-from').val(),
                        "to": $('#edit-to').val(),
                        "institution": $('#edit-institution').val(),
                        "description": $('#edit-edu-desc').val(),
                        "education_id": $('#edit-edu-id').val()

                    }
                    console.log(data);
                    editEducation(data);
                }


            });
            $('#edit-work-form').submit(function(){
                event.preventDefault();
                var formData = [];
                formData['#edit-title'] = 'required|alpha';
                formData['#edit-work-from'] = 'required';

                formData['#edit-company'] = 'required';
                formData['#edit-work-desc'] = 'required|max=500';
                if($('#edit-work-present').is(':checked')){
                   var  end = $('#edit-work-present').val();

                }else if(!isEmpty('#edit-work-to')){

                    var end = $('#edit-work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#edit-title').val(),
                            "from": $('#edit-work-from').val(),
                            "to": end,
                            "company": $('#edit-company').val(),
                            "description": $('#edit-work-desc').val(),
                            "work_id": $('#work_id').val()
                        }
                        console.log(data);
                        editExperience(data);
                    }
                }else{
                    $('.edit-work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }
            });


            $('.upd-profile').click(function(){
                var formData = [];
                formData['#fn'] = 'required|alpha';
                formData['#ln'] = 'required|alpha';
                formData['#phonenum'] = 'number';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#gender'] = 'required';
                formData['#qualification'] = 'required';
                formData['#desc'] = 'max=500';

                if(validate(formData, 'field-error')){
                    var data = {
                        "firstName": $('#fn').val(),
                        "lastName": $('#ln').val(),
                        "phoneNumber": $('#phonenum').val(),
                        "gender": $('#gender').val(),
                        "qualification": $('#qualification').val(),
                        "country": $('#country').val(),
                        "state": $('#state').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateProfile(data);
                }
            });
            $('.add-education').click(function(){
                var formData = [];
                formData['#discipline'] = 'required|alpha';
                formData['#from'] = 'required';
                formData['#to'] = 'required';
                formData['#institution'] = 'required';
                formData['#edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    addEducation(data);
                }
            });
            $('.upd-education').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });
            $('#work-present').click(function(){

                if($(this).is(':checked')){
                   $('.work-end').hide();
                }else{
                    $('.work-end').show();
                }
                $('.work-ended').removeClass('field-error');
            });
            $('#edit-work-present').click(function(){

                if($(this).is(':checked')){
                   $('.edit-work-end').hide();
                }else{
                    $('.edit-work-end').show();
                }
                $('.edit-work-ended').removeClass('field-error');
            });
            $('.add-work').click(function(){
                var formData = [];
                formData['#title'] = 'required|alpha';
                formData['#work-from'] = 'required';

                formData['#company'] = 'required';
                formData['#work-desc'] = 'required|max=500';
                if($('#work-present').is(':checked')){
                   var  end = $('#work-present').val();

                }else if(!isEmpty('#work-to')){
                    console.log($('#work-to').val());
                    var end = $('#work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#title').val(),
                            "from": $('#work-from').val(),
                            "to": end,
                            "company": $('#company').val(),
                            "description": $('#work-desc').val(),

                        }
                        console.log(data);
                        addWork(data);
                    }
                }else{
                    $('.work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }

            });
            $('.upd-work').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });

             $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data);
                }

            });



            $('.addEducation').click(function(){
                $('.edu-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });
            $('.addWork').click(function(){
                $('.work-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });

        });
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>



<script>
    $(document).ready(function(){
        $('#image-upload').change(function(){
            if($(this).val() != ''){
                $('#upload-btn').show();
            }else{
                $('#upload-btn').hide();
            }
        });

        $('.owl-carousel').owlCarousel({
            loop:false,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:4,
                    nav:false
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }
        })


        $('.education-edit').click(function(){
             $('.edit-education-data').val();
            var data = JSON.parse($(this).attr('data'));
            $('#edit-discipline').val(data.discipline);
            $('#edit-to').val(data.to);
            $('#edit-from').val(data.from);
            $('#edit-institution').val(data.institution);
            $('#edit-edu-desc').val(data.description);
            $('#edit-edu-id').val(data.id);
             $('#educationModal').modal();

        });
        $('.edit-work-exp').click(function(){
            $('.edit-work-data').val('');
            var data = JSON.parse($(this).attr('data'));
            $('#edit-title').val(data.title);

            $('#edit-work-from').val(data.from);
            $('#edit-company').val(data.company);
            $('#edit-work-desc').val(data.description);
            $('#work_id').val(data.id);
            if(data.to == 'P'){
                //check the present box
                $('#edit-work-present').prop('checked', true);
                //hide the to input
                $('.edit-work-end').hide();
            }else{
                $('#edit-work-to').val(data.to);
                $('.edit-work-end').show();
                $('#edit-work-present').removeAttr('checked');
            }

             $('#workModal').modal();

        });

        $('.education-delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Education!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.education.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        $('.delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete This Job!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.job.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });



</script>


@endpush
