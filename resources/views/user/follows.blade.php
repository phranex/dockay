@extends('layouts.master')

@push('title')
    {{ auth()->user()->username }} | Follow

@endpush

@section('content')

    <div class="text-cente container">
       
        <div class=''>
            @if(isset($follows))
                 
                @if(count($follows))
                    {{--  <h3 class='text-center'>People you are following</h3>  --}}
                    @foreach($follows as $follow)
                        <div class="col-lg-3 col-12 col-md-4 card d-inline-box  m-t-10">
                            <div class="profile-pix h-100 col-4 col-md-5 ">
                                <img style='width:200px' class="img-fluid  rounded-circle  " src="@if(isset($follow->profileImage->path)){{ $follow->profileImage->path }} @else {{ displayDefaultPhoto($follow) }} @endif "/>

                            </div>
                            <div class="profile-info  col-8 col-md-8">
                                <h6><strong><a href='{{ route('user.profile.view',$follow->username) }}'>{{ getUserFullName($follow) }}</strong></a></h6>
                                {{--  <span>MD, Eye Masters</span><br/>  --}}
                                @if(auth()->user()->isFollowing($follow))
                                     <a href='{{ route('unFollowUser', $follow->username) }}' class="btn btn-link">Unfollow</a>
                                @else
                                   
                                     <a href='{{ route('followUser', $follow->username) }}' class="btn btn-primary">Follow</a>
                                     
                                @endif
                                

                            </div>
                        </div>

                    @endforeach
                @else
                    <h6 class='text-center'>No user found</h6>

                @endif
          
            @endif

           

        </div>


            

            
            
      

        
    </div>



@endsection
