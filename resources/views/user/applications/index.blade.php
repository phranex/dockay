@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <style>

    #image-preview{
            background-size: cover;
        background-position: center center;
    }
    body{
        background: #eaeaea
    }

    .dropdown{
        display: inline-block;
    }

    </style>

@endpush

@push('title')
    {{ auth()->user()->username }} | Applications

@endpush
@section('content')
<div class="container-fluid">
        <div class="row">
                @include('inc.user.profile')
                <div class="col-md-12  bg-transparent dockay-card col-xl-6">
                    <div class="col-xl-10 m-auto">
                        @if(isset($applications))
                            @if(count($applications))
                                @foreach ($applications as $item)

                                        <div class="card m-b-30 ">
                                                <div class="card-header">
                                                    <div style="height:55px">
                                                        {{--  <img src="{{  displayDefaultPhoto($item->user)}}" alt="user" class="rounded-circle avatar-img img-fluid">  --}}
                                                        <a href="{{route('user.profile.view', $item->jobAdvert->facility->user->username)}}"><span class='poster-details'>{{$item->jobAdvert->facility->name}}</span></a>
                                                        <span class='pt job-type'>
                                                        {{$item->jobAdvert->category->name}}
                                                        </span>
                                                        @if($item->shortlist_flag == 1)
                                                        <span style="margin-right:2px" class='pt job-type'>
                                                            shortlisted
                                                        </span>
                                                        @endif

                                                    </div>

                                                </div>
                                                <div class="card-body">
                                                    <blockquote class="blockquote mb-0">
                                                    <h5 class='post-title'><a href="{{route('job.details', $item->jobAdvert->id)}}">{{$item->jobAdvert->title}}</a></h5>
                                                    <p class="post-content">{{$item->jobAdvert->description}}</p>
                                                    <footer class="blockquote-footer inline-block">Posted {{when($item->jobAdvert->created_at)}}</footer>
                                                    <footer class="blockquote-footer inline-block ">Applied {{when($item->created_at)}}</footer>
                                                    </blockquote>
                                                    <div class="card-body flex  text-center">
                                                    <p class="col more-details"><i class="text-success fa fa-money"></i> @if(isset($item->jobAdvert->min)) #{{ format_number($item->jobAdvert->min)}} - #{{ format_number($item->jobAdvert->max)}} @else N/A @endif</p>
                                                    <p class="col more-details" title="Deadline"><i class=" text-danger fa fa-clock-o"></i> {{when($item->jobAdvert->deadline)}}</p>
                                                    {{--  <p class="col more-details"><i class="fa fa-map-marker"></i> {{getFacilityLocation($item)}}</p>  --}}
                                                    </div>
                                                </div>
                                        </div>

                                @endforeach
                            @else
                                <div class='card alert text-center'>
                                    <div>
                                        You are yet to apply for a job
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
               @include('inc.user.quick-links')
        </div>

</div>





@endsection

@push('scripts')
    <script src='{{asset('js/validation.js')}}'></script>
    <script src='{{asset('js/profile.js')}}'></script>
    <script src='{{asset('js/countries.js')}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
    $('.upd-social,.upd-profile,.upd-contact').hide();

        $(document).ready(function(){
            populateCountries("country", "state", '{{auth()->user()->country}}', '{{auth()->user()->state}}');
            $('.profile,.social,.contact').attr('disabled','disabled');
            $('.upd-social,.upd-profile,.upd-contact').hide();

            $('.edit-profile').click(function(){
                $(this).hide();
                $('.profile').removeAttr('disabled');
                $('.upload-info').removeClass('invisible');
                $('.upd-profile').show()
            });
            $('.edit-social').click(function(){
                $(this).hide();
                $('.social').removeAttr('disabled');
                $('.upd-social').show()
            });
            $('.edit-work').click(function(){
                $(this).hide();
                $('.work').removeAttr('disabled');
                $('.upd-work').show();
            });
           // $('.edit-education').click(function(){
             //   $(this).hide();
               // $('.education').removeAttr('disabled');
                //$('.upd-education').show();
            //});

            $('#edit-education-form').submit(function(){
                event.preventDefault();

                var formData = [];
                formData['#edit-discipline'] = 'required|alpha';
                formData['#edit-from'] = 'required';
                formData['#edit-to'] = 'required';
                formData['#edit-institution'] = 'required';
                formData['#edit-edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#edit-discipline').val(),
                        "from": $('#edit-from').val(),
                        "to": $('#edit-to').val(),
                        "institution": $('#edit-institution').val(),
                        "description": $('#edit-edu-desc').val(),
                        "education_id": $('#edit-edu-id').val()

                    }
                    console.log(data);
                    editEducation(data);
                }


            });
            $('#edit-work-form').submit(function(){
                event.preventDefault();
                var formData = [];
                formData['#edit-title'] = 'required|alpha';
                formData['#edit-work-from'] = 'required';

                formData['#edit-company'] = 'required';
                formData['#edit-work-desc'] = 'required|max=500';
                if($('#edit-work-present').is(':checked')){
                   var  end = $('#edit-work-present').val();

                }else if(!isEmpty('#edit-work-to')){

                    var end = $('#edit-work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#edit-title').val(),
                            "from": $('#edit-work-from').val(),
                            "to": end,
                            "company": $('#edit-company').val(),
                            "description": $('#edit-work-desc').val(),
                            "work_id": $('#work_id').val()
                        }
                        console.log(data);
                        editExperience(data);
                    }
                }else{
                    $('.edit-work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }
            });


            $('.upd-profile').click(function(){
                var formData = [];
                formData['#fn'] = 'required|alpha';
                formData['#ln'] = 'required|alpha';
                formData['#phonenum'] = 'number';
                formData['#country'] = 'required';
                formData['#state'] = 'required';
                formData['#gender'] = 'required';
                formData['#qualification'] = 'required';
                formData['#desc'] = 'max=500';

                if(validate(formData, 'field-error')){
                    var data = {
                        "firstName": $('#fn').val(),
                        "lastName": $('#ln').val(),
                        "phoneNumber": $('#phonenum').val(),
                        "gender": $('#gender').val(),
                        "qualification": $('#qualification').val(),
                        "country": $('#country').val(),
                        "state": $('#state').val(),
                        "description": $('#desc').val(),
                    }
                    console.log(data);
                    updateProfile(data);
                }
            });
            $('.add-education').click(function(){
                var formData = [];
                formData['#discipline'] = 'required|alpha';
                formData['#from'] = 'required';
                formData['#to'] = 'required';
                formData['#institution'] = 'required';
                formData['#edu-desc'] = 'required';
                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    addEducation(data);
                }
            });
            $('.upd-education').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });
            $('#work-present').click(function(){

                if($(this).is(':checked')){
                   $('.work-end').hide();
                }else{
                    $('.work-end').show();
                }
                $('.work-ended').removeClass('field-error');
            });
            $('#edit-work-present').click(function(){

                if($(this).is(':checked')){
                   $('.edit-work-end').hide();
                }else{
                    $('.edit-work-end').show();
                }
                $('.edit-work-ended').removeClass('field-error');
            });
            $('.add-work').click(function(){
                var formData = [];
                formData['#title'] = 'required|alpha';
                formData['#work-from'] = 'required';

                formData['#company'] = 'required';
                formData['#work-desc'] = 'required|max=500';
                if($('#work-present').is(':checked')){
                   var  end = $('#work-present').val();

                }else if(!isEmpty('#work-to')){
                    console.log($('#work-to').val());
                    var end = $('#work-to').val();
                }else{
                    var end = false;
                }
                if(end){
                    if(validate(formData, 'field-error')){
                        var data = {
                            "title": $('#title').val(),
                            "from": $('#work-from').val(),
                            "to": end,
                            "company": $('#company').val(),
                            "description": $('#work-desc').val(),

                        }
                        console.log(data);
                        addWork(data);
                    }
                }else{
                    $('.work-ended').addClass('field-error').focus();
                    $.notify('Please Select an end date');
                }

            });
            $('.upd-work').click(function(){
                var formData = [];
                formData['.education-data'] = 'required';
                formData['#discipline'] = 'required|alpha';


                if(validate(formData, 'field-error')){
                    var data = {
                        "discipline": $('#discipline').val(),
                        "from": $('#from').val(),
                        "to": $('#to').val(),
                        "institution": $('#institution').val(),
                        "description": $('#edu-desc').val(),

                    }
                    console.log(data);
                    updateEducation(data);
                }
            });

             $('.add-social').click(function(){
                var formData = [];
                formData['#facebook'] = 'url';
                formData['#twitter'] = 'url';
                formData['#google-plus'] = 'url';
                formData['#linkedin'] = 'url';
                if(validate(formData, 'field-error')){
                        var data = {
                            "facebook": $('#facebook').val(),
                            "twitter": $('#twitter').val(),
                            "linkedin": $('#linkedin').val(),
                            "googleplus": $('#google-plus').val(),
                        }
                        console.log(data);
                        addSocial(data);
                }

            });



            $('.addEducation').click(function(){
                $('.edu-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });
            $('.addWork').click(function(){
                $('.work-box').toggle();
                $(this).find('i').toggleClass('la-plus la-times');
            });

        });
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>



<script>
    $(document).ready(function(){
        $('#image-upload').change(function(){
            if($(this).val() != ''){
                $('#upload-btn').show();
            }else{
                $('#upload-btn').hide();
            }
        });


        $('.education-edit').click(function(){
             $('.edit-education-data').val();
            var data = JSON.parse($(this).attr('data'));
            $('#edit-discipline').val(data.discipline);
            $('#edit-to').val(data.to);
            $('#edit-from').val(data.from);
            $('#edit-institution').val(data.institution);
            $('#edit-edu-desc').val(data.description);
            $('#edit-edu-id').val(data.id);
             $('#educationModal').modal();

        });
        $('.edit-work-exp').click(function(){
            $('.edit-work-data').val('');
            var data = JSON.parse($(this).attr('data'));
            $('#edit-title').val(data.title);

            $('#edit-work-from').val(data.from);
            $('#edit-company').val(data.company);
            $('#edit-work-desc').val(data.description);
            $('#work_id').val(data.id);
            if(data.to == 'P'){
                //check the present box
                $('#edit-work-present').prop('checked', true);
                //hide the to input
                $('.edit-work-end').hide();
            }else{
                $('#edit-work-to').val(data.to);
                $('.edit-work-end').show();
                $('#edit-work-present').removeAttr('checked');
            }

             $('#workModal').modal();

        });

        $('.education-delete').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Education!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.education.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        $('.delete-work-exp').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Work Experience!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('user.work.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
    });

</script>


@endpush
