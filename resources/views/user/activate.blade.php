@extends('layouts.master')
@push('styles')


@endpush
@push('title')
    Authentication | Verification

@endpush

@section('content')
    

        <div class="container-fluid">
            <div class='ro flex '>
                <div style='margin-top:60px !important'  class='col-md-8 m-t-40 card  alert-dange m-auto col-lg-6'>
                    <div class='col-12 text-center'>
                        <i class="em em-cold_sweat animated zoomIn " style='font-size:50px'></i>
                       
                    </div>
                    <div class=''>
                    <p class='text-danger'>You are not allowed to perform that action. Your profile looks ill. Please 
                    @if(!auth()->user()->verified)
                        <a  href='{{ route('user.timeline', getFullNameWithSlug(auth()->user())) }}'>ACTIVATE</a> 
                        and/or 
                    @endif
                    <a  href='@if(auth()->user()->userType == 1){{ route('user.profile', getFullNameWithSlug(auth()->user())) }} @elseif(auth()->user()->userType == 2) {{ route('user.facility.profile', getFullNameWithSlug(auth()->user())) }} @endif '>UPDATE</a> your profile to feel well again .</p>
                        <div class='text-right'>
                            <button class='btn' onclick='history.back()'>Go Back</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>


@endsection

@push('scripts')


@endpush
