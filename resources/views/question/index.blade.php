@extends('layouts.master')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">


@endpush
@push('title')
    {{ auth()->user()->username }} | Questions

@endpush
@section('content')
<div class="container-fluid ">
        <div class="ro mt-20">
                @include('inc.activation')
                @if(isset($questions))
                    @if(count($questions))
                        
                        @foreach ($questions as $question)
                             <div class='col-md-6 m-auto card ' style='margin-top:10px !important; '>
                                    <h6><a href='{{ route('question.show', $question->slug) }}'>{{ $question->title }} ?</a>
                                    <small style='padding-left:10px' class='pull-right'><a href='{{ route('question.edit', $question->slug) }}'> <i class='fa fa-edit'></i> Edit</a></small>
                                    <small class='pull-right text-danger'><a  href='{{ route('question.delete', $question->slug) }}'> <i style='color:red !important' class='fa fa-trash'></i> Delete</a></small>
                                    </h6>
                                    <div class='col-12 text-right'>
                                        <span>{{ count($question->answers) }} Answers</span>
                                        {{--  <span>20 likes</span>  --}}
                                    </div>
                            </div>
                        @endforeach
                    @else
                        <div class='alert alert-default'>
                            <div class='jumbotron text-center'>
                                <h6>You haven't asked a question yet. Something's on your mind? ask the community <a href='{{ route('question.create') }}'>here</a></h6>
                            </div>
                        </div>
                        
                       
                    @endif
                @endif
        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>
<script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
<script src='{{asset('assets2/pages/upload-init.js')}}'></script>


<script>

    $('#loaderImage').show();

    $(document).ready(function(){
        $('#question').modal({
            backdrop: 'static'
        });
        $('#loaderImage').hide();

         {{--  $('#create').submit(function(){
            event.preventDefault();
            var title = $('#q-title').text();
            $('#title').val(title);
           
             $(this).unbind().submit();
         });  --}}

         $('#q-title').keyup(function(event){
            
              

              
             
             //$(this).text(q+ ' ?');
         });
    });

</script>

@endpush
