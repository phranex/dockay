@extends('layouts.master')
@push('styles')
{{--  <link href="{{asset('css/tagInput.css')}}" rel="stylesheet">  --}}
<!-- include summernote css/js -->
{{--  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">  --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css" rel="stylesheet">
<style>
    .q-options{
        padding:10px;
        cursor: pointer;
    }

    html {
    scroll-behavior: smooth;
    }

    .btn{
        background: transparent !important;
        color: unset !important;
    }

    .note-popover .popover-content .dropdown-menu, .panel-heading.note-toolbar .dropdown-menu{
        min-width:150px;
    }

    .note-check li a{
        color: unset;
    }
    .btn-group, .btn-group-vertical{
        margin: 0px 1px !important;
    }

    .dropdown-menu{
        right: unset !important;
    }

    .answer{
        min-height:100px;
        border:1px solid #64bdf9;
        padding: 20px;
    }
    .clicked{
       {{--  background: #ddd !important;  --}}
    }

    .font-formatting{
            padding-left: 14px;
    padding-right: 14px;
             font-weight:bold;

       
         border:1px solid transparent;
         cursor:pointer;
         border-radius:6px;

    }

    .format-box{
        {{--  border:1px solid #ddd;  --}}
        padding: 10px;

    }

    .font-formatting:hover{
        border:1px solid #ddd;
         font-weight:bold;
    }

    @media (min-width: 576px){
        .modallo {
            max-width: 50% !important;
            
        }
    }


    .user-answer{
        padding:20px;
        padding-right: 0;
    }

    .q-comments{
        padding:10px;
        cursor:pointer;
    }
</style>

@endpush
@push('title')
    Dockay| {{ substr($question->title, 0, 150) }} .... 

@endpush
@section('content')
<div class="container-fluid">
        <div class="ro">
            @auth
                @include('inc.activation')
            @endauth

                <div class="col-md-8 card bg-transparent dockay-card">
                        <div>
                            <h5 class='text-capitalize text-bold'>{{ $question->title }} ?</h5>
                            <small class='text-muted'> - Asked by 
                                <strong>
                                  

                                    @if($question->user_id != auth()->id()) 

                                          @if(!$question->isAnonymous)
                                                    <a href="{{route('user.profile.view', $question->user->username)}}"><span class='poster-details'>{{$question->user->username}}</span></a>
                                            @else
                                                <a href="#"><span class='poster-details'>Anonymous</span></a>
                                            @endif
                                    @else you
                                    
                                    
                                    @endif
                                   
                                   
                                    
                                </strong> {{ when($question->created_at) }}
                                
                                
                                </small>

                            <div class='mt-20'>
                                @auth
                                <span class='q-options answer-btn '><i class='fa fa-pencil-square '></i> Answer</span>
                                @endauth
                                <span class='q-options'><i class='fa fa-rss'></i> Follow</span>
                                <span class='q-options'><i type='question' data-url="{{  route('question.show', $question->slug) }}" class=' share-bn fa fa-share-alt-square'></i> Share</span>
                                <span class='text-bold font-12 pull-right'>{{ count($question->answers) }} Answers</span>
                            </div>
                        </div>



                        <div style='display:none' class='answer-box mt-10'>
                            <div class='ro'>
                                
                                <form id='answer-form' action='{{ route('answer.store') }}' method='post'>
                                    @csrf
                                    
                                    <input type='hidden'   value='{{ $question->id }}'  name='id' />

                                    <textarea id="summernote" required name="answer"></textarea>
                                    <div class='col-12 text-right mt-20'>
                                        <button type='submit' class='btn '>Answer</button>
                                    </div>
                                </form>

                                

                                
                            </div>
                        
                        </div>
                        

                </div>
                @if(isset($question->answers))
                    @if(count($question->answers))
                        <div class='text-center col-md-8 mt-20 dockay-card card'>
                           <p> @guest <a href='{{ route('login') }}?p={{ base64_encode(url()->current()) }}'>
                           Login</a> or <a href='{{ route('register') }}?p={{ base64_encode(url()->current()) }}'>Register</a> 
                           to answer  @else <a href='#'> <span class='q-options answer-btn '><i class='fa fa-pencil-square '></i> Answer this question</span></a>  @endguest
                        </p></div>
                        
                        @foreach($question->answers as $answer)
                            <div id='a{{ $answer->id }}' class="col-md-8 card bg-transparent dockay-card mt-20">
                                <div>
                                    @if($question->user_id == $answer->user_id && $question->isAnonymous)
                                        <img style='width:50px;height:50px' src='{{ url()->to('/images/pp/nodp.png') }}' class='img-fluid rounded-circle' />
                                        <span class='text-bold'><a href='#'>Anonymous</a></span>                                 
                                    @else
                                        <img style='width:50px;height:50px' src='{{ displayDefaultPhoto($answer->user) }}' class='img-fluid rounded-circle' />
                                        <span class='text-bold'><a href='{{ route('user.profile.view',$answer->user->username )}}'>{{ title_case($answer->user->username) }}</a> <small>@if($answer->user->userType == 1)Profession: {{ title_case(@$answer->user->practitioner->specialty->name) }} @endif</small></span>                                   
                                    @endif
                                    @if(auth()->id() == $answer->user_id)
                                        <span class='pull-right'>
                                            <i target='#answer{{ $answer->id }}' data='{{ $answer->id }}' class='fa fa-edit cursor-pointer edit-answer text-primary pl-1'></i>
                                            <i   data='{{ $answer->id }}' class='fa fa-trash cursor-pointer delete-answer text-danger pl-1'></i>
                                        </span>
                                    @endif
                                </div>
                                <div id='answer{{ $answer->id }}' class='user-answer'>
                                            {!! $answer->answer !!}
                                </div>



                                <div class='mt-20 '>
                                    <div class=' answer-stat pull-left'>
                                        <span  id='num_of_likes{{ $answer->id }}'>{{ count($answer->likes) }}</span> <span class='fa fa-thumbs-up text-danger'></span>
                                        <span id='no_of_comments'>{{ count($answer->comments) }}</span> <span data='{{ $answer->id }}'  class='fa fa-comments cursor-pointer viewComment text-primary'></span>
                                    </div>
                                    @auth

                                    <div class='pull-right'>
                                        <span data='{{ $answer->id }}' class='q-comments cursor-pointer comment-btn'><i class='fa fa-comment'></i> Comment</span>
                                        <span target='#num_of_likes{{ $answer->id }}' data='{{ $answer->id }}' class='q-comment cursor-pointer text-danger like-btn'><i class='fa @if(!auth()->user()->hasLikedThisAnswer($answer->id))fa-thumbs-o-up @else fa-thumbs-up @endif'></i></span>
                                
                                    </div>
                                    @endauth
                                        
                                    </div>

                        </div>
                        @endforeach
                    @else
                        <div class='text-center col-md-8 mt-20 dockay-card card'>
                           <p> Hey, be the first to answer this question!! @guest <a href='{{ route('login') }}?p={{ base64_encode(url()->current()) }}'>
                           Login</a> or <a href='{{ route('register') }}?p={{ base64_encode(url()->current()) }}'>Register</a> 
                           to answer  @else <a href='#'> <span class='q-options answer-btn '><i class='fa fa-pencil-square animated pulse infinite'></i> Answer this question</span></a>  @endguest
                        </p></div>
                        
                    @endif

                @endif

                <div class="modal fade" id="editAnswerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Edit Answer - {{ $question->title }} ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class=" m-b-30 ">
                                                        
                                    <form id='edit-answer-form' action='{{ route('answer.update') }}' method='post'>
                                            @csrf
                                            
                                            <input type='hidden'   value='' id='answer_id'  name='id2' />

                                            <textarea id="summernote2" required name="answer2"></textarea>
                                            <div class='col-12 text-right mt-20'>
                                                <button type='submit' class='btn '>Edit</button>
                                            </div>
                                        </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editCommentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Edit Comment </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class=" m-b-30 ">
                                                        
                                    <form id='edit-answer-form' action='{{ route('comment.update') }}' method='post'>
                                            @csrf
                                            
                                            <input type='hidden'   value='' id='comment_id'  name='comment_id' />

                                            <textarea id="summernote4" required name="comment_edit"></textarea>
                                            <div class='col-12 text-right mt-20'>
                                                <button type='submit' class='btn '>Edit</button>
                                            </div>
                                        </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                 <div class="modal fade" id="comments" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modallo modal-dialog-centere" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Question - {{ $question->title }} ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class=" m-b-30 ">
                                    <small>- Answered by <span class='text-primary' id='who-answered'></span></small>
                                    <p id='answer-comment' class="post-content "></p>
                                                        
                                    <form id='edit-answer-form' action='{{ route('comment.store') }}' method='post'>
                                            @csrf
                                            
                                            <input type='hidden'   value='' id='a_id'  name='id3' />

                                            <textarea id="summernote3" required name="comment"></textarea>
                                            <div class='col-12 text-right mt-20'>
                                                <button type='submit' class='btn '>Comment</button>
                                            </div>
                                        </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                 <div class="modal fade" id="viewComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modallo modal-dialog-centere" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="viewAnswer"></h5>
                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div id='commentBox' class=" m-b-30 ">
                                    
                                </div>

                                <div id='cm' style='display:none' class='text-center'>Be the first to comment. Click <button id='com' class='btn comment-btn'>here</button></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                




              
        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script src="{{asset('js/socialactions.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>
<script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
<script src='{{asset('assets2/pages/upload-init.js')}}'></script>
{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.js"></script>  --}}
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>


<script>

   

    $(document).ready(function(){
         CKEDITOR.replace( 'summernote' );
         CKEDITOR.replace( 'summernote2' );
         CKEDITOR.replace( 'summernote3' );
         CKEDITOR.replace( 'summernote4' );
       
        $('.btn-note').click(function(){
            
            $(this).toggleClass('clicked');
        });

        $('.viewComment').click(function(){
            var id = $(this).attr('data');
            $('#com').attr('data', id);
            getComments(id, '#commentBox');
            $('#viewComments').modal();
        });

        $('.like-btn').click(function(){
            var id = $(this).attr('data');
            var target = $(this).attr('target');
            var l_btn = $(this).find('i');
            addLike(id, target, l_btn);
        });

        $('.comment-btn').click(function(){
            $('#viewComments').modal('hide');
            var id = $(this).attr('data');
            $('#answer-comment').html('');
            $('#who-answered').html('');
            {{--  $('#who-answered').html('');  --}}
            getAnswer(id, '#answer-comment','#who-answered');
            $('#a_id').val(id);
            $('#comments').modal();
        });

        $('.edit-answer').click(function(){
            $('#answer_id').val($(this).attr('data'));
            var target = $(this).attr('target');
            var ans = $(target).html();
            CKEDITOR.instances.summernote2.setData(ans);
            //$('#summernote2').summernote('code', ans);
            console.log(ans);
            $('#editAnswerModal').modal();
        });

        $('span.answer-btn').click(function(){
            $('.answer-box').toggle();
            $(this).toggleClass('clicked');
        });

        $('#summernote,#summernote2,#summernote3,#summernote4').summernote({
            placeholder: '{{ $question->title }} ?',
            
            minHeight: 300,   
            
        });
        $('.delete-answer').click(function(){
            id= $(this).attr('data');
            $.confirm({
                title: 'Delete Answer!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('answer.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        });
        
            $('#answer-form').submit(function(){
                event.preventDefault();

                {{--  var ans = $('#summernote').summernote('code');  --}}
                var ans =  CKEDITOR.instances.summernote.getData();
                $('#answer').val(ans);
                console.log(ans);
                $(this).unbind().submit();

            });
            $('#edit-answer-form').submit(function(){
                event.preventDefault();
                //var ans = $('#summernote2').summernote('code');
                var ans =  CKEDITOR.instances.summernote2.getData();
                $('#edit-answer').val(ans);
                console.log(ans);
                $(this).unbind().submit();

            });
    });


</script>

@endpush
