@extends('layouts.master')
@push('styles')
<link href="{{asset('css/tagInput.css')}}" rel="stylesheet">
<link href="{{asset('css/slider.css')}}" rel="stylesheet">

<style>

    .switch{
        right: 15%;
        height:unset;
    }

    .slider:before{
        height:20px;
        width:20px;
    }

    .round{
        height:unset;
    }

    .l{
        float:left;
    }
</style>
@endpush
@push('title')
    {{ auth()->user()->username }} | Ask a Question

@endpush
@section('content')
<div class="container-fluid">
        <div class="ro">
                @include('inc.activation')

               <div class="modal fade" id="question" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ask Your Question <i class='fa fa-question-circle '></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="flex text-center">
                            <div>
                                <form enctype="multipart/form-data" id='create' action='{{route('question.store')}}' method='post'>
                                @csrf
                                    <div class="row text-center">
                                            <div class="col-lg-12">
                                                <p> Please make your question look and feel like a question. Just type your question without a question mark!!</p>
                                            </div>
                                            <div style="border:none !important" class="text-center w-100 mb-5 error">

                                            </div>
                                            
                                            <div class='col-12'>
                                                {{--  <p id='q-title' style='border-bottom:1px solid #64bdf9;text-align:left;padding-bottom:7px' class='text-bold text-capitalize' contenteditable=true  class='form-control' >  --}}
                                               
                                                <select class='form-control' name='category'>
                                                    <option value=''>Select a category</option>
                                                    @if(isset($categories))
                                                        @if(count($categories))
                                                        
                                                                @foreach($categories as $category)
                                                                    <option value ='{{ $category->id }}'>{{ $category->name }}</option>
                                                                @endforeach
                                                                
                                                            
                                                        @endif

                                                    @endif
                                                </select>
                                                <div class='form-group'>
                                                    <p id='ti' style='padding:20px;border-bottom:1px solid grey;text-align:left' contenteditable='true'></p>
                                                    <input type='hidden' class='form-control' id='title' name='title' />
                                                </div>
                                                <div class='form-group'>
                                                    <label class="switch">
                                                
                                                    <input name='anonymous' type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>
                                                 <i class='l'>Ask anonymously</i>
                                                </div>
                                               

                                                


                                            </div>
                                            <div class="col-12 mt-20">
                                                   <button  id='up' type='submit' class='btn'>Ask</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        </div>

</div>

@endsection

@push('scripts')

<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/tagInput.js')}}"></script>
<script src='{{asset('js/validation.js')}}'></script>
<script src='{{asset('js/dockay.js')}}'></script>
<script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
<script src='{{asset('assets2/pages/upload-init.js')}}'></script>


<script>

    $('#loaderImage').show();

    $(document).ready(function(){
        $('#question').modal({
            backdrop: 'static'
        });

        $('.close').click(function(){
            window.location.href = '{{ url()->previous() }}';
        });
        $('#loaderImage').hide();

         $('#create').submit(function(){
            event.preventDefault();
            var title = $('#ti').text();
            $('#title').val(title);
            
             $(this).unbind().submit();
         });

         $('#q-title').keyup(function(event){
            
              

              
             
             //$(this).text(q+ ' ?');
         });
    });

</script>

@endpush
