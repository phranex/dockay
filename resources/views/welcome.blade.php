@extends('layouts.master')
@push('styles')
    <style>
     .pt-1 input, .pt-1 a i{
        padding: 10px !important; 
    }
    </style>

@endpush
@push('title')
    Dockay | a doctor's platform...

@endpush
    @push('bg-content')
        <div class="bg-content flex">
            <div>
                <h1>Welcome to {{ config('app.name') }}</h1>
                <small class='text-center font-20 block'>Africa's Largest Medical Platform</small>

                <div class="list-inline-item app-search app-search-2 flex" >
                                        <form action="{{route('product.listings')}}" method='get' role="search" class="m-auto w-50">
                                            @csrf
                                            <div class="form-group pt-1">
                                                <input name='query' type="text" class="form-control w-100  text-center s-icon" placeholder="Search by jobs,equipments,doctors,facilities or articles e.g Stethoscope ">
                                                <a href="#"><button class='q-btn' type='submit'><i class="fa fa-search s-icon"></i></button></a>
                                            </div>
                                        </form>
                                    </div>
                </div>
        
        </div>
    @endpush


@section('content')
<div class='dk-services container '>
    <div class='row'>
        <div class="col-md-4 col-12" >
            <div class='flex' style='height:100px'>
                <img class="img-fluid full-height m-auto"  src="{{asset('images/jobs.png')}}"/>
                
            </div>
            <hr class='dk-underline'/>
             <div class="card-body text-center">
                <h5 class="card-title ">Jobs</h5>
                <p class="card-text">
                    Full-time, Part-time, Locum or Volunteering. Find your next job in the blink of an eye!
                </p>

            </div>

        </div>
         <div class="col-md-4 col-12" >
            <div class='flex' style='height:100px'>
                <img class="img-fluid full-height m-auto"  src="{{asset('images/market.png')}}"/>
                
            </div>
            <hr class='dk-underline'/>
             <div class="card-body text-center">
                <h5 class="card-title ">Market</h5>
                <p class="card-text">
                    Buy and Sell medical equipment in a heartbeat
                </p>

            </div>

        </div>

         <div class="col-md-4 col-12" >
            <div class='flex' style='height:100px'>
                <img class="img-fluid full-height m-auto"  src="{{asset('images/partner.png')}}"/>
                
            </div>
            <hr class='dk-underline'/>
             <div class="card-body text-center">
                <h5 class="card-title ">Question Bank</h5>
                <p class="card-text">
                     Ask questions, get prompt answers from medical professionals.
                </p>

            </div>

        </div>
       
       

    </div>
</div>


<div  class='dk-features containe mt-20'>
    <div class='containe'>
         <div class="row ">
            <div class="col-12 d-none d-md-block">
                <div class="owl-carousel m-auto owl-theme" style="width:90%">
                    @if(isset($questions))
                        @if(count($questions))
                            <div class="content" >
                                    <h6 class='feature-title text-center'>Featured Article</h6>
                                    @foreach ($questions->chunk(2) as $items)
                                        <div class="row">
                                        @foreach ($items as $question)
                                             
                                            <div class="col-md-12 col-xl-6">
                                                <div class="card dockay-bd-rad m-b-30">
                                                    <div class="card-header">
                                                        Quote
                                                    </div>
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <p><a href='{{ route('question.show', $question->slug) }}'>{{ $question->title }}</a></p>
                                                        <footer class="blockquote-footer">Asked <cite title="Source Title">{{ when($question->created_at) }}</cite></footer>
                                                        </blockquote>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                       
                                        @endforeach
                                         </div>
                                    @endforeach
                                   
                                   

                            </div>
                        @endif
                    @endif
                    
                    @if(isset($jobs))
                        @if(count($jobs))
                            <div class="content" >
                                    <h6 class='feature-title text-center'>Featured Jobs</h6>
                                    @foreach ($jobs->chunk(2) as $items)
                                        <div class="row">
                                        @foreach ($items as $job)
                                             
                                            <div class="col-md-12 col-xl-6">
                                                <div class="card dockay-bd-rad m-b-30">
                                                    <div class="card-header">
                                                        Quote
                                                    </div>
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <p><a href='{{ route('job.details', $job->id) }}'>{{ $job->title }}</a></p>
                                                        <footer class="blockquote-footer"> Posted <cite title="Source Title">{{ when($job->created_at) }}</cite></footer>
                                                        </blockquote>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                       
                                        @endforeach
                                         </div>
                                    @endforeach
                                   
                                   

                            </div>
                        @endif
                    @endif
                    @if(isset($products))
                        @if(count($products))
                            <div class="content" >
                                    <h6 class='feature-title text-center'>Featured Items</h6>
                                    @foreach ($products->chunk(2) as $items)
                                        <div class="row">
                                        @foreach ($items as $product)
                                             
                                            <div class="col-md-12 col-xl-6">
                                                <div class="card dockay-bd-rad m-b-30">
                                                    <div class="card-header">
                                                        Quote
                                                    </div>
                                                    <div class="card-body">
                                                        <blockquote class="blockquote mb-0">
                                                        <p><a href='{{ route('product.details', $product->id) }}'>{{ $product->title }}</a></p>
                                                        <footer class="blockquote-footer"> Posted <cite title="Source Title">{{ when($product->created_at) }}</cite></footer>
                                                        </blockquote>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                       
                                        @endforeach
                                         </div>
                                    @endforeach
                                   
                                   

                            </div>
                        @endif
                    @endif
                    
                   
                </div>

            </div>

            <div class="col-12 d-block d-md-none">
                <div class="owl-carousel m-auto owl-theme" style="width:90%">
                    @if(isset($questions))
                        @if(count($questions))
                            
                                    @foreach ($questions as $question)
                                        <div class="content" >                                     
                                            <h6 class='feature-title text-center'> Trending Questions</h6>
                                            <div class="row">
                                        
                                                
                                                <div class="col-md-12 col-xl-6">
                                                    <div class="card dockay-bd-rad m-b-30">
                                                        <div class="card-header">
                                                            Quote
                                                        </div>
                                                        <div class="card-body">
                                                            <blockquote class="blockquote mb-0">
                                                            <p><a href='{{ route('question.show', $question->slug) }}'>{{ $question->title }}</a></p>
                                                            <footer class="blockquote-footer">Asked <cite title="Source Title">{{ when($question->created_at) }}</cite></footer>
                                                            </blockquote>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                        
                                        
                                            </div>
                                         </div>
                                    @endforeach
                                   
                                   

                           
                        @endif
                    @endif
                    @if(isset($products))
                        @if(count($products))
                            
                                    @foreach ($products as $product)
                                        <div class="content" >                                     
                                            <h6 class='feature-title text-center'> Frquesntly Bought</h6>
                                            <div class="row">
                                        
                                                
                                                <div class="col-md-12 col-xl-6">
                                                    <div class="card dockay-bd-rad m-b-30">
                                                        <div class="card-header">
                                                            Quote
                                                        </div>
                                                        <div class="card-body">
                                                            <blockquote class="blockquote mb-0">
                                                            <p><a href='{{ route('product.details', $product->id) }}'>{{ $product->title }}</a></p>
                                                            <footer class="blockquote-footer">Asked <cite title="Source Title">{{ when($product->created_at) }}</cite></footer>
                                                            </blockquote>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                        
                                        
                                            </div>
                                         </div>
                                    @endforeach
                                   
                                   

                           
                        @endif
                    @endif
                    @if(isset($jobs))
                        @if(count($jobs))
                            
                                    @foreach ($jobs as $job)
                                        <div class="content" >                                     
                                            <h6 class='feature-title text-center'> Hot Jobs</h6>
                                            <div class="row">
                                        
                                                
                                                <div class="col-md-12 col-xl-6">
                                                    <div class="card dockay-bd-rad m-b-30">
                                                        <div class="card-header">
                                                            Quote
                                                        </div>
                                                        <div class="card-body">
                                                            <blockquote class="blockquote mb-0">
                                                            <p><a href='{{ route('job.details', $job->id) }}'>{{ $job->title }}</a></p>
                                                            <footer class="blockquote-footer">Asked <cite title="Source Title">{{ when($job->created_at) }}</cite></footer>
                                                            </blockquote>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                        
                                        
                                            </div>
                                         </div>
                                    @endforeach
                                   
                                   

                           
                        @endif
                    @endif
                   

                    
                </div>

            </div>
        </div>
    </div>
   
</div>



<div class='dk-join flex w-100 text-center'>
    <div class='m-auto text-center'>
         <h2 class='font-40 text-bold'>Become a {{ config('app.name') }}</h2>
        <p class='font-20 text-bold'>Sign up to enjoy all these amazing features</p>
        <a href='{{ route('register') }}' style='border-radius:30px !important' class='btn dockay-bd-rad btn-lg'>SIGN UP</a>
    </div>
   
</div>

@endsection


        
   



 @include('inc/js')

  <script>

        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                autoplay:true,
                autoplayTimeout:7000,
                //autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:1,
                        nav:false
                    },
                    1000:{
                        items:1,
                        nav:true,
                        loop:true
                    }
                }
            });
        });

       
    </script>
</body>
</html>
