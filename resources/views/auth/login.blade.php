@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">

    <style>

@media (min-width:980px){
    .carousel-item{
        height: 100vh;
    }

    .carousel-caption{
        bottom: 250px;
        position: absolute;
        z-index: 11;
    }

    .carousel-caption h3{
        font-size: 70px;
        font-weight: 900;

    }
   

    .overlay{
        height: 100%;
        width: 100%;
        background: #64bdf957;
        position: absolute;
        top: 0;
        z-index: 10;
    }

    .service{
        padding: 2vh 17vh;
    }

    .content{
        padding: 2vh;
    }
}

.bg{
                background:url("/assets2/images/carousel/img-01.jpg");
                /* Full height */
            height: -webkit-fill-available;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-color:#64bdf99c;
            background-blend-mode:overlay;
            }

            h5,h3,h4,h2,h1,.medium-text{
        color:black;
    }


    </style>

@endpush
@push('title')
    Dockay | Login

@endpush
@push('bg-content')
    <div class='bg-content'>
        <div class="flex">
                <div class='auth-bo' >
                    <div class="row full-screen">

                            {{-- professioal --}}
                            <div id='HP' class="col-md-12 ffset-lg-6 col-lg-4 m-auto form-box" >
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Login</h5>
                                            <small class='text-muted'>Non-Health Practiitoners/Users click <button style='font-size:inherit' class='btn  btn-sm  btn-link lg' > Here</button> to Login</small>
                                        </div>
                                        <div class="card-body">
                                                {{--  @include('inc.messages')  --}}
                                                <form action='{{route('login')}}' method='post' class="mb-0">
                                                    @csrf
                                                    <div class="form-row">

                                                        <div class="col-md-12 mb-3">
                                                        {{--  <h6 class="text-muted">Username</h6>  --}}
                                                        <span class="bmd-form-group"><div class="input-group">
                                                            
                                                            <input type="text" name='login' class="form-control"  placeholder="Username or email" required>
                                                            @if(!empty(request('p')))
                                                            <input type="hidden"  name='p' value="{{request('p')}}" class="form-control"  placeholder="Username" >
                                                            @endif
                                                        </div></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">

                                                        <div class="col-md-12 mb-3">
                                                        {{--  <h6 class="text-muted">Password</h6>  --}}
                                                        <span class="bmd-form-group"><input name='password' type="password" class="form-control"  placeholder="Password" required></span>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-check">

                                                        <p class="text-muted text-right" style="font-size:12px">
                                                            Don't have an account?
                                                            <a style='font-size: inherit' href='{{route('register')}}?p={{request('p')}}' class="btn btn-link mb-0" >Sign Up</a>
                                                        </p>
                                                        </div>
                                                    </div>

                                                     <div class='row flex'>
                                                        <div class='col m-auto text-center'>

                                                            <div class='text-center'><button class="btn   btn-primary" type="submit">Login </button></div>
                                                             
                                                                {{--  <p class='auth-option'>OR</p>  --}}
                                                            <p class='text-center'><a style='font-size: inherit;background:#0077B5 !important' href='{{route('socialLogin','linkedin')}}?p={{request('p')}}' class="btn mb-0" ><i class='fa fa-linkedin-square'></i> Login with Linkedin</a></p>
                                                            <p class='text-muted'>Forgot  Password? <a href="{{ route('password.request') }}" class="checkbox-link">Reset</a></p>
                                                        </div>
                                                    </div>
                                                </form>
                                            <div class='profile-btn-holder text-center'>


                                            </div>

                                        </div>
                                    </div>

                            </div>

                    </div>
                </div>
        </div>
    </div>


    <div class="modal fade" id="lgn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header w-100">
                    <h5 class="modal-title text-center w-100" id="exampleModalLongTitle">Login via Socials </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="flex text-center">
                                <div class='row w-100'>
                                    <div class="col-md-8 text-center m-auto">
                                        <a href='{{route('socialLogin','facebook')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-facebook"></i></a>
                                        {{--  <a href='{{route('socialLogin','twitter')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-twitter"></i> Login</a>  --}}
                                        <a href='{{route('socialLogin','google')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-google-plus"></i></a>
                                    </div>


                                </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.lg').click(function(){
                $('#lgn').modal();
            });
            $('#next').click(function(){
                $('div.err').text('');
               if($('[name=type]').is(':checked')){

                    var opt = $('[name=type]:checked').val();
                    $('.form-box').hide();
                    $(opt).show();
                    $('#auth-box').hide();
                }else{
                    $('div.err').text('Please select an option').css('color', 'red').addClass('text-center');
                }
            });

            $('.back').click(function(){
                $('.form-box').hide();
                $('#auth-box').show();
            });
        });

    </script>
@endpush
