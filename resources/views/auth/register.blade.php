@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="{{asset('css/checkbox.css')}}">
<link rel="stylesheet" href="{{asset('css/mobile.css')}}">

    <style>


@media (min-width:980px){
    .carousel-item{
        height: 100vh;
    }

    .carousel-caption{
        bottom: 250px;
        position: absolute;
        z-index: 11;
    }

    .carousel-caption h3{
        font-size: 70px;
        font-weight: 900;

    }

    .overlay{
        height: 100%;
        width: 100%;
        background: #64bdf957;
        position: absolute;
        top: 0;
        z-index: 10;
    }

    .service{
        padding: 2vh 17vh;
    }

    .content{
        padding: 2vh;
    }


}

.bgimg-1, .bgimg-2, .bgimg-3 {
    
    margin: auto;
    width: 100%;
   
    background-size: cover;

  }
  .bg{
                background-image:url("/assets2/images/carousel/job.png") !important;
                /* Full height */
            height: -webkit-fill-available;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-color:#64bdf99c;
            background-blend-mode:overlay;
            }

    .bg-content{
        background-color:unset;
       
    }

    .bg-content div{
        margin:unset;
    }

    h5,h3,h4,h2,h1,.medium-text{
        color:black;
    }

    .checkmark{
        right:50px !important;  
    }
    </style>


@endpush

@push('title')
    Dockay | Register

@endpush
@push('bg-content')
    <div class='bg-content flex'>
        <div  class="m-auto" >

            <div class="bgimg-1">
                    <div class="caption">
                            <div class="row">
                            <div id='auth-box' class="col-md-12 ffset-lg-8 col-lg-4 ml-auto reg-box" style="argin-top:100px !important">
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Create your Account</h5>
                                            {{--  <small style='font-size:12px' class='text-muted'>Please do not use yahoo mails. For unknown reasons,Yahoo restrict its user from receiving auto-generated mails. <i class='em em-cry'></i></small>  --}}
                                        </div>
                                        @if(!empty(request('r')))
                                        <div class=''>
                                            <div class='col'>
                                                <div class='alert alert-danger'>
                                                    You cancelled sign up via social media. Please to continue sign up with your email below.
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="card-body">

                                            <div class="medium-text text-left text-bold">Select Profile</div>
                                            <div class='profile-holder'>
                                                    <label class="contain">Health Practitioner/Student
                                                        <input value='#HP' type="radio" name="type">
                                                        <span class="checkmark"></span>
                                                    </label>
                                            </div>
                                            <div class='profile-holder'>
                                                    <label class="contain">Facility/Institution
                                                        <input value='#F' type="radio" name="type">
                                                        <span class="checkmark"></span>
                                                    </label>
                                            </div>

                                            <div class='profile-holder'>
                                                    <label class="contain">Non-Health Practitioner
                                                        <input value='#P' type="radio" name="type">
                                                        <span class="checkmark"></span>
                                                    </label>
                                            </div>
                                            <div class='err'></div>

                                            <div class='profile-btn-holder text-center'>
                                                <div class="m-auto" style="width:30%">
                                                        <button id='next' class='btn btn-block btn-sm btn-primary'>Next</button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                    

                            </div>

                            {{-- professioal --}}
                            <div style="display:none" id='HP' class="col-md-12 ffset-lg-6 col-lg-4 ml-auto form-box" >
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Create your profesional
                                                account</h5>
                                        </div>
                                        <div class="card-body">
                                                {{--  @include('inc.messages')  --}}
                                               
                                                <form action='#' id='health-form' method='post' class="mb-0">

                                                    @csrf
                                                    <input type="hidden" name='open' class='open-form' value="{{old('open')}}">
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">First name</h6>  --}}
                                                        <span class="bmd-form-group is-filled">
                                                            <input name='firstName' required value='{{old('firstName')}}' type="text" class="form-control" id="validationDefault01" placeholder="First name" required></span>
                                                        </div>
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Last name</h6>  --}}
                                                        <span class="bmd-form-group is-filled">
                                                            <input name='lastName' required type="text" value='{{old('lastName')}}' class="form-control"  placeholder="Last name" required></span>
                                                        </div>
                                                       
                                                        
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Email</h6>  --}}
                                                        <span class="bmd-form-group">
                                                            <input type="email" required name='email' value='{{old('email')}}' class="form-control"  placeholder="Email" required></span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-row">
                                                       
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Password</h6>  --}}
                                                        <span class="bmd-form-group">
                                                            <input type="password" required name='password'  class="form-control"  placeholder="Password" required></span>
                                                        </div>
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Repeat Password</h6>  --}}
                                                        <span class="bmd-form-group">
                                                            <input type="password" required name='password_confirmation' class="form-control"  placeholder="Confirm Password" required></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                         <div class="col-md-4 mb-3">
                                                        {{--  <h6 class="text-muted">Username</h6>  --}}
                                                        <span class="bmd-form-group">
                                                           
                                                            <input name='username' required type="text" value='{{old('username')}}' class="form-control"  placeholder="Username" required>
                                                        </div>
                                                        </span>
                                                    </div>
                                                    

                                                    
                                                    <div class="form-group ">
                                                        <div class="form-check text-left">
                                                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required="">
                                                        <label class="form-check-label text-muted" for="invalidCheck2">
                                                            Agree to terms and conditions
                                                        </label>
                                                         <button class="btn pull-right btn-link back  mb-0" type="button">Back</button>

                                                        </div>
                                                    </div>
                                                    <input type='hidden' name='userType' value='health' />
                                                    <div class='row flex'>
                                                        <div class='col m-auto text-center'>

                                                            <div class='text-center'><button class="btn   btn-primary" type="submit">Sign Up</button></div>
                                                                {{--  <p class='auth-option '>OR</p>  --}}
                                                            
                                                            <p class='text-center'><a style='font-size: inherit;background:#0077B5 !important' href='{{route('socialLogin','linkedin')}}?p={{request('p')}}' class="btn mb-0" ><i class='fa fa-linkedin-square'></i> Sign up with Linkedin</a></p>

                                                        </div>
                                                    </div>
                                                   
                                                </form>
                                            <div class='profile-btn-holder text-center'>
                                                 <div class='row'>
                                                    <div class='col'>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                            </div>
                            {{-- facility --}}
                            <div style="display:none" id='F' class="col-md-12 ffset-lg-6 col-lg-4 ml-auto form-box" >
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Create a Facility profile</h5>
                                        </div>
                                        <div class="card-body">
                                                {{--  @include('inc.messages')  --}}
                                                <form id='facility-form' action='{{route('register')}}' method='post' class="mb-0">
                                                        {{--  <input type='hidden' name='userType' value='health' />  --}}
                                                        @csrf
                                                        <input type="hidden" name='open' class='open-form' value="{{old('open')}}">
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Facility/Institution Name</h6>  --}}
                                                        <span class="bmd-form-group is-filled">
                                                            <input type="text" name="facility_name" value="{{old('facility_name')}}" class="form-control" id="validationDefault01" placeholder="Facility Name" required></span>
                                                        </div>
                                                        <div class="col-md-6 mb-3">
                                                            {{--  <h6 class="text-muted">Username</h6>  --}}
                                                            <span class="bmd-form-group">
                                                                <input type="text" class="form-control" name="facility_username" value="{{old('facility_username')}}"  placeholder="Username" required>
                                                            </span>
                                                        </div>

                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                            {{--  <h6 class="text-muted">Establishment Type</h6>  --}}
                                                            <span class="bmd-form-group">
                                                            <select name="facility_type" class="form-control" id="exampleSelect1">
                                                            <option>Select</option>
                                                            @if(isset($establishments))
                                                                @if(count($establishments))
                                                                    @foreach ($establishments as $establishment)
                                                                        <option @if($establishment->id == old('facility_type')) selected @endif value="{{$establishment->id}}">{{$establishment->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                            </select>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Email</h6>  --}}
                                                        <span class="bmd-form-group">
                                                            <input type="email" class="form-control" name="facility_email" value="{{old('facility_email')}}"  placeholder="Email" required></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                    



                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Password</h6>  --}}
                                                        <span class="bmd-form-group"><input type="password" name="facility_password" class="form-control"  placeholder="Password" required></span>
                                                        </div>
                                                        <div class="col-md-6 mb-3">
                                                        {{--  <h6 class="text-muted">Repeat Password</h6>  --}}
                                                        <span class="bmd-form-group"><input type="password" name="facility_password_confirmation" class="form-control"  placeholder="Confirm Password" required></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-check text-left">
                                                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required="">
                                                        <label class="form-check-label text-muted" for="invalidCheck2">
                                                            Agree to terms and conditions
                                                        </label>
                                                    <button class="btn btn-link pull-right back mb-0" type="button">Back</button>

                                                        </div>
                                                    </div>
                                                      <div class='row flex'>
                                                        <div class='col m-auto text-center'>

                                                            <div class='text-center'><button class="btn   btn-primary" type="submit">Sign Up</button></div>

                                                        </div>
                                                    </div>
                                                   
                                                </form>
                                            <div class='profile-btn-holder text-center'>


                                            </div>

                                        </div>
                                    </div>

                            </div>
                            {{-- non-heath prcati --}}
                            <div style="display:none" id='P' class="col-md-12 ffset-lg-6 col-lg-6 ml-auto form-box" >
                                    <div class="card m-b-30 dockay-bd-rad">
                                        <div class="card-header">
                                            <h5 class='text-center'>Register via <button class="btn btn-link back float-right">Back</button></h5>
                                        </div>
                                        <div class="card-body">

                                                <div class='row'>
                                                    <div class="col-md-4 text-center m-auto">
                                                        <a href='{{route('socialLogin','facebook')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-facebook"></i></a>
                                                        {{--  <a href='{{route('socialLogin','twitter')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-twitter"></i></a>  --}}
                                                        <a href='{{route('socialLogin','google')}}?p={{request('p')}}' class='social-reg'><i class="fa fa-google-plus"></i></a>
                                                    </div>


                                                </div>

                                        </div>
                                    </div>

                            </div>
                    </div>
                    </div>
            </div>





        </div>

    </div>

     


@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#next').click(function(){
                $('.messages').text('');
                $('div.err').text('');
               if($('[name=type]').is(':checked')){

                    var opt = $('[name=type]:checked').val();
                    $('.form-box').hide();
                    $(opt).show();
                    $('#auth-box').hide();
                }else{
                    $('div.err').text('Please select an option').css('color', 'red').addClass('text-center');
                }
            });

            $('.back').click(function(){
                $('.form-box').hide();
                $('#auth-box').show();
            });

            if($('.open-form').val() != ''){
                var value = $('.open-form').val();
                $('#auth-box').hide();
                $('.form-box').hide();
                $(value).show();
                $('[name=type]').each(function(){
                    if($(this).val().toLowerCase() == value.toLowerCase()){
                        $(this).prop('checked',true);
                    }
                });
            }



            $('#health-form').submit(function(){
                event.preventDefault();
                id = $(this).closest('.form-box').attr('id');
                $('.open-form').val('#'+id);
                $(this).unbind('submit').submit();
            });
            $('#facility-form').submit(function(){
                event.preventDefault();
                id = $(this).closest('.form-box').attr('id');
                $('.open-form').val('#'+id);
                $(this).unbind('submit').submit();
            });
        });

    </script>
@endpush
