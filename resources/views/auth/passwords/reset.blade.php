@extends('layouts.master')

@push('bg-content')

    <div class='bg-content'>
        <div class="container h-100">
            <div class="row h-100 justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Reset Password') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                                @csrf

                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group row">
                                    {{--  <label for="email" class="col-8 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>  --}}

                                    <div class="col-md-6">
                                        <input id="email" type="email" placeholder='Enter Your email' class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{--  <label for="password" class="col-md-6 col-form-label text-md-right">{{ __('Password') }}</label>  --}}

                                    <div class="col-md-6">
                                        <input id="password" type="password" placeholder='Password' class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{--  <label for="password-confirm" class="col-md-6 col-form-label text-md-right">{{ __('Confirm Password') }}</label>  --}}

                                    <div class="col-md-6">
                                        <input id="password-confirm" placeholder='Confirm Password' type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-0 " style='margin-top:10px'>
                                    <div class="col-md-6 offset-md-4 mt-10">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Reset Password') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush


