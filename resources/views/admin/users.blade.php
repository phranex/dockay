@extends('layouts.admin')
@push('styles')
    
@endpush

@push('title')
   Admin | Users 
@endpush
@section('content')
      <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20">
                <h4 class="c-grey-900 mB-20">users</h4>
                    <p class='pull-right'>
                        
                        {{--  <button type="button" class="btn btn-primary" id='users'>
                            Add users
                        </button>  --}}
                        
                    </p>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Email</th>
                            <th scope="col">Verification</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($users))
                            @if(count($users))
                                @foreach ($users as $user)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ getFullName($user) }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>@if($user->userType == 1) Practitioner @elseif($user->userType == 2) Facility @else Patient @endif @if($user->is_moderator) <small>Moderator</small> @endif </td>
                                        <td>{{ $user->email }}</td>
                                        <td>@if($user->verified) Verified @else Unverified @endif</td>
                                        <td>
                                            <a class='btn btn-xs btn-info' href ='{{ route('user.profile.view', $user->username) }}'>View</a>
                                            <a class='btn btn-xs btn-success' href ='{{ route('user.profile.view', $user->username) }}'>Verify</a>
                                            <a class='btn btn-xs btn-danger' href ='{{ route('user.profile.view', $user->username) }}'>Suspend</a>
                                            <a class='btn btn-xs btn-link' href ='{{ route('admin.user.make-moderator', $user->id) }}'> @if($user->is_moderator) Remove Moderator @else Make Moderator @endif </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td colspan='4'><div class='alert'>No user added yet</div></td>
                                </tr>
                        
                            @endif
                        @endif
                    </tbody>
                </table>


                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    
@endpush