@extends('layouts.admin')
@push('styles')
    
@endpush

@push('title')
   Admin | Users 
@endpush
@section('content')
      <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20">
                <h4 class="c-grey-900 mB-20">users</h4>
                    <p class='pull-right'>
                        
                        {{--  <button type="button" class="btn btn-primary" id='users'>
                            Add users
                        </button>  --}}
                        
                    </p>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Deadline</th>
                           
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($items))
                            @if(count($items))
                                @foreach ($items as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->user->username}}</td>
                                        <td>{{ when($item->deadline) }}</td>
                                        <td>
                                            <a class='btn btn-xs btn-info' href ='{{ route('job.details', $item->id) }}'>View</a>
                                            <a class='btn btn-xs btn-success' href ='{{ route('user.profile.view', $item->user->username) }}'>Activate</a>
                                            <a class='btn btn-xs btn-danger' href ='{{ route('user.profile.view', $item->user->username) }}'>Remove</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td colspan='4'><div class='alert'>No job added yet</div></td>
                                </tr>
                        
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    
@endpush