@extends('layouts.admin')
@push('styles')
    
@endpush

@push('title')
   Admin | establishments 
@endpush
@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20">
                <h4 class="c-grey-900 mB-20">Job category</h4>
                    <p class='pull-right'>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#2">
                            Add establishment
                        </button>
                       
                        
                    </p>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">establishments</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($establishments))
                            @if(count($establishments))
                                @foreach ($establishments as $category)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <button data-name='{{ $category->name }}' data-id='{{ $category->id }}' class='btn-sm btn-primary edit'>Edit</button>
                                           
                                            <button data-id='{{ $category->id }}' class='btn-sm btn-danger delete'>Delete</button>
                                        </td>
                                        </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td colspan='4'><div class='alert'>No establishment added yet</div></td>
                                </tr>
                        
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="modal fade" id="2" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add establishments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action='{{route('store.establishment')}}' method='post'>
                        @csrf
                        {{--  <div class="bgc-white p-20 bd"><h6 class="c-grey-900">Basic Form</h6><div class="mT-30">  --}}
                                
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">establishments</label>
                                        <input type='text' required class="form-control" name='establishments' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter establishments">
                                        <i class='text-muted'>Please add establishments. Separate each only with a comma</i>
                                                                
                                    </div>
                                    
                        </div>
                        <div class="modal-footer">
                            {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                            <button type="submit" class="btn btn-primary">Add</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="editModal" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit establishment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action='{{route('update.establishment')}}' method='post'>
                        @csrf
                        {{--  <div class="bgc-white p-20 bd"><h6 class="c-grey-900">Basic Form</h6><div class="mT-30">  --}}
                                
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">establishments</label>
                                        <input type='text' required  class="form-control" name='establishment' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category email">
                                        <input type="hidden"  name='id' />
                                        {{--  <i class='text-muted'>Please add establishments. Separate each only with a comma</i>  --}}
                                                                
                                    </div>
                                    
                        </div>
                        <div class="modal-footer">
                            {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                            <button type="submit" class="btn btn-primary">Edit</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            $('.edit').click(function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                $('[name=establishment]').val(name);
                $('[name=id]').val(id);
               
                $('#editModal').modal();

            });
            $('.delete').click(function(){
                var answer = confirm('Are you sure?');
                var id = $(this).attr('data-id');
                if(answer){
                    window.location.href = "{{ route('delete.establishment') }}/"+id
                }
               

            });

        
        });
    </script>
@endpush