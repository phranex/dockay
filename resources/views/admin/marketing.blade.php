@extends('layouts.admin')
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css" rel="stylesheet">
    
@endpush

@push('title')
   Admin | Marketing 
@endpush
@section('content')
      <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20">
                <h4 class="c-grey-900 mB-20">Send Mail</h4>
                    <a target='_blank' href='{{route('getUserIncomplete')}}' class='btn btn-primary'>Get Users with Incomplete Profile</batton>
                    <a target='_blank' href='{{route('getAllUsers')}}' class='btn btn-primary'>Get All Users</a>
                    <a target='_blank' href='{{route('getAllFacilities')}}' class='btn btn-primary'>Get All Facilities</a>
                    <a target='_blank' href='{{route('getAllInactiveUsers')}}' class='btn btn-primary'>Get All inactive users</a>
                    <a target='_blank' href='{{route('getAllEmailsInDB')}}' class='btn btn-primary'>Get All Marketing Users</a>
                    
                    <p class='pull-right'>
                        
                        {{--  <button type="button" class="btn btn-primary" id='users'>
                            Add users
                        </button>  --}}
                        
                    </p>
                <form id='answer-form' method='post' action='{{ route('admin.marketing.send') }}'>
                    @csrf
                    <label>Subject</label>
                    <input name='subject' type='text' class='form-control' required placeholder ='enter subject'>
                    <div class='form-group' style='margin-top:20px'></div>
                    <label>Enter Mails</label>
                    <p id='em' style='padding:20px;border-bottom:1px solid grey' contenteditable='true'></p>
                    <input type='hidden' name='emails' id='emails' />
                    <input type='hidden' name='message' id='message' />
<textarea required name="content" id="editor"></textarea>
                    <Div class='form-group text-right' style='margin-top:20px'>
                         <button class='btn btn-success' type='submit'>Send</button>
                    </Div>
                   
                </form>
                
               
            </div>
        </div>
    </div>
@endsection


@push('scripts')
  <script src="{{asset("js/validation.js")}}"></script>

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'editor' );


        $('#answer-form').submit(function(){
                
                event.preventDefault();
                var ed = CKEDITOR.instances.editor.getData();
                console.log(ed);
                var emails = $('#em').text();
                emails = emails.trim();
                emails = emails.split(",");
                console.log(emails);
                if(emails.length){
                    var arr_mail = [];
                    var arr_error = [];
                    for(i = 0; i < emails.length; i++){
                        if(isEmail(emails[i])){
                            arr_mail.push(emails[i]);
                        }else{
                            arr_error.push(emails[i]);
                        }
                    }

                    if(arr_error.length == 0){
                        $('#emails').val(emails);
                        $('#message').val(ed);
                
                        $(this).unbind().submit();  
                    }else{
                        if(arr_error.length){
                            var mess = 'The following mails are incorrect. Please correct and resend' + '<br/>';
                            for(i = 0; i < arr_error.length; i++){
                                mess += '- ' + arr_error[i] + '<br/>';
                            }
                        }
                        $.alert({
                            title: 'Error!',
                            type: 'red',
                            icon: 'fa fa-times',
                            content: mess,
                        });
                    }

                }
                

            });
</script>
@endpush