@extends('layouts.admin')
@push('styles')
    
@endpush

@push('title')
   Admin | Questions 
@endpush
@section('content')
    
     <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20">
                <h4 class="c-grey-900 mB-20">Categories</h4>
                    <p class='pull-right'>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#2">
                            Add a Category
                        </button>
                        {{--  <button type="button" class="btn btn-primary" id='categories'>
                            Add Categories
                        </button>  --}}
                        
                    </p>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Category</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($categories))
                            @if(count($categories))
                                @foreach ($categories as $category)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>@if(isset($category->path)) <img class='img-fluid' style='width:25px;' src='{{ $category->path }}'/> @else <a href=''>Add Image</a> @endif{{ $category->name }}</td>
                                        <td>@if(isset($category->path)) {{ $category->description }} @else Add a description @endif </td>
                                        <td>
                                            <button data-description="{{ $category->description }}" data-name="{{ $category->name }}" data-id='{{ $category->id }}' class='btn-sm btn-primary edit'>Edit</button>
                                           
                                            <button  data-id='{{ $category->id }}' class='btn-sm btn-danger delete'>Delete</button>
                                        </td>
                                        </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td colspan='4'><div class='alert'>No category added yet</div></td>
                                </tr>
                        
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <div class="modal fade" id="editModal" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Specialties</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <form enctype="multipart/form-data" action='{{ route('admin.question.category.update') }}' method='post'>
                        @csrf
                        <div class="bgc-white p-20 bd"><h6 class="c-grey-900">Basic Form</h6><div class="mT-30">
                                
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" class="form-control" required value='{{ old('editCategory') }}' name='editCategory' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category email">
                                        <input type='hidden' name='id' />
                                                                
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Description</label>
                                        <textarea name='editDescription' required class='form-control'>{{ old('editDescription') }}</textarea>
                                                                
                                    </div>
                                    <div class='form-group'>
                                        <input type='checkbox' name='changeImage' id='changeImage' /> Do you want to change the image?
                                    </div>
                                    <div style='display:none' id='editImage' class="form-group">
                                        <label for="exampleInputEmail1">Image</label>
                                       <input type='file' class='form-control'   name='editImage' />
                                                                
                                    </div>


                                    
                        </div>
                        <div class="modal-footer">
                            {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                            <button type="submit" class="btn btn-primary">Save changes</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


     <div class="modal fade" id="addMany" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Specialties</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action='{{ route('admin.question.category.store') }}' method='post'>
                        @csrf
                                
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" class="form-control" name='category' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category email">
                                        <i class='text-muted'>Please add categories. Separate each only with a comma </i>
                                        
                                                                
                                    </div>
                                    
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>

                        </div>
  </form>
                </div>
            </div>
        </div>
    </div>


   
   





    <div class="modal fade"  id="2" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add a  Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" action='{{ route('admin.question.category.store') }}' method='post'>
                        @csrf
                        <div class="bgc-white p-20 bd"><h6 class="c-grey-900">Basic Form</h6><div class="mT-30">
                                
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" class="form-control" required value='{{ old('category') }}' name='category' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category email">
                                        
                                                                
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Description</label>
                                        <textarea name='description' required class='form-control'>{{ old('description') }}</textarea>
                                                                
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Image</label>
                                       <input type='file' class='form-control' required  name='category_image' />
                                                                
                                    </div>


                                    
                        </div>
                        <div class="modal-footer">
                            {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                            <button type="submit" class="btn btn-primary">Save changes</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
   
@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            $('#categories').click(function(){
               
                $('#addMany').modal();

            })
        });
    </script>


     <script>
        $(document).ready(function(){
            $('.edit').click(function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                var description = $(this).attr('data-description');
                $('[name=editCategory]').val(name);
                $('[name=editDescription]').val(description);
                $('[name=id]').val(id);
               
                $('#editModal').modal();

            });
            $('.delete').click(function(){
                var answer = confirm('Are you sure?');
                var id = $(this).attr('data-id');
                if(answer){
                    window.location.href = "{{ route('admin.question.category.delete') }}/"+id
                }
               

            });


            $('#changeImage').change(function(){
                if($(this).is(':checked')){
                    $('#editImage').show();
                }else{
                    $('#editImage').hide();

                }
            });

        
        });
    </script>
@endpush