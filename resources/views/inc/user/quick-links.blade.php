<div class="col-md-12 col-xl-3 d-none d-md-block">
        <div class="card m-b-30 fixed" style="right:5%">
            <div class="card-header">
                   <p>Quick Links</p>
            </div>
            <div class="card-body text-center">
                    <div class="dropdown d-inline-block mo-mb-2 show">
                        <a class="btn btn-link btn-raised dropdown-toggle" href="https://example.com/" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            My Jobs
                        <div class="ripple-container"></div></a>

                        <div class="dropdown-menu " aria-labelledby="dropdownMenuLink" x-placement="bottom-start" style="position: absolute; top: 36px; left: 0px; will-change: top, left;">
                            @if(auth()->user()->userType == 1)
                            <a class="dropdown-item" href="{{route('user.applications.index')}}">Applications</a>
                            <a class="dropdown-item" href="{{route('user.job.index')}}">Posted Jobs</a>
                            <a class="dropdown-item" href="{{route('user.job.create')}}">Post Job</a>
                            @else

                            <a class="dropdown-item" href="{{route('user.adverts.index')}}">Posted Jobs</a>
                            <a class="dropdown-item" href="{{route('user.advert.create')}}">Post Job</a>

                            @endif

                        </div>
                    </div>
                    <div class="dropdown d-inline-block mo-mb-2 show">
                        <a class="btn btn-link btn-raised " href="{{route('user.ad.index')}}"  aria-haspopup="true" aria-expanded="true">
                            Shop Manager
                        </a>


                    </div>
            </div>
        </div>

</div>
