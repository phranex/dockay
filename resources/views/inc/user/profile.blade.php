<div class="col-md-12  col-xl-3 d-none d-md-block">
        <div class="card m-b-30 fixed" style="left:3%;top:15%">
            <div class="card-header relative">
                    <div class='profile-box' styl="height:100px">                        
                        <img style='width:75px;height:75px !important' src="@if(isset(auth()->user()->profileImage->path)){{ auth()->user()->profileImage->path }} @else {{ displayDefaultPhoto(auth()->user()) }} @endif" alt="user" class="h-100 rounded-circle profile-img img-fluid">
                        <div class='w-100 h-100 overlay-prof-pic  flex' style=''>
                                <button data-toggle="modal" data-target="#profile-pic" class='btn btn-outline m-auto'>Upload</button>
                        </div>
                    </div>
            </div>
            <div class="card-body text-center">
                @if(auth()->user()->userType == 1)
               <p class="profile-info">{{getFullName(auth()->user())}} <a href='{{route('user.profile', getFullNameWithSlug(auth()->user()))}}' ><i class="fa fa-pencil"></i></a></p>
               @else
               <p class="profile-info">{{getFullName(auth()->user())}} <a href='{{route('user.facility.profile', getFullNameWithSlug(auth()->user()))}}' ><i class="fa fa-pencil"></i></a></p>
               @endif
               {{-- <span>Optomtrist at <span class='profile-company'>Eye Masters</span></span> --}}
               <div class="flex">
                @if(auth()->user()->userType == 1)
                <a href='{{route('user.job.create')}}' class="btn col btn-sm timeline-action">Post Job</a>
                @else
                <a href='{{ route('user.advert.create')}}' class="btn col btn-sm timeline-action">Post Job</a>
                <a href='{{route('user.job.create')}}' class="btn col btn-sm timeline-action"><i class='fa fa-briefcase'></i></a>
                @endif
                <a href="{{route('user.ad.create')}}" class="btn col btn-xs timeline-action"><i class='fa fa-cart-plus'></i></a>
                </div>
            </div>
        </div>

</div>
<div class="col-md-12  col-xl-3 d-block d-md-none">
        <div class="card m-b-30 " style="">
            <div class="card-header relative">
                    <div class='profile-box' styl="height:100px">                        
                        <img style='width:75px;height:75px !important' src="@if(isset(auth()->user()->profileImage->path)){{ auth()->user()->profileImage->path }} @else {{ displayDefaultPhoto(auth()->user()) }} @endif" alt="user" class="h-100 rounded-circle profile-img img-fluid">
                        <div class='w-100 h-100 overlay-prof-pic  flex' style=''>
                                <button data-toggle="modal" data-target="#profile-pic" class='btn btn-outline m-auto'>Upload</button>
                        </div>
                    </div>
            </div>
            <div class="card-body text-center">
                @if(auth()->user()->userType == 1)
               <p class="profile-info">{{getFullName(auth()->user())}} <a href='{{route('user.profile', getFullNameWithSlug(auth()->user()))}}' ><i class="fa fa-pencil"></i></a></p>
               @else
               <p class="profile-info">{{getFullName(auth()->user())}} <a href='{{route('user.facility.profile', getFullNameWithSlug(auth()->user()))}}' ><i class="fa fa-pencil"></i></a></p>
               @endif
               {{-- <span>Optomtrist at <span class='profile-company'>Eye Masters</span></span> --}}
               <div class="flex">
                @if(auth()->user()->userType == 1)
                <a href='{{route('user.job.create')}}' class="btn col btn-sm timeline-action">Post Job</a>
                @else
                
                <a href='{{route('user.advert.create')}}' class="btn col btn-sm timeline-action">Post Job</a>
                @endif
                <a href="{{route('user.ad.create')}}" class="btn col btn-xs timeline-action">Sell </a>
                </div>
            </div>
        </div>

</div>

{{--  modal for upload profile pic  --}}


