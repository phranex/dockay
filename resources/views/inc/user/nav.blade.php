<ul>
				 					<li class="inner-child">
				 						<a href="#" title=""><i class="la la-file-text"></i>Profile</a>
				 						<ul class='side-bar'>
				 							<li class='@if(request()->route()->getName()  == 'user.profile') active  @endif'><a href="{{route('user.profile', getFullNameWithSlug(auth()->user()))}}"> Profile Settings</a></li>
                                            @if(auth()->user()->userType == 1)
				 							<li class='@if(request()->route()->getName()  == 'job.user.profile') active  @endif'>
                                                <a href="{{route('job.user.profile', getFullNameWithSlug(auth()->user()))}}" >My Profile</a>
                                            </li>
                                            <li><a href="/store" title="">Market Profile</a></li>
                                            @else
                                            <li class='@if(request()->route()->getName()  == 'job.user.profile') active  @endif'>
                                                <a href="{{route('job.user.profile', getFullNameWithSlug(auth()->user()))}}" >My Profile</a>
                                            </li>
                                            <li><a href="{{route('user.facility.edit', str_slug(auth()->user()->facility->name))}}" title="">Company Profile Settings</a></li>
                                            <li><a href="{{route('user.facility.profile', str_slug(auth()->user()->facility->name))}}" title="">Company Profile</a></li>
                                            @endif
                                            <li ><a href="#" title="settinga">Settings</a></li>
				 						</ul>
				 					</li>
				 					<li class="inner-child ">
				 						<a href="#" title=""><i class="la la-briefcase"></i>Jobs</a>
				 						<ul>
                                            @if(auth()->user()->userType == 1)
				 							    <li><a href="{{route('user.applications.index')}}" title="">Applied Jobs</a></li>
                                                <li class='@if(request()->route()->getName()  == 'job.user.create') active  @endif' ><a href="{{route('job.user.create',getFullNameWithSlug(auth()->user()))}}" title="">Create a Job Ad</a></li>
                                                {{-- <li><a href="#" title="">Subcriptions</a></li> --}}
                                                {{--  <li><a href="{{route('user.followers.index')}}" title="">Following</a></li>  --}}
                                            @else
                                                <li><a href="{{route('user.adverts.index')}}" title="">Job Manager</a></li>
                                                <li class='@if(request()->route()->getName()  == 'job.user.create') active  @endif' ><a href="{{route('job.user.create',getFullNameWithSlug(auth()->user()))}}" title="">Create a Job Ad</a></li>
                                                <li><a href="#" title="">My Watch</a></li>
                                            @endif

				 						</ul>
				 					</li>
				 					<li class="inner-child">
				 						<a href="#" title=""><i class="la la-money"></i>Market</a>
				 						<ul>
				 							<li><a href="#" title="">Sell an Item</a></li>
				 							<li><a href="#" title="">My Items</a></li>
				 							<li><a href="#" title="">Subcriptions</a></li>
				 						</ul>
				 					</li>




				 					<li class="inner-chil">
				 						<a href="#" title=""><i class="la la-lock"></i>Change Password</a>

				 					</li>
				 					<li><a href="{{route('user.logout')}}" title=""><i class="la la-unlink"></i>Logout</a></li>
				 				</ul>
