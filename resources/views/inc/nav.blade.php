<nav class="navbar @if(isset($bg)) bg-white @else bg-transparent @endif navbar-light bg-light justify-content-between">
        <a class="navbar-brand" href='{{ route('welcome') }}'>
                            @if(in_array(request()->route()->getName(), array('jobsHomePage','marketHomePage', 'welcome','login')) )
                                <img src="{{asset('assets2/images/logo-white.png')}}" alt=""  height="50" class=" d-none d-md-block logo-big">

                            @else  
                                <img src="{{asset('assets2/images/logo-sm.png')}}" alt=""  height="50" class=" d-none d-md-block logo-big">
                            @endif
                                <img src="{{asset('assets2/images/logo-mobile.png')}}" alt=""  height="50" class=" d-md-none d-block logo-small">
        
        </a>
        <span class="navbar-text">
            <ul class='dk-links'>
                <li><a class='a-link' href='{{ route('forumHomePage') }}'>Forum</a></li>
                

              
                
				@guest
                <li><a class='a-link' href='{{ route('jobsHomePage') }}'>Jobs</a></li>
                <li><a class='a-link' href='{{ route('marketHomePage') }}'>Market</a></li>
                <li><a class='a-link' href='{{ route('login') }}'><i class='fa fa-lock'></i></a></li>
				@else
                @if(auth()->user()->userType != 3)
                    <li><a class='a-link' href='{{ route('jobsHomePage') }}'>Jobs</a></li>
                    <li><a class='a-link' href='{{ route('marketHomePage') }}'>Market</a></li>
                @endif
					<li class="nav-item dropdown"><a class='nav-link dropdown-toggle a-link' href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-user'></i>@if(auth()->user()->userType) Hi,{{ getFullNameWithSlug(auth()->user()) }}@endif</a>

						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @if(auth()->user()->userType != 3)
                                            <a class="dropdown-item" href="{{route('user.timeline', getFullNameWithSlug(auth()->user()))}}"><i class="fa fa-list-alt m-r-5 text-muted"></i> Timeline</a>
                                            @endif
                                            @if(auth()->user()->userType == 1)
                                            <a class="dropdown-item" href="{{route('user.applications.index')}}"><i class=" fa fa-envelope text-muted"></i> Applications</a>
                                            @elseif(auth()->user()->userType == 2)
                                            <a class="dropdown-item" href="{{route('user.adverts.index')}}"><i class=" fa fa-briefcase m-r-5 text-muted"></i>  Job Manager</a>

                                            @endif
                                            @if(auth()->user()->userType == 1)
                                            <a class="dropdown-item" href="{{route('user.profile', getFullNameWithSlug(auth()->user()))}}"><i class=" fa fa-edit m-r-5 text-muted"></i>Edit Profile </a>
                                            @elseif(auth()->user()->userType == 2)
                                            <a class="dropdown-item" href="{{route('user.facility.profile', getFullNameWithSlug(auth()->user()))}}"><i class="fa fa-edit m-r-5 text-muted"></i>Edit Profile </a>
                                            @endif
                                            <a class="dropdown-item" href="{{route('user.profile.view', auth()->user()->username)}}"><i class="fa fa-user m-r-5 text-muted"></i>My Profile </a>
                                            @if(auth()->user()->userType != 3)
                                            <a class="dropdown-item" href="{{route('user.ad.index')}}"><i class="fa fa-shopping-bag m-r-5 text-muted"></i> Shop Manager </a>
                                            @endif
                                            <a class="dropdown-item" href="{{route('question.index')}}"><i class="fa fa-question-circle m-r-5 text-muted"></i> My Questions </a>

                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('user.logout')}}"><i class="fa fa-power-off m-r-5 text-muted"></i> Logout</a>
						</div>
					
					
					</li>
					
				@endguest
            </ul>
        </span>

    </nav>
