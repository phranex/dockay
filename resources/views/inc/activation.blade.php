@if(auth()->user()->verified == 0)
    <style>
        .messages{
            display: none;
        }
    </style>
    <div class="w-100 text-center">
            <i class="card-text">Your account is currently inactive. Activate your account to enjoy the healthy features of <strong>{{config('app.name')}}</strong>
                <br/><i style='font-size:12px'>Activation link was sent to {{auth()->user()->email}}</i>
                <p>Didnt receive an activation link? <a href='{{route('resendActivationMail')}}'>Resend</a></p></i>
    </div>

@endif
