@if(count($errors) > 0)

<div class="alert alert-danger alert-dismissible messages fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <ul id='errors'>
    @foreach($errors->all() as $error)
            <li class='text-danger'>
                {{$error}}
            </li>
    @endforeach
    </ul>
</div>

@endif


@if(session('success'))
     <div class="alert alert-danger alert-dismissible messages fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id='successMess'>
            <p>{{session('success')}}</p>
        </div>
    </div>

@endif


@if(session('error'))
     <div class="alert alert-danger alert-dismissible messages fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div id='errorMess'>
                <p>{{session('error')}}</p>
            </div>
    </div>

@endif
