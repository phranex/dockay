 <script src="{{asset('assets2/js/jquery.min.js')}}"></script>
       <script src="{{asset('assets2/js/popper.min.js')}}"></script>
       <script src="{{asset('assets2/js/bootstrap-material-design.js')}}"></script>
       <script src="{{asset('assets2/js/modernizr.min.js')}}"></script>
       <script src="{{asset('assets2/js/detect.js')}}"></script>
       <script src="{{asset('assets2/js/fastclick.js')}}"></script>
       <script src="{{asset('assets2/js/jquery.slimscroll.js')}}"></script>
       <script src="{{asset('assets2/js/jquery.blockUI.js')}}"></script>
       <script src="{{asset('assets2/js/waves.js')}}"></script>
       <script src="{{asset('assets2/js/jquery.nicescroll.js')}}"></script>
       <script src="{{asset('assets2/js/jquery.scrollTo.min.js')}}"></script>

       <script src="{{asset('assets2/plugins/carousel/owl.carousel.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/fullcalendar/vanillaCalendar.js')}}"></script>
       <script src="{{asset('assets2/plugins/peity/jquery.peity.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
       <script src="{{asset('assets2/plugins/chartist/js/chartist.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/chartist/js/chartist-plugin-tooltip.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/metro/MetroJs.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/raphael/raphael.min.js')}}"></script>
       <script src="{{asset('assets2/plugins/morris/morris.min.js')}}"></script>
       <script src="{{asset('assets2/pages/dashborad.js')}}"></script>


       <!-- App js -->
       <script src="{{asset('assets2/js/app.js')}}"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <script src="{{asset("js/notify.min.js")}}"></script>
        <script src="{{asset("js/notifications.js")}}"></script>
        <script src='{{asset('assets2/plugins/dropify/js/dropify.min.js')}}'></script>
    <script src='{{asset('assets2/pages/upload-init.js')}}'></script>
    <script src="{{asset("js/dockay.js")}}"></script>
    <script src="{{asset("js/fab.js")}}"></script>
    <script src="https://unpkg.com/draggabilly@2/dist/draggabilly.pkgd.min.js"></script>



    <script>
        @if(!isset($bg))
         $(window).scroll(function(){
            var scrollPos = $(document).scrollTop();

                if(scrollPos >= 300){
                          
                    if($('.navbar').hasClass('bg-transparent')){
                        $('.navbar').addClass('bg-white').addClass('fixed-top').removeClass('bg-transparent').css('box-shadow','1px 1px 2px #64bdf9');
                        $('a.a-link').addClass('dockay-color');
                        
                    }
                }else{
                    $('.navbar').addClass('bg-transparent').removeClass('fixed-top').removeClass('bg-white').css('box-shadow','none');
                     $('a.a-link').removeClass('dockay-color');
                }
            
            
            //console.log(scrollPos);
        });

        @endif

        {{--  $('.kc_fab_wrapper').click(function(){
            $drag = $(this).draggabilly();
            
        });  --}}


         $(document).ready(function(){
        $('.share-bn').click(function(){
           
                var url = $(this).attr('data-url');
                console.log(url);
                var type = $(this).attr('type');
                var text = '';
                if(type == 'question')
                    text = "{{displayShareMessage('question')}}"; 
                else if(type == 'sales')
                    text = "{{displayShareMessage('sales')}}";
                var fbUrl = "https://www.facebook.com/sharer/sharer.php?u="+url+"&src=sdkpreparse"
                var twitterUrl = "https://twitter.com/intent/tweet?text="+text+". Copy this link "+url;
                if($('#whatsappBtn').css('display') ==  'block'){
                    var whatsAppUrl = "{{ env('WHATSAPP_URL_WEB') }}text="+text+". "+url;
                    $('#whatsappBtn').attr('href', whatsAppUrl);

                }else{
                    var whatsAppUrl = "{{ env('WHATSAPP_URL') }}text="+text+". "+url;
                    $('#whatsappBtnMobile').attr('href', whatsAppUrl);

                }
                
                $('#twitterBtn').attr('href', twitterUrl);
                $('#facebookBtn').attr('href', fbUrl);
                $('#shareModal').modal();
                
            });
    });
       
    </script>



    {{--  comemnts  --}}
    <Script>
        $(document).on('click', '.editComment', function(){
            var id = $(this).attr('data');
              $.ajax({
                type: 'GET',
                // headers: {
                //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                // },
                
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                url: base_url + '/user/comment/get?id='+id,
                success: function(result) {
                    console.log(result);
                    //$('#loaderImage').hide();
                    if (result.status == 00) {
                        //go to the next tab
                        $('#summernote4').summernote('code', result.data);
                        $('#comment_id').val(result.id);
                        console.log(result.data);
                        $('#editCommentModal').modal();
                        $('#viewComments,#comments,#editAnswerModal').modal('hide');
                       
                    } else {

                        $.alert({
                            title: 'Update!',
                            type: 'red',
                            icon: 'fa fa-times',
                            content: result.message,
                        });
                    }
                },
                error: function(e) {
                    $('#loaderImage').hide();
                    $.alert({
                        title: 'Update!',
                        type: 'red',
                        icon: 'fa fa-times',
                        content: 'An error occurred. Please Try again.',
                    });
                    console.log(e);
                }
            });
            
        })
        $(document).on('click', '.deleteComment', function(){
            var id = $(this).attr('data');
            $.confirm({
                title: 'Delete Comment!',
                content: 'Are you sure?',
                theme: 'supervan',
                buttons: {
                    confirm: function () {
                        window.location.href = "{{route('comment.delete')}}?id="+id
                    },
                    cancel: function () {

                    },

                }
            });
        })
    </Script>