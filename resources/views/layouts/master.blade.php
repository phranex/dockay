<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets2/images/favicon.ico')}}">
 <meta name="csrf-token" content="{{csrf_token()}}">

<title>@stack('title')</title>
      
@include('inc/css')
@stack('styles')
<style>
    .messages{
        display:block;
    }
    .topbar-main{
            background-color: #40566a !important;
            box-shadow: 0px 0px 5px 0.5px #6cabda !important;
        }


    @media (max-width:560px){
        .topbar-main{
            background-color: #40566a !important;
            box-shadow: 0px 0px 5px 0.5px #6cabda !important;
        }

        .logo img{
            width:100px;
            height:auto;
        }
        .notification-list .noti-icon {
            font-size: 14px !important;
        }
        
    }
</style>      
<style>
body, html {
    height: 100%;
    margin: 0;
    padding:0;
    background:white;
}



</style>
</head>
<body>
    
    @include('inc.messages')
   
    @if(!in_array(request()->route()->getAction()['prefix'], array('/market','/password','/user','/jobs','/questions','/follow',null)) || in_array(request()->route()->getName(), array('jobsHomePage','marketHomePage','login','password.reset', 'welcome', 'register', 'user.verifyEmail')) )
    <div class="bg">
        @include('inc/nav')
        
        @stack('bg-content')
        
    </div>
    @else
    @include('inc/nav')

    @endif
    @yield('content')
    <div class='' style='height:100px'></div>
    
        
        <!-- Footer -->
        @if(request()->route()->getName() != 'password.request')
        <footer class='dk-footer fixed-bottom '>
            <span class='font-12'>&copy; {{ config('app.name') }} {{ \Carbon\Carbon::now()->format('Y')  }}. All right reserved </span>
            <div class='pull-right'>
                <span class='dk-s-icon'>
                    <i class='fa fa-facebook icon'></i>
                </span>

                <span class='dk-s-icon'>
                    <i class='fa fa-twitter icon'></i>
                </span>

                <span class='dk-s-icon'>
                    <i class='fa fa-instagram icon'></i>
                </span>
            </div>
        </footer>
        @endif
        <!-- End Footer -->
       


        <div id='loaderImage' style='display:none;z-index:100000' class="preloader invisibl js-preloader flex-center">
            <div class="dots">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>
        </div>
        



       @auth
        <div  class="kc_fab_wrapper"></div>

        <div class="modal fade" id="profile-pic" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Profile Picture Upload </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="flex text-center">
                            <div>
                                <form enctype="multipart/form-data" id='upload' action='{{route('user.profileImage.store')}}' method='post'>
                                @csrf
                                    <div class="row text-center">
                                            <div class="col-lg-12">
                                                <p> Suitable formats are .jpg,.jpeg &amp; .png</p>
                                            </div>
                                            <div style="border:none !important" class="text-center w-100 mb-5 error">

                                            </div>
                                            
                                            <div id='uploadBox' class="row col-lg-12">

                                                <div class="col-xl-8 m-auto">
                                                        <div class="card m-b-30">
                                                            <div class="card-body">
                                                                <input type="file" name='profile'  id="upload-pp" class="dropify" />
                                                                <input type="hidden" name='profile_image_id' value='{{ auth()->user()->profileImage->id }}' />
                                                                
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                   <button style='display:none' id='up' type='submit' class='btn'>Upload</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        @else
            @if(in_array(request()->route()->getAction()['prefix'], array('/jobs')))
                 <div class="modal fade" id="conversionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h2 class="modal-title text-center w-100" id="exampleModalLongTitle">Get Notified !!</h2>
                            <button type="button" class="close cancelConversion" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class="">
                                   <p style='font-size:16px' class='text-center'>Want to get notifications when jobs matching your profile are posted? </p>
                                   <div class='col-12 mt-20'>
                                        <form action='{{ route('job.notify') }}' method='get'>
                                            <label>Enter your email here:</label>
                                            <input class='form-control' required placeholder='Enter your email here' name='email' type='email' />
                                            <input class='form-control' placeholder='Enter your email here' value=' @if(in_array(request()->route()->getAction()['prefix'], array('/jobs'))) j @else m @endif ' name='type' type='hidden' />
                                            <button class='btn mt-20 pull-right'>Notify Me</button>
                                        </form>
                                   </div>
                                   
                                </div>
                            </div>
                            {{--  <div class="modal-footer">
                                <button type="button" class="btn btn-raised btn-danger ml-2" data-dismiss="modal">Close</button>
                            </div>  --}}
                        </div>
                    </div>
                </div>
               
            @endif
       

        
       @endauth

       <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Share </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="flex text-center">
                    <a id='facebookBtn' target='_blank' href='#' class='col social-reg '><i class="fa fa-facebook w-100"></i></a>
                    <a id='twitterBtn' target='_blank' href='#' class='col social-reg '><i class="fa fa-twitter w-100"></i></a>
                    {{--  <a id='whaBtn'  href='#' class='col social-reg '><i class="fa fa-google-plus w-100"></i></a>  --}}

                    <a id='whatsappBtn' target='_blank' href='javascript:void(0)' class='col d-none d-md-block social-reg whatsapp'><i style='color:white !important' class="fa fa-whatsapp w-100"></i></a>
                    <a id='whatsappBtnMobile' target='_blank' href='javascript:void(0)' class='col d-block d-md-none social-reg whatsapp'><i style='color:white !important' class="fa fa-whatsapp w-100"></i></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-danger ml-2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

        @if(isset($itemJob))
        <div class="modal fade" id="rjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Job on {{config('app.name')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class=" m-b-30 ">
                                                
                                                <div class="card-header">
                                                <div style="height:55px">
                                                        <img src="{{displayDefaultPhoto($itemJob->user)}}" alt="user" class="rounded-circle avatar avatar-img img-fluid">
                                                    <a href="{{route('user.profile.view', $itemJob->user->username)}}"><span class='poster-details'>{{$itemJob->user->username}}</span></a>
                                                    <span class='pt job-type'>
                                                        {{$itemJob->category->name}}
                                                    </span>

                                                    </div>

                                                </div>
                                                
                                                <div class="card-body ">
                                                    <blockquote class="blockquote mb-0">
                                                    <h5 class='post-content'>{{$itemJob->description}}</h5>
                                                    <footer class="blockquote-footer">Posted <small>{{when($itemJob->created_at)}}</small></footer>
                                                    </blockquote>

                                                    
                                                    
                                                    <div class='card-body text-center'>
                                                        Tags: 
                                                        <span class='more-details-mobile'><i class="fa fa-{{$itemJob->gender}}"></i> {{$itemJob->gender}}</span>
                                                        <span class='more-details-mobile'>{{$itemJob->specialty->name}}</span>
                                                        <span class='more-details-mobile'>{{$itemJob->establishment->name}}</span>
                                                        {{--  <span class='more-details-mobile'>
                                                                <a class='share-b' target='_blank' href="{{ env('WHATSAPP_URL') }}text=Hi guys, I found a job I think you should checkout on {{ env('APP_URL') }}/?rjob={{$item->id}}. {{  title_case($item->description)}}" data-url='' ><i class="fa fa-share"> Share</i></a>

                                                        </span>  --}}
                                                         @if($itemJob->allow_whatsapp || $itemJob->contact)
                                                          <a class='more-details-mobile btn-outline cursor-pointer' onclick="window.open('{{ env('WHATSAPP_URL') }}phone={{convertToInternationalNumber($itemJob->contact)}}&text=I am interested in the following job posted on {{ env('APP_URL') }}. {{  title_case($itemJob->description)}}')" href='javascript:void()'>
                                                            <span class=''>contact
                                                            <i class='fa fa-whatsapp'></i>
                                                            </span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        @endif

       <!-- jQuery  -->
      @include('inc/js')
       @stack('scripts')
       <script>

        $(document).ready(function(){
            notif();

            $('.cancelConversion').click(function(){
                 $.ajax({
                    type: 'GET',
                   
                  
                    
                    url: '{{ route('job.notify') }}?cancel=1',
                    success: function(result) {
                        console.log(result);
                      
                    },
                    error: function(e) {
                        $('#loaderImage').hide();
                        $.alert({
                            title: 'Update!',
                            type: 'red',
                            icon: 'fa fa-times',
                            content: 'An error occurred. Please Try again.',
                        });
                        console.log(e);
                    }
                });
            });

            
            @if(!session()->has('emailSaved'))
            $('#conversionModal').modal({
                    backdrop: 'static',
                });
            @endif
           
           @if(isset($itemJob) || session()->has('itemJob'))
                $('#rjob').modal({
                    backdrop: 'static',
                });
               
           @endif
           

            
            $('#upload-pp').change(function(){
                console.log($(this).val());
                if($(this).val().length > 0){
                    var ext = $(this).val().split(".").pop().toLowerCase();
                    console.log(ext);
                    if(ext =="jpg" || ext == "jpeg" || ext == "png"){
                       
                        $('#up').show();
                        $('div.error').text('');
                    }else{
                        $('div.error').text('Only .jpg,.jpeg and .png files are allowed');
                        $('#up').hide();
                        $('.dropify-clear').trigger('click');

                    }

                }else{
                    $('#fileStat').text('Upload CV');
                }
            });
        });

        
        $(document).ready(function(){
            var links = [
                {
                    "bgcolor":"red",
                    "icon":"+"
                },
                {
                    "url":"{{ route('question.create') }}",
                    "bgcolor":"white",
                    "color":"#64bdf9",
                    "icon":"<i class='fa fa-question'></i>",
                  
                },
                {
                    "url":"#",
                    "bgcolor":"black",
                    "color":"white",
                    "icon":"<i class='fa fa-edit'></i>"
                }
            ]
            $('.kc_fab_wrapper').kc_fab(links);
        });
       </script>

       <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bf9b1ff79ed6453ccaaf65c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130300010-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130300010-1');
</script>

<!--End of Tawk.to Script-->

    </body>

</html>
